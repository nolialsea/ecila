import {parse} from 'acorn'
import * as eslexer from 'es-module-lexer'
import fs from 'fs/promises'
import path from 'path'

const projectDir = './'
const outputFile = 'AST.txt'

async function generateAST(filePath) {
    await eslexer.init
    const code = await fs.readFile(filePath, 'utf8')
    const comments = []
    const ast = parse(code, {ecmaVersion: "latest", sourceType: 'module', onComment: comments})
    // Add leadingComments property to the nodes having comments right before them
    ast.body.forEach((node, i) => {
        if (node.type === 'ClassDeclaration') {
            node.body.body.forEach((methodNode, j) => {
                const associatedComment = comments.reverse().find((comment) =>
                    (
                        (methodNode.start - comment.end <= 10 && comment.type === 'Block' && comment.value.trim().startsWith('*')) ||
                        (methodNode.start - comment.end <= 2 && comment.type === 'Line')
                    ) &&
                    (j === 0 || (methodNode.start - comments.find((prevComment) => prevComment === comment).end) > 1));
                if (associatedComment) {
                    methodNode.leadingComments = [associatedComment];
                }
            });
        } else {
            const associatedComment = comments.reverse().find(
                (comment) => (node.start - comment.end <= 10 && comment.type === 'Block' && comment.value.trim().startsWith('*'))
            );
            if (associatedComment) {
                node.leadingComments = [associatedComment];
            }
        }
    });
    return ast;
}

async function appendOutput(text) {
    await fs.appendFile(outputFile, `${text}\n`)
}

function extractTypeFromJSDoc(jsdoc) {
    const regex = /@returns?\s*\{([^}]+)}/
    const match = regex.exec(jsdoc)
    return match ? match[1] : null
}

async function printASTNodes(node, depth = 1) {
    let nodeType = node.type;
    const indent = '  '.repeat(depth);
    const isAsync = node.async;
    let returnType = null
    if (node.leadingComments && node.leadingComments.length > 0) {
        const lastComment = node.leadingComments[node.leadingComments.length - 1].value;
        returnType = extractTypeFromJSDoc(lastComment)
    }
    if (nodeType === 'FunctionDeclaration') {
        const functionName = node.id?.name;
        const params = node.params.map(p => {
            if (p.type === 'AssignmentPattern') {
                return `${p.left.name}=${JSON.stringify(p.right.value)}`;
            }
            return p.name;
        }).join(', ');
        await appendOutput(`${indent}${isAsync ? 'async ' : ''}function ${functionName}(${params})${returnType ? `: ${returnType}` : ''}`);
    } else if (nodeType === 'ClassDeclaration') {
        await appendOutput(`${indent}Class ${node.id.name}`);
        depth += 1; // Increase indentation for class members
    } else if (nodeType === 'MethodDefinition') {
        const methodName = node.key.name;
        if (!methodName.startsWith('#')) {
            const params = node.value.params.map(p => {
                if (p.type === 'AssignmentPattern') {
                    return `${p.left.name}=${JSON.stringify(p.right.value)}`;
                }
                return p.name;
            }).join(', ');
            await appendOutput(`${indent}${node.static ? 'static' : ''}${isAsync ? ' async ' : ''}method ${methodName}(${params})${returnType ? `: ${returnType}` : ''}`);
        }
    }
    // Recursive call for child nodes
    for (const key in node) {
        const child = node[key];
        if (Array.isArray(child)) {
            for (const c of child) {
                if (c && typeof c === 'object') {
                    await printASTNodes(c, depth);
                }
            }
        } else if (child && typeof child === 'object') {
            await printASTNodes(child, depth);
        }
    }
}

async function processDirectory(directory, firstFile = true) {
    const files = await fs.readdir(directory)
    for (const file of files) {
        const fullPath = path.join(directory, file)
        const fileStat = await fs.stat(fullPath)
        if (fileStat.isDirectory()) {
            if (file !== 'node_modules' && file !== 'mods') {
                await processDirectory(fullPath, firstFile) // Recursively process subdirectories
            }
        } else if (fileStat.isFile() && path.extname(fullPath) === '.js') {
            await appendOutput(`\n${fullPath}`)
            const ast = await generateAST(fullPath)
            await printASTNodes(ast)
            firstFile = false
        }
    }
}

(async function () {
    await fs.writeFile(outputFile, 'AST of the ECILA project (app to host discord chatbots)\n')
    await processDirectory(projectDir)
})()
