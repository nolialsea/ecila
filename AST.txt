AST of the ECILA project (app to host discord chatbots)

generateAST.js
  async function generateAST(filePath)
  async function appendOutput(text)
  function extractTypeFromJSDoc(jsdoc)
  async function printASTNodes(node, depth=1)
  async function processDirectory(directory, firstFile=true)

index.js
  async function connectBot(bot): Promise<void>
  async function connectAllBots(): Promise<void>

public/frontend.js
  function populateForm(form, data)
  function populateMainConfig(data)
  function showSection(sectionId)
  async function fetchGlobalConfig()
  async function fetchPresets()
  async function processCategory(categoryPath, categoryStructure)
  async function fetchBotConfigs()
  async function fetchPreset(presetPath, className)
  async function getPresetFolder(path)
  async function fetchOpenAITextGenPresets()
  async function fetchNovelAITextGenPresets()
  async function displayAllTextGenPresets()
  function displayTextGenNovelAIPresets(presets)
  function displayTextGenOpenAIPresets(presets)
  async function loadTextGenOpenAIPreset(preset)
  async function loadTextGenNovelAIPreset(preset)
  function displayBotConfigs(botConfigs)
  function displayBotConfig(botConfig)

src/Utils.js
  Class Utils
    staticmethod shuffleArrayInPlace(array)
    staticmethod isObjectEmpty(objectName)
    staticmethod shuffleArray(array)
    staticmethod textToJson(text)
    staticmethod jsonToCharacterText(json)
    staticmethod jsonToText(json, noValueMode=false, placeholderMode=false)
    staticmethod limitConsecutiveEmojis(input, max=null)
    staticmethod retryFunction(func, args=undefined, maxRetries=3)
    staticmethod sleep(ms)
    staticmethod tryCatch(func, failFunc)

src/classes/BotConfig.js
  Class BotConfig
    method constructor(args)

src/classes/Config.js
  Class Config
    method constructor(args)
    staticmethod replaceFileContent(jsonObj)
    function readFileContent(filePath)
    function processValue(value)
    staticmethod load()

src/classes/errors/Errors.js
  Class ApplicationError
    method constructor(message, statusCode)
  Class NotFoundError
    method constructor(resource)
  Class ValidationError
    method constructor(message)

src/classes/presets/TTS/ElevenLabsTTSConfig.js
  Class ElevenLabsTTSConfig
    method constructor(args)

src/classes/presets/TTS/NovelAITTSConfig.js
  Class NovelAITTSConfig
    method constructor(args)

src/classes/presets/TTS/TTSConfig.js
  Class TTSConfig
    method constructor(args)

src/classes/presets/images/ImagesConfig.js
  Class ImagesConfig
    method constructor(args)

src/classes/presets/images/ImagesNovelAIConfig.js
  Class ImagesNovelAIConfig
    method constructor(args)

src/classes/presets/images/ImagesStableDiffusionConfig.js
  Class ImagesStableDiffusionConfig
    method constructor(args)

src/classes/presets/textGen/TextGenParametersConfig.js
  Class TextGenParametersConfig
    method constructor(args)

src/classes/presets/textGen/TextGenParametersConfigOpenAI.js
  Class TextGenParametersConfigOpenAI
    method constructor(args)

src/routes/config.js
  function readConfig(filePath)
  function saveConfig(filePath, config)
  function getConfigFilePath(nestedPath)
  function castConfigToClass(className, configData)

src/routes/presets.js
  function readConfig(filePath)
  function readDirectory(filePath)
  function saveConfig(filePath, config)
  function getConfigFilePath(nestedPath)
  function castConfigToClass(className, configData)

src/scripts/addModsDependencies.js

src/scripts/removeModsDependencies.js

src/services/AiService.js
  function getApiUrl(): string
  Class AIService
    staticmethod generateMessageChatGPT(bot, messages, maxTokens=null, channel=null)
    staticmethod generateMessageClaude(bot, messages, maxTokens=null, channel=null): Promise<string|null>
    staticmethod generateMessageChatGPTRaw(bot, messages, maxTokens=null, channel=null): Promise<string|null>
    staticmethod generatorChatGPT(input, generator, data_=undefined, bot=undefined): Promise<string|null>
    staticmethod generatorNovelAI(input, generator)
    staticmethod generateMessageNovelAI(bot, messages, continueLastMessage=false): Promise<string|null>
    staticmethod generateMessageKoboldAI(bot, messages, continueLastMessage=false): Promise<string|null>
    staticmethod generateMessageHF(bot, messages, continueLastMessage=false): Promise<string|null>
    staticmethod generateMessage(bot, messages, topic=null, maxTokens=null, continueLastMessage=false, channel=null): Promise<string|null>
    function removeEncasedAsterisks(text)
    staticmethod generateMessageOAIRaw(bot, messages, topic=null, maxTokens=null, continueLastMessage=false, channel=null): Promise<string|null>
    staticmethod generateNextAuthorPrediction(bot, messages, topic=null): Promise<string[]|null>
    staticmethod getModerationData(messages, useRealBackend=false): Promise<{flagged: boolean, categories: Array<string>, category_scores: Array<string>
    staticmethod prepareModerationData(moderationResults): {flagged: boolean, categories: Array<string>, category_scores: Array<string>
    staticmethod generateNaiImage(prompt, opts=null): Promise<null|Buffer>
    staticmethod generateSDImage(prompt, opts=undefined): Promise<null|Buffer>
    staticmethod generateComfyUIImage(prompt, URL="http://localhost:8188/", opts=undefined): Promise<null|Buffer>
    async function getRequest(promptId)
    async function fetchFileAndConvertToBuffer(endpointURL)
    staticmethod generateComfyUIAnimation(prompt, URL="http://localhost:8188/", opts=undefined): Promise<null|Buffer>
    async function getRequest(promptId)
    async function fetchFileAndConvertToBuffer(endpointURL)
    staticmethod get11LabsStream(apiKey, text, voiceID, stability=1, similarity_boost=1, style=0, use_speaker_boost=true, modelID="eleven_multilingual_v2")
    staticmethod playWithElevenLabs(apiKey, text, voiceID, stability=1, similarity_boost=1, style=0, use_speaker_boost=true, modelID="eleven_multilingual_v2")
    staticmethod downloadFrom11Labs(outputFilePath, apiKey, text, voiceID, stability=1, similarity_boost=1, style=0, use_speaker_boost=true, modelID="eleven_multilingual_v2")

src/services/AudioService.js
  Class AudioService
    staticmethod play(response, outputFilePath=null)

src/services/BotDataService.js
  Class BotDataService
    staticmethod getData(bot, channel)
    staticmethod setData(bot, channel, data)
    staticmethod reset(bot, channel)
    staticmethod save()
    staticmethod load()
    staticmethod updateRevamped(userMessage, content, bot, messages, nbMessageToLoad=null)

src/services/CardService.js
  Class CardService
    staticmethod extractCardContent(buffer)
    staticmethod applyCardOntoBot(card, bot, client=null)
    staticmethod createCard(imageBuffer, card)
    staticmethod extractCharacterName(card)
    staticmethod extractDynamicProperties(card)
    staticmethod cleanupDynamicProperties(card)

src/services/ComfyWorkflowService.js
  Class ComfyWorkflowService
    staticmethod getAnimationWorkflow(prompt, negativePrompt, seed, steps=24, cfg=5, width=512, height=768, frames=20, contextOverlap=8, contextLength=16, motionScale=1)
    staticmethod getImageWorkflow(prompt, negativePrompt, seed, steps=64, cfg=5, width=640, height=960, model="realcartoonPixar_v6", sampler="euler_ancestral", scheduler="karras")
    staticmethod getImageWorkflowSDXL(prompt, negativePrompt, seed, steps=64, cfg=5, width=640, height=960)

src/services/DiscordAudioService.js
  function getApiUrl(): Promise<AttachmentBuilder>
  Class DiscordAudioService
    staticmethod getTTSAudioDiscordAttachment(bot, botMessage)
    staticmethod getNAITTSAudioDiscordAttachment(bot, botMessage)
    staticmethod get11LabsTTSAudioDiscordAttachment(bot, botMessage)

src/services/DiscordImageService.js
  Class DiscordImageService
    staticmethod getImageAttachment(imagePrompt, portrait=false, landscape=false, backend="NovelAI", negativePrompt="", model=undefined, bot=null)
    staticmethod getComfyImageAttachment(URL, imagePrompt, portrait=false, landscape=false, negativePrompt="", isNSFW=false, model=undefined, bot=null)
    staticmethod getComfyAnimationAttachment(URL, imagePrompt, portrait=false, landscape=false, negativePrompt="", isNSFW=false)
    staticmethod getNAIImageAttachment(imagePrompt, portrait=false, landscape=false, negativePrompt="child, loli")
    staticmethod getSDImageAttachment(imagePrompt, portrait=false, landscape=false, negativePrompt="child, loli", model=undefined, bot=null)
    staticmethod generateImageAttachmentsRevamped(userMessage, content, bot, forceNovelAI=false, forceStableDiffusion=false, messages=undefined, nbMessageToLoad=null)
    staticmethod generateImagesAndSendMessage(userMessage, content, bot, external=false, preventMessageDeletion=false, components=null, channel=null, isReply=false, nbMessageToLoad=null)
    function parseDiscordFileUpload(botMessage)

src/services/DiscordMessageService.js
  async function continueMessage(userMessage, bot, channelTopic, messages_=null): ?import('discord.js').TextMessage
  async function retryMessage(userMessage, bot, channelTopic): Promise<void>
  function isCommand(userMessage): ?import('discord.js').TextMessage
  Class DiscordMessageService
    staticmethod sendMessageWithTTS(channel, botMessage, userMessage, bot, files, preventMessageDeletion, isReply=false)
    staticmethod sendMessageWithoutTTS(channel, botMessage, userMessage, files, preventMessageDeletion, isReply=false)
    staticmethod sendContent(channel, content, files, userMessage, preventMessageDeletion, isReply=false)
    staticmethod sendLongMessage(channel, botMessage, userMessage, preventTTS, bot, preventMessageDeletion, files, isReply=false)
    staticmethod sendMessage(userMessage, botMessage, bot, preventMessageDeletion=false, preventTTS=false, files=undefined, components=null, channel=null, isReply=false)

src/services/DiscordService.js
  function getApiUrl(): string
  Class DiscordService
    staticmethod replaceAlias(authorName)
    staticmethod fetchMessages(channel, messageCount=200)
    staticmethod getChannelMessages(channel, lastMessage=null, bot)
    function countImageTokens(width, height)
    async function getImageDimensions(input)
    staticmethod generateAndSendMessage(userMessage, bot, channelTopic=null, messages_=null, functions=null, oldOAIParameters=null)
    staticmethod sendCommandError(userMessage, errorMessage=null)
    staticmethod sendCommandConfirmation(userMessage)

src/services/DiscordTypingService.js
  Class DiscordTypingService
    staticmethod sendTyping(userMessage, channel=null)

src/services/FileDownloaderService.js
  Class FileDownloaderService
    staticmethod downloadFile(url, outputFilename)

src/services/FunctionCallingService.js
  Class FunctionCallingService
    staticmethod functionCall(discordMessage, bot, channelTopic, messages, functionDefinitions, function_call="auto", functions=null)
    staticmethod functionCallRaw(discordMessage, bot, channelTopic, messages, functionDefinitions, function_call="auto", functions=null)
    staticmethod processFunctionCall(userReaction, bot, channelTopic, messages, functionDefinitions, functionCall, functions)

src/services/GeneratorService.js
  Class GeneratorService
    staticmethod generator(userMessage, message, bot, json, removeNewlines=false)
    staticmethod generatorToImage(userMessage, message, bot, json, removeNewlines=false)

src/services/InitializeClientService.js
  function loadModList()
  async function initClientCommands(client, bot, clients): Promise<void>
  function executeMiddleware(middleware, index, args)

src/services/JsonService.js
  Class JsonService
    staticmethod loadJsonUrl(url, silentError=false)
    staticmethod loadJsonFile(path, silentError=false)

src/services/MemoryService.js
  Class MemoryService
    staticmethod add_memory_block(block_name, block_data, tags, chatbot)
    staticmethod forget_memory_block(block_name, chatbot)
    staticmethod get_memory_blocks_by_tags(tags, chatbot)

src/services/ModdingService.js
  Class ModdingService
    staticmethod isBotMasterBot(bot, userMessage)

src/services/ModerationService.js
  Class ModerationService
    staticmethod getModerationStatus(messages, isAdmin)

src/services/SmartAnswerService.js
  Class SmartAnswerService
    staticmethod sendSmartAnswer(userMessage, bot, channelTopic, useEmojiInsteadOfGeneration=false, clients=undefined, functions=null)

src/services/TimeoutService.js
  Class TimeoutService
    staticmethod clearSmartAnswerTimeout(userMessage)
    staticmethod clearAnswerTimeout(userMessage, client)
    staticmethod clearTimeout(userMessage, client)

src/services/UserService.js
  Class UserService
    staticmethod isAdmin(userMessage)

src/services/WebServerService.js
