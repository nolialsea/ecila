# The ECILA Nexus
Hello and welcome to The ECILA Nexus!
## What is there to do?
Our discord server is a playground for AI enthusiasts and chatbot fans.
Here, you can engage in discussions with fellow humans, chat up with our diverse range of bots, craft your very own chatbots, dabble in AI theory, and even generate images with a few simple commands.
### Chat with humans
(present the `CHAT WITH HUMANS` category and the `#general` channel)
### Chat with bots
(present the `CHAT WITH BOTS` category)
### Create bots
(present the `CREATE BOTS` category)
(present the !character command and the card system)
### Chat about AI
(present the `CHAT ABOUT AI` category)
### Generate images
(present the `IMAGE GENERATION` category)
(present the image gen commands)
## Want to host your own bots?
(explain that it's required to become patreon)
### OpenAI warnings
(explain that OpenAI is billed per-request and per-model so pricing can vary vastly)