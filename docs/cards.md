# ECILA Card system
The card system brings an element of tangibility to these characters, storing and representing them as easily accessible and shareable entities. From the genesis of a character to their dynamic evolution, everything can be stored in a compact, state-of-the-art 'Card'. Consider the card as the DNA of your AI characters—carrying their identity, traits, context, and other essential parameters.  
What if you want to share your characters with your friends? Just pass the card! What if you want to document the growth and evolution of your AI entity? Just update the card! And guess what, these cards can be created, managed, and shared right here in our Discord channel via the powerful ECILA commands.  
In the following segments, we'll dive into the specifics of these commands, delving deep into their functionalities and usage guidelines.

# `!character` Command
The `!character` command is designed to generate a character using the inputs along with any instruction requests that are part of the command parameters.
### Usage
`!character DESCRIPTION [INSTRUCTION_REQUEST]`
- `DESCRIPTION`: This is where you define your character in broad strokes, such as general appearance, personality traits, or any other distinct characteristic.
- `INSTRUCTION_REQUEST` (Optional): Here you can specify any additional instructions or requests for CharacterGenerator to consider when generating your character.
### Example
`!character A cute blonde elf girl named Alice. Please add a section to keep track of her current weapon.`
In this example, 'A cute blonde elf girl named Alice' is the descriptive input for the character to be generated. The additional instruction request 'Please add a section to keep track of her current weapon.' will prompt the CharacterGenerator to add a field for tracking Alice's current weapon.
### Command Returns
This command is stateless and doesn't retain results from previous uses. Upon usage, the command will return both the character card PNG image (featuring the generated character details) and an associated text file. The fields prefixed with `!!` are dynamic and will auto-update over time.
### Warning
Remember, the `!character` command doesn't have any memory of previous results. It's a tool for character generation and not for the subsequent management of the generated characters.

# `!world` Command
A world in your pockets, that's what the `!world` command delivers.
### Usage
`!world DESCRIPTION [INSTRUCTION_REQUEST]`
- `DESCRIPTION`: This is where you etch out the initial stroke of your world; it can be a setting, a culture, a phenomenon, or an event.
- `INSTRUCTION_REQUEST` (Optional): Use this to issue additional instructions or requests to WorldGenerator.
### Example
For instance, `!world A medieval fantasy where elves are oppressed. Please add a section to document that oppression.` Here, 'A medieval fantasy where elves are oppressed' forms the base input, while 'Please add a section to document that oppression.' is an additional instruction for WorldGenerator.
### Command Returns
This command is stateless and doesn't store prior results. Upon calling it, the command returns a world, crafted around your inputs and instructions. Don't rely on the `!world` command to keep track of changes or developments in your world.
### Warning
Always remember, the `!world` command has no memory of previous results - it's more a compass guiding you to unexplored territories than a cartographer documenting your journeys. So venture forth with it, but remember to chart your path!

# `!cardWrite` Command
The `!cardWrite` command is used to manually create an Image Card by combining a PNG image and a text file (either .txt, .md, or .json files).
### Usage
`!cardWrite`
Upon executing this command, you need to attach a PNG image and the designated text file to the command. The Image Card will be a combination of the provided PNG image and the text content.
### Example Usage
Let's say you want to create an Image Card for your newly generated AI character, Alice (from the `!character` command example). You've got a PNG profile picture of Alice and a .txt file containing her description and other specifics. Just type `!cardWrite` in the message input and attach both Alice's picture and the .txt file to the message.
### Command Returns
Upon execution, this command will return the newly minted Image Card combining the PNG image and the text information from the attached files.
### Warning
Please remember that only PNG images are supported for the Image Card creation. Other formats such as JPEG or GIF won't work with the `!cardWrite` command.

# `!cardRead` Command
The `!cardRead` command is your go-to tool to read the contents of an Image Card. Hand it an Image Card, and it will dutifully reveal the text stored inside.
### Usage
`!cardRead`
Before executing this command, attach the Image Card you want to read in the same message.
### Example Usage
Let's say someone handed you an Image Card of an intriguing character, and you're interested to unravel their details. Attach this Image Card to the `!cardRead` command, and see their character details unfurl!
### Command Returns
Upon its execution, the `!cardRead` command will uncover and display the text content that was stored within the Image Card.
### Warning
Please keep in mind, the `!cardRead` command works only with Image Cards. It can't decipher or extract any data from ordinary images or other file formats.

# `!load` Command
The `!load` command lets you load a character or context onto a bot within the server. You will need a character Image Card or an equivalent text file for this.
### Usage
`!load BOT_USER_ID`
- `BOT_USER_ID`: This is the ID of the bot where you want to load the character or context. To get the BOT_USER_ID, right-click on the bot user's icon and select `Copy ID`. Remember, on our official discord server, only bots with the `Custom Bot` role can be edited by normal users.
  Attach the Image Card or the equivalent text file containing the character data to the command message.
### Example Usage
If you have created an AI character (using the `!character` command or manually) and want to load this context onto a bot, namely AliceBot. Type `!load` into the message input, attach the character Image Card or the text file, and include the BOT_USER_ID of AliceBot in your message.
### Command Returns
This command will load the attached character entirely, replacing any previous context the bot might have had.
### Warning
Ensure the bot you are loading the character onto has the `Custom Bot` role. Also, understand that this command is final. Any previous context the bot had will be irreversibly replaced.
