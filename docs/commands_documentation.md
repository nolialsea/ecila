# ECILA Mod Command Documentation

### List of Commands:

`!options`
Generates and displays options for the previous message.

`!badoptions`
Generates a series of deliberately bad options for the previous message.

`!cardread <URL or Attachment>`
Extracts text content from an attached image card.

`!cardwrite <Attachment>`
Creates an image card by combining provided text with a PNG image.

`!cspeakas <BOT_NAME> "<CHARACTER_NAME>"`
Generates a message as another character with specific context.

`!extractdata <DataType>`
Extracts specified data from conversation history.

`!setname <BOT_ID> "NEW_NAME"`
Changes the bot's username to a specified name.

`!getavatar <BOT_NAME>`
Fetches and displays the avatar URL of the bot.

`!setavatar <BOT_ID> <URL or Attachment>`
Changes the bot's avatar using a provided URL or attachment.

`!getdata <BOT_NAME> "PROPERTY_NAME"`
Retrieves the bot’s stored data for the current session or context.

`!setdata <BOT_ID> "PROPERTY_NAME" VALUE <Attachment>`
Sets or updates the bot's stored data using a provided JSON file or data string.

`!img <Description>`
Generates an image based on bot and user-defined parameters.

`!lastcommit <X>`
Lists the last commits in the Git repository.

`!instructions <BOT_ID> <Attachment>`
Loads additional instructions onto the bot and refreshes context.

`!load <BOT_ID> <URL or Attachment>`
Loads a character onto the bot from a provided card or URL.

`!settings <BOT_ID> <Attachment>`
Loads and applies settings from an attached file onto the bot.

`!narrator <Input>`
Acts as a game master, narrating events based on player actions.

`!nextauthor <Prediction>`
Predicts the potential next author of a message.

`!ping`
Basic command to ensure bot responsiveness.

`!reset <BOT_NAME>`
Resets the bot's context and cached data for the current channel.

`!selfupdate <BOT_NAME>`
Updates the bot's data autonomously based on recent messages.

`!summary <Topic>`
Summarizes the current context or conversation.

`!world <Description>`
Generates a detailed world description based on input.

`!character <Description>`
Generates a character description based on user input.
