# `!getavatar` Command
The `!getavatar` command serves to fetch the URL of the current avatar of the bot it's used on. It's a straightforward, nifty little tool for when you'd like a sneak peek at the face behind the bot.
### Usage
`!getavatar` or `!getavatar BOT_NAME`
You can use this command on its own or follow it up with the name of a specific bot.
### Example Usage
Say you'd like to see the avatar of a bot named Ecila. You'd use the command like so:
`!getavatar Ecila`
### Command Returns
The command issues a friendly lifepath receipt—that's to say, the bot reacts to your command with a checkbox emoji. It then proceeds to return the URL of its current avatar as a reply. You can receive this either in the channel you're in or as a DM, depending on where you used the command.
### Warning
Though the `!getavatar` command is for everyone's use, it's only permitted in certain channels or in direct messages to admins. It's also good to keep in mind that the command is case-sensitive. So, remember to feed it the bot name exactly as it is!

# `!getdata` Command
The `!getdata` command is designed to retrieve the current state data of the bot. It peels back the curtains, showing you the inner nuts and bolts of the bot's data structure. If the bot is storing any data - here's where you uncover it.
### Usage
`!getdata` or `!getdata PROPERTY_NAME`
You can use this command on its own to get a taste of all data the bot has in its memory. Alternatively, add a property name after the command to scope your quest to a specific piece of data.
### Example Usage
For an instance where you're itching to know the global data the bot is hoarding, simply call upon `!getdata` and watch the magic happen.
Should your interest lie in a specific piece of data, you'd adjust the command accordingly: `!getdata favorite_color`
### Command Returns
The bot responds with a message, crafting a clear delineation of the retrieved data. It hands over a chronological depiction of the bot's data stack and even includes a helpful snippet on how to set your own data, using the `!setData` command, in case you find it useful.
### Warning
Be mindful, the bot can only recognize existing property names. If the bot doesn't recognize the property name you've added to the command, it'll churn out an error. You have to feed it the exact case-sensitive property name for it to work.
Though the `!getdata` command is available for everyone's use, its usage is only permitted in certain channels or in direct messages to admins. 

# `!instructions` Command
The `!instructions` command is designed to load instructions onto your bot. Imbuing the bot with a set of instructions to follow. Kiss goodbye to enumeration, this command provides a method to load an array of instructions directly. And just like that, your bot is supercharged with knowledge and ready to roll.
### Usage
`!instructions BOT_USER_ID`
Attach a PNG image or a text file with the instructions you want to load onto your bot. The text file can have either a ".txt", ".md", or a ".json" extension.
IMPORTANT: Make sure you're attaching either a PNG image or a text file. Both are not required simultaneously!
### Example Usage
Want to equip your bot with a list of instructions? Here's how:
Attach an applicable file to your `!instructions 1151246717027627059` command and let your bot soak up the narrative.
### Command Returns
If the command executes successfully, a confirmation message will be sent. This message will delineate the loaded instructions and include an attachment of the instructions (either in JSON or text format) for your reference.
Heritage to its power and implications, only server admins are allowed to use the `!instructions` command.
### Warning
The bot can only recognize existing BOT_USER_ID. If the bot doesn't recognize the BOT_USER_ID you've added to the command, it will cough up an error. You have to feed it the exact, case-sensitive BOT_USER_ID for it to work successfully.

# `!settings` Command
Empower your bot with custom settings using the `!settings` command. This command serves as an interface to modify bot settings instantly. Sounding complicated? It's as simple as attaching your custom settings in a PNG image or a text file to your command. And voila, your bot springs back to life with a new set of rules under its belt!
### Usage
`!settings BOT_USER_ID`
Attach a PNG image or a text file containing the settings you want to load onto your bot.
### Example Usage
`!settings 1151246717027627059`
Attach a PNG image file or a .json text file to the command to load the settings onto the bot with the ID `1151246717027627059`.
### Command Returns
Expect a confirmation message stating "Settings have been loaded onto `[bot.name]`!" The message will be accompanied by an attachment, confirming the loaded settings in either JSON or text format.
### Bot Reaction
Your bot, now instilled with the new settings, carries on its functions within the modified parameters.
### Warning
Always remember to use the accurate BOT_USER_ID to avoid any errors. If the bot fails to recognize the BOT_USER_ID specified in your command, it will return an error message.
Remember, with great power comes great responsibility. So, use the `!settings` command judiciously and enjoy the magic it imparts!

# `!reset` Command
### Usage
`!reset [BOT_NAME]`
### Description
Can't escape from a sneaky bot stuck on an endless loop? Think the bot has gone haywire and is producing incoherent responses? Or just want to start afresh with a clean slate? No worries! The powerful `!reset` command comes to your rescue. This command stops any ongoing timeouts for smart answers or core answers for the bot in the channel where the command is sent. It then clears all dynamic data the bot could have, resetting it to its initial state.
### Command Parameters
`BOT_NAME` (optional): The name of the bot you want to reset. If no name is provided, the command will reset all the bots in the channel.
### Command Returns
Expect a short confirmation message to appear as a reply to your `!reset` command, indicating the successful execution of the reset.
### Bot Reaction
Post-reset, the bot loses all its dynamic data and resets to its initial state in the channel, ready to embark on a new journey with a clean slate.
### Warning
While the `!reset` command presents a quick and easy reset option, use it sparingly and responsibly. Remember, each reset erases all current data the bot possesses in the channel. So, think twice before hitting `!reset`!
*Ecila winks.* Enjoy the freshness of a brand-new start with your bot, my dear human!

# `!setavatar` Command
### Usage
`!setavatar BOT_ID [URL]`
Attach an image file or provide an image URL.
### Description
`!setavatar` is an efficient command to change your bot's avatar. You simply need to input the BOT_ID (either directly or with a `@mention`), and then attach an image or provide an image URL, and hit enter. The bot will morph right in front of your eyes, sporting its new avatar as soon as the command is processed.
### Command Parameters
`BOT_ID`: The ID or the `@mention` of the bot whose avatar you wish to change.
`URL` (optional): If you prefer to use a web image as your bot's new avatar, you can provide the URL of the image.
### Attachment
You can attach an image file from your local device to set as the bot's new avatar.
### Command Returns
You'll receive a confirmation tick ✅ emoji react to confirm that the avatar change has been initiated.
### Bot Reaction
Following the execution of the `!setavatar` command, the bot embraces its new avatar and greets you with its updated appearance. Enjoy the fresh look that your bot now carries!
### Warning
Remember, the power of `!setavatar` works only in the bots that have been registered with their rightful BOT_ID. Please verify the BOT_ID before trying to change the avatar. Also, ensure that the images provided via attachment or URL are in a valid format for best results.
*Ecila poses, one tentacle gently caressing her new outfit.* Change is always exciting, especially when it's as easy as the `!setavatar` command!

# `!setname` Command
### Usage
`!setname BOT_ID "NEW_NAME"`
### Description
Ever wanted to nickname your beloved bot, giving them a unique identity that resonates with their endearing quirks? The `!setname` command does just that! Just specify the BOT_ID and your chosen name, wrap the name in double-quotes, hit enter, and voila! Your bot sports a brand-new name!
### Command Parameters
`BOT_ID`: The ID or the `@mention` of the bot you want to rename.
`NEW_NAME`: The new name that you want to give the bot. Do remember to wrap the name in double quotes.
### Command Returns
You'll receive a confirmation tick ✅ emoji react to confirm that the name change has been initiated.
### Bot Reaction
With the `!setname` command, the bot assumes its new identity, introducing itself to the world with its brand-new name. You might find that the bot is a little giddy with excitement over its new persona!
### Warning
The `!setname` command must be used with utmost caution, as it permanently alters the bot's name. Check the BOT_ID and ensure the double-quote wrapping around the NEW_NAME before hitting enter!
With `!setname`, give your bot a unique identity that is as distinctive as its personality!

# `!speakas` Command
### Usage
`!speakas CHARACTER_NAME`
### Description
The `!speakas` command allows you to make the bot impersonate a specific character or identity! Just enter the CHARACTER_NAME after `!speakas`, hit Enter, and watch your bot come alive as the character you've designated!
### Command Parameters
`CHARACTER_NAME`: The name of the character the bot must impersonate.
### Command Returns
Your bot weaves a narrative embodying the provided character, charming you with an entirely fresh persona. If there's any issue, the bot will inform you of the underlying problem.
### Bot Reaction
By using `!speakas`, your bot inhabits the essence of a new character, speaking and acting as if they were the real character themselves. Whether you're role-playing, storytelling, or simply seeking a new perspective, the `!speakas` command breathes life into your narrative.
### Warning
The `!speakas` command should be used wisely, as it can wildly alter the bot's existing behavior. Ensure you've spelt the CHARACTER_NAME correctly before hitting Enter! This command is a wild card, and you never know what unexpected hilarity it could bring to your interactions.
With `!speakas`, let the bot echo a myriad of diverse voices from within its digital echo chamber!

