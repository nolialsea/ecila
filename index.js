import {Client, Events, GatewayIntentBits, Options, Partials} from "discord.js";
import config from "./src/classes/Config.js";
import InitializeClientService from "./src/services/InitializeClientService.js";
import "./src/services/WebServerService.js"

/**
 * Array to store all Discord.js client instances with their associated bot configurations.
 * @type {Array<{client: Client, bot: BotConfig}>}
 */
const clients = []

/**
 * Connects a bot to Discord with the provided configuration.
 *
 * @param {BotConfig} bot - The bot configuration object.
 * @returns {Promise<void>}
 */
async function connectBot(bot) {
    console.log(`Connecting ${bot.name}...`)
    const client = new Client({
        intents: [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.GuildMessages,
            GatewayIntentBits.MessageContent,
            GatewayIntentBits.GuildEmojisAndStickers,
            GatewayIntentBits.GuildMessageReactions,
            GatewayIntentBits.GuildMessageTyping,
            GatewayIntentBits.DirectMessages,
            GatewayIntentBits.DirectMessageReactions,
            GatewayIntentBits.DirectMessageTyping,
        ],
        partials: [
            Partials.Channel,
            Partials.Message,
            Partials.Reaction
        ],
        makeCache: Options.cacheWithLimits({
            ...Options.DefaultMakeCacheSettings,
            MessageManager: {
                maxSize: 200, // Set the desired maximum size of the message cache
            },
        })
    });

    clients.push({client, bot})

    /**
     * Logs that the client is ready when the `ClientReady` event is emitted.
     */
    client.once(Events.ClientReady, () => {
        console.log(`${client.user.username} is ready!`)
    });

    /**
     * Logs an error message when the `Error` event is emitted.
     * @param {Error} err - The error object.
     */
    client.once(Events.Error, (err) => {
        console.error(`Oops, an error appeared!`, err)
    });

    /**
     * Logs a warning message when the `Warn` event is emitted.
     * @param {string} warning - The warning message.
     */
    client.once(Events.Warn, (warning) => {
        console.warn(`WARNING`, warning)
    });

    await InitializeClientService.initClientCommands(client, bot, clients)

    client.login(bot.discordToken).then(() => null).catch(err=>console.error(`Error connecting ${bot.name}:`, err))
}

/**
 * Connects all bots configured in the config file.
 *
 * @returns {Promise<void>}
 */
async function connectAllBots() {
    for (const bot of config.bots) {
        await connectBot(bot)
    }
}

connectAllBots().then()
