# EcilaDiscord

Noli's Discord.js bot is a unique application that empowers users to create their own AI bots for their Discord servers.
Our bots use natural language processing and can understand and respond to user commands and messages in real-time!
Whether it be as simple as greetings or something more complex like moderating conversations, our unique approach
ensures that customizations can be made easily to suit your exact needs.

For more detailed infomation check here: https://gitlab.com/nolialsea/ecila-community

## Installation

https://gitlab.com/nolialsea/ecila-community/-/blob/main/ECILA_V2.md

# Bot Context and Data

The two key notions to understand unlocking all the potential of bot creation.

### Context

It's nothing else than one or several bits of text inserted at specific places in the "memory" or "vision" of the bot.  
When using NovelAI, everything goes at the top of the prompt. This is because NovelAI has a lot of prompt inertia and
tends to record those info easily.  
When using OpenAI however, context can be split and inserted into either top, middle or bottom of the prompt.

- Top: those info will be remembered by the bot, but everything visible after can alter the effectiveness of those. Used
  to set things that are meant to never change.
- Middle: best used for reminders and accentuation of some rules/facts.
- Bottom: most effective, best used for instructions and jailbreaks rather than storing information.

### Data

While it's possible to simply write everything inside context, it's also possible to store individual info into a "data"
object.  
This object is a JSON dictionary (a list of key/value pairs)  
The main goal of having data into a separate object is to **unify the various AI models** used by the app to simulate a
chatbot.  

Once a **property** is defined into data, it becomes available as a **placeholder** for contexts and generators.  
For example, with a config file setup like this:

```json
{
  "...": "...",
  "bots": [
    {
      "name": "Ecila",
      "...": "...",
      "data": {
        "species": "tentacle-girl"
      }
    }
  ]
}
```
You can use the `${species}` **placeholder** inside any of the contexts or generators to insert the value "tentacle-girl".

While it may seem overcomplicated at first (you don't have to use it btw), it prevents having to edit multiple files each time you edit a value that is shared between them.  
In addition to that, those data can be **updated automatically by AI**.

## Support

Discord: https://discord.gg/GuEXxPS3E8

## Contributing

This is Open Source, as such any contributions are greatly accepted.

## Authors and acknowledgment

Noli - Creator of the App
