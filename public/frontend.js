let globalConfig

function populateForm(form, data) {
    for (let prop in data) {
        if (prop.startsWith("#")) continue;
        let inputElement = form.querySelector("#" + prop);
        if (inputElement) {
            inputElement.value = data[prop];
            if (data[prop] === true) {
                inputElement.checked = true;
            }
        }
    }
}

function populateMainConfig(data) {
    // Fetch the form
    const mainConfigForm = document.querySelector("#main-config-form");
    populateForm(mainConfigForm, data);
}

function showSection(sectionId) {
    const sectionIds = [
        'main-configuration',
        'bots-view',
        'bot-configuration',
        'presets-view',
        'preset-configuration',
        'preset-rule',
        'preset-ruleset',
        'preset-images-novelai',
        'preset-images-comfyui',
        'preset-images-automatic1111',
        'preset-text-novelai',
        'preset-text-openai',
        'preset-tts-novelai',
        'preset-tts-elevenlabs',
    ]
    for (let id of sectionIds) {
        document.getElementById(id).style.display = (id === sectionId) ? 'block' : 'none'
    }
}

async function fetchGlobalConfig() {
    try {
        const response = await fetch("/api/config/config.json?className=Config");
        if (!response.ok) {
            const message = `Error fetching global config. Status: ${response.status}`;
            console.error(message);
            // You can display a user-friendly message here
            alert("Failed to fetch global config data.");
            return;
        }
        const config = await response.json();
        globalConfig = config;
        populateMainConfig(config);
    } catch (error) {
        console.error("Error fetching global config:", error);
        // You can display a user-friendly message here
        alert("Failed to fetch global config data due to a network error.");
    }
}

async function fetchPresets() {
    const categories = {
        contextBottom: {
            rules: 'string',
            ruleSets: 'Array',
        },
        images: {
            NovelAI: 'ImagesNovelAIConfig',
            StableDiffusion: {
                Automatic1111: 'ImagesStableDiffusionConfig',
                ComfyUI: 'ImagesStableDiffusionConfig',
            },
        },
        textGeneration: {
            NovelAI: 'any',
            OpenAI: 'TextGenParametersConfigOpenAI',
        },
        TTS: {
            NovelAI: 'NovelAITTSConfig',
            ElevenLabs: 'ElevenLabsTTSConfig',
        },
    };

    async function processCategory(categoryPath, categoryStructure) {
        for (const [key, value] of Object.entries(categoryStructure)) {
            const currentPath = `${categoryPath}/${key}`;
            if (typeof value === 'object') {
                await processCategory(currentPath, value);
            } else {
                const files = await getPresetFolder(currentPath)
                for (const file of files) {
                    if (!file.includes('.')) continue
                    const preset = await fetchPreset(`${currentPath}/${file}`, value);
                }
            }
        }
    }

    try {
        await processCategory('', categories);
    } catch (error) {
        console.error("Error fetching presets:", error);
    }
}

async function fetchBotConfigs() {
    let bots = [];
    try {
        for (const bot of globalConfig.bots) {
            const response = await fetch(`/api/config/${bot}?className=BotConfig`);
            if (!response.ok) {
                const message = `Error fetching bot config. Status: ${response.status}`;
                console.error(message);
                // You can display a user-friendly message here
                alert(`Failed to fetch bot config data for ${bot.name}.`);
                continue;
            }
            const botConfig = await response.json();
            bots.push(botConfig);
        }
        displayBotConfigs(bots);
    } catch (error) {
        console.error("Error fetching bot configs:", error);
        // You can display a user-friendly message here
        alert("Failed to fetch bot configs data due to a network error.");
    }
}

async function fetchPreset(presetPath, className) {
    const response = await fetch(`/api/presets/getFile/${presetPath}?className=${className}`);
    if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const result = await response.text()
    try {
        return JSON.parse(result)
    } catch {
        return result
    }
}

async function getPresetFolder(path) {
    const response = await fetch(`/api/presets/getFolder${path ? `/${path}` : ''}`)
    if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`)
    }
    return await response.json()
}

// Fetch OpenAI text generation presets
async function fetchOpenAITextGenPresets() {
    const presetFolder = "/textGeneration/OpenAI";
    return await getPresetFolder(presetFolder);
}

// Fetch NovelAI text generation presets
async function fetchNovelAITextGenPresets() {
    const presetFolder = "/textGeneration/NovelAI";
    return await getPresetFolder(presetFolder);
}

// Example usage:
async function displayAllTextGenPresets() {
    const openAIPresets = await fetchOpenAITextGenPresets();
    const novelAIPresets = await fetchNovelAITextGenPresets();

    displayTextGenOpenAIPresets(openAIPresets)
    displayTextGenNovelAIPresets(novelAIPresets)
}

function displayTextGenNovelAIPresets(presets) {
    const presetList = document.getElementById("preset-text-novelai-list");
    presetList.innerHTML = "";
    presets.forEach((preset) => {
        const listItem = document.createElement("li");
        listItem.textContent = preset;
        listItem.addEventListener("click", () => {
            // Call a function to load a text gen preset when an item is clicked
            loadTextGenNovelAIPreset(preset);
        });
        presetList.appendChild(listItem);
    });
}

function displayTextGenOpenAIPresets(presets) {
    const presetList = document.getElementById("preset-text-openai-list");
    presetList.innerHTML = "";
    presets.forEach((preset) => {
        const listItem = document.createElement("li");
        listItem.textContent = preset;
        listItem.addEventListener("click", () => {
            // Call a function to load a text gen preset when an item is clicked
            loadTextGenOpenAIPreset(preset);
        });
        presetList.appendChild(listItem);
    });
}

async function loadTextGenOpenAIPreset(preset) {
    try {
        const className = 'TextGenParametersConfigOpenAI';
        const presetData = await fetchPreset(`textGeneration/OpenAI/${preset}`, className);
        const botConfigForm = document.getElementById('preset-text-openai');
        populateForm(botConfigForm, presetData);
        showSection('preset-text-openai');
    } catch (error) {
        console.error("Error loading text gen preset:", error);
        alert("Failed to load the text gen preset.");
    }
}

async function loadTextGenNovelAIPreset(preset) {
    try {
        const className = 'TextGenParametersConfigNovelAI';
        const presetData = await fetchPreset(`textGeneration/NovelAI/${preset}`, className);
        const botConfigForm = document.getElementById('preset-text-novelai');
        populateForm(botConfigForm, presetData);
        showSection('preset-text-novelai');
    } catch (error) {
        console.error("Error loading text gen preset:", error);
        alert("Failed to load the text gen preset.");
    }
}


function displayBotConfigs(botConfigs) {
    const botConfigList = document.getElementById('bot-config-list')
    botConfigList.innerHTML = ''
    botConfigs.forEach((config) => {
        const listItem = document.createElement('li')
        listItem.textContent = config.name
        listItem.addEventListener('click', () => {
            displayBotConfig(config)
        })
        botConfigList.appendChild(listItem)
    })
}

function displayBotConfig(botConfig) {
    showSection('bot-configuration');
    const botConfigForm = document.getElementById('bot-config-form');
    // Populate the form with the fetched bot config data
    populateForm(botConfigForm, botConfig);
    botConfigForm.querySelector('#botName').innerHTML = `Currently editing ${botConfig.name}`
}

document.getElementById('main-navigation')
    .addEventListener('click', (event) => {
        if (event.target.textContent === 'General Config') {
            fetchGlobalConfig()
        } else if (event.target.textContent === 'Bots') {
            fetchBotConfigs()
        } else if (event.target.textContent === 'Presets') {
            displayAllTextGenPresets()
        }
    })

showSection("main-configuration")
fetchGlobalConfig()