import config from "./classes/Config.js";

export default class Utils {
    static shuffleArrayInPlace(array) {
        let currentIndex = array.length, randomIndex;

        // While there remain elements to shuffle...
        while (currentIndex !== 0) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;

            // And swap it with the current element.
            [array[currentIndex], array[randomIndex]] = [
                array[randomIndex], array[currentIndex]];
        }

        return array;
    }

    static isObjectEmpty(objectName) {
        return (
            objectName &&
            Object.keys(objectName).length === 0 &&
            objectName.constructor === Object
        );
    };

    static shuffleArray(array) {
        try {
            return this.shuffleArrayInPlace(JSON.parse(JSON.stringify(array)))
        } catch {
            return array
        }
    }

    static textToJson(text) {
        const lines = text.split('\n');
        const jsonObject = {};
        const stack = [jsonObject];

        for (const line of lines) {
            const trimmedLine = line.trim();

            if (trimmedLine.endsWith(':')) {
                const property = trimmedLine.slice(0, -1).trim();
                const newObject = {};
                const currentObject = stack[stack.length - 1];
                if (currentObject) {
                    currentObject[property] = newObject;
                    stack.push(newObject);
                }
            } else if (trimmedLine.startsWith('-')) {
                const listItem = trimmedLine.slice(1).trim();
                const currentObject = stack[stack.length - 1];
                const currentProperty = Object.keys(currentObject)[0];

                if (!Array.isArray(currentObject[currentProperty])) {
                    currentObject[currentProperty] = [];
                }

                currentObject[currentProperty].push(listItem);
            } else if (trimmedLine === '') {
                stack.pop();
            } else {
                const [property, ...values] = line.split(':');
                const trimmedProperty = property.trim();
                let trimmedValue = values.join(':').trim();
                if (!trimmedProperty || !trimmedValue) continue
                const currentObject = stack[stack.length - 1];
                if (trimmedValue.endsWith(',')) {
                    trimmedValue = trimmedValue.substring(0, trimmedValue.length - 1) + '.'
                }
                if (currentObject) currentObject[trimmedProperty] = trimmedValue;
            }
        }

        const r = Object.entries(jsonObject)
        for (const [n, v] of r) {
            if (Array.isArray(v)) {
                jsonObject[n] = v.join(', ')
            } else if (typeof v === "object") {
                jsonObject[n] = Object.entries(v).map(([n2, v2]) => Array.isArray(v2) ? v2.join(', ') : `${n2}: ${v2}`).join('; ')
            } else {
                // No change
            }
        }

        return jsonObject;
    }

    /**
     *
     * @param {Object} json
     * @returns {string}
     */
    static jsonToCharacterText(json) {
        let text = 'CHARACTER:\n';

        for (const key in json) {
            const value = json[key];

            // Construct the text format
            if (typeof value === "string" || typeof value === "number") {
                text += `${key}: ${value}\n`;
            } else if (typeof value === "object") {
                // Array
                if (Array.isArray(value)) {
                    text += `${key}: ${value.map(v => `${v}`).join(', ')}\n`;
                }
                // Object
                else {
                    // TODO
                }
            }
        }

        return text.trim();
    }

    static jsonToText(json, noValueMode = false, placeholderMode = false) {
        let text = '';

        for (const key in json) {
            const value = json[key];

            // Construct the text format
            text += `${key}: ${noValueMode ? '...' : placeholderMode ? `\${${key}}` : value}\n`;
        }

        return text.trim();
    }

    static limitConsecutiveEmojis(input, max = null) {
        if (typeof input !== "string") return input

        if (max === null) {
            if (config?.maxConsecutiveEmojis) {
                max = config?.maxConsecutiveEmojis
            } else {
                max = 5
            }
        }

        if (!input) return input

        let output = '';
        let emojiBuffer = [];

        // Split the string into tokens based on potential emojis and other characters
        const tokens = input?.split?.(/( ?:\w+:|[^:]+)/g)?.filter?.(Boolean);

        if (tokens && tokens.length > 0) {
            for (const token of tokens) {
                if (token.match(/^ ?:\w+:$/)) {
                    // This is an emoji
                    emojiBuffer.push(token);
                } else {
                    // This is not an emoji
                    if (emojiBuffer.length > 0) {
                        output += emojiBuffer.slice(0, max).join('');
                        emojiBuffer = [];
                    }
                    output += token;
                }
            }
        }

        // Handle any remaining emojis in the buffer
        if (emojiBuffer.length > 0) {
            output += emojiBuffer.slice(0, max).join('');
        }

        return output;
    }

    static async retryFunction(func, args = [], maxRetries = 3) {
        let retries = 0;
        let result;
        while (!result && retries < maxRetries) {
            result = await func(...args);
            retries++;
            if (!result) await Utils.sleep(10000)
        }
        return result;
    }

    static sleep(ms) {
        // Everybody needs a little rest, don't we?
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    static async tryCatch(func, failFunc) {
        try {
            return await func()
        } catch (e) {
            console.error(e)
            await failFunc(e)
        }
    }
}
