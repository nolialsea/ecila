export default class BotConfig {
    /**
     * @type {string[]}
     * @description List of Discord user IDs that are banned. The bot won't answer to any messages or actions from these users.
     */
    bannedUsers

    /**
     * @type {string}
     * @description The Discord bot token for this bot instance. Each bot should have its own unique token.
     */
    discordToken

    /**
     * @type {string}
     * @description The name of the bot. This should match the bot's Discord username.
     */
    name

    /**
     * @type {?any}
     * @deprecated Do not use, use openAIContext or novelAIContext instead.
     */
    context

    /**
     * @type {?string|?{top: ?string|?string[], middle:?string|?string[], bottom: ?string|?string[]}}
     * @description The context for the bot when using the OpenAI language model. Can be a string or an object with top, middle, and bottom context sections.
     */
    openAIContext

    /**
     * @type {?string}
     * @description The context for the bot when using the OpenAI language model. If provided, this will completely override the openAIContext property.
     */
    card

    /**
     * @type {?string}
     * @description The context for the bot when using the NovelAI language model.
     */
    novelAIContext

    /**
     * @type {?TTSConfig}
     * @description Configuration options for text-to-speech functionality.
     */
    TTS

    /**
     * @type {TextGenParametersConfig}
     * @description Configuration options for text generation parameters.
     */
    textGenParameters

    /**
     * @type {?TextGenParametersConfigOpenAI}
     * @description OpenAI text generation parameters that will be used during self-update operations.
     */
    selfUpdateParameters

    /**
     * @type {?TextGenParametersConfigOpenAI}
     * @description OpenAI text generation parameters that will be used for image prompt generation.
     */
    imageGenerationParameters

    /**
     * @type {string[]}
     * @description List of command triggers that will cause the bot to generate a response.
     */
    commandTriggers

    /**
     * @type {?boolean|string|string[]}
     * @description Determines whether this bot should act as the master bot in a server. Can be a boolean (true/false for all servers), a string (server name or ID), or an array of strings (server names or IDs).
     */
    isMasterBot

    /**
     * @type {?Array<string>}
     * @description List of channel names in which the bot will always reply after a few seconds.
     */
    monoBotChannels

    /**
     * @type {?Array<string>}
     * @description List of channel names in which the bot may occasionally reply after a few seconds.
     */
    multiBotChannels

    /**
     * @type {?Array<string>}
     * @description List of channel names or IDs in which generated images will be marked as NSFW.
     */
    NSFWChannels

    /**
     * @type {string}
     * @description The main AI backend to use, either "ChatGPT", "Claude" or "ECILA" (for NovelAI).
     */
    mainBackend

    /**
     * @type {?boolean}
     * @description Set to true to enable the bot to provide smart answers in multi-bot channels. Unstable, leave false.
     */
    monoBotSmartAnswers = false

    /**
     * @type {?boolean}
     * @description Set to true to disable all image generation functionality.
     */
    disableImages = false

    /**
     * @type {?ImagesNovelAIConfig}
     * @description Configuration options for image generation using NovelAI.
     */
    imagesNovelAI

    /**
     * @type {?ImagesStableDiffusionConfig}
     * @description Configuration options for image generation using Stable Diffusion.
     */
    imagesStableDiffusion

    /**
     * @type {?boolean}
     * @description Set to true to enable the bot to maintain a memory of the conversation.
     */
    enableMemory

    /**
     * @type {?any}
     * @description Additional data specific to the bot instance.
     */
    data

    /**
     * @type {?any}
     * @description Additional constants specific to the bot instance.
     */
    constants = {}

    /**
     * @type {?boolean}
     * @description Set to true to allow the bot to be edited via the !load and !instructions commands.
     */
    editable = false

    /**
     * @type {?boolean}
     * @description Set to true to enable additional debug messages from the bot.
     */
    debug = false

    /**
     * @type {?boolean}
     * @description Set to true to disable NSFW images in all channels.
     */
    disableNSFW = false

    /**
     * @type {?number}
     * @description Time (in milliseconds) to wait before replying in mono-bot channels.
     */
    answerCooldown = 15000

    /**
     * @type {?number}
     * @description Time (in milliseconds) to wait before replying in multi-bot channels.
     */
    smartAnswerCooldown = 20000

    /**
     * @type {?string}
     * @description Instructions used to generate image prompts.
     */
    imagePromptGenerationInstructions

    /**
     * @type {?string}
     * @description Instructions used for self-editing operations.
     */
    selfEditionInstructionFile

    /**
     * @type {?ImagesConfig}
     * @description Image configurations used for the !character command. If not present, it will use the same as the bot's general settings.
     */
    characterGenerationImagesConfig

    /**
     * @type {?TextGenParametersConfigOpenAI}
     * @description Text generation configuration (OpenAI only) used for the !character command. If not present, it will use the same as the bot's general settings.
     */
    characterGenerationTextConfig

    /**
     * @type {?String[]}
     * @description List of channel names or IDs that the bot is allowed to operate in.
     */
    channelWhitelist

    /**
     * @type {?String[]}
     * @description List of usernames or IDs that the bot is allowed to interact with.
     */
    userWhitelist

    constructor(args) {
        Object.assign(this, args)
    }
}
