export default class NovelAITTSConfig {
    /** @type {?boolean} Enables auto NovelAI TTS */
    enabled

    /** @type {string} A string to define the voice, can be anything (think minecraft world seed) */
    seed

    /** @type {string} A second voice to read the third-person texts in */
    seedThirdPerson

    /** @type {?boolean} Will disregard anything in between asterisks. If set to false, it will use the third person seed for third-person text */
    skipThirdPerson = true

    constructor(args) {
        Object.assign(this, args)
    }
}