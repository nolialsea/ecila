export default class TTSConfig {
    /** @type {?boolean} Entirely disables TTS */
    disabled

    /** @type {?ElevenLabsTTSConfig}  */
    ElevenLabs

    /** @type {?NovelAITTSConfig}  */
    NovelAI

    constructor(args) {
        Object.assign(this, args)
    }
}