export default class ElevenLabsTTSConfig {
    /** @type {?boolean} Enables auto 11labs TTS */
    enabled

    /** @type {string} Your ElevenLabs API key */
    apiKey

    /** @type {string} Voice ID of the voice to use */
    voiceID

    /** @type {?boolean} Will disregard anything in between asterisks */
    skipThirdPerson = true

    /** @type {?number}  */
    stability = 1

    /** @type {?number}  */
    similarityBoost = 1

    /** @type {?number}  */
    style = 0

    /** @type {?boolean}  */
    useSpeakerBoost = true

    constructor(args) {
        Object.assign(this, args)
    }
}