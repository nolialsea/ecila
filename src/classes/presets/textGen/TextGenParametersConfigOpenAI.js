export default class TextGenParametersConfigOpenAI {
    /** @type {?boolean}  */
    enabled

    /** @type {?string} URL of the proxy ONLY IF YOU ARE USING ONE */
    URL

    /** @type {?string} Key of the proxy ONLY IF YOU ARE USING ONE */
    key

    /** @type {?string} Refer to this page to get the model names: https://platform.openai.com/docs/models */
    model

    /** @type {?number} Default 1 */
    temperature = 1

    /** @type {?number} Default 0 */
    presence_penalty = 0

    /** @type {?number} Default 0 */
    frequency_penalty = 0

    /** @type {?number} Default 6000 */
    max_prompt_length

    /** @type {?number} Default 4096, LOWER TO 2048 IF USING PROXY */
    max_tokens = 2048

    constructor(args) {
        Object.assign(this, args)
    }
}