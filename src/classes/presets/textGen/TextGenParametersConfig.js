export default class TextGenParametersConfig {
    /** @type {?TextGenParametersConfigOpenAI}  */
    OpenAI

    /** @type {?any}  */
    NovelAI

    /** @type {?any}  */
    Claude

    constructor(args) {
        Object.assign(this, args)
    }
}