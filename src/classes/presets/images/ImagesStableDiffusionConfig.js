export default class ImagesStableDiffusionConfig {
    /** @type {?boolean}  */
    enabled

    /** @type {?boolean}  */
    debug

    /** @type {?string}  */
    prePrompt

    /** @type {?string}  */
    postPrompt

    /** @type {?string}  */
    negativePrompt

    /** @type {?boolean}  */
    portrait

    /** @type {?boolean}  */
    landscape

    /** @type {?number}  */
    imageCount

    /** @type {?Array<string>}  */
    model

    /** @type {?string} Either "ComfyUI" or "A1111" */
    backend

    /** @type {?string} URL of the image backend (only for comfy right now, a1111 url is in general conf...) */
    backendURL

    constructor(args) {
        Object.assign(this, args)
    }
}