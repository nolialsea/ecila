export default class ImagesConfig{
    /** @type {?ImagesNovelAIConfig} */
    imagesNovelAI

    /** @type {?ImagesStableDiffusionConfig} */
    imagesStableDiffusion

    constructor(args) {
        Object.assign(this, args)
    }
}