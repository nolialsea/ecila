export default class ImagesNovelAIConfig {
    /** @type {?boolean}  */
    enabled

    /** @type {?boolean}  */
    debug

    /** @type {?string}  */
    prePrompt

    /** @type {?string}  */
    postPrompt

    /** @type {?string}  */
    negativePrompt

    /** @type {?boolean}  */
    portrait

    /** @type {?boolean}  */
    landscape

    /** @type {?number}  */
    imageCount

    constructor(args) {
        Object.assign(this, args)
    }
}