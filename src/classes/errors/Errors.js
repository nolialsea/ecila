/**
 * @class ApplicationError
 * @extends Error
 * @description A base class for custom application errors.
 * @param {string} message - The error message.
 * @param {number} statusCode - The HTTP status code associated with the error.
 */
export class ApplicationError extends Error {
    constructor(message, statusCode) {
        super(message);
        this.name = this.constructor.name;
        this.statusCode = statusCode;
        Error.captureStackTrace(this, this.constructor);
    }
}

/**
 * @class NotFoundError
 * @extends ApplicationError
 * @description An error class representing a resource not found error.
 * @param {string} resource - The resource that was not found.
 */
export class NotFoundError extends ApplicationError {
    constructor(resource) {
        super(`${resource} not found`, 404);
    }
}

/**
 * @class ValidationError
 * @extends ApplicationError
 * @description An error class representing a validation error.
 * @param {string} message - The validation error message.
 */
export class ValidationError extends ApplicationError {
    constructor(message) {
        super(message, 400);
    }
}
