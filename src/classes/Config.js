import fs, {readFileSync} from "fs"
import CardService from "../services/CardService.js"

export class Config {
    static config

    /** @type {string} URL of the ECILA API */
    ECILA_API_URL = "https://ecila.space"

    /** @type {string} Your ECILA API key */
    ECILAToken

    /** @type {?string} (Optional) Your OpenAI API key */
    openAIToken

    /** @type {string[]} List of discord roles to consider as admin */
    discordAdminRoleNames

    /** @type {string[]} List of discord user IDs to consider as admin (allows DM messages too) */
    discordAdminIds

    /** @type {?Object} List of username > alias to make the bots see users with a different name */
    aliases

    /** @type {BotConfig[]} List of BotConfigs or paths to the bots' JSON config file */
    bots

    /** @type {?string} Either "ECILA" or "ChatGPT" */
    mainBackend

    /** @type {?string} (Optional) Default URL for Automatic1111 */
    stableDiffusionBackendURL = "http://127.0.0.1:7860/"

    /** @type {?boolean} Enable DMs for users defined as admins in discordAdminIds */
    enableDirectMessages = false

    /** @type {?string} (Optional) Name of the channel in which deleted messages will be sent */
    deletedMessagesChannel = null

    /** @type {?number} Cooldown between answers for bots in mono-bot channels */
    answerCooldown = 15000

    /** @type {?number} Cooldown between answers for bots in multi-bot channels */
    smartAnswerCooldown = 20000

    /** @type {?number} Mostly for NovelAI, limits the number of consecutive emojis (NAI bots easily fall into repetition) */
    maxConsecutiveEmojis = 5

    /** @type {?string} Instruction set for the image prompt generation, can be text or a path to a text file */
    defaultImagePromptGenerationInstructions = './configs/presets/generators/imagePromptGeneration_contextBottomClaude.txt'

    /** @type {?string} Instruction set for the self-edition, can be text or a path to a text file */
    defaultSelfEditionGenerationInstructions = './configs/presets/generators/selfEdition_contextBottom.txt'

    /** @type {?TextGenParametersConfig} */
    defaultTextGenParameters

    /** @type {?ImagesConfig} Image configurations used for the !character command. If not present, it will use the same as the bot's */
    characterGenerationImagesConfig

    /** @type {?TextGenParametersConfigOpenAI} Text configurations (OpenAI only) used for the !character command. If not present, it will use the same as the bot's */
    characterGenerationTextConfig

    constructor(args) {
        Object.assign(this, args)
    }

    /**
     * Tries to replace any text values nested inside an object by the content of the file they point to
     * (if they point to a valid file)
     * @param {Object} jsonObj
     */
    static replaceFileContent(jsonObj) {
        function readFileContent(filePath) {
            const fileContent = fs.readFileSync(filePath, 'utf8')
            if (filePath.endsWith('.json')) {
                try {
                    const content = JSON.parse(fileContent)
                    Config.replaceFileContent(content)
                    return content
                } catch (error) {
                    console.error(`Error parsing JSON file: ${filePath}`)
                    return fileContent // Return plain text if JSON parsing fails
                }
            }
            return fileContent // Return plain text for non-JSON files
        }

        function processValue(value) {
            const isImage = typeof value === 'string' && value.toLowerCase().endsWith('.png')
            if (typeof value === 'string' && value.startsWith('./') && !isImage && fs.existsSync(value)) {
                return readFileContent(value)
            } else if (typeof value === 'string' && value.startsWith('./') && !fs.existsSync(value)) {
                console.log(`ERROR: couldn't load the file ${value}`)
            } else if (typeof value === 'object') {
                Config.replaceFileContent(value)
            } else if (Array.isArray(value)) {
                value.forEach((element, index) => {
                    value[index] = processValue(element)
                })
            }
            return value
        }

        for (let key in jsonObj) {
            if (jsonObj.hasOwnProperty(key)) {
                jsonObj[key] = processValue(jsonObj[key])
            }
        }
    }

    static async load() {
        const baseConfig = JSON.parse(readFileSync('./config.json').toString())
        const config = new Config(baseConfig)
        Config.replaceFileContent(config)
        for (const bot of config.bots) {
            if (bot.card && typeof bot.card === "string") {
                if (bot.card.toLowerCase().includes('.png')) {
                    const buffer = fs.readFileSync(bot.card)
                    bot.card = await CardService.extractCardContent(buffer)
                }
                await CardService.applyCardOntoBot(bot.card, bot)
            }
        }

        Config.config = config
        return Config.config
    }
}

/**
 * @type {Config}
 */
const config = await Config.load()

export default config
