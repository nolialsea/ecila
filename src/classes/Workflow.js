export default class Workflow {
    currentIndex
    nodes

    constructor() {
        this.currentIndex = 0
        this.nodes = {}
    }

    /**
     *
     * @param node
     * @returns {Node}
     */
    addNode(node) {
        this.currentIndex++
        node.index = `${this.currentIndex}`
        this.nodes[`${this.currentIndex}`] = node
        return node
    }

    /**
     *
     * @param nodes
     * @returns {Node[]}
     */
    addNodes(nodes) {
        const returnNodes = []
        for (const node of nodes) {
            returnNodes.push(this.addNode(node))
        }
        return returnNodes
    }
}
