/**
 * Enum for combining image operation
 * @readonly
 * @enum {string}
 */
const CombineOperation = Object.freeze({
    ADD: "add",
    UNION_MAX: "union (max)",
})

export default CombineOperation
