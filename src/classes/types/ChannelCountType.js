/**
 * Enum for channel count
 * // TODO: add missing channel count
 * @readonly
 * @enum {string}
 */
const ChannelCountType = Object.freeze({
    RGB: "RGB",
    RGBA: "RGBA"
})

export default ChannelCountType
