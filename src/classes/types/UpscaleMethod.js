/**
 * Enum for upscale methods
 * // TODO: add missing upscale methods
 * @readonly
 * @enum {string}
 */
const UpscaleMethod = Object.freeze({
    BICUBIC: "bicubic"
})

export default UpscaleMethod
