/**
 * Enum for crop methods
 * // TODO: add missing crop methods
 * @readonly
 * @enum {string}
 */
const CropMethod = Object.freeze({
    CROP: "center"
})

export default CropMethod
