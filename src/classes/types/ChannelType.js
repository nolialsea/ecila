/**
 * Enum for channel count
 * @readonly
 * @enum {string}
 */
const ChannelType = Object.freeze({
    RED: "red",
    BLUE: "blue",
    GREEN: "green"
})

export default ChannelType
