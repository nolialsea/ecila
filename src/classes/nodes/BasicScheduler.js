import Node from "./Node.js"

export default class BasicScheduler extends Node {
    /**
     *
     * @param {number} index
     * @param model
     * @param {string} scheduler
     * @param {number} steps
     * @param {number} denoise
     */
    constructor(index, model, scheduler = "simple", steps = 20, denoise = 1) {
        super(index, "BasicScheduler", {
                model: [model.index.toString(), 0],
                scheduler,
                steps,
                denoise
            }
        )
    }
}
