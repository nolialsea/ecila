import Node from "./Node.js"

export default class EmptySD3LatentImage extends Node {
    /**
     *
     * @param {number} index
     * @param {?number} width
     * @param {?number} height
     * @param {?number} batchSize
     */
    constructor(index, width = 512, height = 512, batchSize = 1) {
        super(index, "EmptySD3LatentImage", {width: width, height: height, batch_size: batchSize})
    }
}
