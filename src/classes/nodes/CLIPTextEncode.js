import Node from "./Node.js"

export default class CLIPTextEncode extends Node {
    /**
     *
     * @param {number} index
     * @param {ModelLoader|LoraLoader} modelNode
     * @param {string} prompt
     * @param {boolean} flux
     */
    constructor(index, modelNode, prompt, flux = false) {
        super(index, "CLIPTextEncode", {text: prompt, clip: [modelNode.index.toString(), !flux ? 1 : 0]})
    }
}
