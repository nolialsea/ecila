/**
 * @file VHS_VideoCombine.js
 * @description Node class for combining frames into a final video.
 */

import Node from "./Node.js";

/**
 * Represents a node that takes decoded frames and combines them into a final video.
 * @class
 * @extends Node
 */
export default class VHS_VideoCombine extends Node {
    /**
     * Creates an instance of VHS_VideoCombine.
     * @param {number} index - The index of this node in the workflow.
     * @param {Node} imagesNode - The node providing frames to combine.
     * @param {number} [frameRate=24] - The final video frame rate.
     * @param {number} [loopCount=0] - Number of times the video loops (0 for infinite).
     * @param {string} [filenamePrefix="AnimateDiff"] - Prefix for the output file name.
     * @param {string} [format="video/webm"] - Output video format.
     * @param {string} [pixFmt="yuv420p"] - Pixel format used in encoding.
     * @param {number} [crf=20] - Quality factor (lower = better, bigger file).
     * @param {boolean} [saveMetadata=true] - Whether to embed metadata.
     * @param {boolean} [pingpong=false] - Whether to do a ping-pong style replay.
     * @param {boolean} [saveOutput=true] - Whether to save the final output to disk.
     */
    constructor(
        index,
        imagesNode,
        frameRate = 24,
        loopCount = 0,
        filenamePrefix = "AnimateDiff",
        format = "video/webm",
        pixFmt = "yuv420p",
        crf = 20,
        saveMetadata = true,
        pingpong = false,
        saveOutput = true
    ) {
        super(index, "VHS_VideoCombine", {
            frame_rate: frameRate,
            loop_count: loopCount,
            filename_prefix: filenamePrefix,
            format,
            pix_fmt: pixFmt,
            crf,
            save_metadata: saveMetadata,
            pingpong,
            save_output: saveOutput,
            images: [imagesNode.index.toString(), 0]
        });
    }
}
