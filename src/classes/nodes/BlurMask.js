import Node from "./Node.js"

export default class BlurMask extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} imageNode
     * @param amount
     * @param device
     */
    constructor(index, imageNode, amount = 32, device = "gpu") {
        super(index, "MaskBlur+", {
            amount,
            device,
            mask: [imageNode.index.toString(), 0]
        })
    }
}
