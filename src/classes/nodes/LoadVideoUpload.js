import Node from "./Node.js"

export default class LoadVideoUpload extends Node {
    /**
     *
     * @param {number} index
     * @param {string} videoName
     * @param {number} width
     * @param {number} height
     * @param {number} length
     */
    constructor(index, videoName, width = 512, height = 512, length = 49) {
        super(index, "VHS_LoadVideo", {
            "video": videoName,
            "force_rate": 24,
            "force_size": "Custom",
            "custom_width": width,
            "custom_height": height,
            "frame_load_cap": length,
            "skip_first_frames": 0,
            "select_every_nth": 1
        })
    }
}
