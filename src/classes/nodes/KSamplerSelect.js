import Node from "./Node.js"

export default class KSamplerSelect extends Node {
    /**
     *
     * @param {number} index
     * @param {string} sampler_name
     */
    constructor(index, sampler_name = "euler") {
        super(index, "KSamplerSelect", {sampler_name})
    }
}
