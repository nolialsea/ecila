import Node from "./Node.js"

export default class VAEDecode extends Node {
    /**
     *
     * @param {number} index
     * @param {KSampler} kSamplerNode
     * @param {ModelLoader} modelNode
     * @param {boolean} flux
     */
    constructor(index, kSamplerNode, modelNode, flux = false) {
        super(index, "VAEDecode", {
            samples: [kSamplerNode.index.toString(), 0],
            vae: [modelNode.index.toString(), flux ? 0 : 2]
        })
    }
}
