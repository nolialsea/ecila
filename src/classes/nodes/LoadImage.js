import Node from "./Node.js"

export default class LoadImage extends Node {
    /**
     *
     * @param {number} index
     * @param {string} image_name
     * @param {?string} upload
     */
    constructor(index, image_name, upload = "image") {
        super(index, "LoadImage", {image: image_name, upload})
    }
}
