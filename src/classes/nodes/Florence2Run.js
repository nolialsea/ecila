import Node from "./Node.js"

export default class Florence2Run extends Node {
    constructor(index, imageNode, florence2ModelNode, text_input = "", task = "more_detailed_caption", fill_mask = true, keep_model_loaded = false, max_new_tokens = 1024, num_beams = 3, do_sample = true, output_mask_select = "", seed = null, speak_and_recognation = true) {
        super(index, "Florence2Run", {
            image: [imageNode.index.toString(), 0],
            florence2_model: [florence2ModelNode.index.toString(), 0],
            text_input,
            task,
            fill_mask,
            keep_model_loaded,
            max_new_tokens,
            num_beams,
            do_sample,
            output_mask_select,
            seed: seed ?? Math.floor(Math.random() * 1000000),
            speak_and_recognation
        });
    }
}
