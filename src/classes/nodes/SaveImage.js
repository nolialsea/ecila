import Node from "./Node.js"

export default class SaveImage extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} VAEDecodeNode
     * @param {?string} filename_prefix
     */
    constructor(index, VAEDecodeNode, filename_prefix = "ComfyUI") {
        super(index, "SaveImage", {filename_prefix, images: [VAEDecodeNode.index.toString(), 0]})
    }
}
