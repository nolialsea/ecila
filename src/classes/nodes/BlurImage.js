import Node from "./Node.js"

export default class BlurImage extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} imageNode
     * @param {number} radius
     * @param {number} sigmaFactor
     */
    constructor(index, imageNode, radius = 10, sigmaFactor = 1) {
        super(index, "Blur", {
            radius,
            sigma_factor: sigmaFactor,
            image: [imageNode.index.toString(), 0]
        })
    }
}
