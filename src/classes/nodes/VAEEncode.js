import Node from "./Node.js"

export default class VAEEncode extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} loadImageNode
     * @param {ModelLoader|LoraLoader} modelNode
     * @param {boolean} vaeOnly
     */
    constructor(index, loadImageNode, modelNode,
                tile_size = 128,
                overlap = 32,
                temporal_size = 64,
                temporal_overlap = 8, vaeOnly = false) {
        super(index, "VAEEncodeTiled", {
            pixels: [loadImageNode.index.toString(), 0],
            vae: [modelNode.index.toString(), vaeOnly ? 0 : 2],
            tile_size,
            overlap,
            temporal_size,
            temporal_overlap
        })
    }
}
