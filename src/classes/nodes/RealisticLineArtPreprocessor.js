import Node from "./Node.js"

export default class RealisticLineArtPreprocessor extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} loadImageNode
     * @param {number} resolution
     * @param {boolean} coarse
     */
    constructor(index, loadImageNode, resolution = 640, coarse = false) {
        super(index, "LineArtPreprocessor", {
            coarse: coarse ? 'enable' : 'disable',
            resolution,
            image: [loadImageNode.index.toString(), 0]
        })
    }
}
