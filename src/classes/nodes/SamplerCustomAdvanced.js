import Node from "./Node.js"

export default class SamplerCustomAdvanced extends Node {
    constructor(index, noise, guider, sampler, sigmas, latent_image) {
        super(index, "SamplerCustomAdvanced", {
            noise: [noise.index.toString(), 0],
            guider: [guider.index.toString(), 0],
            sampler: [sampler.index.toString(), 0],
            sigmas: [sigmas.index.toString(), 0],
            latent_image: [latent_image.index.toString(), 0],
        });
    }
}
