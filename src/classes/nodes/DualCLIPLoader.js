import Node from "./Node.js"

export default class DualCLIPLoader extends Node {
    /**
     *
     * @param {number} index
     * @param {string} clip_name1
     * @param {string} clip_name2
     * @param {string} type
     */
    constructor(index, clip_name1 = "t5xxl_fp8_e4m3fn.safetensors", clip_name2 = "clip_l.safetensors", type = "flux") {
        super(index, "DualCLIPLoader", {clip_name1, clip_name2, type})
    }
}
