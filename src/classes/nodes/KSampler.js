import Node from "./Node.js"

export default class KSampler extends Node {
    /**
     *
     * @param {number} index
     * @param {ModelLoader|LoraLoader} modelNode
     * @param {CLIPTextEncode} positiveCLIPTextEncodeNode
     * @param {CLIPTextEncode} negativeCLIPTextEncodeNode
     * @param {EmptyLatentImage|VAEEncode} latentImageNode
     * @param {?number} steps
     * @param {?number} cfg
     * @param {?string} sampler_name
     * @param {?string} scheduler
     * @param {?number} seed
     * @param {?number} denoise
     */
    constructor(index, modelNode, positiveCLIPTextEncodeNode, negativeCLIPTextEncodeNode, latentImageNode, steps = 64, cfg = 5, sampler_name = "dpmpp_3m_sde_gpu", scheduler = "exponential", seed = Math.floor(Math.random() * 9999999), denoise = 1) {
        super(index, "KSampler", {
            model: [modelNode.index.toString(), 0],
            positive: [positiveCLIPTextEncodeNode.index.toString(), 0],
            negative: negativeCLIPTextEncodeNode ? [negativeCLIPTextEncodeNode.index.toString(), 0] : [positiveCLIPTextEncodeNode.index.toString(), 1],
            latent_image: [latentImageNode.index.toString(), 0],
            seed,
            steps,
            cfg,
            sampler_name,
            scheduler,
            denoise
        });
    }
}
