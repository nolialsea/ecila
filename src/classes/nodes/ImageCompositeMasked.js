import Node from "./Node.js"

export default class ImageCompositeMasked extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} imageNodeSource
     * @param {Node} imageNodeDestination
     * @param {Node} maskNode
     * @param {number} x
     * @param {number} y
     * @param {boolean} resizeSource
     */
    constructor(index, imageNodeSource, imageNodeDestination, maskNode, resizeSource = false, x = 0, y = 0) {
        super(index, "ImageCompositeMasked", {
            x,
            y,
            resize_source: resizeSource,
            source: [imageNodeSource.index.toString(), 0],
            destination: [imageNodeDestination.index.toString(), 0],
            mask: [maskNode.index.toString(), 0]
        })
    }
}
