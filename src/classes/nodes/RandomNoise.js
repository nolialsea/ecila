import Node from "./Node.js"

export default class RandomNoise extends Node {
    /**
     *
     * @param {number} index
     * @param {?number} noise_seed
     */
    constructor(index, noise_seed = null) {
        super(index, "RandomNoise", {
            noise_seed: noise_seed === null ? Math.floor(Math.random() * 1000000) : noise_seed
        })
    }
}
