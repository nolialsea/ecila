import Node from "./Node.js"

export default class AnimeLineArtPreprocessor extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} loadImageNode
     * @param {number} resolution
     */
    constructor(index, loadImageNode, resolution = 640) {
        super(index, "AnimeLineArtPreprocessor", {
            resolution,
            image: [loadImageNode.index.toString(), 0]
        })
    }
}
