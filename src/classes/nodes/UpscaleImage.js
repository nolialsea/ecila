import Node from "./Node.js"

export default class UpscaleImage extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} imageNode
     * @param {number} width
     * @param {number} height
     * @param {UpscaleMethod} upscaleMethod
     * @param {CropMethod} cropMethod
     */
    constructor(index, imageNode, width = 512, height = 512, upscaleMethod = "bicubic", cropMethod = "center") {
        super(index, "ImageScale", {
            upscale_method: "bicubic",
            crop: cropMethod,
            width,
            height,
            image: [imageNode.index.toString(), 0]
        })
    }
}
