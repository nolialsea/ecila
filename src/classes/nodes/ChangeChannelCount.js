import Node from "./Node.js"

export default class ChangeChannelCount extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} imageNode
     * @param {ChannelCountType} kind
     */
    constructor(index, imageNode, kind = "RGB") {
        super(index, "Change Channel Count", {
            kind,
            image: [imageNode.index.toString(), 0],
        })
    }
}
