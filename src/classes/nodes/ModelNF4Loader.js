import Node from "./Node.js"

export default class ModelNF4Loader extends Node {
    /**
     *
     * @param {number} index
     * @param {string} modelName
     */
    constructor(index, modelName) {
        super(index, "CheckpointLoaderNF4", {ckpt_name: modelName})
    }
}
