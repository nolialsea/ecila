import Node from "./Node.js"

export default class VAELoader extends Node {
    /**
     *
     * @param {number} index
     * @param {string} vae_name
     */
    constructor(index, vae_name) {
        super(index, "VAELoader", {vae_name})
    }
}
