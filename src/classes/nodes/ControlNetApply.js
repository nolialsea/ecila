import Node from "./Node.js"

export default class ControlNetApply extends Node {
    /**
     *
     * @param {number} index
     * @param {CLIPTextEncode} positiveConditioningNode
     * @param {CLIPTextEncode} negativeConditioningNode
     * @param {ControlNetLoader} controlNetNode
     * @param {Node} imageNode
     * @param {number} strength
     * @param {number} startPercent
     * @param {number} endPercent
     */
    constructor(index, positiveConditioningNode, negativeConditioningNode, controlNetNode, imageNode, strength, startPercent, endPercent) {
        super(index, "ControlNetApplyAdvanced", {
            strength,
            start_percent: startPercent,
            end_percent: endPercent,
            positive: [positiveConditioningNode.index.toString(), 0],
            negative: negativeConditioningNode ? [negativeConditioningNode.index.toString(), 0] : [positiveConditioningNode.index.toString(), 1],
            control_net: [controlNetNode.index.toString(), 0],
            image: [imageNode.index.toString(), 0]
        })

    }
}
