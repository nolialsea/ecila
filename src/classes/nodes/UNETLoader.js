import Node from "./Node.js"

export default class UNETLoader extends Node {
    /**
     *
     * @param {number} index
     * @param {string} unet_name
     * @param {string} weight_dtype
     */
    constructor(index, unet_name = "flux1-dev.sft", weight_dtype = "default") {
        super(index, "UNETLoader", {unet_name, weight_dtype})
    }
}
