import Node from "./Node.js"

export default class ModelLoader extends Node {
    /**
     *
     * @param {number} index
     * @param {string} modelName
     */
    constructor(index, modelName) {
        super(index, "CheckpointLoaderSimple", {ckpt_name: modelName})
    }
}
