import Node from "./Node.js"

export default class SetLatentNoiseMask extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} latentNode
     * @param {Node} maskNode
     */
    constructor(index, latentNode, maskNode) {
        super(index, "SetLatentNoiseMask", {
            samples: [latentNode.index.toString(), 0],
            mask: [maskNode.index.toString(), 0]
        })
    }
}
