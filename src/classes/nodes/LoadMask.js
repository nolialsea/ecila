import Node from "./Node.js"

export default class LoadMask extends Node {
    /**
     *
     * @param {number} index
     * @param {string} image_name
     * @param {?string} upload
     * @param {?string} channel
     */
    constructor(index, image_name, upload = "image", channel="alpha") {
        super(index, "LoadImageMask", {image: image_name, upload, channel})
    }
}
