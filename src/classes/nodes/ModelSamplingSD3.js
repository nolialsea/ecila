/**
 * @file ModelSamplingSD3.js
 * @description Node class for advanced sampling logic in SD3 pipelines.
 */

import Node from "./Node.js";

/**
 * Represents a ModelSamplingSD3 node, which adjusts certain model-based sampling parameters
 * for advanced flows like Hunyuan video.
 * @class
 * @extends Node
 */
export default class ModelSamplingSD3 extends Node {
    /**
     * Creates an instance of ModelSamplingSD3.
     * @param {number} index - The index of this node in the workflow.
     * @param {Node} modelNode - The node representing the main model.
     * @param {number} [shift=7] - Some shift value used internally by the pipeline.
     */
    constructor(index, modelNode, shift = 7) {
        super(index, "ModelSamplingSD3", {
            model: [modelNode.index.toString(), 0],
            shift
        });
    }
}
