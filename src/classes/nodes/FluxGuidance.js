import Node from "./Node.js"

export default class FluxGuidance extends Node {
    constructor(index, conditioning, guidance = 3.5) {
        super(index, "FluxGuidance", {
            conditioning: [conditioning.index.toString(), 0],
            guidance
        })
    }
}
