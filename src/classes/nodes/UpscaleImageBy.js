import Node from "./Node.js"

export default class UpscaleImageBy extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} imageNode
     * @param {number} scaleBy
     * @param {UpscaleMethod} upscaleMethod
     */
    constructor(index, imageNode, scaleBy = 2, upscaleMethod = "bicubic") {
        super(index, "ImageScaleBy", {
            upscale_method: "bicubic",
            scale_by: scaleBy,
            image: [imageNode.index.toString(), 0]
        })
    }
}
