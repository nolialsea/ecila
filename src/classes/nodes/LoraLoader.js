import Node from "./Node.js"

export default class LoraLoader extends Node {
    /**
     *
     * @param {number} index
     * @param {ModelLoader|LoraLoader} modelNode
     * @param {string} lora_name
     * @param {?number} strength_model
     * @param {?number} strength_clip
     */
    constructor(index, modelNode, lora_name, strength_model = 1, strength_clip = 1, clipNode = null) {
        super(index, "LoraLoader", {
            model: [modelNode.index.toString(), 0],
            clip: clipNode ? [clipNode.index.toString(), 0] : [modelNode.index.toString(), 1],
            lora_name,
            strength_model,
            strength_clip
        });
    }
}
