import Node from "./Node.js"

export default class DepthAnythingPreprocessor extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} loadImageNode
     * @param {number} resolution
     */
    constructor(index, loadImageNode, resolution = 640) {
        super(index, "DepthAnythingPreprocessor", {
            resolution,
            image: [loadImageNode.index.toString(), 0],
            ckpt_name: "depth_anything_vits14.pth"
        })
    }
}
