import Node from "./Node.js"

export default class EmptyLatentImage extends Node {
    /**
     *
     * @param {number} index
     * @param {?number} width
     * @param {?number} height
     * @param {?number} batchSize
     */
    constructor(index, width = 512, height = 512, batchSize = 1) {
        super(index, "EmptyLatentImage", {width: width, height: height, batch_size: batchSize})
    }
}
