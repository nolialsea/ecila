import Node from "./Node.js"

export default class CombineMasks extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} imageNode1
     * @param {Node} imageNode2
     * @param {CombineOperation} operation
     * @param {boolean} clampResult
     * @param {boolean} roundResult
     */
    constructor(index, imageNode1, imageNode2, operation = "add", clampResult = true, roundResult = false) {
        super(index, "Combine Masks", {
            op: operation,
            clamp_result: clampResult ? "yes" : "no",
            round_result: roundResult ? "yes" : "no",
            image1: [imageNode1.index.toString(), 0],
            image2: imageNode2 ? [imageNode2.index.toString(), 0] : [imageNode1.index.toString(), 1],
        })
    }
}
