/**
 * @file VAEDecodeTiled.js
 * @description Node class for decoding latent frames with a tiled approach (often used for big images or videos).
 */

import Node from "./Node.js";

/**
 * Represents a node that decodes latents using a tiled VAE approach, which helps
 * handle large resolutions without blowing up GPU memory.
 * @class
 * @extends Node
 */
export default class VAEDecodeTiled extends Node {
    /**
     * Creates an instance of VAEDecodeTiled.
     * @param {number} index - The index of this node in the workflow.
     * @param {Node} samplesNode - The node providing latents to decode.
     * @param {Node} vaeNode - The node providing the loaded VAE.
     * @param {number} [tileSize=128] - Size of each tile in pixels.
     * @param {number} [overlap=32] - Overlap in pixels between tiles.
     * @param {number} [temporal_size=64] temporal_size
     * @param {number} [temporal_overlap=8] temporal_overlap
     */
    constructor(index, samplesNode, vaeNode, tileSize = 192, overlap = 64, temporal_size = 64, temporal_overlap = 8) {
        super(index, "VAEDecodeTiled", {
            tile_size: tileSize,
            overlap,
            temporal_size,
            temporal_overlap,
            samples: [samplesNode.index.toString(), 0],
            vae: [vaeNode.index.toString(), 0]
        });
    }
}
