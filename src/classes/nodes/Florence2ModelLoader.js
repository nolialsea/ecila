import Node from "./Node.js"

export default class Florence2ModelLoader extends Node {
    constructor(index, model = "CogFlorence-2.1-Large", precision = "fp16", attention = "eager") {
        super(index, "Florence2ModelLoader", {
            model,
            precision,
            attention,
        });
    }
}
