import Node from "./Node.js"

export default class ShowText extends Node {
    constructor(index, textNode) {
        super(index, "ShowText|pysssss", {
            text: [textNode.index.toString(), 2]
        })
    }
}
