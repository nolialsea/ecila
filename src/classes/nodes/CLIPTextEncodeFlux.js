import Node from "./Node.js"

export default class CLIPTextEncodeFlux extends Node {
    /**
     *
     * @param {number} index
     * @param {ModelLoader|LoraLoader} modelNode
     * @param {string} prompt
     * @param {?string} clipPrompt
     * @param {?number} guidance
     * @param {boolean} flux
     */
    constructor(index, modelNode, prompt, clipPrompt = "", guidance = 3.5, flux = false) {
        super(index, "CLIPTextEncodeFlux", {
            t5xxl: prompt,
            clip_l: clipPrompt ?? "",
            guidance: guidance ?? 3.5,
            speak_and_recognation: true,
            clip: [modelNode.index.toString(), !flux ? 1 : 0]
        })
    }
}
