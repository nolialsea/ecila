export default class Node {
    index
    class_type
    inputs

    constructor(index, type, inputs = {}) {
        this.index = index
        this.class_type = type
        this.inputs = inputs
    }
}
