/**
 * @file EmptyHunyuanLatentVideo.js
 * @description Node class for creating an empty latent video (Hunyuan style).
 */

import Node from "./Node.js";

/**
 * Represents an empty Hunyuan latent video used as a starting point for generation.
 * @class
 * @extends Node
 */
export default class EmptyHunyuanLatentVideo extends Node {
    /**
     * Creates an instance of EmptyHunyuanLatentVideo.
     * @param {number} index - The index of this node in the workflow.
     * @param {number} [width=320] - The width of the video frames.
     * @param {number} [height=480] - The height of the video frames.
     * @param {number} [length=49] - The number of frames in the video.
     * @param {number} [batchSize=1] - The batch size.
     */
    constructor(index, width = 320, height = 480, length = 49, batchSize = 1) {
        super(index, "EmptyHunyuanLatentVideo", {
            width,
            height,
            length,
            batch_size: batchSize
        });
    }
}
