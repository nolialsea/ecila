import Node from "./Node.js"

export default class ImageToMask extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} imageNode
     * @param {ChannelType} channel
     */
    constructor(index, imageNode, channel = "red") {
        super(index, "ImageToMask", {
            channel,
            image: [imageNode.index.toString(), 0],
        })
    }
}
