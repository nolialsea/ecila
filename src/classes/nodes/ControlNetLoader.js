import Node from "./Node.js"

export default class ControlNetLoader extends Node {
    /**
     *
     * @param {number} index
     * @param {string} modelName
     */
    constructor(index, modelName) {
        super(index, "ControlNetLoader", {control_net_name: modelName})
    }
}
