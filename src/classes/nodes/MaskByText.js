import Node from "./Node.js"

export default class MaskByText extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} imageNode
     * @param {string} prompt
     * @param {string} negativePrompt
     * @param {number} precision
     * @param {boolean} normalize
     */
    constructor(index, imageNode, prompt, negativePrompt, precision = 0.5, normalize = false) {
        super(index, "Mask By Text", {
            prompt,
            negative_prompt: negativePrompt,
            precision,
            normalize: normalize ? 'yes' : 'no',
            image: [imageNode.index.toString(), 0]
        })
    }
}
