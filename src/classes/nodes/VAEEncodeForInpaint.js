import Node from "./Node.js"

export default class VAEEncodeForInpaint extends Node {
    /**
     *
     * @param {number} index
     * @param {Node} loadImageNode
     * @param {ModelLoader|LoraLoader} modelNode
     * @param {Node} maskNode
     * @param {number} growMaskBy
     */
    constructor(index, loadImageNode, modelNode, maskNode, growMaskBy = 6) {
        super(index, "VAEEncodeForInpaint", {grow_mask_by: growMaskBy, pixels: [loadImageNode.index.toString(), 0], vae: [modelNode.index.toString(), 2], mask: [maskNode.index.toString(), 0]})
    }
}
