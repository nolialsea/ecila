import Node from "./Node.js"

export default class BasicGuider extends Node {
    constructor(index, model, conditioning) {
        super(index, "BasicGuider", {
            model: [model.index.toString(), 0],
            conditioning: [conditioning.index.toString(), 0],
        })
    }
}
