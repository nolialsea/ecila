export default function paramExtractor(parameters, paramPrefix, property, type = "string", hasSpaces = false, min = 0, max = 1) {
    let prompt = parameters["prompt"]

    const newParams = {
        ...parameters
    }

    let pattern

    if (type === "string" && !hasSpaces) {
        pattern = new RegExp(`${paramPrefix} "?([a-zA-Z0-9\/\._,-]+)"?`, "");
    } else if (type === "string" && hasSpaces) {
        pattern = new RegExp(`${paramPrefix} ["“”]([^"“”]+)["“”]`, "");
    } else if (type === "number" || type === "float") {
        pattern = new RegExp(`${paramPrefix} ([0-9\.,]+)`, "");
    }

    let value
    let match = pattern.exec(prompt)
    if (match !== null) {
        value = match[1]
        prompt = prompt.replace(match[0], '')

        if (type === "number") value = parseInt(value)
        if (type === "float") value = parseFloat(value)

        if (type === "number" || type === "float") {
            value = Math.min(max, Math.max(min, value))
        }

        prompt = prompt.replaceAll('  ', ' ')
        prompt = prompt.replaceAll(', ,', ',')
        prompt = prompt.replaceAll(',,', ',')
        prompt = prompt.trim()

        newParams[property] = value
        newParams["prompt"] = prompt
    }

    return newParams;
}
