function extractDenoise(str, denoiseDefault = 0.5) {
    const pattern = /<denoise:([0-9-]*\.?[0-9-]*)?>/i;
    let denoise = denoiseDefault
    let match = pattern.exec(str)
    if (match !== null) {
        denoise = match[1]
        str = str.replace(match[0], '')
    }

    str = str.replaceAll('  ', ' ')
    str = str.replaceAll(', ,', ',')
    str = str.replaceAll(',,', ',')
    str = str.trim()

    return {denoise, promptWithoutDenoise: str};
}

export default extractDenoise
