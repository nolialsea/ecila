function extractTextToMask(str) {
    const pattern = /<mask:([a-zA-Z0-9_\-. ]*):?([0-9a-zA-Z0-9_\-. ]*)?:?([0-9-]*\.?[0-9-]*)?>/i;
    const extractedMasks = [];
    let match;
    while (true) {
        match = pattern.exec(str)

        if (match !== null) {
            const positivePrompt = match[1]
            let negativePrompt = match[2]
            let precision = match[3]

            if (!precision && !isNaN(parseFloat(negativePrompt))) {
                precision = parseFloat(negativePrompt)
                negativePrompt = ""
            }

            if (!precision) precision = 0.5

            if (typeof precision === "string") {
                precision = parseFloat(precision)
            }

            if (!negativePrompt) negativePrompt = ""

            extractedMasks.push({
                positivePrompt,
                negativePrompt,
                precision
            });
            str = str.replace(match[0], '')
        } else {
            break
        }
    }

    str = str.replaceAll('  ', ' ')
    str = str.replaceAll(', ,', ',')
    str = str.replaceAll(',,', ',')
    str = str.trim()

    return {extractedMasks, promptWithoutMasks: str};
}

export default extractTextToMask
