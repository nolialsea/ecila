import axios from "axios";

export default async function getImageBuffer(url) {
    let response = await axios.get(url, {responseType: 'arraybuffer'});
    return Buffer.from(response.data, 'binary');
}
