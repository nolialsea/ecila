import paramExtractor from "./ParameterExtractor.js";


let sharp;
try {
    sharp = await import('sharp');
} catch (error) {
    console.warn("Sharp library could not be loaded. Image processing will be limited.", error);
}

async function getImageDimensions(buffer) {
    if (!sharp) return {width: 512, height: 512}
    let metadata = await sharp(buffer).metadata();
    return {
        width: metadata.width,
        height: metadata.height
    };
}

export default async function paramsExtractor(params, prompt, imageBuffer = null) {
    let newParams = {...params, prompt, seed: Math.floor(Math.random() * 9999999)}
    newParams = paramExtractor(newParams, `-M`, 'model')
    newParams = paramExtractor(newParams, `--model`, 'model')
    newParams = paramExtractor(newParams, `-MT`, 'modelType')
    newParams = paramExtractor(newParams, `--model-type`, 'modelType')
    newParams = paramExtractor(newParams, `-N`, 'negativePrompt', 'string', true)
    newParams = paramExtractor(newParams, `--negative-prompt`, 'negativePrompt', 'string', true)
    newParams = paramExtractor(newParams, `-C`, 'clipPrompt', 'string', true)
    newParams = paramExtractor(newParams, `--clip-prompt`, 'clipPrompt', 'string', true)
    newParams = paramExtractor(newParams, `-SA`, 'sampler')
    newParams = paramExtractor(newParams, `--sampler`, 'sampler')
    newParams = paramExtractor(newParams, `-SC`, 'scheduler')
    newParams = paramExtractor(newParams, `--scheduler`, 'scheduler')
    newParams = paramExtractor(newParams, `-S`, 'seed', 'number', false, 0, 9999999)
    newParams = paramExtractor(newParams, `--seed`, 'seed', 'number', false, 0, 9999999)
    newParams = paramExtractor(newParams, `-w`, 'width', 'number', false, 128, 4096)
    newParams = paramExtractor(newParams, `--width`, 'width', 'number', false, 128, 4096)
    newParams = paramExtractor(newParams, `-h`, 'height', 'number', false, 128, 4096)
    newParams = paramExtractor(newParams, `--height`, 'height', 'number', false, 128, 4096)
    newParams = paramExtractor(newParams, `-l`, 'length', 'number', false, 1, 121)
    newParams = paramExtractor(newParams, `--length`, 'length', 'number', false, 1, 121)
    newParams = paramExtractor(newParams, `-s`, 'steps', 'number', false, 1, 128)
    newParams = paramExtractor(newParams, `--steps`, 'steps', 'number', false, 1, 128)
    newParams = paramExtractor(newParams, `-i`, 'iteration', 'number', false, 1, 4)
    newParams = paramExtractor(newParams, `--iteration`, 'iteration', 'number', false, 1, 4)
    newParams = paramExtractor(newParams, `-c`, 'cfg', 'float', false, 1, 20)
    newParams = paramExtractor(newParams, `--cfg`, 'cfg', 'float', false, 1, 20)
    newParams = paramExtractor(newParams, `-d`, 'denoise', 'float', false, 0, 1)
    newParams = paramExtractor(newParams, `--denoise`, 'denoise', 'float', false, 0, 1)
    newParams = paramExtractor(newParams, `-mg`, 'maskGrowth', 'number', false, 0, 64)
    newParams = paramExtractor(newParams, `--mask-growth`, 'maskGrowth', 'number', false, 0, 64)
    newParams = paramExtractor(newParams, `-br`, 'blurRadius', 'number', false, 0, 48)
    newParams = paramExtractor(newParams, `--blur-radius`, 'blurRadius', 'number', false, 0, 48)
    newParams = paramExtractor(newParams, `-bs`, 'blurSigmaFactor', 'float', false, 0.01, 1024)
    newParams = paramExtractor(newParams, `--blur-sigma`, 'blurSigmaFactor', 'float', false, 0.01, 1024)
    newParams = paramExtractor(newParams, `-cnp`, 'controlNetStrength', 'float', false, 0, 1)
    newParams = paramExtractor(newParams, `--controlnet-strength`, 'controlNetStrength', 'float', false, 0, 1)
    newParams = paramExtractor(newParams, `-cns`, 'controlNetStart', 'float', false, 0, 1)
    newParams = paramExtractor(newParams, `--controlnet-start`, 'controlNetStart', 'float', false, 0, 1)
    newParams = paramExtractor(newParams, `-cne`, 'controlNetEnd', 'float', false, 0, 1)
    newParams = paramExtractor(newParams, `--controlnet-end`, 'controlNetEnd', 'float', false, 0, 1)
    newParams = paramExtractor(newParams, `-ts`, 'temporalSize', 'number', false, 64, 512)
    newParams = paramExtractor(newParams, `--temporal-size`, 'temporalSize', 'number', false, 64, 512)
    newParams = paramExtractor(newParams, `-to`, 'temporalOverlap', 'number', false, 1, 64)
    newParams = paramExtractor(newParams, `--temporal-overlap`, 'temporalOverlap', 'number', false, 1, 64)
    newParams = paramExtractor(newParams, `-f`, 'format', 'string', false)
    newParams = paramExtractor(newParams, `--format`, 'format', 'string', false)


    if (imageBuffer && !prompt.match(/-w [0-9]*/) && !prompt.match(/-h [0-9]*/)) {
        /*
        try {
            const resolution = await getImageDimensions(imageBuffer)
            const portraitMode = resolution.width < resolution.height
            const landscapeMode = resolution.width > resolution.height

            const width = landscapeMode ? params.height * 1.5 : portraitMode ? params.width : params.height
            const height = portraitMode ? params.height : params.height
            newParams.width = width
            newParams.height = height
        }catch(err){
            console.error(err)
        }
        */
    }

    return newParams
}
