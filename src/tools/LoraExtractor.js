function extractLoras(str) {
    // Define the regex pattern
    const pattern = /<lora:([a-zA-Z0-9_\-.]*):?([0-9-]*\.?[0-9-]*)?:?([0-9-]*\.?[0-9-]*)?>/i;

    // Initialize an array to hold our findings
    const extractedLoras = [];

    // Use regex .exec method to keep extracting matches from the string
    let match;
    while (true) {
        match = pattern.exec(str)

        if (match !== null) {

            let filename = match[1]
            if (!([".ckpt", ".pt", ".safetensors"].some(f=> filename?.includes?.(f)))){
                filename += '.safetensors'
            }

            extractedLoras.push({
                filename,
                strength_model: match[2] ? parseFloat(match[2]) : 1.0,
                strength_clip: match[3] ? parseFloat(match[3]) : 1.0
            });
            str = str.replace(match[0], '')
        } else {
            break
        }
    }

    str = str.replaceAll('  ', ' ')
    str = str.replaceAll(', ,', ',')
    str = str.replaceAll(',,', ',')
    str = str.trim()

    // Return the array of lora objects
    return {extractedLoras, promptWithoutLoras: str};
}

export default extractLoras
