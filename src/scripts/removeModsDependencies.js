import fs from "fs";

if (fs.existsSync(`package-base-dependencies.json`)) {
    const baseDependencies = JSON.parse(fs.readFileSync(`package-base-dependencies.json`).toString())
    const currentPackageDotJson = JSON.parse(fs.readFileSync(`package.json`).toString())

    currentPackageDotJson.dependencies = baseDependencies
    fs.writeFileSync(`package.json`, JSON.stringify(currentPackageDotJson, null, 4))
}

process.exit()