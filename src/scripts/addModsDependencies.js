import fs from "fs";
import {loadModList} from "../services/InitializeClientService.js";

const modList = loadModList()

const currentPackageDotJson = JSON.parse(fs.readFileSync(`package.json`).toString())

// backup old dependencies
fs.writeFileSync(`package-base-dependencies.json`, JSON.stringify(currentPackageDotJson.dependencies, null, 4))

for (const {name, enabled} of modList) {
    if (!enabled) continue

    if (fs.existsSync(`mods/${name}`) && fs.existsSync(`mods/${name}/package.json`)) {
        const modPackageDotJson = JSON.parse(fs.readFileSync(`mods/${name}/package.json`).toString())

        console.log("Mod dependencies found:", modPackageDotJson)

        for (const [dependency, version] of Object.entries(modPackageDotJson.dependencies)) {
            currentPackageDotJson.dependencies[dependency] = version
        }
    }
}

process.exit()