import express from 'express'
import fs from 'fs'
import path from "path"
import {Config} from "../classes/Config.js";
import BotConfig from "../classes/BotConfig.js";

const router = express.Router()

function readConfig(filePath) {
    try {
        const json = fs.readFileSync(filePath).toString()
        return JSON.parse(json)
    } catch (err) {
        console.error('Error reading file:', err)
        throw new Error('File not found')
    }
}

function saveConfig(filePath, config) {
    try {
        const json = JSON.stringify(config, null, 2)
        fs.writeFileSync(filePath, json)
    } catch (err) {
        console.error('Error writing file:', err)
        throw new Error('Internal server error')
    }
}

function getConfigFilePath(nestedPath) {
    return path.join('./', nestedPath)
}

const configClasses = {
    'Config': Config,
    'BotConfig': BotConfig
}

function castConfigToClass(className, configData) {
    if (className && configClasses[className]) {
        return new configClasses[className](configData)
    }
    return configData
}

router.get('/*', (req, res, next) => {
    const nestedPath = req.params[0] || '';
    const className = req.query.className;
    const filePath = getConfigFilePath(nestedPath);
    try {
        const config = readConfig(filePath);
        const castedConfig = castConfigToClass(className, config);
        res.json(castedConfig);
    } catch (err) {
        if (err.message === 'File not found') {
            next(new NotFoundError(`Config file at ${filePath}`));
        } else {
            next(err);
        }
    }
});

router.post('/*', (req, res) => {
    const nestedPath = req.params[0] || ''
    const filePath = getConfigFilePath(nestedPath)
    try {
        const config = req.body
        saveConfig(filePath, config)
        res.status(200).send('Configuration saved successfully!')
    } catch (err) {
        res.status(500).send(err.message)
    }
})

export default router