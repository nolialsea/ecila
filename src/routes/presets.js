import express from 'express'
import fs from 'fs'
import path from "path"
import ImagesConfig from "../classes/presets/images/ImagesConfig.js";
import ImagesNovelAIConfig from "../classes/presets/images/ImagesNovelAIConfig.js";
import ImagesStableDiffusionConfig from "../classes/presets/images/ImagesStableDiffusionConfig.js";
import TextGenParametersConfig from "../classes/presets/textGen/TextGenParametersConfig.js";
import TextGenParametersConfigOpenAI from "../classes/presets/textGen/TextGenParametersConfigOpenAI.js";
import TTSConfig from "../classes/presets/TTS/TTSConfig.js";
import ElevenLabsTTSConfig from "../classes/presets/TTS/ElevenLabsTTSConfig.js";
import NovelAITTSConfig from "../classes/presets/TTS/NovelAITTSConfig.js";

const router = express.Router()

function readConfig(filePath) {
    try {
        const json = fs.readFileSync(filePath).toString()
        try {
            return JSON.parse(json)
        } catch {
            return json
        }

    } catch (err) {
        console.error('Error reading file:', err)
        throw new Error('File not found')
    }
}

function readDirectory(filePath) {
    try {
        return fs.readdirSync(filePath)
    } catch (err) {
        console.error('Error reading directory:', err)
        throw new Error('File not found')
    }
}

function saveConfig(filePath, config) {
    try {
        const json = JSON.stringify(config, null, 2)
        fs.writeFileSync(filePath, json)
    } catch (err) {
        console.error('Error writing file:', err)
        throw new Error('Internal server error')
    }
}

function getConfigFilePath(nestedPath) {
    return path.join('./configs/presets/', nestedPath)
}

const configClasses = {
    'ImagesConfig': ImagesConfig,
    'ImagesNovelAIConfig': ImagesNovelAIConfig,
    'ImagesStableDiffusionConfig': ImagesStableDiffusionConfig,
    'TextGenParametersConfig': TextGenParametersConfig,
    'TextGenParametersConfigOpenAI': TextGenParametersConfigOpenAI,
    'TTSConfig': TTSConfig,
    'ElevenLabsTTSConfig': ElevenLabsTTSConfig,
    'NovelAITTSConfig': NovelAITTSConfig
}

function castConfigToClass(className, configData) {
    if (className && configClasses[className]) {
        return new configClasses[className](configData)
    }
    return configData
}

router.get('/getFolder', (req, res) => {
    const nestedPath = req.params[0] || ''
    const filePath = getConfigFilePath(nestedPath)
    try {
        const fileList = readDirectory(filePath)
        res.json(fileList)
    } catch (err) {
        res.status(400).send(err.message)
    }
})
router.get('/getFolder/*', (req, res) => {
    const nestedPath = req.params[0] || ''
    const filePath = getConfigFilePath(nestedPath)
    try {
        const fileList = readDirectory(filePath)
        res.json(fileList)
    } catch (err) {
        res.status(400).send(err.message)
    }
})

router.get('/getFile/*', (req, res) => {
    const nestedPath = req.params[0] || ''
    const className = req.query.className
    const filePath = getConfigFilePath(nestedPath)

    try {
        const config = readConfig(filePath)
        const castedConfig = castConfigToClass(className, config)
        if (typeof castedConfig === "string") {
            res.send(castedConfig)
        } else {
            res.json(castedConfig)
        }
    } catch (err) {
        res.status(400).send(err.message)
    }
})

router.post('/post/*', (req, res) => {
    const nestedPath = req.params[0] || ''
    const filePath = getConfigFilePath(nestedPath)
    try {
        const config = req.body
        saveConfig(filePath, config)
        res.status(200).send('Configuration saved successfully!')
    } catch (err) {
        res.status(500).send(err.message)
    }
})

export default router