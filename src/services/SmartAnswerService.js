import DiscordService from "./DiscordService.js";
import AIService from "./AiService.js";

export default class SmartAnswerService {
    /**
     *
     * @param {Message} userMessage
     * @param {BotConfig} bot
     * @param {string} channelTopic
     * @param {boolean} useEmojiInsteadOfGeneration
     * @param {{client: Client, bot: BotConfig}[]} clients
     * @param {?object} functions
     * @returns {Promise<void>}
     */
    static async sendSmartAnswer(userMessage, bot, channelTopic, useEmojiInsteadOfGeneration = false, clients = [], functions = null) {
        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)
        const predictedAuthors = (await AIService.generateNextAuthorPrediction({
            context: ''
        }, messages, channelTopic)) || []

        console.log(`Predicted next authors in #${userMessage?.channel?.name} for ${userMessage?.author?.username}'s last message:`, predictedAuthors, "message: ", userMessage.cleanContent?.substring?.(0, 30))

        if (useEmojiInsteadOfGeneration) {
            const score = predictedAuthors.reduce((prev, curr) => {
                if (curr?.toLowerCase() === bot?.name?.toLowerCase()) {
                    return ++prev
                }
                return prev
            }, 0)

        } else {
            const clientScores = clients
                .map(c => {
                    const score = predictedAuthors.reduce((prev, curr) => {
                        if (curr?.toLowerCase() === c?.bot?.name?.toLowerCase()) {
                            return ++prev
                        }
                        return prev
                    }, 0)
                    return {score, client: c}
                })
                .sort((a, b) => b.score - a.score)
                .filter(c => c.score > 0)
                .filter(c => c.client.bot.monoBotChannels.includes(userMessage.channel?.name) || c.client.bot.multiBotChannels.includes(userMessage.channel?.name))

            for (let client of clientScores) {
                const channel = await client.client.client.channels?.fetch?.(userMessage.channel.id, {
                    force: true,
                    allowUnknownGuild: true
                }).catch(() => null)

                if (!channel) {
                    continue
                }

                const userMessages = await DiscordService.fetchMessages(channel)
                if (userMessages.length === 0) {
                    continue
                }

                const userMessage_ = userMessages.find(m => m[0] === userMessage.id)?.[1]
                if (!userMessage_) {
                    continue
                }

                const chanceToTrigger = client.score + 1 / 4
                if (Math.random() < chanceToTrigger) {
                    if (client.client.client.application.id === userMessage_.author.id) {

                    } else {
                        await DiscordService.generateAndSendMessage(userMessage_, client.client.bot, channelTopic, messages, functions)
                        break
                    }
                } else {
                    console.log(client.client.bot.name, 'failed random dice roll with', (chanceToTrigger * 100).toFixed(2), '% chances')
                }
            }
        }
    }
}