import axios from 'axios'
import {Buffer} from "node:buffer"

export default class ComfyService {
    /**
     *
     * @param {string} URL
     * @param {any} workflow
     * @returns {Promise<Buffer[]>}
     * @throws {Error}
     */
    static async generateComfyUIImage(URL = "http://localhost:8188/", workflow) {
        try {
            workflow = JSON.parse(JSON.stringify(workflow.nodes))
        } catch (err) {

        }

        const response = await axios.post(`${URL}prompt`, {
            prompt: workflow
        }, {
            timeout: 1000 * 60 * 20, headers: {
                'Content-Type': 'application/json'
            }
        }).catch(err => {
            try {
                console.log(JSON.stringify(err?.response?.data, null, 2))
            } catch {
            }
        })

        const promptId = response?.data?.prompt_id

        if (!promptId) throw new Error("Couldn't execute the request.")

        let r = null

        async function getRequest(promptId) {
            return new Promise((resolve, reject) => {
                axios.get(`${URL}history/${promptId}`)
                    .then(resp => resolve(resp.data))
                    .catch(reason => reject(reason))
            })
        }

        while (promptId) {
            const result = await getRequest(promptId)
            if (result && Object.keys(result).length > 0) {
                r = result
                await new Promise(r => setTimeout(r, 3000))
                break
            } else {
                await new Promise(r => setTimeout(r, 2000))
            }
        }

        const outputNodes = Object.values(workflow).filter(n => n.class_type === "SaveImage" || n.class_type === "VHS_VideoCombine")

        let image_filenames

        try {
            image_filenames = outputNodes.map(on => r?.[promptId]?.['outputs']?.[on.index.toString()]?.['images'] || r?.[promptId]?.['outputs']?.[on.index.toString()]?.['gifs'])
        } catch (err) {
            console.error(err)
            image_filenames = Object.values(r?.[promptId]?.['outputs'])?.[0]?.['gifs']
        }

        async function fetchFileAndConvertToBuffer(endpointURL) {
            try {
                // Fetch file from the API
                const response = await axios.get(endpointURL, {
                    responseType: 'arraybuffer' // Tell axios to return data as ArrayBuffer
                });

                // Convert the ArrayBuffer to base64
                const base64Data = Buffer.from(response.data, 'binary').toString('base64');

                // Convert base64 to Buffer
                return Buffer.from(base64Data, 'base64');
            } catch (error) {
                console.error('Error fetching and converting file:', error.data);
                // throw error;
            }
        }

        let data = []

        if (image_filenames && image_filenames.length > 0) {
            for (const imageFilename of image_filenames) {
                if (Array.isArray(imageFilename)) {
                    for (const image_filename of imageFilename) {
                        const filename = image_filename['filename']
                        data.push(await fetchFileAndConvertToBuffer(`${URL}view?filename=${filename}`))
                    }
                } else {
                    const filename = imageFilename['filename']
                    data.push(await fetchFileAndConvertToBuffer(`${URL}view?filename=${filename}`))
                }
            }
        }

        if (!data || data.length === 0) return console.log(`ERROR: no image data was found.`)

        return data.map(d => Buffer.from(d, 'base64'))
    }

    static async generateComfyUIText(URL = "http://localhost:8188/", workflow) {
        workflow = JSON.parse(JSON.stringify(workflow.nodes))

        const response = await axios.post(`${URL}prompt`, {
            prompt: workflow
        }, {
            timeout: 1000 * 60 * 20, headers: {
                'Content-Type': 'application/json'
            }
        }).catch(err => console.log(JSON.stringify(err.response.data, null, 2)))

        const promptId = response?.data?.prompt_id

        if (!promptId) throw new Error("Couldn't execute the request.")

        let r = null

        async function getRequest(promptId) {
            return new Promise((resolve, reject) => {
                axios.get(`${URL}history/${promptId}`)
                    .then(resp => resolve(resp.data))
                    .catch(reason => reject(reason))
            })
        }

        while (promptId) {
            const result = await getRequest(promptId)
            if (result && Object.keys(result).length > 0) {
                r = result
                break
            } else {
                await new Promise(r => setTimeout(r, 2000))
            }
        }

        const outputNodes = Object.values(workflow).filter(n => n.class_type === "ShowText|pysssss")

        return outputNodes.map(on => r?.[promptId]?.['outputs']?.[on.index.toString()]?.['text'])
    }

    /**
     *
     * @param {string} URL
     * @param {any} workflow
     * @returns {Promise<Buffer[]>}
     * @throws {Error}
     */
    static async callComfyUI(URL = "http://localhost:8188/", workflow) {
        workflow = JSON.parse(JSON.stringify(workflow.nodes))

        const response = await axios.post(`${URL}prompt`, {
            prompt: workflow
        }, {
            timeout: 1000 * 60 * 20, headers: {
                'Content-Type': 'application/json'
            }
        }).catch(err => console.log(JSON.stringify(err.response.data, null, 2)))

        const promptId = response?.data?.prompt_id

        if (!promptId) throw new Error("Couldn't execute the request.")

        let r = null

        async function getRequest(promptId) {
            return new Promise((resolve, reject) => {
                axios.get(`${URL}history/${promptId}`)
                    .then(resp => resolve(resp.data))
                    .catch(reason => reject(reason))
            })
        }

        while (promptId) {
            const result = await getRequest(promptId)
            if (result && Object.keys(result).length > 0) {
                r = result
                break
            } else {
                await new Promise(r => setTimeout(r, 2000))
            }
        }

        return r
    }
}
