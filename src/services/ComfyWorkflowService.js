export class LoRA {
    /** @type {string} Name of the LoRA file with extension */
    filename

    /** @type {number} Strength of the LoRA */
    strength_model = 1

    /** @type {number} Strength of the LoRA on CLIP */
    strength_clip = 1
}

export function extractLoras(str) {
    // Define the regex pattern
    const pattern = /<lora:([a-zA-Z0-9_\-.]*):([0-9]*\.?[0-9]*)?:?([0-9]*\.?[0-9]*)?>/i;

    // Initialize an array to hold our findings
    const extractedLoras = [];

    // Use regex .exec method to keep extracting matches from the string
    let match;
    while (true) {
        match = pattern.exec(str)

        if (match !== null){
            // Push an object with the matched groups to the array
            // Also, check if strength_clip is provided, else default it to 1.0
            extractedLoras.push({
                filename: match[1],
                strength_model: match[2] ? parseFloat(match[2]) : 1.0,
                strength_clip: match[3] ? parseFloat(match[3]) : 1.0
            });
            str = str.replace(match[0], '')
        }else{
            break
        }
    }

    str = str.replaceAll('  ', ' ')
    str = str.replaceAll(', ,', ',')
    str = str.replaceAll(',,', ',')
    str = str.trim()

    // Return the array of lora objects
    return {extractedLoras, newPrompt: str};
}

export default class ComfyWorkflowService {
    /**
     *
     * @param {string} prompt
     * @param {string} negativePrompt
     * @param {number} seed
     * @param {?number} steps
     * @param {?number} cfg
     * @param {?number} width
     * @param {?number} height
     * @param {?string} model
     * @param {?string} sampler
     * @param {?string} scheduler
     * @param {?number} batchSize
     * @param {?LoRA[]} loras
     */
    static buildWorkflowWithLoras(prompt, negativePrompt, seed, steps = 64, cfg = 5, width = 640, height = 960, model = "realcartoonPixar_v6.safetensors", sampler = "euler_ancestral", scheduler = "karras", batchSize = 1, loras = []) {
        if (!loras || loras.length === 0) {
            return this.getImageWorkflow2(prompt, negativePrompt, seed, steps, cfg, width, height, model, sampler, scheduler, batchSize)
        }

        let lastNode = 4

        const baseWorkflow = {
            "3": {
                "inputs": {
                    "seed": seed,
                    "steps": steps,
                    "cfg": cfg,
                    "sampler_name": sampler,
                    "scheduler": scheduler,
                    "denoise": 1,
                    "model": ["11", 0],
                    "positive": ["6", 0],
                    "negative": ["7", 0],
                    "latent_image": ["5", 0]
                }, "class_type": "KSampler", "_meta": {
                    "title": "KSampler"
                }
            }, "4": {
                "inputs": {
                    "ckpt_name": `${model}`
                }, "class_type": "CheckpointLoaderSimple", "_meta": {
                    "title": "Load Checkpoint"
                }
            }, "5": {
                "inputs": {
                    "width": width, "height": height, "batch_size": batchSize
                }, "class_type": "EmptyLatentImage", "_meta": {
                    "title": "Empty Latent Image"
                }
            }, "6": {
                "inputs": {
                    "text": prompt, "clip": ["11", 1]
                }, "class_type": "CLIPTextEncode", "_meta": {
                    "title": "CLIP Text Encode (Prompt)"
                }
            }, "7": {
                "inputs": {
                    "text": negativePrompt, "clip": ["11", 1]
                }, "class_type": "CLIPTextEncode", "_meta": {
                    "title": "CLIP Text Encode (Prompt)"
                }
            }, "8": {
                "inputs": {
                    "samples": ["3", 0], "vae": ["4", 2]
                }, "class_type": "VAEDecode", "_meta": {
                    "title": "VAE Decode"
                }
            }, "9": {
                "inputs": {
                    "filename_prefix": "ComfyUI", "images": ["8", 0]
                }, "class_type": "SaveImage", "_meta": {
                    "title": "Save Image"
                }
            }
        }

        for (let i = 10; i < 10 + loras.length; i++) {
            baseWorkflow[i.toString()] = {
                "inputs": {
                    "lora_name": loras[i - 10].filename,
                    "strength_model": loras[i - 10].strength_model,
                    "strength_clip": loras[i - 10].strength_clip,
                    "model": [lastNode.toString(), 0],
                    "clip": [lastNode.toString(), 1]
                }, "class_type": "LoraLoader", "_meta": {
                    "title": "Load LoRA"
                }
            }
            lastNode = i
        }

        baseWorkflow["3"]["inputs"]["model"] = [lastNode.toString(), 0]
        baseWorkflow["6"]["inputs"]["clip"] = [lastNode.toString(), 1]
        baseWorkflow["7"]["inputs"]["clip"] = [lastNode.toString(), 1]
        return baseWorkflow
    }


    static getImageWorkflow2(prompt, negativePrompt, seed, steps = 64, cfg = 5, width = 640, height = 960, model = "realcartoonPixar_v6.safetensors", sampler = "euler_ancestral", scheduler = "karras", batchSize = 1) {
        return {
            "3": {
                "inputs": {
                    "seed": seed,
                    "steps": steps,
                    "cfg": cfg,
                    "sampler_name": sampler,
                    "scheduler": scheduler,
                    "denoise": 1,
                    "model": ["4", 0],
                    "positive": ["6", 0],
                    "negative": ["7", 0],
                    "latent_image": ["5", 0]
                }, "class_type": "KSampler"
            }, "4": {
                "inputs": {
                    "ckpt_name": `${model}`
                }, "class_type": "CheckpointLoaderSimple"
            }, "5": {
                "inputs": {
                    "width": width, "height": height, "batch_size": batchSize
                }, "class_type": "EmptyLatentImage"
            }, "6": {
                "inputs": {
                    "text": prompt, "clip": ["4", 1]
                }, "class_type": "CLIPTextEncode"
            }, "7": {
                "inputs": {
                    "text": negativePrompt, "clip": ["4", 1]
                }, "class_type": "CLIPTextEncode"
            }, "8": {
                "inputs": {
                    "samples": ["3", 0], "vae": ["4", 2]
                }, "class_type": "VAEDecode"
            }, "9": {
                "inputs": {
                    "filename_prefix": "ComfyUI", "images": ["8", 0]
                }, "class_type": "SaveImage"
            }, "10": {
                "inputs": {
                    "vae_name": "vae-ft-mse-840000-ema-pruned.safetensors"
                }, "class_type": "VAELoader"
            }
        }
    }

    static getAnimationWorkflow(prompt, negativePrompt, seed, steps = 24, cfg = 5, width = 512, height = 768, frames = 20, contextOverlap = 8, contextLength = 16, motionScale = 1, model = "analogMadness_v70.safetensors") {
        return {
            "1": {
                "inputs": {
                    "ckpt_name": model
                },
                "class_type": "CheckpointLoaderSimple",
                "_meta": {
                    "title": "Load Checkpoint"
                }
            },
            "2": {
                "inputs": {
                    "text": prompt,
                    "speak_and_recognation": true,
                    "clip": [
                        "1",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode",
                "_meta": {
                    "title": "CLIP Text Encode (Prompt)"
                }
            },
            "3": {
                "inputs": {
                    "text": negativePrompt,
                    "speak_and_recognation": true,
                    "clip": [
                        "1",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode",
                "_meta": {
                    "title": "CLIP Text Encode (Prompt)"
                }
            },
            "5": {
                "inputs": {
                    "model_name": "v3_sd15_mm.ckpt"
                },
                "class_type": "ADE_LoadAnimateDiffModel",
                "_meta": {
                    "title": "Load AnimateDiff Model 🎭🅐🅓②"
                }
            },
            "6": {
                "inputs": {
                    "start_percent": 0,
                    "end_percent": 1,
                    "motion_model": [
                        "5",
                        0
                    ]
                },
                "class_type": "ADE_ApplyAnimateDiffModel",
                "_meta": {
                    "title": "Apply AnimateDiff Model (Adv.) 🎭🅐🅓②"
                }
            },
            "7": {
                "inputs": {
                    "seed": seed,
                    "steps": 32,
                    "cfg": 5,
                    "sampler_name": "euler_ancestral",
                    "scheduler": "karras",
                    "denoise": 1,
                    "model": [
                        "8",
                        0
                    ],
                    "positive": [
                        "2",
                        0
                    ],
                    "negative": [
                        "3",
                        0
                    ],
                    "latent_image": [
                        "9",
                        0
                    ]
                },
                "class_type": "KSampler",
                "_meta": {
                    "title": "KSampler"
                }
            },
            "8": {
                "inputs": {
                    "beta_schedule": "autoselect",
                    "model": [
                        "1",
                        0
                    ],
                    "m_models": [
                        "6",
                        0
                    ],
                    "context_options": [
                        "13",
                        0
                    ],
                    "sample_settings": [
                        "16",
                        0
                    ]
                },
                "class_type": "ADE_UseEvolvedSampling",
                "_meta": {
                    "title": "Use Evolved Sampling 🎭🅐🅓②"
                }
            },
            "9": {
                "inputs": {
                    "width": 512,
                    "height": 515,
                    "batch_size": 16
                },
                "class_type": "ADE_EmptyLatentImageLarge",
                "_meta": {
                    "title": "Empty Latent Image (Big Batch) 🎭🅐🅓"
                }
            },
            "10": {
                "inputs": {
                    "samples": [
                        "7",
                        0
                    ],
                    "vae": [
                        "1",
                        2
                    ]
                },
                "class_type": "VAEDecode",
                "_meta": {
                    "title": "VAE Decode"
                }
            },
            "11": {
                "inputs": {
                    "ckpt_name": "rife49.pth",
                    "clear_cache_after_n_frames": 10,
                    "multiplier": 16,
                    "fast_mode": true,
                    "ensemble": true,
                    "scale_factor": 1,
                    "frames": [
                        "10",
                        0
                    ]
                },
                "class_type": "RIFE VFI",
                "_meta": {
                    "title": "RIFE VFI (recommend rife47 and rife49)"
                }
            },
            "12": {
                "inputs": {
                    "frame_rate": 64,
                    "loop_count": 0,
                    "filename_prefix": "AnimateDiff",
                    "format": "video/webm",
                    "pix_fmt": "yuv420p",
                    "crf": 20,
                    "save_metadata": false,
                    "pingpong": false,
                    "save_output": true,
                    "images": [
                        "11",
                        0
                    ]
                },
                "class_type": "VHS_VideoCombine",
                "_meta": {
                    "title": "Video Combine 🎥🅥🅗🅢"
                }
            },
            "13": {
                "inputs": {
                    "context_length": 16,
                    "context_stride": 1,
                    "context_overlap": 4,
                    "closed_loop": true,
                    "fuse_method": "pyramid",
                    "use_on_equal_length": true,
                    "start_percent": 0,
                    "guarantee_steps": 1
                },
                "class_type": "ADE_LoopedUniformContextOptions",
                "_meta": {
                    "title": "Context Options◆Looped Uniform 🎭🅐🅓"
                }
            },
            "14": {
                "inputs": {
                    "iterations": 1,
                    "filter": "gaussian",
                    "d_s": 0.25,
                    "d_t": 0.25,
                    "n_butterworth": 4,
                    "sigma_step": 999,
                    "apply_to_1st_iter": false,
                    "init_type": "FreeInit [sampler sigma]",
                    "iter_batch_offset": 0,
                    "iter_seed_offset": 1
                },
                "class_type": "ADE_IterationOptsFreeInit",
                "_meta": {
                    "title": "FreeInit Iteration Options 🎭🅐🅓"
                }
            },
            "16": {
                "inputs": {
                    "batch_offset": 0,
                    "noise_type": "default",
                    "seed_gen": "comfy",
                    "seed_offset": 0,
                    "adapt_denoise_steps": false,
                    "iteration_opts": [
                        "14",
                        0
                    ]
                },
                "class_type": "ADE_AnimateDiffSamplingSettings",
                "_meta": {
                    "title": "Sample Settings 🎭🅐🅓"
                }
            }
        }
    }

    static getImageWorkflow(prompt, negativePrompt, seed, steps = 64, cfg = 5, width = 640, height = 960, model = "realcartoonPixar_v6", sampler = "euler_ancestral", scheduler = "karras") {
        return {
            "3": {
                "inputs": {
                    "seed": seed,
                    "steps": steps,
                    "cfg": cfg,
                    "sampler_name": sampler,
                    "scheduler": scheduler,
                    "denoise": 1,
                    "model": [
                        "4",
                        0
                    ],
                    "positive": [
                        "6",
                        0
                    ],
                    "negative": [
                        "7",
                        0
                    ],
                    "latent_image": [
                        "5",
                        0
                    ]
                },
                "class_type": "KSampler"
            },
            "4": {
                "inputs": {
                    //"ckpt_name": "analogMadness_v60.safetensors"
                    //"ckpt_name": "analogMadness_v70.safetensors"
                    //"ckpt_name": "dreamshaperXL10_alpha2Xl10.safetensors"
                    //"ckpt_name": "aZovyaRPGArtistTools_v3.safetensors"
                    "ckpt_name": `${model}.safetensors`
                },
                "class_type": "CheckpointLoaderSimple"
            },
            "5": {
                "inputs": {
                    "width": width,
                    "height": height,
                    "batch_size": 1
                },
                "class_type": "EmptyLatentImage"
            },
            "6": {
                "inputs": {
                    "text": prompt,
                    "clip": [
                        "4",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode"
            },
            "7": {
                "inputs": {
                    "text": negativePrompt,
                    "clip": [
                        "4",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode"
            },
            "8": {
                "inputs": {
                    "samples": [
                        "3",
                        0
                    ],
                    "vae": [
                        "4",
                        2
                    ]
                },
                "class_type": "VAEDecode"
            },
            "9": {
                "inputs": {
                    "filename_prefix": "ComfyUI",
                    "images": [
                        "8",
                        0
                    ]
                },
                "class_type": "SaveImage"
            },
            "10": {
                "inputs": {
                    "vae_name": "vae-ft-mse-840000-ema-pruned.safetensors"
                },
                "class_type": "VAELoader"
            }
        }

        return {
            "3": {
                "inputs": {
                    "seed": seed,
                    "steps": steps,
                    "cfg": cfg,
                    "sampler_name": "euler_ancestral",
                    "scheduler": "karras",
                    "denoise": 1,
                    "model": [
                        "4",
                        0
                    ],
                    "positive": [
                        "6",
                        0
                    ],
                    "negative": [
                        "7",
                        0
                    ],
                    "latent_image": [
                        "5",
                        0
                    ]
                },
                "class_type": "KSampler"
            },
            "4": {
                "inputs": {
                    //"ckpt_name": "analogMadness_v60.safetensors"
                    //"ckpt_name": "analogMadness_v70.safetensors"
                    //"ckpt_name": "dreamshaperXL10_alpha2Xl10.safetensors"
                    //"ckpt_name": "aZovyaRPGArtistTools_v3.safetensors"
                    "ckpt_name": `${model}.safetensors`
                },
                "class_type": "CheckpointLoaderSimple"
            },
            "5": {
                "inputs": {
                    "width": width,
                    "height": height,
                    "batch_size": 1
                },
                "class_type": "EmptyLatentImage"
            },
            "6": {
                "inputs": {
                    "text": prompt,
                    "clip": [
                        "4",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode"
            },
            "7": {
                "inputs": {
                    "text": negativePrompt,
                    "clip": [
                        "4",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode"
            },
            "8": {
                "inputs": {
                    "samples": [
                        "3",
                        0
                    ],
                    "vae": [
                        "10",
                        0
                    ]
                },
                "class_type": "VAEDecode"
            },
            "9": {
                "inputs": {
                    "filename_prefix": "ComfyUI",
                    "images": [
                        "12",
                        0
                    ]
                },
                "class_type": "SaveImage"
            },
            "10": {
                "inputs": {
                    "vae_name": "vae-ft-mse-840000-ema-pruned.safetensors"
                },
                "class_type": "VAELoader"
            },
            "11": {
                "inputs": {
                    "model_name": "realesr-general-wdn-x4v3.pth"
                },
                "class_type": "UpscaleModelLoader"
            },
            "12": {
                "inputs": {
                    "upscale_model": [
                        "11",
                        0
                    ],
                    "image": [
                        "8",
                        0
                    ]
                },
                "class_type": "ImageUpscaleWithModel"
            }
        }
    }

    static getImageWorkflowSDXL(prompt, negativePrompt, seed, steps = 64, cfg = 5, width = 640, height = 960) {
        return {
            "3": {
                "inputs": {
                    "seed": seed,
                    "steps": steps,
                    "cfg": cfg,
                    "sampler_name": "euler_ancestral",
                    "scheduler": "karras",
                    "denoise": 1,
                    "model": [
                        "4",
                        0
                    ],
                    "positive": [
                        "6",
                        0
                    ],
                    "negative": [
                        "7",
                        0
                    ],
                    "latent_image": [
                        "5",
                        0
                    ]
                },
                "class_type": "KSampler"
            },
            "4": {
                "inputs": {
                    //"ckpt_name": "dreamshaperXL10_alpha2Xl10.safetensors"
                    "ckpt_name": "betterThanWords_v20.safetensors"
                },
                "class_type": "CheckpointLoaderSimple"
            },
            "5": {
                "inputs": {
                    "width": width,
                    "height": height,
                    "batch_size": 1
                },
                "class_type": "EmptyLatentImage"
            },
            "6": {
                "inputs": {
                    "text": prompt,
                    "clip": [
                        "4",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode"
            },
            "7": {
                "inputs": {
                    "text": negativePrompt,
                    "clip": [
                        "4",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode"
            },
            "8": {
                "inputs": {
                    "samples": [
                        "3",
                        0
                    ],
                    "vae": [
                        "4",
                        2
                    ]
                },
                "class_type": "VAEDecode"
            },
            "9": {
                "inputs": {
                    "filename_prefix": "ComfyUI",
                    "images": [
                        "8",
                        0
                    ]
                },
                "class_type": "SaveImage"
            }
        }

        return {
            "3": {
                "inputs": {
                    "seed": seed,
                    "steps": steps,
                    "cfg": cfg,
                    "sampler_name": "euler_ancestral",
                    "scheduler": "karras",
                    "denoise": 1,
                    "model": [
                        "4",
                        0
                    ],
                    "positive": [
                        "6",
                        0
                    ],
                    "negative": [
                        "7",
                        0
                    ],
                    "latent_image": [
                        "5",
                        0
                    ]
                },
                "class_type": "KSampler"
            },
            "4": {
                "inputs": {
                    //"ckpt_name": "dreamshaperXL10_alpha2Xl10.safetensors"
                    //"ckpt_name": "bluePencilXL_v100.safetensors"
                    "ckpt_name": "betterThanWords_v20.safetensors"
                },
                "class_type": "CheckpointLoaderSimple"
            },
            "5": {
                "inputs": {
                    "width": width,
                    "height": height,
                    "batch_size": 1
                },
                "class_type": "EmptyLatentImage"
            },
            "6": {
                "inputs": {
                    "text": prompt,
                    "clip": [
                        "4",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode"
            },
            "7": {
                "inputs": {
                    "text": negativePrompt,
                    "clip": [
                        "4",
                        1
                    ]
                },
                "class_type": "CLIPTextEncode"
            },
            "8": {
                "inputs": {
                    "samples": [
                        "3",
                        0
                    ],
                    "vae": [
                        "4",
                        2
                    ]
                },
                "class_type": "VAEDecode"
            },
            "9": {
                "inputs": {
                    "filename_prefix": "ComfyUI",
                    "images": [
                        "12",
                        0
                    ]
                },
                "class_type": "SaveImage"
            },
            "11": {
                "inputs": {
                    "model_name": "realesr-general-wdn-x4v3.pth"
                },
                "class_type": "UpscaleModelLoader"
            },
            "12": {
                "inputs": {
                    "upscale_model": [
                        "11",
                        0
                    ],
                    "image": [
                        "8",
                        0
                    ]
                },
                "class_type": "ImageUpscaleWithModel"
            }
        }
    }
}
