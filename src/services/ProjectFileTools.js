import fs from "node:fs";
import path from "node:path";

class FileUpload {
    /**
     * @constructor
     * @param {string} fileName - The path to the file
     * @param {string} sender - The sender of the file
     * @param {string} content - The file content
     */
    constructor(fileName, sender, content) {
        this.fileName = fileName
        this.sender = sender
        this.content = content
    }
}


const ProjectFileTools = {
    /**
     * Generates a tree-like string representation of a directory structure.
     * @param {string} directoryPath - The path to the directory.
     * @param {number} [depth=0] - The depth for indentation.
     * @param {string[]} [ignoredFiles=[]] - An array of file names to ignore.
     * @param {string[]} [ignoredFolders=[]] - An array of directory names to ignore.
     * @returns {string} The string representation of the directory structure.
     */
    generateProjectTree(directoryPath, depth = 0, ignoredFiles = [], ignoredFolders = []) {
        let structure = ''
        const filesAndFolders = fs.readdirSync(directoryPath)
        for (const item of filesAndFolders) {
            const fullPath = path.join(directoryPath, item)
            const stat = fs.statSync(fullPath)

            if (stat.isDirectory()) {
                if (!ignoredFolders.includes(item)) {
                    structure += `${' '.repeat(depth * 2)}${item}/\n`
                    structure += this.generateProjectTree(fullPath, depth + 1, ignoredFiles, ignoredFolders)
                }
            } else if (!ignoredFiles.includes(item)) {
                structure += `${' '.repeat(depth * 2)}${item}\n`
            }
        }

        return structure
    },

    /**
     * Recursively traverses a directory and generates an array of FileUpload objects containing file information.
     * @param {string} originalDirectoryPath - The path to the original directory.
     * @param {string[]} [ignoredFiles=[]] - An array of file names to ignore.
     * @param {string[]} [ignoredDirectories=[]] - An array of directory names to ignore.
     * @param {string} [directoryPath=originalDirPath] - The path to the current directory being traversed.
     * @param {FileUpload[]} [result=[]] - The array to store the FileUpload objects.
     * @returns {FileUpload[]} An array of FileUpload objects containing file information.
     */
    parseProjectIntoFileInfo(originalDirectoryPath, ignoredFiles = [], ignoredDirectories = [], directoryPath = originalDirectoryPath, result = []) {
        const entries = fs.readdirSync(directoryPath, {withFileTypes: true})

        for (const entry of entries) {
            const entryPath = path.join(directoryPath, entry.name)
            if (entry.isDirectory() && !ignoredDirectories.includes(entry.name)) {
                this.parseProjectIntoFileInfo(originalDirectoryPath, ignoredFiles, ignoredDirectories, entryPath, result)
            } else if (!ignoredDirectories.includes(entry.name) && !ignoredFiles.includes(entry.name)) {
                const content = fs.readFileSync(entryPath).toString().trim()
                result.push(new FileUpload(entryPath.replace(originalDirectoryPath, '.'), null, content))
            }
        }

        return result
    },

    /**
     * Generates the project files prompt based on the provided folder and configuration.
     * @param {string} sender - The name of the sender, defaults to 'System'.
     * @param {string} folderToShare
     * @param {string[]} ignoredFiles
     * @param {string[]} ignoredDirectories
     * @returns {string} The generated project files prompt.
     */
    async generateProjectFilesPrompt(sender = 'System', folderToShare, ignoredFiles = [], ignoredDirectories = []) {
        const projectFiles = this.parseProjectIntoFileInfo(folderToShare, ignoredFiles, ignoredDirectories)
        const projectTree = this.generateProjectTree(folderToShare, 0, ignoredFiles, ignoredDirectories)

        if (projectFiles.length > 0) {
            projectFiles.push({fileName: 'projectStructure.txt', sender, content: projectTree.trim()})
        }

        return projectFiles
            .map(pf => `[Attached file: ${pf.fileName}\nSent by: ${sender}\nContent:\n\`\`\`\n${pf.content}\n\`\`\`]`)
            .join('\n\n')
    }
}

export default ProjectFileTools
