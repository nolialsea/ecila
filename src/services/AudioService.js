import {spawn} from "child_process";
import ffmpeg from "fluent-ffmpeg";
import fs from "fs";

/**
 * This service class is responsible for playing audio.
 * It pipes the audio data through an audio player (vlc) and plays it.
 * Optionally, it can also write the audio data to an output file.
 */
export default class AudioService {
    /**
     * Plays the audio from the data in the response and optionally writes it to output file
     * @param {Object} response - The response object containing the audio data
     * @param {string|null} outputFilePath - The outputPath to write the audio data to (optional)
     * @returns {Promise<void>} - Does not resolve to a value
     */
    static async play(response, outputFilePath = null) {
        const audioPlayer = spawn('vlc', ['--no-video', '--intf', 'dummy', '-']);
        audioPlayer.on('error', (error) => {
            console.error(`Error spawning VLC: ${error}`);
        });
        const writer = outputFilePath ? fs.createWriteStream(outputFilePath) : null
        return new Promise((resolve, reject) => {
            ffmpeg(response.data)
                .format('mp3') // Make sure to set the correct audio format
                .on('data', (chunk) => {
                    if (writer) {
                        writer.write(chunk)
                    }
                })
                .on('end', () => {
                    if (writer) {
                        writer.end()
                    }
                    resolve()
                })
                .on('error', (err) => {
                    reject(err)
                })
                .pipe(audioPlayer.stdin)
        })
    }
}