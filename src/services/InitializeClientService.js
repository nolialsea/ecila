import fs from "fs";
import path from "path";
import {ChannelType, Events} from "discord.js";
import DiscordMessageService from "./DiscordMessageService.js";
import UserService from "./UserService.js";
import config from "../classes/Config.js";
import ModdingService from "./ModdingService.js";

export function loadModList() {
    let json

    // Try to load mods
    try {
        json = JSON.parse(fs.readFileSync('mods/modList.json').toString())
    } catch {
        console.warn(`Failed to load mod list from "mods/modList.json"`)
    }

    return json || []
        // Transform into a list of {name, enabled} mods
        .map(folder => {
            if (typeof folder === "string") {
                return {name: folder, enabled: true}
            } else if (typeof folder === "object") {
                if (folder.name) {
                    return {
                        name: folder.name,
                        enabled: folder.enabled || false
                    }
                } else {
                    throw new Error(`Couldn't load mod ${folder}`)
                }
            }
        })
        // Keep only existing mods
        .filter(mod => fs.lstatSync(`mods/${mod.name}`).isDirectory())
        // Remove base mod
        .filter(mod => mod.name !== "base")
}

/**
 * @param {Client} client
 * @param {BotConfig} bot
 * @param {Client[]} clients
 * @returns {Promise<void>}
 */
async function initClientCommands(client, bot, clients) {
    let commands = [];
    let commandFiles = fs.readdirSync('mods/base/commands').filter(file => file.endsWith('.js'));
    for (const file of commandFiles) {
        const command = await import(`../../mods/base/commands/${file}`);
        commands.push({name: path.parse(file).name, func: command.default});
    }

    let reactions = [];
    let reactionFiles = fs.readdirSync('mods/base/reactions').filter(file => file.endsWith('.js'));
    for (const file of reactionFiles) {
        const reaction = await import(`../../mods/base/reactions/${file}`);
        reactions.push({name: path.parse(file).name, func: reaction.default});
    }

    let interactions = [];
    let interactionFiles = fs.readdirSync('mods/base/interactions').filter(file => file.endsWith('.js'));
    for (const file of interactionFiles) {
        const reaction = await import(`../../mods/base/interactions/${file}`);
        interactions.push({name: path.parse(file).name, func: reaction.default});
    }

    let scripts = [];
    let scriptFiles = fs.readdirSync('mods/base/scripts').filter(file => file.endsWith('.js'));
    for (const file of scriptFiles) {
        const reaction = await import(`../../mods/base/scripts/${file}`);
        scripts.push({name: path.parse(file).name, func: reaction.default});
    }

    const modList = loadModList()

    for (const {name, enabled} of modList) {
        // Only load enabled mods
        if (!enabled) continue

        // Load user mod
        if (fs.existsSync(`mods/${name}/commands`)) {
            const userCommandFiles = fs.readdirSync(`mods/${name}/commands`)?.filter(file => file.endsWith('.js')) || []

            for (const file of userCommandFiles) {
                const fileName = file.replace(/.js/i, '')
                const userCommand = await import(`../../mods/${name}/commands/${file}`);

                // If this is a removal, remove the specified base commands
                if (userCommand.remove) {
                    commands = commands.filter(c => !userCommand.remove.includes(c.name));
                }

                // If this is a replacement, replace the specified base command
                if (commands.some(c => c.name === fileName)) {
                    const index = commands.findIndex(c => c.name === fileName);
                    commands[index] = {name: fileName, func: userCommand.default};
                } else {
                    // Otherwise, this is an addition
                    commands.push({name: fileName, func: userCommand.default});
                }
            }
        }

        if (fs.existsSync(`mods/${name}/reactions`)) {
            const userReactionFiles = fs.readdirSync(`mods/${name}/reactions`).filter(file => file.endsWith('.js')) || []

            // Repeat the above for reactions...
            for (const file of userReactionFiles) {
                const fileName = file.replace(/.js/i, '')
                const userReaction = await import(`../../mods/${name}/reactions/${file}`);

                // If this is a removal, remove the specified base reactions
                if (userReaction.remove) {
                    reactions = reactions.filter(c => !userReaction.remove.includes(c.name));
                }

                // If this is a replacement, replace the specified base reactions
                if (reactions.some(c => c.name === fileName)) {
                    const index = reactions.findIndex(c => c.name === fileName);
                    reactions[index] = {name: fileName, func: userReaction.default};
                } else {
                    // Otherwise, this is an addition
                    reactions.push({name: fileName, func: userReaction.default});
                }
            }
        }

        if (fs.existsSync(`mods/${name}/interactions`)) {
            const interactionFiles = fs.readdirSync(`mods/${name}/interactions`).filter(file => file.endsWith('.js')) || []

            // Repeat the above for interactions...
            for (const file of interactionFiles) {
                const fileName = file.replace(/.js/i, '')
                const userInteraction = await import(`../../mods/${name}/interactions/${file}`);

                // If this is a removal, remove the specified base interactions
                if (userInteraction.remove) {
                    interactions = interactions.filter(c => !userInteraction.remove.includes(c.name));
                }

                // If this is a replacement, replace the specified base interactions
                if (interactions.some(c => c.name === fileName)) {
                    const index = interactions.findIndex(c => c.name === fileName);
                    interactions[index] = {name: fileName, func: userInteraction.default};
                } else {
                    // Otherwise, this is an addition
                    interactions.push({name: fileName, func: userInteraction.default});
                }
            }
        }

        if (fs.existsSync(`mods/${name}/scripts`)) {
            const scriptFiles = fs.readdirSync(`mods/${name}/scripts`).filter(file => file.endsWith('.js')) || []

            for (const file of scriptFiles) {
                const fileName = file.replace(/.js/i, '')
                const userScript = await import(`../../mods/${name}/scripts/${file}`);

                if (userScript.remove) {
                    scripts = scripts.filter(c => !userScript.remove.includes(c.name));
                }

                if (scripts.some(c => c.name === fileName)) {
                    const index = scripts.findIndex(c => c.name === fileName);
                    scripts[index] = {name: fileName, func: userScript.default};
                } else {
                    scripts.push({name: fileName, func: userScript.default});
                }
            }
        }
    }

    function executeMiddleware(middleware, index, args) {
        if (index < middleware.length) {
            middleware[index].func(...args, () => executeMiddleware(middleware, index + 1, args));
        }
    }

    client.on(Events.MessageCreate, async (userMessage) => {
        if (userMessage.partial) {
            userMessage = await userMessage.fetch();
        }

        if (!DiscordMessageService.isCommand(userMessage)) return

        const isAdmin = UserService.isAdmin(userMessage)

        // Only enable DMs for authorized users if config.enableDirectMessages is true
        if (userMessage.channel.type === ChannelType.DM) {
            if (!config.enableDirectMessages) return console.log(`BLOCKED PRIVATE MESSAGE from ${userMessage.author.username}:\n  ${userMessage.cleanContent}`)
            if (!config.discordAdminIds[userMessage.author.id.toString()]) return console.log(`UNAUTHORIZED PRIVATE MESSAGE from ${userMessage.author.username}:\n  ${userMessage.cleanContent}`)
        }

        // If bot has channel whitelist
        if (bot.channelWhitelist && !bot.channelWhitelist.some(cw => userMessage.channel.name === cw || userMessage.channel.id === cw)) {
            return
        }

        // If bot has user whitelist
        if (bot.userWhitelist && !bot.userWhitelist.some(uw => userMessage.author.username === uw || userMessage.author.id === uw)) {
            return
        }

        if (userMessage.channel.type !== ChannelType.DM && ModdingService.isBotMasterBot(bot, userMessage) && !userMessage.author.bot) {
            // Create a Date object from the timestamp
            const date = new Date(Date.now());

            // Extract hours, minutes, and seconds
            let hours = date.getHours();
            let minutes = date.getMinutes();
            let seconds = date.getSeconds();

            // Pad single digit minutes and seconds with leading zeros
            if (hours < 10) hours = `0${hours}`;
            if (minutes < 10) minutes = `0${minutes}`;
            if (seconds < 10) seconds = `0${seconds}`;

            let channel = userMessage.channel
            if (channel.partial) {
                channel = await channel.fetch().catch(console.error)
            }
            let guild = userMessage.guild

            console.debug(`[${hours}:${minutes}:${seconds}] ${guild.name} #${channel.name}, User ${userMessage.author.username} (${userMessage.author.id}) said: ${userMessage.cleanContent}`)
        }

        // Run command middleware
        executeMiddleware(commands, 0, [userMessage, bot, isAdmin, client, clients]);
    });

    client.on(Events.InteractionCreate, async (interaction) => {
        // Only enable DMs for authorized users if config.enableDirectMessages is true
        if (interaction.channel.type === ChannelType.DM) {
            if (!config.enableDirectMessages) return
            if (!config.discordAdminIds[interaction.user.id.toString()]) return
        }

        // Run command middleware
        executeMiddleware(interactions, 0, [interaction, bot, client, clients]);
    });

    client.on(Events.MessageReactionAdd, async (userReaction, user) => {
        if (userReaction.partial) {
            userReaction = await userReaction.fetch();
        }

        if (user.partial) {
            user = await user.fetch();
        }

        // prevent bots from using their own emoji triggers
        if (client.user.id.toString() === user.id.toString()) return
        if (client.application.id.toString() === user.id.toString()) return
        if (client.user.id === user.id) return
        if (client.application.id === user.id) return


        // only execute if bot is message author
        if (client.user.id.toString() !== userReaction?.message?.author?.id.toString()) return

        // only execute if channel is allowed
        const channelName = userReaction.message?.channel?.name
        const isDirectMessage = userReaction.message.channel.type === ChannelType.DM
            && config.enableDirectMessages && config.discordAdminIds[userReaction.message.author.id.toString()]

        if (userReaction.message.channel.type === ChannelType.DM && !isDirectMessage) return

        // If bot has channel whitelist
        if (bot.channelWhitelist && !bot.channelWhitelist.some(cw => userMessage.channel.name === cw || userMessage.channel.id === cw)) {
            return
        }

        // If bot has user whitelist
        if (bot.userWhitelist && !bot.userWhitelist.some(uw => userMessage.author.username === uw || userMessage.author.id === uw)) {
            return
        }

        const isAllowedChannel = bot.monoBotChannels?.some(c => c === channelName) || bot.multiBotChannels?.some(c => c === channelName)
            || isDirectMessage

        const channelTopic = userReaction?.message?.channel?.topic || null

        // Run reaction middleware
        executeMiddleware(reactions, 0, [userReaction, user, bot, channelTopic, client, clients]);
    });

    client.on(Events.ClientReady, async () => {

    })
}

export default {
    initClientCommands
}
