import AIService from "./AiService.js";

export default class ModerationService {
    static async getModerationStatus(messages, isAdmin) {
        const moderationData = await AIService.getModerationData(messages)
        const moderationResult = AIService.prepareModerationData(moderationData)
        if (moderationResult?.flagged && !isAdmin) {
            const newMessage =
                `# ${messages.length} visible messages have been loaded\n` +
                `# **Oops, looks like the prompt is flagged! I can't generate messages safely, I don't want OpenAI to be mad at me!**\n` +
                `# Here is the moderation result for those messages:\n\n` +
                `**Flagged**: **${moderationResult.flagged}**\n\nGranular values:\n\`\`\`diff\n${Object.entries(moderationResult.category_scores).map(v => `${v[1]}`).join('\n')}\n\`\`\``
            throw new Error(newMessage)
        }
        return {contextFlaggedForModeration: moderationResult?.flagged}
    }
}