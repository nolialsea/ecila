// fileDownloader.js
import axios from 'axios'
import fs from 'fs'

export default class FileDownloaderService {
    static async downloadFile(url, outputFilename) {
        try {
            // Make a GET request to fetch the raw content
            const response = await axios({
                method: 'GET',
                url: url,
                responseType: 'stream',
            })

            // Create a writable stream for saving file
            const writer = fs.createWriteStream(outputFilename)

            // Pipe the readable stream into the writable stream
            response.data.pipe(writer)

            return new Promise((resolve, reject) => {
                writer.on('finish', resolve)
                writer.on('error', reject)
            })
        } catch (error) {
            console.error(`Error downloading file: ${error}`)
            throw error
        }
    }
}