import config from "../classes/Config.js";
import axios from "axios";
import {Buffer} from "buffer";
import BotDataService from "./BotDataService.js";
import Utils from "../Utils.js";
import fs from "fs";
import {Readable} from "stream";
import AudioService from "./AudioService.js";
import ComfyWorkflowService, {extractLoras} from "./ComfyWorkflowService.js";
import ProjectFileTools from "./ProjectFileTools.js";
import GPTAPIService from "./GPTAPIService.js";
import OpenAIService from "./OpenAIService.js";

/**
 * @returns {string} URL of the API with the last / trimmed off
 */
function getApiUrl() {
    return config.ECILA_API_URL?.endsWith('/') ? config.ECILA_API_URL.substring(0, config.ECILA_API_URL.length - 1) : config.ECILA_API_URL
}

class AIService {
    /**
     * Generates a bot message using ChatGPT
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {null|number} maxTokens - (Optional) Maximum number of tokens to generate.
     * @param {import('discord.js').TextChannel} channel - (Optional) The Discord text channel where the message was sent.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessageChatGPT2(bot, messages, maxTokens = null, channel = null) {
        const context = typeof bot?.context === 'string' ? bot.context : typeof bot?.context === 'object' ? JSON.parse(JSON.stringify(bot.context)) : ''
        const data = {
            access_token: config.ECILAToken,
            oai_token: config.openAIToken,
            bot: {
                name: bot.name,
                context: context,
                data: BotDataService.getData(bot, channel)
            },
            history: messages
        }

        if (bot.textGenParameters?.OpenAI && bot.textGenParameters.OpenAI?.enabled) {
            data.parameters = JSON.parse(JSON.stringify(bot.textGenParameters.OpenAI))
        }

        if (data && data.parameters?.["enabled"]) {
            delete data.parameters["enabled"]
        }

        if (data && data.parameters?.["mainBackend"]) {
            delete data.parameters["mainBackend"]
        }

        /*if (maxTokens) {
            if (!data.parameters) {
                data.parameters = {}
            }
            data.parameters.max_tokens = maxTokens
        }*/

        let errorCounter = 6
        try {
            return await Utils.retryFunction(async () => {
                let responseMessage = await axios({
                    method: 'post',
                    url: getApiUrl() + '/api/v1/generate/chatbotMessageOpenAI',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify(data)
                });
                console.log('Visible Message Count: ', responseMessage?.data?.visibleMessageCount)
                return responseMessage?.data?.messages[0]?.content
            }, [], errorCounter--)
        } catch (err) {
            console.log(err)
            if (err?.response?.data?.message?.startsWith('The response was filtered due to the prompt triggering Azure')) {
                console.log('Filtered by Azure, LOL!')
            }
            return null
        }
    }

    /**
     * Generates a bot message using ChatGPT
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {null|number} maxTokens - (Optional) Maximum number of tokens to generate.
     * @param {import('discord.js').TextChannel} channel - (Optional) The Discord text channel where the message was sent.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessageChatGPT(bot, messages, maxTokens = null, channel = null) {
        const context = typeof bot?.context === 'string'
            ? bot.context
            : typeof bot?.context === 'object'
                ? JSON.parse(JSON.stringify(bot.context))
                : '';

        const botData = {
            name: bot.name,
            context: context,
            data: {
                ...BotDataService.getData(bot, channel),
                ...bot.constants,
            },
        };

        const history = messages;

        let OAIParameters = {};

        if (bot.textGenParameters?.OpenAI && bot.textGenParameters.OpenAI?.enabled) {
            OAIParameters = {...bot.textGenParameters.OpenAI};
            delete OAIParameters.enabled;
            delete OAIParameters.mainBackend;
        }

        let errorCounter = 6;
        try {
            return await Utils.retryFunction(async () => {
                const preparedMessages = OpenAIService.prepareMessages(botData, history, OAIParameters);
                return await OpenAIService.runGenerationChatGPT(
                    preparedMessages,
                    bot.textGenParameters.OpenAI.key ?? config.openAIToken,
                    OAIParameters
                );
            }, [], errorCounter--);
        } catch (error) {
            console.error(JSON.stringify(error, null, 2));
            if (error?.response?.data?.message?.startsWith('The response was filtered due to the prompt triggering Azure')) {
                console.log('Filtered by Azure, action required.');
            }
            return null;
        }
    }

    /**
     * Generates a bot message using Claude
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {null|number} maxTokens - (Optional) Maximum number of tokens to generate.
     * @param {import('discord.js').TextChannel} channel - (Optional) The Discord text channel where the message was sent.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessageClaude(bot, messages, maxTokens = null, channel = null) {
        const context = typeof bot?.context === 'string' ? bot.context : typeof bot?.context === 'object' ? JSON.parse(JSON.stringify(bot.context)) : ''
        const data = {
            access_token: config.ECILAToken,
            oai_token: config.openAIToken,
            bot: {
                name: bot.name,
                context: context,
                data: BotDataService.getData(bot, channel)
            },
            history: messages
        }

        if (bot.textGenParameters?.Claude && bot.textGenParameters.Claude?.enabled) {
            data.parameters = JSON.parse(JSON.stringify(bot.textGenParameters.Claude))
            if (data.parameters["enabled"]) {
                delete data.parameters["enabled"]
            }
        }

        let endpoint = ''
        if (data?.parameters?.endpoint) {
            endpoint = data?.parameters?.endpoint
            delete data.parameters["endpoint"]
        }

        if (data && data.parameters?.["mainBackend"]) {
            delete data.parameters["mainBackend"]
        }

        return await Utils.retryFunction(async () => {
            const responseMessage = await axios({
                method: 'post',
                url: getApiUrl() + `/api/v1/generate/chatbotMessageClaude${endpoint}`,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            }).catch(error => console.error(error?.response?.data ? error?.response?.data : error))
            console.log('Visible Message Count: ', responseMessage?.data?.visibleMessageCount)

            return (responseMessage?.data?.messages[0]?.content
                //?.replace?.(/(\r\n|\r|\n)+/g, '$1') // removes empty lines
                || responseMessage?.data?.messages[0]?.content
                || '')?.replace?.(/\nH: (.*)/g, '')?.replace?.(/\nA: (.*)/g, '')
        }, [], 6)
    }

    /**
     * Generates a bot message using ChatGPT (raw response)
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {null|number} maxTokens - (Optional) Maximum number of tokens to generate.
     * @param {import('discord.js').TextChannel} channel - (Optional) The Discord text channel where the message was sent.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessageChatGPTRaw(bot, messages, maxTokens = null, channel = null) {
        const context = typeof bot?.context === 'string' ? bot.context : typeof bot?.context === 'object' ? JSON.parse(JSON.stringify(bot.context)) : ''
        const data = {
            access_token: config.ECILAToken,
            oai_token: config.openAIToken,
            bot: {
                name: bot.name,
                context: context,
                data: BotDataService.getData(bot, channel)
            },
            history: messages
        }
        if (bot.textGenParameters?.OpenAI && bot.textGenParameters.OpenAI?.enabled) {
            data.parameters = bot.textGenParameters.OpenAI
            delete data.parameters["enabled"]
        }

        if (maxTokens) {
            if (!data.parameters) {
                data.parameters = {}
            }
            data.parameters.max_tokens = maxTokens
        }

        if (data && data.parameters?.["mainBackend"]) {
            delete data.parameters["mainBackend"]
        }

        return await Utils.retryFunction(async () => {
            const responseMessage = await axios({
                method: 'post',
                url: getApiUrl() + '/api/v1/generate/chatbotMessageOpenAIRaw',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            }).catch(console.error)
            return responseMessage?.data?.messages[0]?.content
                    ?.replace?.(/(\r\n|\r|\n)+/g, '$1') // removes empty lines
                || responseMessage?.data?.messages[0]?.content
        }, [])
    }

    /**
     * Generates a message using the generator configured in the bot
     * @param {string|string[]} input - The input text for the generator.
     * @param {?any} generator - The generator configuration object.
     * @param data_ - (Optional) Additional data to be used by the generator.
     * @param {BotConfig} bot - The bot configuration object.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generatorChatGPT(input, generator, data_ = {}, bot = {}) {
        const context = typeof generator?.context === 'string' ? generator.context : typeof generator?.context === 'object' ? JSON.parse(JSON.stringify(generator.context)) : ''
        if (context.top) {
            generator.context = context
        }

        if (!generator.parameters) {
            generator.parameters = {}
        }

        if (bot?.textGenParameters?.OpenAI?.URL && bot?.textGenParameters?.OpenAI?.key) {
            generator.parameters.URL = bot?.textGenParameters?.OpenAI?.URL
            generator.parameters.key = bot?.textGenParameters?.OpenAI?.key
            generator.parameters.max_tokens = 2048
        }

        const data = {
            access_token: config.ECILAToken,
            oai_token: config.openAIToken,
            input,
            data: data_,
            context: generator.context,
            examples: generator.examples,
            parameters: generator.parameters
        }

        return await Utils.retryFunction(async () => {
            const responseMessage = await axios({
                method: 'post',
                url: getApiUrl() + '/api/v1/generate/generatorOpenAI',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            }).catch(console.error)
            return responseMessage?.data?.messages[0]?.content
                ?.replace?.(/(\r\n|\r|\n)+/g, '$1') // removes empty lines
        }, [])
    }

    /**
     * Generates a message using the NovelAI generator
     * @param {string} input - The input text for the generator.
     * @param {?any} generator - The generator configuration object.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generatorNovelAI(input, generator) {
        const data = {
            access_token: config.ECILAToken,
            input,
            generator,
        }

        return await Utils.retryFunction(async () => {
            const responseMessage = await axios({
                method: 'post',
                url: getApiUrl() + '/api/v1/generate/generatorNovelAI',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            }).catch(console.error)
            return responseMessage?.data?.result?.object
        }, [])
    }

    /**
     * Generates a bot message using NovelAI
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {boolean} continueLastMessage - Flag indicating whether to continue the last message or generate a new one.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessageNovelAI(bot, messages, continueLastMessage = false) {
        const data = {
            access_token: config.ECILAToken,
            bot: {
                name: bot.name,
                context: bot.context || ''
            },
            continue_last_message: continueLastMessage,
            history: messages
        }
        if (bot.textGenParameters?.NovelAI && bot.textGenParameters.NovelAI?.enabled) {
            data.parameters = bot.textGenParameters.NovelAI
            delete data.parameters["enabled"]
        }

        return await Utils.retryFunction(async () => {
            const responseMessage = await axios({
                method: 'post',
                url: getApiUrl() + '/api/v1/generate/chatbotMessage',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            }).catch(console.error)
            return responseMessage?.data?.messages[0]?.content
                ?.replace?.(/(\r\n|\r|\n)+/g, '$1') // removes empty lines
        }, [])
    }

    /**
     * Generates a bot message using KoboldAI
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {boolean} continueLastMessage - Flag indicating whether to continue the last message or generate a new one.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessageKoboldAI(bot, messages, continueLastMessage = false) {
        const data = {
            access_token: config.ECILAToken,
            bot: {
                name: bot.name,
                context: bot.context || ''
            },
            continue_last_message: continueLastMessage,
            history: messages
        }
        if (bot.textGenParameters?.NovelAI && bot.textGenParameters.NovelAI?.enabled) {
            data.parameters = bot.textGenParameters.NovelAI
            delete data.parameters["enabled"]
        }

        return await Utils.retryFunction(async () => {
            const responseMessage = await axios({
                method: 'post',
                url: getApiUrl() + '/api/v1/generate/chatbotMessageKoboldAI',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            }).catch(console.error)
            return responseMessage?.data?.messages[0]?.content
                ?.replace?.(/(\r\n|\r|\n)+/g, '$1') // removes empty lines
        }, [])
    }

    /**
     * Generates a bot message using HF (HuggingFace)
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {boolean} continueLastMessage - Flag indicating whether to continue the last message or generate a new one.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessageHF(bot, messages, continueLastMessage = false) {
        const data = {
            access_token: config.ECILAToken,
            bot: {
                name: bot.name,
                context: bot.context || ''
            },
            continue_last_message: continueLastMessage,
            history: messages
        }

        if (bot.textGenParameters?.NovelAI && bot.textGenParameters.NovelAI?.enabled) {
            // data.parameters = bot.textGenParameters.NovelAI
            // delete data.parameters["enabled"]
        }

        return await Utils.retryFunction(async () => {
            const responseMessage = await axios({
                method: 'post',
                url: getApiUrl() + '/api/v1/generate/chatbotMessageHF',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            }).catch(console.error)
            return responseMessage?.data?.messages[0]?.content
                ?.replace?.(/(\r\n|\r|\n)+/g, '$1') // removes empty lines
        }, [])
    }

    /**
     * Generates a bot message using Aphrodite API
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessageAphrodite(bot, messages) {
        const data = {
            access_token: config.ECILAToken,
            backendType: "Aphrodite",
            bot: {
                name: bot.name,
                context: bot.context || ''
            },
            history: messages
        }

        data.parameters = bot.textGenParameters.Aphrodite

        return await Utils.retryFunction(async () => {
            const responseMessage = await axios({
                method: 'post',
                url: getApiUrl() + '/api/v1/generate/chatbotMessageSelfHosted',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            }).catch(console.error)
            return responseMessage?.data?.messages[0]?.content
                ?.replace?.(/(\r\n|\r|\n)+/g, '$1') // removes empty lines
        }, [])
    }


    /**
     * Generates a bot message using the configured backend
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {?string} topic - (Optional) The topic of the conversation.
     * @param {null|number} maxTokens - (Optional) Maximum number of tokens to generate.
     * @param {boolean} continueLastMessage - Flag indicating whether to continue the last message or generate a new one.
     * @param {import('discord.js').TextMessage} channel - (Optional) The Discord text channel where the message was sent.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessage(bot, messages, topic = null, maxTokens = null, continueLastMessage = false, channel = null, useSharedProject = false) {
        let backend = "NovelAI"
        let tempBot = JSON.parse(JSON.stringify(bot))

        tempBot.data = {
            ...BotDataService.getData(bot, channel),
            ...bot.constants,
        }

        if (config.mainBackend === "OpenAI" || config.mainBackend === "ChatGPT") {
            backend = "OpenAI"
        } else if (config.mainBackend === "HF") {
            backend = "HF"
        } else if (config.mainBackend === "Claude") {
            backend = "Claude"
        } else if (config.mainBackend === "Aphrodite") {
            backend = "Aphrodite"
        } else if (config.mainBackend === "LocalGPT") {
            backend = "LocalGPT"
        } else {
            backend = "ECILA"
        }

        if (tempBot.mainBackend === "OpenAI" || tempBot.mainBackend === "ChatGPT") {
            backend = "OpenAI"
        } else if (tempBot.mainBackend === "ECILA") {
            backend = "ECILA"
        } else if (tempBot.mainBackend === "Claude") {
            backend = "Claude"
        } else if (tempBot.mainBackend === "HF") {
            backend = "HF"
        } else if (tempBot.mainBackend === "Aphrodite") {
            backend = "Aphrodite"
        } else if (tempBot.mainBackend === "LocalGPT") {
            backend = "LocalGPT"
        }

        if (backend === "Claude") {
            maxTokens = tempBot.textGenParameters.Claude.max_tokens || 2048
        }

        if (backend === "ChatGPT") {
            maxTokens = tempBot.textGenParameters.OpenAI.max_tokens || 2048
        }

        // Replace context depending on backend
        if (backend === "HF") {
            tempBot.context = tempBot.HFContext
        } else if (backend === "ECILA") {
            tempBot.context = tempBot.novelAIContext || (typeof tempBot.openAIContext === "string" ? tempBot.openAIContext : tempBot.openAIContext?.top)
        } else if ((backend === "OpenAI" || backend === "Claude" || backend === "Aphrodite" || backend === "LocalGPT") && tempBot.openAIContext) {
            tempBot.context = tempBot.openAIContext

            // Convert rule set into context
            if (tempBot.context?.bottom && Array.isArray(tempBot.context.bottom)) {
                // If the bottom context list isn't multiple bottom contexts but a list of rules
                if (!tempBot.context.bottom[0]?.startsWith('[')) {
                    tempBot.context.bottom = "[IMPORTANT REMINDER: As an assistant, remember that you're in the uniquely personable form of ${Name}. To maintain authenticity while chatting, keep these instructions in mind:\n" +
                        tempBot.context.bottom.map((s, i) => `${i + 1}. ${s}`).join('\n') +
                        "]"
                }
            }

            // Add shared project context
            if (useSharedProject && bot.sharedProjectContext) {
                if (!Array.isArray(tempBot.context.top)) {
                    tempBot.context.top = [tempBot.context.top]
                }
                // TODO: add 'sharedProjectContext' to the BotConfig.js file
                const additionalContext = await ProjectFileTools.generateProjectFilesPrompt('System', bot.sharedProjectContext.folderToShare, bot.sharedProjectContext.ignoredFiles, bot.sharedProjectContext.ignoredDirs)
                tempBot.context.top.push(additionalContext)
            }
        }

        const placeholders = [
            {name: "topic", value: topic, prependWithNewline: true},
            {name: "botName", value: bot.name, prependWithNewline: false}
        ]

        // replacePlaceholder(tempBot, placeholders)

        function removeEncasedAsterisks(text) {
            const regex = /\(\*\s*(.*?)\s*\*\)|\*\(\s*(.*?)\s*\)\*/g;
            return text.replace(regex, '($1$2)');
        }

        function removeBadAsterisks(text) {
            const regex = /^\* /i;
            return text.replace(regex, '*');
        }

        let result = Utils.limitConsecutiveEmojis(
            backend === "OpenAI" && !continueLastMessage ?
                await this.generateMessageChatGPT(tempBot, messages, maxTokens, channel)
                : backend === "Claude" ?
                    await this.generateMessageClaude(tempBot, messages, maxTokens, channel)
                    : backend === "HF" ?
                        await this.generateMessageHF(tempBot, messages, continueLastMessage)
                        : backend === "Aphrodite" ?
                            await this.generateMessageAphrodite(tempBot, messages)
                            : backend === "LocalGPT" ?
                                await GPTAPIService.generate(tempBot, messages, tempBot.context.top, tempBot.context.bottom, bot.textGenParameters.OpenAI)
                                : await this.generateMessageNovelAI(tempBot, messages, continueLastMessage)
        )

        if (typeof result === 'string') {
            result = removeEncasedAsterisks(result)
            result = removeBadAsterisks(result)
        }

        return result
    }

    /**
     * Generates a bot message using the configured backend (raw response)
     * @param {BotConfig} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {?string} topic - (Optional) The topic of the conversation.
     * @param {null|number} maxTokens - (Optional) Maximum number of tokens to generate.
     * @param {boolean} continueLastMessage - Flag indicating whether to continue the last message or generate a new one.
     * @param {import('discord.js').TextMessage} channel - (Optional) The Discord text channel where the message was sent.
     * @returns {Promise<string|null>} The generated message or null if generation failed.
     */
    static async generateMessageOAIRaw(bot, messages, topic = null, maxTokens = null, continueLastMessage = false, channel = null) {
        let tempBot = JSON.parse(JSON.stringify(bot))

        tempBot.data = BotDataService.getData(bot, channel)

        if (tempBot.openAIContext) {
            tempBot.context = tempBot.openAIContext
        }

        return Utils.limitConsecutiveEmojis(
            await this.generateMessageChatGPTRaw(tempBot, messages, maxTokens, channel)
        )
    }

    /**
     * Generates a prediction of the next message's author name
     * @param {{name: string, context: string}} bot - The bot configuration object.
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be used as context.
     * @param {?string} topic - (Optional) The topic of the conversation.
     * @returns {Promise<string[]|null>} An array of predicted author names or null if prediction failed.
     */
    static async generateNextAuthorPrediction(bot, messages, topic = null) {
        const data = {
            access_token: config.ECILAToken,
            result_count: 3,
            bot: {
                name: bot.name,
                context: `${bot.context ? bot.context : ''}${bot.context && topic ? '\n' : ''}${topic ? `${topic}` : ''}`
            },
            history: messages
        }
        if (bot.textGenParameters?.NovelAI && bot.textGenParameters.NovelAI?.enabled) {
            data.parameters = bot.textGenParameters.NovelAI
            delete data.parameters["enabled"]
        }
        return await Utils.retryFunction(async () => {
            const responseMessage = await axios({
                method: 'post',
                url: getApiUrl() + '/api/v1/generate/smartAnswer',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(data)
            }).catch(console.error)

            return responseMessage?.data?.results
        }, [])
    }

    /**
     * Gets the moderation data for a set of messages
     * @param {Array<{author: ?string, content: string}>} messages - An array of messages to be moderated.
     * @param {boolean} useRealBackend - Flag indicating whether to use the real backend for moderation or not.
     * @returns {Promise<{flagged: boolean, categories: Array<string>, category_scores: Array<string>}>} The moderation data object.
     */
    static async getModerationData(messages, useRealBackend = false) {
        if (!useRealBackend) {
            return {
                flagged: false, categories: [], category_scores: []
            }
        } else {
            const input = messages
                ?.map(h => {
                    const contentAuthor = h.content?.match(/^\(as ([^)]*)\)/i)?.[1]?.trim?.()
                    const content = `${h.content?.replace(/^\(as [^)]*\)/i, '').trim()}` || ''
                    const startsAndEndsWithBrackets = content.startsWith('[') && content.endsWith(']')
                    const author = contentAuthor ? `${contentAuthor}: `
                        : !h.author ? ''
                            : `${h.author.trim()}: `
                    return `${startsAndEndsWithBrackets ? '' : author}${content}`
                })
                .join('\n')

            return await Utils.retryFunction(async () => {
                const result = await axios({
                    method: 'post',
                    url: getApiUrl() + '/api/v1/generate/moderation',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        access_token: config.ECILAToken,
                        input: input
                    })
                }).catch(console.error)

                return result?.data
            }, [])
        }
    }

    /**
     * Prepares the moderation data for display
     * @param {{flagged: boolean, categories: Array<string>, category_scores: Array<string>}} moderationResults - The moderation data object.
     * @returns {{flagged: boolean, categories: Array<string>, category_scores: Array<string>}|null} The prepared moderation data object or null if input is falsy.
     */
    static prepareModerationData(moderationResults) {
        if (!moderationResults) return null
        const fractionDigits = 3
        const categoriesToFix = ["sexual", "hate", "violence", "self-harm", "sexual/minors", "hate/threatening", "violence/graphic"]
        for (const c of categoriesToFix) {
            if (!moderationResults) break
            if (moderationResults.category_scores[c]) {
                const value = Math.round(moderationResults.category_scores[c] * 20)
                const X = 'X'.repeat(value)
                const _ = '_'.repeat(20 - value)
                moderationResults.category_scores[c] = `${moderationResults.categories[c] ? '-' : '+'} [${X}${_}] ${c}: ` + (moderationResults.category_scores[c] * 100).toFixed(fractionDigits) + '%'
            }
        }
        return moderationResults
    }

    /**
     * Generates an image using NovelAI
     * @param {string} prompt - The prompt for the image generation.
     * @param {?any} opts - (Optional) Additional options for image generation.
     * @returns {Promise<null|Buffer>} The generated image as a Buffer or null if generation failed.
     */
    static async generateNaiImage(prompt, opts = null) {
        try {
            const parameters = {
                seed: opts?.seed || Math.floor(Math.random() * 1000000),
                width: opts?.width || 1024,
                height: opts?.height || 1024,
                steps: opts?.steps || 28,
                scale: opts?.scale || 5,
                prompt,
                negative_prompt: opts?.negativePrompt || '',
                override_negative_prompt: true,
                model: opts?.model || 'nai-diffusion-3',
                legacy: false,
                legacy_v3_extend: false,
                cfg_rescale: 0,
                noise_schedule: 'native',
                sampler: 'k_euler_ancestral',
                ucPreset: 1,
                sm: false,
                sm_dyn: false,
                uncond_scale: 1,
                params_version: 1
            }

            if (parameters.width * parameters.height > 1024 * 1024) throw new Error("Image too big!")

            return await Utils.retryFunction(async () => {
                const response = await axios.post(
                    getApiUrl() + '/api/v1/generate/textToImageNovelAI',
                    {
                        access_token: config.ECILAToken,
                        prompt,
                        width: parameters.width,
                        height: parameters.height,
                        parameters,
                    },
                    {
                        responseType: 'arraybuffer',
                        timeout: 1000 * 60,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })

                return await response.data
            }, [])
        } catch (e) {
            console.error("Error!", e)
        }
    }

    /**
     * Generates an image using Stable Diffusion
     * @param {string} prompt - The prompt for the image generation.
     * @param {?any} opts - (Optional) Additional options for image generation.
     * @returns {Promise<null|Buffer>} The generated image as a Buffer or null if generation failed.
     */
    static async generateSDImage(prompt, opts = {
        steps: 20,
        scale: 7,
        seed: null,
        width: 512,
        height: 512,
        negativePrompt: '',
        disableHighresFix: false,
        overrideNegativePrompt: false
    }) {
        try {
            const obj = {
                seed: opts.seed || Math.floor(Math.random() * 1000000),
                width: opts.width || 512,
                height: opts.height || 512,
                steps: opts.steps || 20,
                scale: opts.scale || 7,
                prompt,
                disableHighresFix: opts.disableHighresFix ?? true,
                uc: opts.negativePrompt || '',
                overrideNegativePrompt: opts.overrideNegativePrompt || false
            }
            if (obj.width * obj.height > 2048 * 2048) throw new Error("Image too big!")

            const width = Math.min(2048, opts.width || 512)
            const height = Math.min(2048, opts.height || 512)

            if (width === 512 && height === 512) {
                obj.disableHighresFix = true
            }

            return await Utils.retryFunction(async () => {
                const response = await axios.post(
                    `${config.stableDiffusionBackendURL || "http://127.0.0.1:7860/"}sdapi/v1/txt2img`,
                    {
                        prompt,
                        width,
                        height,
                        cfg_scale: obj.scale,
                        sampler_name: null,
                        denoising_strength: (width !== 512 || height !== 512) ? 0.7 : 0,
                        "steps": Math.min(24, opts.steps || 20),
                        "seed": obj.seed,
                        restore_faces: true,
                        enable_hr: (obj.disableHighresFix ? false : (width !== 512 || height !== 512)),
                        "negative_prompt": (obj.overrideNegativePrompt ? ''
                                : "lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry")
                            + (obj.uc ? (obj.overrideNegativePrompt ? '' : ', ') + obj.uc : '')
                    },
                    {
                        timeout: 1000 * 60 * 3,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })

                const data = response?.data?.images?.[0]
                return Buffer
                    .from(data, 'base64')
            }, [])
        } catch (e) {
            console.error("Error!", e)
        }
    }

    /**
     * Generates an image using ComfyUI
     * @param {string} prompt - The prompt for the image generation.
     * @param {?string} URL - (Optional) The URL of the ComfyUI backend.
     * @param {?any} opts - (Optional) Additional options for image generation.
     * @returns {Promise<null|Buffer>} The generated image as a Buffer or null if generation failed.
     */
    static async generateComfyUIImage(prompt, URL = "http://localhost:8188/", opts = {
        steps: 24,
        scale: 5,
        seed: null,
        width: 512,
        height: 512,
        negativePrompt: '',
        disableHighresFix: false,
        overrideNegativePrompt: false,
        sampler: "euler_ancestral",
        scheduler: "karras"
    }) {
        try {
            const obj = {
                seed: opts.seed || Math.floor(Math.random() * 1000000),
                width: opts.width || 512,
                height: opts.height || 512,
                steps: opts.steps || 24,
                scale: opts.scale || 5,
                sampler: opts.sampler || "euler_ancestral",
                scheduler: opts.scheduler || "karras",
                prompt,
                disableHighresFix: opts.disableHighresFix ?? true,
                uc: opts.negativePrompt || '',
                overrideNegativePrompt: opts.overrideNegativePrompt || false
            }
            if (obj.width * obj.height > 2048 * 2048) throw new Error("Image too big!")

            const width = Math.min(2048, opts.width || 512)
            const height = Math.min(2048, opts.height || 512)
            const negativePrompt = (obj.overrideNegativePrompt ? ''
                    : "lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry")
                + (obj.uc ? (obj.overrideNegativePrompt ? '' : ', ') + obj.uc : '')

            if (width === 512 && height === 512) {
                obj.disableHighresFix = true
            }

            return await Utils.retryFunction(async () => {
                const steps = obj.steps
                const cfg = obj.cfg
                const width = obj.width
                const height = obj.height
                const sampler = obj.sampler
                const scheduler = obj.scheduler

                const {extractedLoras, newPrompt} = extractLoras(prompt)
                const workflow = ComfyWorkflowService.buildWorkflowWithLoras(newPrompt, negativePrompt, Math.floor(Math.random() * 9999999), steps, cfg, width, height, opts.model ?? undefined, sampler, scheduler, 1, extractedLoras)

                const response = await axios.post(
                    `${URL}prompt`,
                    {
                        prompt: workflow// ComfyWorkflowService.getImageWorkflow(prompt, negativePrompt, Math.floor(Math.random() * 9999999), steps, cfg, width, height, opts.model ?? undefined, sampler, scheduler)
                    },
                    {
                        timeout: 1000 * 60 * 10,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })

                const promptId = response.data?.prompt_id

                let r = null

                async function getRequest(promptId) {
                    return new Promise((resolve, reject) => {
                        axios.get(
                            `${URL}history/${promptId}`
                        )
                            .then(resp => resolve(resp.data))
                            .catch(reason => reject(reason))
                    })
                }

                const startTimestamp = Date.now()
                while (promptId) {
                    const result = await getRequest(promptId)
                    if (result && Object.keys(result).length > 0) {
                        r = result
                        break
                    } else {
                        //console.log(`Processing image... (${((Date.now() - startTimestamp) / 1000).toFixed(2)}seconds)`)
                        await new Promise(r => setTimeout(r, 2000))
                    }
                }

                const image_filenames = r?.[promptId]?.['outputs']?.['9']?.['images']

                async function fetchFileAndConvertToBuffer(endpointURL) {
                    try {
                        // Fetch file from the API
                        const response = await axios.get(endpointURL, {
                            responseType: 'arraybuffer' // Tell axios to return data as ArrayBuffer
                        });

                        // Convert the ArrayBuffer to base64
                        const base64Data = Buffer.from(response.data, 'binary').toString('base64');

                        // Convert base64 to Buffer
                        return Buffer.from(base64Data, 'base64');
                    } catch (error) {
                        console.error('Error fetching and converting file:', error);
                        throw error;
                    }
                }


                let data = null
                if (image_filenames) {
                    for (const image_filename of image_filenames) {
                        const filename = image_filename['filename']
                        data = await fetchFileAndConvertToBuffer(`${URL}view?filename=${filename}`)
                    }
                }

                if (!data) return console.log(`ERROR: no image data was found.`)

                return Buffer
                    .from(data, 'base64')
            }, [])
        } catch (e) {
            console.error("Error!", e)
        }
    }


    /**
     * Generates an animation using ComfyUI
     * @param {string} prompt - The prompt for the animation generation.
     * @param {?string} URL - (Optional) The URL of the ComfyUI backend.
     * @param {?any} opts - (Optional) Additional options for animation generation.
     * @returns {Promise<null|Buffer>} The generated animation as a Buffer or null if generation failed.
     */
    static async generateComfyUIAnimation(prompt, URL = "http://localhost:8188/", opts = {
        steps: 20,
        scale: 7,
        seed: null,
        width: 512,
        height: 512,
        negativePrompt: '',
        disableHighresFix: false,
        overrideNegativePrompt: false
    }) {
        try {
            const obj = {
                seed: opts.seed || Math.floor(Math.random() * 1000000),
                width: opts.width || 512,
                height: opts.height || 512,
                steps: opts.steps || 20,
                scale: opts.scale || 7,
                prompt,
                disableHighresFix: opts.disableHighresFix ?? true,
                uc: opts.negativePrompt || '',
                overrideNegativePrompt: opts.overrideNegativePrompt || false
            }
            if (obj.width * obj.height > 2048 * 2048) throw new Error("Image too big!")

            const width = Math.min(2048, opts.width || 512)
            const height = Math.min(2048, opts.height || 512)
            const negativePrompt = (obj.overrideNegativePrompt ? ''
                    : "lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry")
                + (obj.uc ? (obj.overrideNegativePrompt ? '' : ', ') + obj.uc : '')

            if (width === 512 && height === 512) {
                obj.disableHighresFix = true
            }

            return await Utils.retryFunction(async () => {
                const steps = 24
                const cfg = 5
                const width = obj.width
                const height = obj.height
                const frames = 24
                const contextLength = 16
                const contextOverlap = 4
                const motionScale = 1

                const response = await axios.post(
                    `${URL}prompt`,
                    {
                        prompt: ComfyWorkflowService.getAnimationWorkflow(prompt, negativePrompt, Math.floor(Math.random() * 9999999), steps, cfg, width, height, frames, contextOverlap, contextLength, motionScale, opts.model)
                    },
                    {
                        timeout: 1000 * 60 * 10,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })

                if (response.status === 400) {
                    console.log(`ERROR`, JSON.stringify(response.data, null, 4))
                }

                const promptId = response.data?.prompt_id

                let r = null

                async function getRequest(promptId) {
                    return new Promise((resolve, reject) => {
                        axios.get(
                            `${URL}history/${promptId}`
                        )
                            .then(resp => resolve(resp.data))
                            .catch(reason => reject(reason))
                    })
                }

                const startTimestamp = Date.now()
                while (promptId) {
                    const result = await getRequest(promptId)
                    if (result && Object.keys(result).length > 0) {
                        r = result
                        break
                    } else {
                        //console.log(`Processing image... (${((Date.now() - startTimestamp) / 1000).toFixed(2)}seconds)`)
                        await new Promise(r => setTimeout(r, 2000))
                    }
                }

                console.log(r?.[promptId]?.['outputs']?.['12'])
                const image_filenames = r?.[promptId]?.['outputs']?.['12']?.['gifs']

                async function fetchFileAndConvertToBuffer(endpointURL) {
                    try {
                        // Fetch file from the API
                        const response = await axios.get(endpointURL, {
                            responseType: 'arraybuffer' // Tell axios to return data as ArrayBuffer
                        });

                        // Convert the ArrayBuffer to base64
                        const base64Data = Buffer.from(response.data, 'binary').toString('base64');

                        // Convert base64 to Buffer
                        return Buffer.from(base64Data, 'base64');
                    } catch (error) {
                        console.error('Error fetching and converting file:', error);
                        throw error;
                    }
                }


                let data = null
                if (image_filenames) {
                    for (const image_filename of image_filenames) {
                        const filename = image_filename['filename']
                        data = await fetchFileAndConvertToBuffer(`${URL}view?filename=${filename}`)
                    }
                }

                if (!data) return null

                return Buffer
                    .from(data, 'base64')
            }, [])
        } catch (e) {
            console.error("Error!", e)
        }
    }

    /**
     * Fetches an audio stream from ElevenLabs for text-to-speech
     * @param {string} apiKey - The ElevenLabs API key.
     * @param {string} text - The text to be converted to speech.
     * @param {string} voiceID - The ID of the voice to use.
     * @param {number} [stability=1] - The stability level of the voice.
     * @param {number} [similarity_boost=1] - The similarity boost level.
     * @param {number} [style=0] - The style of the voice.
     * @param {boolean} [use_speaker_boost=true] - Whether to use speaker boost or not.
     * @param {string} [modelID="eleven_multilingual_v2"] - The ID of the model to use.
     * @returns {Promise<any>} The audio stream from ElevenLabs.
     */
    static async get11LabsStream(apiKey, text, voiceID, stability = 1, similarity_boost = 1, style = 0, use_speaker_boost = true, modelID = "eleven_multilingual_v2") {
        return await axios
            .post(`https://api.elevenlabs.io/v1/text-to-speech/${voiceID}/stream`,
                {
                    text: text,
                    model_id: modelID,
                    voice_settings: {
                        stability: stability,
                        similarity_boost: similarity_boost,
                        style: style,
                        use_speaker_boost: use_speaker_boost
                    }
                },
                {
                    timeout: 60000,
                    responseType: 'stream',
                    headers: {
                        'Content-Type': 'application/json',
                        'accept': 'audio/mpeg',
                        'xi-api-key': apiKey
                    }
                })
            .catch(e => console.error(e));
    }

    /**
     * Plays an audio stream from ElevenLabs using the configured voice settings
     * @param {string} apiKey - The ElevenLabs API key.
     * @param {string} text - The text to be converted to speech.
     * @param {string} voiceID - The ID of the voice to use.
     * @param {number} [stability=1] - The stability level of the voice.
     * @param {number} [similarity_boost=1] - The similarity boost level.
     * @param {number} [style=0] - The style of the voice.
     * @param {boolean} [use_speaker_boost=true] - Whether to use speaker boost or not.
     * @param {string} [modelID="eleven_multilingual_v2"] - The ID of the model to use.
     * @returns {Promise<void>}
     */
    static async playWithElevenLabs(apiKey, text, voiceID, stability = 1, similarity_boost = 1, style = 0, use_speaker_boost = true, modelID = "eleven_multilingual_v2") {
        const response = await AIService.get11LabsStream(apiKey, text, voiceID, stability, similarity_boost, style, use_speaker_boost, modelID)
        await AudioService.play(response)
    }

    /**
     * Downloads an audio file from ElevenLabs using the configured voice settings
     * @param {string} outputFilePath - The output file path for the downloaded audio file.
     * @param {string} apiKey - The ElevenLabs API key.
     * @param {string} text - The text to be converted to speech.
     * @param {string} voiceID - The ID of the voice to use.
     * @param {number} [stability=1] - The stability level of the voice.
     * @param {number} [similarity_boost=1] - The similarity boost level.
     * @param {number} [style=0] - The style of the voice.
     * @param {boolean} [use_speaker_boost=true] - Whether to use speaker boost or not.
     * @param {string} [modelID="eleven_multilingual_v2"] - The ID of the model to use.
     * @returns {Promise<void>}
     */
    static async downloadFrom11Labs(outputFilePath, apiKey, text, voiceID, stability = 1, similarity_boost = 1, style = 0, use_speaker_boost = true, modelID = "eleven_multilingual_v2") {
        const response = await AIService.get11LabsStream(apiKey, text, voiceID, stability, similarity_boost, style, use_speaker_boost, modelID)

        try {
            // Create a writable stream for saving file
            const writer = fs.createWriteStream(outputFilePath)

            const readable = Readable.from(response.data);

            readable.on('data', (chunk) => {
                writer.write(chunk)
            })
            readable.on('error', (err) => {
                console.error(err)
            })
            readable.on('end', () => {
                writer.end()
            })

            return new Promise((resolve, reject) => {
                writer.on('finish', resolve)
                writer.on('error', reject)
            })
        } catch (err) {
            console.error(err)
        }
    }
}

export default AIService
