import config from "../classes/Config.js";
import DiscordImageService from "./DiscordImageService.js";
import FunctionCallingService from "./FunctionCallingService.js";
import AIService from "./AiService.js";
import DiscordTypingService from "./DiscordTypingService.js";
import axios from "axios";

let sharp;
try {
    sharp = await import('sharp');
} catch (error) {
    console.warn("Sharp library could not be loaded. Image processing will be limited.", error);
}

/**
 * Utility function to sanitize and retrieve the API URL.
 * @returns {string} URL of the API with the trailing slash removed.
 */
function getApiUrl() {
    return config.ECILA_API_URL?.replace(/\/+$/, '') || '';
}

const IMAGE_EXTENSIONS = ['.png', '.jpg', '.jpeg', '.webp', '.gif'];
const TEXT_FILE_EXTENSIONS = ['.txt', '.json', '.md', '.html', '.css', '.js', '.mjs', '.ts', '.py', '.ahk', '.srt'];
const PREFIXES_TO_IGNORE = ['!', '#', '/', '. '];

export default class DiscordService {
    static botsGeneratingMessageInChannel = {};

    /**
     * Replace the author's name with an alias if available.
     * @param {string} authorName
     * @returns {string}
     */
    static replaceAlias(authorName) {
        return config.aliases?.[authorName] || authorName;
    }

    /**
     * Fetches messages from a Discord channel with pagination.
     * @param {import('discord.js').TextChannel} channel
     * @param {number} messageCount
     * @returns {Promise<Array>}
     */
    static async fetchMessages(channel, messageCount = 200) {
        const result = [];
        let lastMessageID = null;

        try {
            while (result.length < messageCount) {
                const fetchOptions = lastMessageID
                    ? { limit: 100, before: lastMessageID }
                    : { limit: 100 };
                const fetched = await channel.messages.fetch(fetchOptions);
                if (fetched.size === 0) break;
                const messagesArray = Array.from(fetched);
                result.push(...messagesArray);
                lastMessageID = messagesArray[messagesArray.length - 1][0];
            }

            // Ensure cache is updated for messages beyond 200 if needed
            if (messageCount > 200) {
                await channel.messages.fetch({ limit: 100 });
            }
        } catch (error) {
            console.error("Error fetching messages:", error);
            return [];
        }

        return result.slice(0, messageCount);
    }

    /**
     * Retrieves and processes messages from a channel.
     * @param {import('discord.js').TextChannel} channel
     * @param {?import('discord.js').Message} lastMessage
     * @param {BotConfig} bot
     * @returns {Promise<Array<{author: string, content: string}>>}
     */
    static async getChannelMessages(channel, lastMessage = null, bot) {
        try {
            let pulledMessages = await this.fetchMessages(channel);
            if (lastMessage) {
                const index = pulledMessages.findIndex(m => m[0] === lastMessage.id.toString());
                if (index !== -1) {
                    pulledMessages = pulledMessages.slice(index + 1);
                }
            }

            let resetMessageFound = false;
            const allMessages = [];

            for (const [, message] of pulledMessages) {
                const content = message.cleanContent?.toLowerCase();
                if (content === '!reset' || content === `!reset ${bot.name.toLowerCase()}`) {
                    resetMessageFound = true;
                    continue;
                }
                if (resetMessageFound) continue;

                // Ignore messages starting with specific prefixes
                if (PREFIXES_TO_IGNORE.some(prefix => content?.startsWith(prefix))) continue;

                // Process attachments
                const processedMessages = await this.processAttachments(message);
                allMessages.push(...processedMessages, message);
            }

            // Process images and other content
            const processedContent = await Promise.all(
                allMessages
                    .filter(m => m.cleanContent || (m.attachments && m.attachments.size > 0))
                    .reverse()
                    .map(m => this.processMessage(m, bot))
            );

            return processedContent.filter(m => m.content || (m.imageUrls && m.imageUrls.length > 0));
        } catch (error) {
            console.error("Error processing channel messages:", error);
            return [];
        }
    }

    /**
     * Processes attachments of a message, converting supported file types into message content.
     * @param {import('discord.js').Message} message
     * @returns {Promise<Array<{author: string, content: string}>>}
     */
    static async processAttachments(message) {
        const processed = [];

        if (!message.attachments || message.attachments.size === 0) return processed;

        for (const attachment of message.attachments.values()) {
            const { name, url } = attachment;
            const extension = name.substring(name.lastIndexOf('.')).toLowerCase();

            if (TEXT_FILE_EXTENSIONS.includes(extension)) {
                try {
                    const response = await axios.get(url);
                    const content = response.data;
                    const filename = name.replace(/\?[^]*/g, '');
                    const sentBy = this.replaceAlias(message.member?.displayName || message.author?.username) || 'Guest';
                    const formattedContent = `\nSent by: ${sentBy}\nContent:\n\`\`\`\n${typeof content === 'object' ? JSON.stringify(content, null, 2) : content}\n\`\`\`\n`;
                    processed.push({
                        author: this.replaceAlias(message.member?.displayName || message.author?.username || 'Guest'),
                        content: `[Attached file: ${filename}${formattedContent}]`,
                    });
                } catch (error) {
                    console.error(`Failed to fetch attachment ${url}:`, error);
                }
            }
        }

        return processed;
    }

    /**
     * Processes a single message, handling content and image URLs.
     * @param {import('discord.js').Message} message
     * @param {BotConfig} bot
     * @returns {Promise<{author: string, content: string, imageUrls: Array<string>, useHighRes: boolean}>}
     */
    static async processMessage(message, bot) {
        const isAuthorBot = message.author?.bot;
        const author = this.replaceAlias(message.member?.displayName || message.author?.username || 'Guest');
        const content = message.cleanContent.replace(/<\|([^|]+)\|>/g, (_, p1) => `<${p1.trim()}>`);

        const imageUrls = message.attachments
            ?.filter(a => IMAGE_EXTENSIONS.some(ext => a.name.toLowerCase().endsWith(ext)))
            .map(a => a.url.replace(/\?[^]*/g, '')) || [];

        const shouldUseImages = !isAuthorBot && this.isVisionCompatible(bot);
        let useHighRes = false;

        if (imageUrls.length > 0) {
            useHighRes = true;
        }

        let processedImageUrls = imageUrls;

        if (shouldUseImages && useHighRes) {
            processedImageUrls = await this.handleImageUrls(imageUrls, bot);
        }

        return {
            author,
            content,
            imageUrls: processedImageUrls,
            useHighRes,
        };
    }

    /**
     * Determines if the bot supports vision capabilities.
     * @param {BotConfig} bot
     * @returns {boolean}
     */
    static isVisionCompatible(bot) {
        const isChatGPT = bot.mainBackend === "ChatGPT" && (
            bot.textGenParameters?.OpenAI?.model?.startsWith('gpt-4-vision') ||
            bot.textGenParameters?.OpenAI?.model?.startsWith('gpt-4o')
        );
        const isClaude = bot.mainBackend === "Claude" && (
            bot.textGenParameters?.Claude?.model?.startsWith('claude-3')
        );
        return isChatGPT || isClaude;
    }

    /**
     * Handles image URLs by converting them to Base64 if required.
     * @param {Array<string>} imageUrls
     * @param {BotConfig} bot
     * @returns {Promise<Array<string>>}
     */
    static async handleImageUrls(imageUrls, bot) {
        const isProxy = (bot.mainBackend === "ChatGPT" && bot.textGenParameters?.OpenAI?.URL) ||
            (bot.mainBackend === "Claude" && bot.textGenParameters?.Claude?.URL);

        if (isProxy) {
            return Promise.all(imageUrls.map(async (url) => {
                try {
                    const response = await axios.get(url, { responseType: 'arraybuffer' });
                    const contentType = response.headers['content-type'] || 'image/png';
                    return `data:${contentType};base64,${Buffer.from(response.data).toString('base64')}`;
                } catch (error) {
                    console.error(`Failed to convert image ${url} to Base64:`, error);
                    return null;
                }
            })).then(urls => urls.filter(url => url !== null));
        }

        return imageUrls;
    }

    /**
     * Generates and sends a message based on user input and bot configuration.
     * @param {import('discord.js').Message} userMessage
     * @param {BotConfig} bot
     * @param {?string} channelTopic
     * @param {?Array<{author: string, content: string}>} messages_
     * @param {?object} functions
     * @param {?object} oldOAIParameters
     * @param {boolean} useSharedFolder
     * @returns {Promise<void>}
     */
    static async generateAndSendMessage(userMessage, bot, channelTopic = null, messages_ = null, functions = null, oldOAIParameters = null, useSharedFolder = false) {
        const channelId = userMessage.channelId;
        const botName = bot.name;

        if (this.botsGeneratingMessageInChannel[channelId]?.[botName]) {
            // await userMessage?.react('⌛').catch(() => null);
            // console.log(`Bot ${botName} is already generating a message in channel ${userMessage.channel?.name}.`);
        }

        this.botsGeneratingMessageInChannel[channelId] = {
            ...this.botsGeneratingMessageInChannel[channelId],
            [botName]: true,
        };

        try {
            const messages = messages_ || await this.getChannelMessages(userMessage.channel, null, bot);
            await DiscordTypingService.sendTyping(userMessage);

            let response;
            if (bot.textGenParameters?.OpenAI?.functions?.length > 0) {
                response = await FunctionCallingService.functionCall(
                    userMessage, bot, channelTopic, messages,
                    bot.textGenParameters.OpenAI.functions,
                    bot.textGenParameters.OpenAI.function_call || "auto",
                    functions
                );
            } else {
                response = await AIService.generateMessage(bot, messages, channelTopic, null, false, userMessage.channel, useSharedFolder);
            }

            if (oldOAIParameters) {
                bot.textGenParameters.OpenAI = oldOAIParameters;
            }

            if (response) {
                if (typeof response === 'string') {
                    await DiscordImageService.generateImagesAndSendMessage(userMessage, response, bot);
                } else {
                    console.warn("Unexpected response type:", typeof response);
                }
            } else {
                const errorMsg = bot.debug
                    ? "# Oh no, variable `functionCallOrMessage` was null! :headstone:"
                    : null;
                await this.sendCommandError(userMessage, errorMsg);
            }
        } catch (error) {
            console.error("Error generating and sending message:", error);
            await this.sendCommandError(userMessage);
        } finally {
            delete this.botsGeneratingMessageInChannel[channelId][botName];
        }
    }

    /**
     * Sends an error reaction and optionally a message reply.
     * @param {import('discord.js').Message} userMessage
     * @param {?string} errorMessage
     * @returns {Promise<void>}
     */
    static async sendCommandError(userMessage, errorMessage = null) {
        await userMessage?.react('🐞').catch(() => null);
        if (errorMessage) {
            await userMessage?.reply(errorMessage).catch(console.error);
        }
    }

    /**
     * Sends a confirmation reaction to the user's message.
     * @param {import('discord.js').Message} userMessage
     * @returns {Promise<void>}
     */
    static async sendCommandConfirmation(userMessage) {
        await userMessage?.react('✅').catch(() => null);
    }
}
