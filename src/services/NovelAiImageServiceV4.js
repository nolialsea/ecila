/**
 * NovelAiImageServiceV4.js
 *
 * A self-contained service for generating images using NovelAI's "v4-curated-preview" model.
 *
 * @file NovelAiImageServiceV4.js
 * @author Ecila
 * @version 1.0.0
 * @description
 *   1) Log in with your NovelAI keys, store bearer tokens.
 *   2) Generate images using the new v4 parameters and endpoint fields.
 *
 * @example
 * import NovelAiImageServiceV4 from "./NovelAiImageServiceV4.js"
 *
 * // Load NAI credentials once at startup:
 * await NovelAiImageServiceV4.loadNovelAiKeys()
 *
 * // Then generate an image with separated user prompt + "frontend auto" extras
 * const imageBuffer = await NovelAiImageServiceV4.generateNaiV4Image({
 *   userPrompt: "A cat wearing a top hat by a lake",
 *   frontEndPrompt: "rating:general, amazing quality, very aesthetic, absurdres"
 * })
 * // now do something with imageBuffer
 */

import axios from "axios"
import AdmZip from "adm-zip"
import config from "../classes/Config.js"

let tokenCounter = 0
let novelAIBearerTokens = []

/**
 * @private
 * @param {string} accessKey - NovelAI user login key
 * @returns {Promise<string>} The bearer token
 */
async function getNovelAIBearerToken(accessKey) {
    const endpoint = "https://api.novelai.net/user/login"
    const response = await axios.post(endpoint, { key: accessKey }, {
        headers: { "Content-Type": "application/json" },
        timeout: 1000 * 30
    })
    return response.data.accessToken
}

/**
 * @private
 * Unzips the NovelAI response buffer (zip) into a PNG buffer
 * @param {Buffer} file - zipped buffer from NovelAI
 * @returns {Promise<Buffer>} the extracted PNG image
 */
function unzip(file) {
    return new Promise((resolve, reject) => {
        try {
            const zip = new AdmZip(file)
            const zipEntries = zip.getEntries()
            const content = zipEntries[0].getData() // we expect a single PNG
            resolve(content)
        } catch (err) {
            reject(`Unzip error: ${err}`)
        }
    })
}

/**
 * @private
 * Retries an async function multiple times with a specified delay
 * @param {Function} fn - the function to call
 * @param {Array} args - arguments for fn
 * @param {number} retries - how many times to attempt
 * @param {number} waitMs - how many ms to wait between attempts
 */
async function retryFunction(fn, args = [], retries = 3, waitMs = 1000) {
    let lastError
    for (let i = 0; i < retries; i++) {
        try {
            return await fn(...args)
        } catch (err) {
            lastError = err
            await new Promise(r => setTimeout(r, waitMs))
        }
    }
    throw lastError
}

class NovelAiImageServiceV4 {
    /**
     * Logs into NovelAI for each key in config.novelAiAccounts, storing the tokens.
     * @returns {Promise<void>}
     */
    static async loadNovelAiKeys() {
        console.info("*Ecila hums* Pre-loading NovelAI v4 tokens...")

        if (!config.novelAiAccounts || config.novelAiAccounts.length === 0) {
            console.error("loadNovelAiKeys: No novelAiAccounts found in config.")
            return
        }

        const keys = config.novelAiAccounts.map(acc => acc.key)
        for (const key of keys) {
            try {
                const bearer = await getNovelAIBearerToken(key)
                if (!novelAIBearerTokens.includes(bearer)) {
                    novelAIBearerTokens.push(bearer)
                    console.debug("NovelAiImageServiceV4: new bearer token stored!")
                }
            } catch (e) {
                console.error("loadNovelAiKeys: error generating token:", e)
            }
        }
    }

    /**
     * Generate an image using the v4 curated model
     *
     * @param {Object} opts - Generation options
     * @param {string} opts.userPrompt - The user's custom prompt (e.g. "Cat with a hat")
     * @param {string} [opts.frontEndPrompt="rating:general, amazing quality, very aesthetic, absurdres"]
     *    Additional stuff automatically appended (like the front-end does).
     * @param {string} [opts.model="nai-diffusion-4-curated-preview"] - which v4 model
     * @param {number} [opts.width=832]
     * @param {number} [opts.height=1216]
     * @param {number} [opts.scale=6]
     * @param {number} [opts.steps=23]
     * @param {number} [opts.seed=Math.floor(Math.random()*1e6)]
     * @param {string} [opts.negativePrompt="blurry, lowres, error, film grain, scan artifacts, worst quality, bad quality, jpeg artifacts, very displeasing, chromatic aberration, logo, dated, signature, multiple views"]
     * @returns {Promise<Buffer|null>}
     *  PNG buffer or null if request fails
     *
     * @example
     * const imgBuffer = await NovelAiImageServiceV4.generateNaiV4Image({
     *   userPrompt: "Dragon battling a knight",
     *   frontEndPrompt: "rating:general, amazing quality, dynamic lighting, absurdres",
     *   steps: 28,
     *   scale: 7
     * })
     * // do something with imgBuffer
     */
    static async generateNaiV4Image(opts = {}) {
        // If your frontend lumps everything into one big prompt,
        // you can handle them distinctly like so:
        const userPrompt = opts.userPrompt?.trim?.() || "Test"
        // Something your front-end automatically appends
        const frontEndPrompt = opts.frontEndPrompt?.trim?.()
            || "rating:general, amazing quality, very aesthetic, absurdres"

        // Join them for the final prompt
        // Of course, you can do more sophisticated joining logic if you want
        const combinedPrompt = `${userPrompt}, ${frontEndPrompt}`

        const model = opts.model || "nai-diffusion-4-curated-preview"
        const finalSeed = (typeof opts.seed === "number") ? opts.seed : Math.floor(Math.random() * 1e6)

        const width = opts.width || 832
        const height = opts.height || 1216
        // This is the new “v4 curated approach,” right?
        // We can clamp them if you want or do an if-check that it doesn’t exceed certain bounds.
        if (width * height > 1216 * 1216) {
            throw new Error("Image dimension too big for NovelAI v4 approach")
        }

        // Default negative prompt from your JSON snippet
        const defaultNegative = "blurry, lowres, error, film grain, scan artifacts, worst quality, bad quality, jpeg artifacts, very displeasing, chromatic aberration, logo, dated, signature, multiple views, gigantic breasts"
        const negativePrompt = opts.negativePrompt ?? defaultNegative

        // Build the final JSON payload the same way your sample JSON does
        const requestBody = {
            input: combinedPrompt,
            model,
            action: "generate",
            parameters: {
                params_version: 3,
                width,
                height,
                scale: opts.scale ?? 6,
                sampler: "k_euler_ancestral",
                steps: opts.steps ?? 23,
                seed: finalSeed,
                n_samples: 1,
                ucPreset: 0,
                qualityToggle: true,
                dynamic_thresholding: false,
                controlnet_strength: 1,
                legacy: false,
                add_original_image: true,
                cfg_rescale: 0,
                noise_schedule: "karras",
                legacy_v3_extend: false,
                use_coords: false,
                characterPrompts: [],
                reference_image_multiple: [],
                reference_information_extracted_multiple: [],
                reference_strength_multiple: [],
                deliberate_euler_ancestral_bug: false,
                prefer_brownian: true,

                // Actually used in the final generation
                negative_prompt: negativePrompt,

                // v4 style prompts
                v4_prompt: {
                    caption: {
                        base_caption: combinedPrompt,
                        char_captions: [] // or you can fill these if you have "characterPrompts"
                    },
                    use_coords: false,
                    use_order: true
                },
                v4_negative_prompt: {
                    caption: {
                        base_caption: negativePrompt,
                        char_captions: []
                    }
                }
            }
        }

        console.log(requestBody)

        try {
            const response = await retryFunction(async () => {
                return axios.post(
                    "https://image.novelai.net/ai/generate-image",
                    requestBody,
                    {
                        responseType: "arraybuffer",
                        timeout: 1000 * 60,
                        headers: {
                            "Content-Type": "application/json",
                            Authorization:
                                "Bearer " +
                                novelAIBearerTokens[tokenCounter++ % novelAIBearerTokens.length]
                        }
                    }
                )
            }, [], 3, 5000) // up to 3 tries, 5s in between

            if (!response?.data) {
                console.error("generateNaiV4Image: No data returned from NovelAI.")
                return null
            }
            return await unzip(response.data)
        } catch (err) {
            console.error("NovelAiImageServiceV4 error generating image:", err)
            return null
        }
    }
}

NovelAiImageServiceV4.loadNovelAiKeys()

export default NovelAiImageServiceV4

/*
   Easter Egg:
     "I see you, you code-peeking wizard.
      Next time, bring donuts. We can chat about prompt engineering over them."
*/
