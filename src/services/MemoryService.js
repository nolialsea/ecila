export default class MemoryService {

    static definition_create_or_edit_memory_block = {
        "name": "create_or_edit_memory_block",
        "description": "Creates or edit a memory block by name, using 4 relevant tags minimum for classification, and creating the data object containing key/value pairs describing the memory block (a memory block and its data object should represent a whole person, or location, item, event, etc. Make sure to always write properties in the data object. Always prefer updating a larger object instead of creating a new smaller one)",
        "parameters": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string",
                    "description": "Name of the memory block (name of the person, location, item, etc)"
                },
                "type": {
                    "type": "string",
                    "description": "Type of the memory block (person, location, item)"
                },
                "tags": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    },
                    "description": "List of minimum 4 tags for the memory block to be classified (use tags as categories, tags are used to quickly retrieve relevant memory blocks with a simple word search)"
                },
                "data": {
                    "type": "object",
                    "description": "Arbitrary data object representing the memory block (key/value pairs of information, with null value to remove info. You can also update the tags using this function)",
                    "properties": {
                        "/": {}
                    }
                }
            },
            "required": [
                "name",
                "type",
                "tags",
                "data"
            ]
        }
    }

    static async add_memory_block(block_name, block_data, tags, chatbot) {
        // TODO
    }

    static definition_forget_memory_block = {
        "name": "forget_memory_block",
        "description": "Deletes a property of a memory block (delete irrelevant or outdated memories, keep the memories clean and light)",
        "parameters": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string",
                    "description": "Name of the memory block to delete"
                }
            },
            "required": [
                "name"
            ]
        }
    }

    static async forget_memory_block(block_name, chatbot) {
        // TODO
    }

    static definition_get_memory_blocks_by_tags = {
        "name": "get_memory_blocks_by_tags",
        "description": "Retrieves memory blocks associated with specific classification tags relevant to the current narration (4 tags minimum for better classification, always use many tags to improve chances of finding matching memory blocks)",
        "parameters": {
            "type": "object",
            "properties": {
                "tags": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    },
                    "description": "List of tags to use for retrieving memory blocks (the more tags, the better)"
                }
            },
            "required": [
                "tags"
            ]
        }
    }

    static async get_memory_blocks_by_tags(tags, chatbot) {
        // TODO: Add code to retrieve all memory blocks that have at least one tag in common with the tags parameter
    }

    static FUNCTIONS = [
        MemoryService.definition_create_or_edit_memory_block,
        MemoryService.definition_forget_memory_block,
        MemoryService.definition_get_memory_blocks_by_tags
    ]
}