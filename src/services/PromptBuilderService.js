import TokenizerService from "./TokenizerService.js";

export class Format {
    beginningOfText = "<|begin_of_text|>"
    stopToken = "<|eot_id|>"
    rolePrefix = "<|start_header_id|>"
    roleSuffix = "<|end_header_id|>\n\n"
    disableRole = false
    roleMap = null
    outputRegexReplace = null

    constructor(beginningOfText = "<|begin_of_text|>", stopToken = "<|eot_id|>", rolePrefix = "<|start_header_id|>", roleSuffix = "<|end_header_id|>\n\n") {
        this.beginningOfText = beginningOfText
        this.stopToken = stopToken
        this.rolePrefix = rolePrefix
        this.roleSuffix = roleSuffix
        this.disableRole = false
        this.roleMap = null
    }
}

export default class PromptBuilderService {
    /**
     * Replaces the author's name with `X` on messages starting with `(as X)`
     *
     * @param {Array<{content: string, author: ?string}>} messages - The array of messages.
     * @returns {Array<{author: string, content: string}>} The replaced array of messages.
     */
    static replaceAsXImpersonation(messages) {
        return messages.map(m => {
            const impersonatedAuthor = m.content?.match?.(/^\(as ([^)]*)\)/i)?.[1]?.trim?.()
            if (impersonatedAuthor) {
                m.author = impersonatedAuthor.trim()
                m.content = m.content.replace(/^\(as [^)]*\)/i, '').trim()
            }
            return m
        })
    }

    /**
     * Replaces the user messages starting and ending with [] to inject as system prompt.
     *
     * @param {Array<{content: string, author: ?string}>} messages - The array of messages.
     * @returns {Array<{author: string, content: string}>} The replaced array of messages.
     */
    static replaceSystemPromptInjection(messages) {
        return messages.map(m => {
            if (m.content.startsWith('[') && m.content.endsWith(']')) {
                m.author = 'System'
            }
            return m
        })
    }

    /**
     * Concatenates context and messages into a single array.
     *
     * @param {string|?Array<string>} context - The context for the messages.
     * @param {string|?Array<string>} instructions - The instructions for the reply.
     * @param {Array<{content: string, author: ?string}>} messages - The array of messages to concatenate with the context.
     * @returns {Array<{author: string, content: string}>} The concatenated array of messages with context.
     */
    static concatContextAndMessages(context, instructions, messages) {
        const oldMessages = [...messages]
        const lastMessage = oldMessages.pop()
        const newMessages = [
            ...(context ? [{author: 'System', content: context}] : []),
            ...oldMessages,
            ...(instructions ? [{author: 'System', content: instructions}] : []),
        ]
        if (lastMessage) {
            newMessages.push(lastMessage)
        }
        return newMessages
    }

    /**
     * Converts messages into a structured prompt string.
     *
     * @param {string|?Array<string>} context - The context for the messages.
     * @param {string|?Array<string>} instructions - The instructions for the reply.
     * @param {{content: string, author: ?string}[]} messages - Array of message objects.
     * @param {Format} [format=new Format()] - Format object defining structure tokens.
     * @param {Object} [bot={}] - Bot information.
     * @returns {string} The formatted prompt string.
     */
    static messagesToPrompt(context, instructions, messages, format = new Format(), bot = {}) {
        const allMessages = this.concatContextAndMessages(context, instructions, messages)

        let prompt = format.beginningOfText || '';
        prompt += allMessages
            .map(m => {
                const role = bot.name === m.author ?
                    'assistant'
                    : m.author.toLowerCase() === 'system' ?
                        'system'
                        : !m.author ?
                            'system' :
                            'user';

                const mappedRole = !format.roleMap ?
                    role
                    : format.roleMap[role] ?
                        format.roleMap[role]
                        : role

                const name = `${format.disableRole ? '' : mappedRole}${m.author && !format.disableRole ? ` ${m.author}` : m.author && format.disableRole ? `${m.author}` : ''}`
                return `${format.rolePrefix}${name}${format.roleSuffix}${m.content}`;
            })
            .join(format.stopToken);

        prompt += PromptBuilderService.addBotMessageStart(format, bot)

        return prompt;
    }

    /**
     * Adds the start sequence for a bot message.
     *
     * @param {Format} [format=new Format()] - Format object defining structure tokens.
     * @param {Object} [bot=null] - Bot information.
     * @returns {string} The formatted start string for a bot message.
     */
    static addBotMessageStart(format = new Format(), bot = null) {
        const mappedRole = !format.roleMap ?
            'assistant'
            : format.roleMap['assistant'] ?
                format.roleMap['assistant']
                : 'assistant'
        return `${format.stopToken}${format.rolePrefix}${format.disableRole ? '' : `${mappedRole} `}${bot.name}${format.roleSuffix}`;
    }

    /**
     * Builds a structured prompt string within a limited prompt length.
     *
     * @param {string|?Array<string>} context - The context for the messages.
     * @param {string|?Array<string>} instructions - The instructions for the reply.
     * @param {{content: string, author: ?string}[]} messages - Array of message objects.
     * @param {Format} [format=new Format()] - Format object defining structure tokens.
     * @param {Object} [bot={}] - Bot information.
     * @param {Object} tokenizer - The tokenizer to use.
     * @param {number} maxPromptLength - The max length of the prompt, in tokens.
     * @param {number} maxLength - The max length of the output, in tokens.
     * @returns {string} The formatted prompt string.
     */
    static async buildPrompt(context, instructions, messages, format = new Format(), bot, tokenizer, maxPromptLength, maxLength) {
        const messagesTemp = this.replaceSystemPromptInjection(this.replaceAsXImpersonation([...messages]))

        let prompt = this.messagesToPrompt(context, instructions, messagesTemp, format, bot);

        let lastPrompt = prompt
        while (TokenizerService.getTokenLength(prompt, tokenizer) > maxPromptLength - maxLength) {
            messagesTemp.shift()

            if (messagesTemp.length === 0) {
                throw new Error(`Couldn't build the prompt, no message can fit in addition to the context (context too big).`)
            }

            prompt = this.messagesToPrompt(context, instructions, messagesTemp, format, bot);
        }

        return prompt;
    }
}
