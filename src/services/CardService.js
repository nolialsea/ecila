import extract from "png-chunks-extract";
import PNGtext from "png-chunk-text";
import encode from "png-chunks-encode";

export default class CardService {
    /**
     *
     * @param {Buffer} buffer
     * @returns {Promise<string>}
     */
    static async extractCardContent(buffer) {
        let card
        let chunks
        try {
            chunks = extract(buffer)
        } catch {
            throw new Error(`Error: Invalid .png file header`)
        }

        const textChunks = chunks.filter(function (chunk) {
            return chunk.name === 'tEXt'
        }).map(function (chunk) {
            return PNGtext.decode(chunk.data)
        });

        if (textChunks.length === 0) {
            throw new Error(`The card's PNG metadata does not contain any character data.`)
        }

        if (textChunks?.[0]) {
            card = Buffer.from(textChunks[0].text, 'base64').toString('utf8')
        } else {
            throw new Error(`Card content couldn't be loaded...`)
        }

        return card
    }

    /**
     *
     * @param {string} card
     * @param {BotConfig} bot
     * @param {?Client} client
     */
    static async applyCardOntoBot(card, bot, client = null) {
        const argsJSON = []
        const constantsJSON = []
        const newBotName = CardService.extractCharacterName(card)

        if (!newBotName) return false

        // Auto detect dynamic properties
        argsJSON.push(...CardService.extractDynamicProperties(card))
        constantsJSON.push(...CardService.extractConstantProperties(card))

        // Clean up dynamic properties symbol
        card = CardService.cleanupDynamicProperties(card)
        card = CardService.cleanupConstantProperties(card)

        // Update the bot config (not saved)
        bot.name = newBotName

        bot.data = {Name: newBotName}
        bot.constants = {Name: newBotName}

        if (argsJSON && Array.isArray(argsJSON) && argsJSON.length > 0) {
            for (const field of argsJSON) {
                const line = card
                    .split('\n')
                    .map(s => s.trim())
                    .find(s => s.startsWith(field))
                if (!line) {
                    console.log(`Field "${field}" not found!`)
                } else {
                    const value = line.replace(`${field}:`, '').trim()
                    card = card.replace(line, `${field}: \${${field}}`)
                    bot.data[field] = value
                }
            }
        }

        if (constantsJSON && Array.isArray(constantsJSON) && constantsJSON.length > 0) {
            for (const field of constantsJSON) {
                const line = card
                    .split('\n')
                    .map(s => s.trim())
                    .find(s => s.startsWith(field))
                if (!line) {
                    console.log(`Field "${field}" not found!`)
                } else {
                    const value = line.replace(`${field}:`, '').trim()
                    card = card.replace(line, `${field}: \${${field}}`)
                    bot.constants[field] = value
                }
            }
        }

        if (bot.openAIContext) bot.openAIContext.top = `[IMPORTANT INSTRUCTIONS: It's crucial to keep in mind these essential details about the ongoing narrative:\n\n${card}\n]`

        // Update discord bot name
        if (client && client.user.username !== newBotName) {
            await client.user.setUsername(newBotName)
                .catch(err => {
                    console.log(err)
                })
        }

        return true
    }

    /**
     *
     * @param {Buffer} imageBuffer
     * @param {string} card
     * @returns {Buffer}
     */
    static createCard(imageBuffer, card) {
        // Get the chunks
        let chunks
        try {
            chunks = extract(imageBuffer);
        } catch {
            throw new Error('Oh no, the card creation failed!!!')
        }

        const tEXtChunks = chunks.filter(chunk => chunk.name === 'tEXt');

        // Remove all existing tEXt chunks
        for (let tEXtChunk of tEXtChunks) {
            chunks.splice(chunks.indexOf(tEXtChunk), 1);
        }

        // Add new chunks before the IEND chunk
        const base64EncodedData = Buffer.from(card, 'utf8').toString('base64');
        chunks.splice(-1, 0, PNGtext.encode('chara', base64EncodedData));

        return Buffer.from(encode(chunks))
    }

    /**
     *
     * @param {string} card
     * @returns {string | undefined}
     */
    static extractCharacterName(card) {
        return card
            ?.split('\n')
            ?.find(s => s.toLowerCase().includes(`name`))
            ?.replace(/first name:/i, '')
            ?.replace(/name:/i, '')
            ?.trim()
    }

    /**
     *
     * @param {string} card
     * @returns {*[]}
     */
    static extractDynamicProperties(card) {
        const argsJSON = []
        for (const line of card.split('\n').map(s => s.trim())) {
            if (line.startsWith("!!")) {
                const parsedLine = line.replace(/!!/i, '')
                const field = parsedLine.replace(/:.*/i, '')
                argsJSON.push(field)
            }
        }
        return argsJSON
    }

    /**
     *
     * @param {string} card
     * @returns {*[]}
     */
    static extractConstantProperties(card) {
        const argsJSON = []
        for (const line of card.split('\n').map(s => s.trim())) {
            if (line.startsWith(";;")) {
                const parsedLine = line.replace(/;;/i, '')
                const field = parsedLine.replace(/:.*/i, '')
                argsJSON.push(field)
            }
        }
        return argsJSON
    }

    /**
     *
     * @param {string} card
     * @returns {string}
     */
    static cleanupDynamicProperties(card) {
        return card.replace(/^!!/mg, '')
    }

    /**
     *
     * @param {string} card
     * @returns {string}
     */
    static cleanupConstantProperties(card) {
        return card.replace(/^;;/mg, '')
    }
}
