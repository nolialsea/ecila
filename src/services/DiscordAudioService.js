import {AttachmentBuilder} from "discord.js";
import config from "../classes/Config.js";
import fs from "fs";
import AiService from "./AiService.js";
import axios from "axios";
import generateTTS from "./TTSService.js";


function getApiUrl() {
    return config.ECILA_API_URL?.endsWith('/') ? config.ECILA_API_URL.substring(0, config.ECILA_API_URL.length - 1) : config.ECILA_API_URL
}

try {
    fs.mkdirSync(`./temp`)
} catch {

}


export default class DiscordAudioService {
    /**
     *
     * @param {BotConfig} bot
     * @param {string} botMessage
     * @returns {Promise<AttachmentBuilder>}
     */
    static async getTTSAudioDiscordAttachment(bot, botMessage) {
        if (bot.TTS?.disabled) return

        console.log("Generating TTS...")
        if (bot.TTS?.ElevenLabs?.enabled) {
            return await DiscordAudioService.get11LabsTTSAudioDiscordAttachment(bot, botMessage)
        } else if (bot.TTS?.NovelAI?.enabled) {
            return DiscordAudioService.getNAITTSAudioDiscordAttachment(bot, botMessage)
        } else if (bot.TTS?.GPT_SOVITS?.enabled) {
            const audioBuffer = await generateTTS(
                botMessage,
                bot.TTS?.GPT_SOVITS?.ref_audio_path,
                bot.TTS?.GPT_SOVITS?.prompt_text,
                bot.TTS?.GPT_SOVITS?.skipThirdPerson ?? true,
                bot.TTS?.GPT_SOVITS?.gpt_weights,
                bot.TTS?.GPT_SOVITS?.sovits_weights,
                bot.TTS?.GPT_SOVITS?.ref_audio_path_third_person,
                bot.TTS?.GPT_SOVITS?.prompt_text_third_person,
                bot.TTS?.GPT_SOVITS?.gpt_weights_third_person,
                bot.TTS?.GPT_SOVITS?.sovits_weights_third_person
            );

            if (audioBuffer)
                // Create an attachment from the buffer
                return new AttachmentBuilder(audioBuffer, {name: 'tts.wav'});
        }
    }

    /**
     *
     * @param {BotConfig} bot
     * @param {string} botMessage
     * @returns {AttachmentBuilder}
     */
    static getNAITTSAudioDiscordAttachment(bot, botMessage) {
        console.log("Generating NovelAI TTS...")
        const seed = bot?.TTS?.NovelAI?.seed ?? "Aini"
        const thirdPersonVoice = bot?.TTS?.NovelAI?.seedThirdPerson ?? "Lynx"
        const skipThirdPerson = bot?.TTS?.NovelAI?.skipThirdPerson ?? true
        const text = botMessage
            .replace(/\n/, ' ')         // removes newlines
            .replace(/  +/g, ' ')       // removes double spaces
            .replace(/~/g, '')          // replaces tilde
        const finalText = skipThirdPerson ?
            text.replace(/\*[^*]*\*/gi, '') // removes parts in between asterisks
            : text

        return new AttachmentBuilder(`${getApiUrl()}/api/v1/generate/textToSpeech2?access_token=${encodeURIComponent(config.ECILAToken)}&seed=${encodeURIComponent(seed)}&text=${encodeURIComponent(finalText)}&third_person_seed=${encodeURIComponent(thirdPersonVoice)}`, {name: 'tts.mp3'})
    }

    /**
     *
     * @param {BotConfig} bot
     * @param {string} botMessage
     * @returns {Promise<AttachmentBuilder>}
     */
    static async get11LabsTTSAudioDiscordAttachment(bot, botMessage) {
        console.log("Generating ElevenLabs TTS...")
        const apiKey = bot?.TTS?.ElevenLabs?.apiKey
        const voiceID = bot?.TTS?.ElevenLabs?.voiceID
        const style = bot?.TTS?.ElevenLabs?.style ?? 0
        const stability = bot?.TTS?.ElevenLabs?.stability ?? 1
        const similarityBoost = bot?.TTS?.ElevenLabs?.similarityBoost ?? 1
        const useSpeakerBoost = bot?.TTS?.ElevenLabs?.useSpeakerBoost ?? true
        const skipThirdPerson = bot?.TTS?.ElevenLabs?.skipThirdPerson ?? true

        const text = botMessage
            .replace(/\n/, ' ')         // removes newlines
            .replace(/  +/g, ' ')       // removes double spaces
            .replace(/~/g, '')          // replaces tilde
        const finalText = skipThirdPerson ?
            text.replace(/\*[^*]*\*/gi, '') // removes parts in between asterisks
            : text

        const ts = Date.now()
        const filePath = `./temp/${ts}.mp3`

        await AiService.downloadFrom11Labs(filePath, apiKey, finalText, voiceID, stability, similarityBoost, style, useSpeakerBoost)

        return new AttachmentBuilder(fs.createReadStream(filePath), {name: 'tts.mp3'})
    }
}
