// Assuming Node.js environment
import express from 'express';
import bodyParser from 'body-parser';
import configRouter from '../routes/config.js';
import presetsRouter from '../routes/presets.js';

const app = express(); // Create the Express app
const port = 3002; // Define port number

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public')) // Serves the static webpage from public directory

app.use('/api/config', configRouter);
app.use('/api/presets', presetsRouter);

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(err.statusCode || 500).json({error: err.message});
});

try {
    app.listen(port, () => {
        console.log(`Server listening on port ${port}`);
    });
} catch (err) {
    console.error(err)
}