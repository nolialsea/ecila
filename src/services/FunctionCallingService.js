import AIService from "./AiService.js";
import DiscordService from "./DiscordService.js";

export default class FunctionCallingService {
    /**
     *
     * @param {Message} discordMessage
     * @param {BotConfig} bot
     * @param {?string} channelTopic
     * @param {object[]} messages
     * @param {object[]} functionDefinitions
     * @param {object|string|null} function_call
     * @param {?object} functions
     * @returns {Promise<string|object|null>}
     */
    static async functionCall(discordMessage, bot, channelTopic, messages, functionDefinitions, function_call = "auto", functions = null) {

        // Edit settings
        bot.textGenParameters.OpenAI.functions = functionDefinitions
        bot.textGenParameters.OpenAI.function_call = function_call

        // Generate the message
        const content = await AIService.generateMessage(bot, messages, channelTopic, null, false, discordMessage?.channel)

        // If AI generation failed, we send an error and exit the function
        if (!content) {
            if (bot.debug) {
                await DiscordService.sendCommandError(discordMessage, `# Oops, the generation failed!`)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            // When you return inside a mod, ALWAYS use `next()` before or at the return!
            return null
        }

        if (typeof content === 'string') {
            return content
        }

        // Check if the content is an object!
        // If the content is a string, it means the AI generated a bot message and not a function call
        // In which case, we instead call the normal message generation tool
        // Same if the content doesn't have arguments as a string
        if (typeof content !== "object") {
            if (bot.debug) {
                await DiscordService.sendCommandError(discordMessage, `# Oops, the generation failed! (no json arguments)\n\`\`\`\n${JSON.stringify(content, null, 2)}\n\`\`\``)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            return null
        }

        try {
            if (typeof content.arguments === 'string') {
                // Since arguments are always sent as a string, we need to parse them back into a JSON object
                content.arguments = JSON.parse(content.arguments)
            } else if (typeof content.arguments === "object") {
                // All good, hopefully
            } else {
                console.error('Oh no!!!')
                return null
            }
        } catch {
            if (bot.debug) {
                // If the JSON fails to parse, we send an error and return
                await DiscordService.sendCommandError(discordMessage, `# Oops, the generation failed! (no valid json arguments)\n\`\`\`\n${JSON.stringify(content, null, 2)}\n\`\`\``)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            return null
        }

        // If the function call has no name, error
        if (!functionDefinitions.find(f => f.name === content?.name)?.name) {
            if (bot.debug) {
                const errorMessage = `# Oops, the function call failed! Missing function name or unknown function! (name: \`${content?.name}\`)`
                await DiscordService.sendCommandError(discordMessage, errorMessage)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            return null
        }

        // If the arguments are missing, error
        if (!functionDefinitions.find(f => f.name === content?.name)?.parameters.required.every(p => content.arguments[p] != null)) {
            if (bot.debug) {
                const errorMessage = `# Oops, the function call failed! (missing arguments)\n\`\`\`\n${JSON.stringify(content, null, 2)}\n\`\`\``
                await DiscordService.sendCommandError(discordMessage, errorMessage)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            return null
        }

        await this.processFunctionCall(discordMessage, bot, channelTopic, messages, functionDefinitions, content, functions)

        return content
    }

    /**
     *
     * @param {Message} discordMessage
     * @param {BotConfig} bot
     * @param {?string} channelTopic
     * @param {object[]} messages
     * @param {object[]} functionDefinitions
     * @param {object|string|null} function_call
     * @param {?object} functions
     * @returns {Promise<string|object|null>}
     */
    static async functionCallRaw(discordMessage, bot, channelTopic, messages, functionDefinitions, function_call = "auto", functions = null) {

        // Edit settings
        bot.textGenParameters.OpenAI.functions = functionDefinitions
        bot.textGenParameters.OpenAI.function_call = function_call

        // Generate the message
        const content = await AIService.generateMessageOAIRaw(bot, messages, channelTopic, null, false, discordMessage?.channel)

        // If AI generation failed, we send an error and exit the function
        if (!content) {
            if (bot.debug) {
                await DiscordService.sendCommandError(discordMessage, `# Oops, the generation failed!`)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            // When you return inside a mod, ALWAYS use `next()` before or at the return!
            return null
        }

        if (typeof content === 'string') {
            return content
        }

        // Check if the content is an object!
        // If the content is a string, it means the AI generated a bot message and not a function call
        // In which case, we instead call the normal message generation tool
        // Same if the content doesn't have arguments as a string
        if (typeof content !== "object") {
            if (bot.debug) {
                await DiscordService.sendCommandError(discordMessage, `# Oops, the generation failed! (no json arguments)\n\`\`\`\n${JSON.stringify(content, null, 2)}\n\`\`\``)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            return null
        }

        try {
            if (typeof content.arguments === 'string') {
                // Since arguments are always sent as a string, we need to parse them back into a JSON object
                content.arguments = JSON.parse(content.arguments)
            } else if (typeof content.arguments === "object") {
                // All good, hopefully
            } else {
                console.error('Oh no!!!')
                return null
            }
        } catch {
            if (bot.debug) {
                // If the JSON fails to parse, we send an error and return
                await DiscordService.sendCommandError(discordMessage, `# Oops, the generation failed! (no valid json arguments)\n\`\`\`\n${JSON.stringify(content, null, 2)}\n\`\`\``)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            return null
        }

        // If the function call has no name, error
        if (!functionDefinitions.find(f => f.name === content?.name)?.name) {
            if (bot.debug) {
                const errorMessage = `# Oops, the function call failed! Missing function name or unknown function! (name: \`${content?.name}\`)`
                await DiscordService.sendCommandError(discordMessage, errorMessage)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            return null
        }

        // If the arguments are missing, error
        if (!functionDefinitions.find(f => f.name === content?.name)?.parameters.required.every(p => content.arguments[p] != null)) {
            if (bot.debug) {
                const errorMessage = `# Oops, the function call failed! (missing arguments)\n\`\`\`\n${JSON.stringify(content, null, 2)}\n\`\`\``
                await DiscordService.sendCommandError(discordMessage, errorMessage)
            } else {
                await discordMessage?.react('🐞').catch(() => null)
            }
            return null
        }

        await this.processFunctionCall(discordMessage, bot, channelTopic, messages, functionDefinitions, content, functions)

        return content
    }

    static async processFunctionCall(userReaction, bot, channelTopic, messages, functionDefinitions, functionCall, functions) {
        if (functions?.[functionCall?.name]) {
            await functions?.[functionCall.name]?.(functionCall?.arguments)
        }
    }
}