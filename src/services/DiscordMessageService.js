import DiscordService from "./DiscordService.js";
import AIService from "./AiService.js";
import DiscordAudioService from "./DiscordAudioService.js";
import DiscordTypingService from "./DiscordTypingService.js";

/**
 *
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {?string} channelTopic
 * @param messages_
 * @returns {Promise<void>}
 */
async function continueMessage(userMessage, bot, channelTopic, messages_ = null) {
    const messages = messages_ ?? await DiscordService.getChannelMessages(userMessage.channel, userMessage, bot)
    await DiscordTypingService.sendTyping(userMessage)

    const oldContent = userMessage.cleanContent
    const content = await AIService.generateMessage(bot, messages, channelTopic, null, false, userMessage.channel)
    await userMessage.edit(oldContent.trim() + ' ' + content?.trim()).catch(console.error)
}

/**
 *
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {?string} channelTopic
 * @returns {Promise<void>}
 */
async function retryMessage(userMessage, bot, channelTopic) {
    const messages = (await DiscordService.getChannelMessages(userMessage.channel, userMessage, bot))
    await DiscordTypingService.sendTyping(userMessage)
    messages.pop()

    const content = await AIService.generateMessage(bot, messages, channelTopic, null, false, userMessage.channel)
    if (content) {
        await userMessage.edit(content.trim()).catch(console.error)
    }
}

/**
 * @param {Message<boolean>} userMessage
 * @returns {boolean}
 */
function isCommand(userMessage) {
    return !userMessage.cleanContent?.startsWith('#') && !userMessage.cleanContent?.startsWith('//') && !userMessage.cleanContent?.startsWith('. ')
}

export default class DiscordMessageService {
    static continueMessage = continueMessage
    static retryMessage = retryMessage
    static isCommand = isCommand

    /**
     * Sends a message with TTS (Text-to-Speech) support.
     * @param {import('discord.js').TextChannel} channel
     * @param {string} botMessage
     * @param {import('discord.js').TextMessage} userMessage
     * @param {BotConfig} bot
     * @param {import('discord.js').MessageAttachment[]} files
     * @param {boolean} preventMessageDeletion
     * @param {boolean} isReply
     * @returns {?Promise<import('discord.js').TextMessage>}
     */
    static async sendMessageWithTTS(channel, botMessage, userMessage, bot, files, preventMessageDeletion, isReply = false) {
        if (bot.TTS?.disabled) {
            const attachments = [...files];
            return await this.sendContent(channel, botMessage, attachments, userMessage, preventMessageDeletion, isReply);
        }

        const attachments = [...files, await DiscordAudioService.getTTSAudioDiscordAttachment(bot, botMessage)];
        return await this.sendContent(channel, botMessage, attachments, userMessage, preventMessageDeletion, isReply);
    }

    /**
     * Sends a message without TTS (Text-to-Speech) support.
     * @param {import('discord.js').TextChannel} channel
     * @param {string} botMessage
     * @param {import('discord.js').TextMessage} userMessage
     * @param {import('discord.js').MessageAttachment[]} files
     * @param {boolean} preventMessageDeletion
     * @param {boolean} isReply
     * @returns {?Promise<import('discord.js').TextMessage>}
     */
    static async sendMessageWithoutTTS(channel, botMessage, userMessage, files, preventMessageDeletion, isReply = false) {
        return await this.sendContent(channel, botMessage, files, userMessage, preventMessageDeletion, isReply);
    }

    /**
     * Sends a message content with optional files and handles message deletion.
     * @param {import('discord.js').TextChannel} channel
     * @param {string} content
     * @param {import('discord.js').MessageAttachment[]} files
     * @param {import('discord.js').TextMessage} userMessage
     * @param {boolean} preventMessageDeletion
     * @param {boolean} isReply
     * @returns {?Promise<import('discord.js').TextMessage>}
     */
    static async sendContent(channel, content, files, userMessage, preventMessageDeletion, isReply = false) {
        let discordMessage

        if (channel) {
            if (isReply) {
                discordMessage = userMessage?.reply({content, files}).catch(async (err) => {
                    console.error(err)
                    if (channel) {
                        if (isReply) {
                            userMessage?.reply({content, files}).catch(console.error)
                        } else {
                            channel.send?.({content, files}).catch(console.error)
                        }
                    }
                });
            } else {
                discordMessage = channel.send?.({content, files}).catch(async (err) => {
                    console.error(err)
                    if (channel) {
                        if (isReply) {
                            userMessage?.reply({content, files}).catch(console.error)
                        } else {
                            channel.send?.({content, files}).catch(console.error)
                        }
                    }
                });
            }
        }

        if (userMessage?.cleanContent?.startsWith?.('!') && !preventMessageDeletion) {
            userMessage?.delete?.().catch(() => {
            });
        }
        return discordMessage
    }

    /**
     * Sends a long message by breaking it into multiple parts if needed.
     * @param {import('discord.js').TextChannel} channel
     * @param {string} botMessage
     * @param {import('discord.js').TextMessage} userMessage
     * @param {boolean} preventTTS
     * @param {BotConfig} bot
     * @param {boolean} preventMessageDeletion
     * @param {import('discord.js').MessageAttachment[]} files
     * @param {boolean} isReply
     * @returns {?Promise<import('discord.js').TextMessage>}
     */
    static async sendLongMessage(channel, botMessage, userMessage, preventTTS, bot, preventMessageDeletion, files, isReply = false) {
        let lines = botMessage.split('\n');
        const commentMessage = botMessage.startsWith('#')
        const isImpersonation = botMessage.startsWith(`(as `)
        const impersonatedAuthor = isImpersonation ? botMessage.match(/\(as ([^)]*)/i)?.[1]?.trim() : null
        let tripleBackticksOpen = false;
        let tripleBackticksLanguage = null
        let message = '';
        let firstLoop = true;

        const allLines = []
        for (const line of lines) {
            let newLine = line
            if (line.length >= 1997) {
                // TODO: don't add newline to every sentence, only when necessary
                newLine = line.split('. ').join('.\n')
            } else {
                newLine = line
            }
            allLines.push(newLine)
        }

        lines = allLines.join('\n').split('\n')

        while (lines.length > 0) {
            const line = lines.shift();
            const foundTripleBacktick = line.match(/```/gi)?.length === 1
            if (foundTripleBacktick) {
                tripleBackticksOpen = !tripleBackticksOpen
                if (tripleBackticksOpen) {
                    tripleBackticksLanguage = line.replace(/^```/gi, '')
                }
            }
            if (line.length + '\n'.length + message.length >= 1997) {
                const attachments = (preventTTS || bot?.TTS?.disabled || (!bot?.TTS?.NovelAI?.enabled && !bot?.TTS?.ElevenLabs?.enabled && !bot?.TTS?.GPT_SOVITS?.enabled)) ? null : [await DiscordAudioService.getTTSAudioDiscordAttachment(bot, message)];
                await this.sendContent(channel, message + (tripleBackticksOpen ? `\n\`\`\`` : ''), attachments, userMessage, preventMessageDeletion, isReply);
                message = (isImpersonation ? `(as ${impersonatedAuthor}) ` : '')
                    + (tripleBackticksOpen && commentMessage ?
                        '#Rest of the message...\n```' + (tripleBackticksLanguage ?? '') + '\n'
                        : tripleBackticksOpen ?
                            '```' + (tripleBackticksLanguage ?? '') + '\n'
                            : '')
                firstLoop = false;
                tripleBackticksLanguage = null
            }
            message += `\n${line}`;
        }

        if (message) {
            const attachments = (preventTTS || bot?.TTS?.disabled || (!bot?.TTS?.NovelAI?.enabled && !bot?.TTS?.ElevenLabs?.enabled && !bot?.TTS?.GPT_SOVITS?.enabled)) ? files : [...files, await DiscordAudioService.getTTSAudioDiscordAttachment(bot, message)];
            return await this.sendContent(channel, message, attachments, userMessage, preventMessageDeletion, isReply);
        }
    }

    /**
     * Sends a message to a user.
     * @param {import('discord.js').TextMessage} userMessage
     * @param {string} botMessage
     * @param {BotConfig} bot
     * @param {boolean} [preventMessageDeletion=false]
     * @param {boolean} [preventTTS=false]
     * @param {import('discord.js').MessageAttachment[]} [files=[]]
     * @param {import('discord.js').MessageActionRow[]} [components=null]
     * @param {import('discord.js').TextChannel} [channel=null]
     * @param {boolean} isReply
     * @returns {?import('discord.js').TextMessage}
     */
    static async sendMessage(userMessage, botMessage, bot, preventMessageDeletion = false, preventTTS = false, files = [], components = null, channel = null, isReply = false) {
        if (!channel) channel = await userMessage.channel.fetch();
        files = files.filter(f => !!f);
        if (botMessage?.match?.(/@everyone/i)) botMessage = botMessage.replace(/@everyone/gi, 'everyone');

        if (components && botMessage) {
            if (isReply) {
                return await userMessage?.reply({content: botMessage, components: [components]}).catch(console.error)
            } else {
                return await channel.send({content: botMessage, components: [components]}).catch(console.error)
            }
        } else if (botMessage || files) {
            if (botMessage.length >= 2000) {
                return await DiscordMessageService.sendLongMessage(channel, botMessage, userMessage, preventTTS, bot, preventMessageDeletion, files, isReply);
            } else if (!preventTTS && !bot?.TTS?.disabled && (bot?.TTS?.NovelAI?.enabled || bot?.TTS?.ElevenLabs?.enabled)) {
                return await DiscordMessageService.sendMessageWithTTS(channel, botMessage, userMessage, bot, files, preventMessageDeletion, isReply);
            } else {
                return await DiscordMessageService.sendMessageWithoutTTS(channel, botMessage, userMessage, files, preventMessageDeletion, isReply);
            }
        } else {
            await userMessage?.react?.('🐞').catch(console.error);
        }
    }
}
