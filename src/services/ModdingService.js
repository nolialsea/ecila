export default class ModdingService {
    /**
     *
     * @param {BotConfig} bot
     * @param {Message} userMessage
     */
    static isBotMasterBot(bot, userMessage) {
        const serverName = userMessage?.channel?.guild?.name
        const serverId = userMessage?.channel?.guild?.id
        if (typeof bot.isMasterBot === "boolean" && bot.isMasterBot) {
            return true
        }

        if (typeof bot.isMasterBot === "string") {
            return serverName === bot.isMasterBot || serverId === bot.isMasterBot
        }

        if (Array.isArray(bot.isMasterBot)) {
            let isMaster = false
            for (const s of bot.isMasterBot) {
                if (serverName === s || serverId === s){
                    isMaster = true
                    break
                }
            }
            return isMaster
        }

        return false
    }
}