import Workflow from "../classes/Workflow.js";
import SaveImage from "../classes/nodes/SaveImage.js";
import VAEDecode from "../classes/nodes/VAEDecode.js";
import KSampler from "../classes/nodes/KSampler.js";
import LoraLoader from "../classes/nodes/LoraLoader.js";
import CLIPTextEncode from "../classes/nodes/CLIPTextEncode.js";
import VAEEncode from "../classes/nodes/VAEEncode.js";
import LoadImage from "../classes/nodes/LoadImage.js";
import ModelLoader from "../classes/nodes/ModelLoader.js";
import EmptyLatentImage from "../classes/nodes/EmptyLatentImage.js";
import AnimeLineArtPreprocessor from "../classes/nodes/AnimeLineArtPreprocessor.js";
import ControlNetLoader from "../classes/nodes/ControlNetLoader.js";
import UpscaleImage from "../classes/nodes/UpscaleImage.js";
import ControlNetApply from "../classes/nodes/ControlNetApply.js";
import UpscaleImageBy from "../classes/nodes/UpscaleImageBy.js";
import UpscaleMethod from "../classes/types/UpscaleMethod.js";
import MaskByText from "../classes/nodes/MaskByText.js";
import CombineMasks from "../classes/nodes/CombineMasks.js";
import CombineOperation from "../classes/types/CombineOperation.js";
import BlurImage from "../classes/nodes/BlurImage.js";
import ChangeChannelCount from "../classes/nodes/ChangeChannelCount.js";
import ChannelCountType from "../classes/types/ChannelCountType.js";
import ImageToMask from "../classes/nodes/ImageToMask.js";
import ChannelType from "../classes/types/ChannelType.js";
import ImageCompositeMasked from "../classes/nodes/ImageCompositeMasked.js";
import VAEEncodeForInpaint from "../classes/nodes/VAEEncodeForInpaint.js";
import DepthAnythingPreprocessor from "../classes/nodes/DepthAnythingPreprocessor.js";
import EmptySD3LatentImage from '../classes/nodes/EmptySD3LatentImage.js';
import FluxGuidance from '../classes/nodes/FluxGuidance.js';
import ModelNF4Loader from "../classes/nodes/ModelNF4Loader.js";
import Florence2ModelLoader from "../classes/nodes/Florence2ModelLoader.js";
import Florence2Run from "../classes/nodes/Florence2Run.js";
import ShowText from "../classes/nodes/ShowText.js";
import LoadMask from "../classes/nodes/LoadMask.js";
import BlurMask from "../classes/nodes/BlurMask.js";
import SetLatentNoiseMask from "../classes/nodes/SetLatentNoiseMask.js";
import UNETLoader from "../classes/nodes/UNETLoader.js";
import DualCLIPLoader from "../classes/nodes/DualCLIPLoader.js";
import VAELoader from "../classes/nodes/VAELoader.js";
import CLIPTextEncodeFlux from "../classes/nodes/CLIPTextEncodeFlux.js";
import VHS_VideoCombine from "../classes/nodes/VHS_VideoCombine.js";
import VAEDecodeTiled from "../classes/nodes/VAEDecodeTiled.js";
import EmptyHunyuanLatentVideo from "../classes/nodes/EmptyHunyuanLatentVideo.js";
import ModelSamplingSD3 from "../classes/nodes/ModelSamplingSD3.js";
import KSamplerSelect from "../classes/nodes/KSamplerSelect.js";
import SamplerCustomAdvanced from "../classes/nodes/SamplerCustomAdvanced.js";
import BasicGuider from "../classes/nodes/BasicGuider.js";
import RandomNoise from "../classes/nodes/RandomNoise.js";
import BasicScheduler from "../classes/nodes/BasicScheduler.js";
import LoadVideoUpload from "../classes/nodes/LoadVideoUpload.js";

class LoRA {
    /** @type {string} Name of the LoRA file with extension */
    filename

    /** @type {number} Strength of the LoRA */
    strength_model = 1

    /** @type {number} Strength of the LoRA on CLIP */
    strength_clip = 1
}

export default class NewComfyWorkflowService {
    static getFluxImageWorkflow(prompt, seed, steps = 20, cfg = 3.5, width = 1024, height = 1024, sampler = "euler", scheduler = "simple") {
        const workflow = new Workflow()
        const nodeModel = workflow.addNode(new ModelNF4Loader(-1, "flux1-dev-bnb-nf4-v2.safetensors"))
        const negative = workflow.addNode(new CLIPTextEncode(-1, nodeModel, ""))
        const clipencode = workflow.addNode(new CLIPTextEncode(-1, nodeModel, prompt, false))
        const latentimage = workflow.addNode(new EmptySD3LatentImage(-1, width, height, 1))
        const fluxguidance = workflow.addNode(new FluxGuidance(-1, clipencode, cfg))
        const nodeSampler = workflow.addNode(new KSampler(-1, nodeModel, fluxguidance, negative, latentimage, steps, 1, sampler, scheduler, seed))
        const vadedcode = workflow.addNode(new VAEDecode(-1, nodeSampler, nodeModel, false))
        const saveimage = workflow.addNode(new SaveImage(-1, vadedcode, "ComfyUI"))

        return workflow
    }

    static getFluxFP8ImageToImageWorkflow(imageName, maskName, prompt, seed, steps = 20, cfg = 3.5, width = 1024, height = 1024, sampler = "euler", scheduler = "simple", model = "flux1-dev-fp8.safetensors", denoise = 0.5, loras = [], blur = 32, clipPrompt = "") {
        const workflow = new Workflow()

        const nodeModel = workflow.addNode(new ModelLoader(-1, model))

        let lastLora = nodeModel
        for (const lora of loras) {
            lastLora = workflow.addNode(new LoraLoader(-1, lastLora, lora.filename, lora.strength_model, lora.strength_clip))
        }

        const nodeLoadImage = workflow.addNode(new LoadImage(-1, imageName))

        const nodeUpscaleImage = workflow.addNode(new UpscaleImage(-1, nodeLoadImage, width, height))
        const nodeVAEEncode = workflow.addNode(new VAEEncode(-1, nodeUpscaleImage, nodeModel))

        const negative = workflow.addNode(new CLIPTextEncode(-1, nodeModel, ""))

        const clipEncode = workflow.addNode(new CLIPTextEncodeFlux(-1, lastLora, prompt, clipPrompt, cfg, false))

        let nodeSampler
        if (maskName) {
            const nodeLoadMask = workflow.addNode(new LoadMask(-1, maskName))
            const nodeMaskBlur = workflow.addNode(new BlurMask(-1, nodeLoadMask, blur))
            const nodeSetLatentNoiseMask = workflow.addNode(new SetLatentNoiseMask(-1, nodeVAEEncode, nodeMaskBlur))
            nodeSampler = workflow.addNode(new KSampler(-1, lastLora, clipEncode, negative, nodeSetLatentNoiseMask, steps, 1, sampler, scheduler, seed, denoise))
        } else {
            nodeSampler = workflow.addNode(new KSampler(-1, lastLora, clipEncode, negative, nodeVAEEncode, steps, 1, sampler, scheduler, seed, denoise))
        }

        const vaeDecode = workflow.addNode(new VAEDecode(-1, nodeSampler, nodeModel, false))
        const saveImage = workflow.addNode(new SaveImage(-1, vaeDecode, "ComfyUI"))

        return workflow
    }

    static getFluxFP8ImageWorkflow(prompt, seed, steps = 20, cfg = 3.5, width = 1024, height = 1024, sampler = "euler", scheduler = "simple", model = "flux1-dev-fp8.safetensors", loras = [], modelType = "normal", clipPrompt = "") {
        const workflow = new Workflow()

        let nodeModel
        let nodeDualClip
        let nodeVae
        if (modelType === "unet") {
            nodeModel = workflow.addNode(new UNETLoader(-1, model))
            nodeDualClip = workflow.addNode(new DualCLIPLoader(-1))
            nodeVae = workflow.addNode(new VAELoader(-1, "ae.sft"))
        } else {
            nodeModel = workflow.addNode(new ModelLoader(-1, model))
        }

        let lastLora = nodeModel
        for (const lora of loras) {
            if (modelType === "unet" && lastLora === nodeModel) {
                lastLora = workflow.addNode(new LoraLoader(-1, lastLora, lora.filename, lora.strength_model, lora.strength_clip, nodeDualClip))
            } else {
                lastLora = workflow.addNode(new LoraLoader(-1, lastLora, lora.filename, lora.strength_model, lora.strength_clip))
            }
        }

        const negative = workflow.addNode(new CLIPTextEncode(-1, nodeDualClip ?? nodeModel, "", modelType === "unet"))
        const clipEncode = workflow.addNode(new CLIPTextEncodeFlux(-1, nodeDualClip ?? lastLora, prompt, clipPrompt, cfg, modelType === "unet"))
        const latentImage = workflow.addNode(new EmptySD3LatentImage(-1, width, height, 1))
        const nodeSampler = workflow.addNode(new KSampler(-1, lastLora, clipEncode, negative, latentImage, steps, 1, sampler, scheduler, seed))
        const vaeDecode = workflow.addNode(new VAEDecode(-1, nodeSampler, modelType === "unet" ? nodeVae : nodeModel, modelType === "unet"))
        const saveImage = workflow.addNode(new SaveImage(-1, vaeDecode, "ComfyUI"))

        return workflow
    }

    /**
     * Creates a ComfyUI workflow for the hunyuan video generation.
     * This refactors the old JSON approach into the Workflow-based approach.
     *
     * @param {string} prompt - Prompt text for your video
     * @param {number} seed - Random seed
     * @param {number} steps - Number of diffusion steps
     * @param {number} cfg - Guidance scale
     * @param {number} width - Width of the frames
     * @param {number} height - Height of the frames
     * @param {string} sampler - Sampler type
     * @param {string} scheduler - Sampler scheduler
     * @param {number} frameCount - How many frames in the video
     * @param {?LoRA[]} loras - Optional array of LoRAs
     * @param {string} format - Output format (video/webm, video/mp4, etc.)
     * @param {number} temporal_size
     * @param {number} temporal_overlap
     * @param {?string} videoName
     * @param {number} denoise
     * @returns {Workflow}
     */
    static getHunyuanVideoWorkflow2(
        prompt,
        seed,
        steps = 20,
        cfg = 6.0,
        width = 512,
        height = 512,
        sampler = "euler",
        scheduler = "simple",
        frameCount = 49,
        loras = null,
        format = "video/webm",
        temporal_size = 64,
        temporal_overlap = 8,
        videoName = null,
        denoise = 1
    ) {
        const workflow = new Workflow();

        // 1) VAE
        const nodeVaeLoader = workflow.addNode(new VAELoader(-1, "hunyuan_video_vae_bf16.safetensors"));

        // 2) DualCLIP (for hunyuan_video)
        let nodeDualClipLoader = workflow.addNode(
            new DualCLIPLoader(-1, "clip_l.safetensors", "llava_llama3_fp8_scaled.safetensors", "hunyuan_video")
        );

        // 3) UNET
        let nodeUnetLoader = workflow.addNode(
            new UNETLoader(-1, "hunyuan_video_fp8_e4m3fn.safetensors", "fp8_e4m3fn")
        );

        // Attach LoRAs if any
        if (Array.isArray(loras) && loras.length > 0) {
            let lastClipLora = nodeDualClipLoader;
            let lastUnetLora = nodeUnetLoader;
            let firstPass = true
            for (const lora of loras) {
                lastClipLora = workflow.addNode(
                    new LoraLoader(-1, lastUnetLora, lora.filename, lora.strength_model, lora.strength_clip, firstPass ? nodeDualClipLoader : null)
                );
                lastUnetLora = lastClipLora
                firstPass = false
            }
            nodeDualClipLoader = lastClipLora;
            nodeUnetLoader = lastUnetLora;
        }

        // 4) ModelSamplingSD3
        const nodeModelSampling = workflow.addNode(new ModelSamplingSD3(-1, nodeUnetLoader, 7));

        // 5) KSamplerSelect
        const nodeKSamplerSelect = workflow.addNode(new KSamplerSelect(-1, sampler));

        // 6) BasicScheduler
        const nodeBasicScheduler = workflow.addNode(new BasicScheduler(-1, nodeUnetLoader, scheduler, steps, denoise));

        // 7) RandomNoise
        const nodeRandomNoise = workflow.addNode(new RandomNoise(-1, seed ?? Math.floor(Math.random() * 1e9)));

        // 8) CLIPTextEncode
        //    The fix for "tuple index out of range" is to pass `true` for flux
        //    so that it references output #0 of the DualCLIPLoader.
        const nodeClipTextEncode = workflow.addNode(new CLIPTextEncode(-1, nodeDualClipLoader, prompt, !(Array.isArray(loras) && loras.length > 0)));

        // 9) FluxGuidance
        const nodeFluxGuidance = workflow.addNode(new FluxGuidance(-1, nodeClipTextEncode, cfg));

        // 10) BasicGuider => references the model sampling + flux guidance
        const nodeBasicGuider = workflow.addNode(new BasicGuider(-1, nodeModelSampling, nodeFluxGuidance));

        // 11) EmptyHunyuanLatentVideo
        let nodeVideoLatent
        if (!videoName) {
            nodeVideoLatent = workflow.addNode(new EmptyHunyuanLatentVideo(-1, width, height, frameCount, 1));
        } else {
            const nodeLoadVideo = workflow.addNode(new LoadVideoUpload(-1, videoName, width, height, frameCount));
            nodeVideoLatent = workflow.addNode(new VAEEncode(-1, nodeLoadVideo, nodeVaeLoader, 192, 64, temporal_size, temporal_overlap, true));
        }

        // 12) SamplerCustomAdvanced
        const nodeSamplerCustomAdvanced = workflow.addNode(
            new SamplerCustomAdvanced(
                -1,
                nodeRandomNoise,
                nodeBasicGuider,
                nodeKSamplerSelect,
                nodeBasicScheduler,
                nodeVideoLatent
            )
        );

        // 13) VAEDecodeTiled
        //    We'll keep tile_size=256, overlap=64 for now
        const nodeVaeDecodeTiled = workflow.addNode(
            new VAEDecodeTiled(-1, nodeSamplerCustomAdvanced, nodeVaeLoader, 192, 64, temporal_size, temporal_overlap)
        );

        // 14) VHS_VideoCombine
        workflow.addNode(
            new VHS_VideoCombine(-1, nodeVaeDecodeTiled, 24, 0, "AnimateDiff", format, "yuv420p", 20, false, false, true)
        );

        return workflow;
    }

    static getFluxSchnellImageWorkflow(prompt, seed, steps = 8, cfg = 3.5, width = 1024, height = 1024, sampler = "euler", scheduler = "simple") {
        const workflow = new Workflow()
        const nodeModel = workflow.addNode(new ModelNF4Loader(-1, "flux1-schnell-bnb-nf4.safetensors"))
        const negative = workflow.addNode(new CLIPTextEncode(-1, nodeModel, ""))
        const clipencode = workflow.addNode(new CLIPTextEncode(-1, nodeModel, prompt, false))
        const latentimage = workflow.addNode(new EmptySD3LatentImage(-1, width, height, 1))
        const fluxguidance = workflow.addNode(new FluxGuidance(-1, clipencode, cfg))
        const nodeSampler = workflow.addNode(new KSampler(-1, nodeModel, fluxguidance, negative, latentimage, steps, 1, sampler, scheduler, seed))
        const vadedcode = workflow.addNode(new VAEDecode(-1, nodeSampler, nodeModel, false))
        const saveimage = workflow.addNode(new SaveImage(-1, vadedcode, "ComfyUI"))

        return workflow
    }

    static getImageWorkflow(prompt, negativePrompt, seed, steps = 64, cfg = 5, width = 640, height = 960, model = "realcartoonPixar_v6.safetensors", sampler = "euler_ancestral", scheduler = "karras", loras = [], iteration = 1) {
        const workflow = new Workflow()
        const nodeModel = workflow.addNode(new ModelLoader(-1, model))

        let lastLora = nodeModel
        for (const lora of loras) {
            lastLora = workflow.addNode(new LoraLoader(-1, lastLora, lora.filename, lora.strength_model, lora.strength_clip))
        }

        const positive = workflow.addNode(new CLIPTextEncode(-1, lastLora, prompt))
        const negative = workflow.addNode(new CLIPTextEncode(-1, lastLora, negativePrompt))
        const latent = workflow.addNode(new EmptyLatentImage(-1, width, height, iteration))
        const nodeSampler = workflow.addNode(new KSampler(-1, lastLora, positive, negative, latent, steps, cfg, sampler, scheduler, seed))
        const nodeVAEDecode = workflow.addNode(new VAEDecode(-1, nodeSampler, nodeModel))
        workflow.addNode(new SaveImage(-1, nodeVAEDecode))

        return workflow
    }

    static getFaceToFaceImageWorkflow(imageName, prompt, negativePrompt, seed, steps = 32, cfg = 5, width = 1280, height = 1920, model = "realcartoonPixar_v6.safetensors", sampler = "dpmpp_3m_sde_gpu", scheduler = "exponential", denoise = 0.5, loras = [], controlNetModelName = "control_v11p_sd15_lineart_fp16.safetensors", extractedMasks = [{
        positivePrompt: "face",
        negativePrompt: "",
        precision: 0.5
    }], controlNetStrength = 0.5, controlNetStart = 0, controlNetEnd = 1, blurRadius = 10, blurSigmaFactor = 1) {
        const workflow = new Workflow()

        // Model + LoRA part
        const nodeModel = workflow.addNode(new ModelLoader(-1, model))
        let lastLora = nodeModel
        for (const lora of loras) {
            lastLora = workflow.addNode(new LoraLoader(-1, lastLora, lora.filename, lora.strength_model, lora.strength_clip))
        }

        // Prompt part
        const nodePositivePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, prompt))
        const nodeNegativePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, negativePrompt))

        // Loaded image preprocess
        const nodeLoadImage = workflow.addNode(new LoadImage(-1, imageName))
        const nodeAnimeLineArt = workflow.addNode(new AnimeLineArtPreprocessor(-1, nodeLoadImage, width < height ? width : height))
        const nodeControlNetLoader = workflow.addNode(new ControlNetLoader(-1, controlNetModelName))
        const nodeControlNetApply = workflow.addNode(new ControlNetApply(-1, nodePositivePrompt, nodeNegativePrompt, nodeControlNetLoader, nodeAnimeLineArt, controlNetStrength, controlNetStart, controlNetEnd))

        // Image upscale
        const nodeUpscaleImage = workflow.addNode(new UpscaleImage(-1, nodeLoadImage, width, height))
        const nodeVAEEncode = workflow.addNode(new VAEEncode(-1, nodeUpscaleImage, nodeModel))

        // Sample
        const nodeSampler = workflow.addNode(new KSampler(-1, lastLora, nodeControlNetApply, null, nodeVAEEncode, steps, cfg, sampler, scheduler, seed, denoise))
        const nodeVAEDecode = workflow.addNode(new VAEDecode(-1, nodeSampler, nodeModel))

        // Build mask image for original image
        const nodeMaskImageOriginal = this.buildMasksImage(workflow, nodeUpscaleImage, extractedMasks, blurRadius, blurSigmaFactor)
        // Build mask image for generated image
        const nodeMaskImageGenerated = this.buildMasksImage(workflow, nodeVAEDecode, extractedMasks, blurRadius, blurSigmaFactor)

        const nodeCombineMasks = workflow.addNode(new CombineMasks(-1, nodeMaskImageOriginal, nodeMaskImageGenerated, CombineOperation.UNION_MAX, true, false))

        const nodeConvertImageToMask = workflow.addNode(new ImageToMask(-1, nodeCombineMasks, ChannelType.RED))
        const nodeCompositeMasked = workflow.addNode(new ImageCompositeMasked(-1, nodeVAEDecode, nodeUpscaleImage, nodeConvertImageToMask))

        workflow.addNode(new SaveImage(-1, nodeCompositeMasked))
        workflow.addNode(new SaveImage(-1, nodeCombineMasks))

        return workflow
    }

    static getFaceToFaceWithoutControlnetImageWorkflow(imageName, prompt, negativePrompt, seed, steps = 32, cfg = 5, width = 1280, height = 1920, model = "realcartoonPixar_v6.safetensors", sampler = "dpmpp_3m_sde_gpu", scheduler = "exponential", denoise = 0.5, loras = [], controlNetModelName = "control_v11p_sd15_lineart_fp16.safetensors", extractedMasks = [{
        positivePrompt: "face",
        negativePrompt: "",
        precision: 0.5
    }], controlNetStrength = 0.5, controlNetStart = 0, controlNetEnd = 1, blurRadius = 10, blurSigmaFactor = 1) {
        const workflow = new Workflow()

        // Model + LoRA part
        const nodeModel = workflow.addNode(new ModelLoader(-1, model))
        let lastLora = nodeModel
        for (const lora of loras) {
            lastLora = workflow.addNode(new LoraLoader(-1, lastLora, lora.filename, lora.strength_model, lora.strength_clip))
        }

        // Prompt part
        const nodePositivePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, prompt))
        const nodeNegativePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, negativePrompt))

        // Loaded image preprocess
        const nodeLoadImage = workflow.addNode(new LoadImage(-1, imageName))

        // Image upscale
        const nodeUpscaleImage = workflow.addNode(new UpscaleImage(-1, nodeLoadImage, width, height))
        const nodeVAEEncode = workflow.addNode(new VAEEncode(-1, nodeUpscaleImage, nodeModel))

        // Sample
        const nodeSampler = workflow.addNode(new KSampler(-1, lastLora, nodePositivePrompt, nodeNegativePrompt, nodeVAEEncode, steps, cfg, sampler, scheduler, seed, denoise))
        const nodeVAEDecode = workflow.addNode(new VAEDecode(-1, nodeSampler, nodeModel))

        // Build mask image for original image
        const nodeMaskImageOriginal = this.buildMasksImage(workflow, nodeUpscaleImage, extractedMasks, blurRadius, blurSigmaFactor)
        // Build mask image for generated image
        const nodeMaskImageGenerated = this.buildMasksImage(workflow, nodeVAEDecode, extractedMasks, blurRadius, blurSigmaFactor)

        const nodeCombineMasks = workflow.addNode(new CombineMasks(-1, nodeMaskImageOriginal, nodeMaskImageGenerated, CombineOperation.UNION_MAX, true, false))

        const nodeConvertImageToMask = workflow.addNode(new ImageToMask(-1, nodeCombineMasks, ChannelType.RED))
        const nodeCompositeMasked = workflow.addNode(new ImageCompositeMasked(-1, nodeVAEDecode, nodeUpscaleImage, nodeConvertImageToMask))

        workflow.addNode(new SaveImage(-1, nodeCompositeMasked))

        return workflow
    }

    static getVisionWorkflow(imageName, prompt = "", model = "CogFlorence-2.1-Large", task = "more_detailed_caption", seed = null) {
        const workflow = new Workflow()

        const nodeLoadImage = workflow.addNode(new LoadImage(-1, imageName))
        const nodeFlorence2ModelLoader = workflow.addNode(new Florence2ModelLoader(-1, model))

        const nodeFlorence2Run = workflow.addNode(new Florence2Run(-1, nodeLoadImage, nodeFlorence2ModelLoader, prompt, task))

        const nodeShowText = workflow.addNode(new ShowText(-1, nodeFlorence2Run))

        return workflow
    }

    static getRefinerImageWorkflow(imageName, prompt, negativePrompt, seed, steps = 32, cfg = 5, width = 1280, height = 1920, model = "realcartoonPixar_v6.safetensors", sampler = "dpmpp_3m_sde_gpu", scheduler = "exponential", denoise = 0.5, loras = [], controlNetModelName = "control_v11p_sd15_lineart_fp16.safetensors", controlNetStrength = 0.5, controlNetStart = 0, controlNetEnd = 1) {
        const workflow = new Workflow()

        // Model + LoRA part
        const nodeModel = workflow.addNode(new ModelLoader(-1, model))
        let lastLora = nodeModel
        for (const lora of loras) {
            lastLora = workflow.addNode(new LoraLoader(-1, lastLora, lora.filename, lora.strength_model, lora.strength_clip))
        }

        // Prompt part
        const nodePositivePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, prompt))
        const nodeNegativePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, negativePrompt))

        // Loaded image preprocess
        const nodeLoadImage = workflow.addNode(new LoadImage(-1, imageName))
        const nodeAnimeLineArt = workflow.addNode(new AnimeLineArtPreprocessor(-1, nodeLoadImage, width < height ? width : height))
        const nodeControlNetLoader = workflow.addNode(new ControlNetLoader(-1, controlNetModelName))
        const nodeControlNetApply = workflow.addNode(new ControlNetApply(-1, nodePositivePrompt, nodeNegativePrompt, nodeControlNetLoader, nodeAnimeLineArt, controlNetStrength, controlNetStart, controlNetEnd))

        // Image upscale
        const nodeUpscaleImage = workflow.addNode(new UpscaleImage(-1, nodeLoadImage, width, height))
        const nodeVAEEncode = workflow.addNode(new VAEEncode(-1, nodeUpscaleImage, nodeModel))

        // Sample
        const nodeSampler = workflow.addNode(new KSampler(-1, lastLora, nodeControlNetApply, null, nodeVAEEncode, steps, cfg, sampler, scheduler, seed, denoise))
        const nodeVAEDecode = workflow.addNode(new VAEDecode(-1, nodeSampler, nodeModel))

        workflow.addNode(new SaveImage(-1, nodeVAEDecode))

        return workflow
    }

    static getDepthLineartImageWorkflow(imageName, prompt, negativePrompt, seed, steps = 32, cfg = 5, width = 1280, height = 1920, model = "realcartoonPixar_v6.safetensors", sampler = "dpmpp_3m_sde_gpu", scheduler = "exponential", denoise = 0.5, loras = [], controlNetModelName = "control_v11p_sd15_lineart_fp16.safetensors", controlNetStrength = 0.5, controlNetStart = 0, controlNetEnd = 1) {
        const workflow = new Workflow()

        // Model + LoRA part
        const nodeModel = workflow.addNode(new ModelLoader(-1, model))
        let lastLora = nodeModel
        for (const lora of loras) {
            lastLora = workflow.addNode(new LoraLoader(-1, lastLora, lora.filename, lora.strength_model, lora.strength_clip))
        }

        // Prompt part
        const nodePositivePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, prompt))
        const nodeNegativePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, negativePrompt))

        // Loaded image preprocess
        const nodeLoadImage = workflow.addNode(new LoadImage(-1, imageName))
        // Image upscale
        const nodeUpscaleImage = workflow.addNode(new UpscaleImage(-1, nodeLoadImage, width, height))

        // Controlnets
        const nodeAnimeLineArt = workflow.addNode(new AnimeLineArtPreprocessor(-1, nodeUpscaleImage, width < height ? width : height))
        const nodeDepthAnything = workflow.addNode(new DepthAnythingPreprocessor(-1, nodeUpscaleImage, width < height ? width : height))
        const nodeControlNetLoaderLineart = workflow.addNode(new ControlNetLoader(-1, controlNetModelName))
        const nodeControlNetLoaderDepth = workflow.addNode(new ControlNetLoader(-1, "control_v11f1p_sd15_depth_fp16.safetensors"))

        const nodeControlNetApplyLineart = workflow.addNode(new ControlNetApply(-1, nodePositivePrompt, nodeNegativePrompt, nodeControlNetLoaderLineart, nodeAnimeLineArt, controlNetStrength, controlNetStart, controlNetEnd))
        const nodeControlNetApplyDepth = workflow.addNode(new ControlNetApply(-1, nodeControlNetApplyLineart, null, nodeControlNetLoaderDepth, nodeDepthAnything, controlNetStrength, controlNetStart, controlNetEnd))

        const nodeVAEEncode = workflow.addNode(new VAEEncode(-1, nodeUpscaleImage, nodeModel))

        // Sample
        const nodeSampler = workflow.addNode(new KSampler(-1, lastLora, nodeControlNetApplyDepth, null, nodeVAEEncode, steps, cfg, sampler, scheduler, seed, denoise))
        const nodeVAEDecode = workflow.addNode(new VAEDecode(-1, nodeSampler, nodeModel))

        workflow.addNode(new SaveImage(-1, nodeUpscaleImage))
        workflow.addNode(new SaveImage(-1, nodeVAEDecode))
        workflow.addNode(new SaveImage(-1, nodeAnimeLineArt))
        workflow.addNode(new SaveImage(-1, nodeDepthAnything))

        return workflow
    }

    static getInpaintImageWorkflow(imageName, prompt, negativePrompt, seed, steps = 32, cfg = 5, width = 1280, height = 1920, model = "realcartoonPixar_v6.safetensors", sampler = "dpmpp_3m_sde_gpu", scheduler = "exponential", denoise = 0.5, loras = [], extractedMasks = [{
        positivePrompt: "face",
        negativePrompt: "",
        precision: 0.5
    }], maskGrowth = 6, blurRadius = 10, blurSigmaFactor = 1) {
        const workflow = new Workflow()

        // Model + LoRA part
        const nodeModel = workflow.addNode(new ModelLoader(-1, model))
        let lastLora = nodeModel
        for (const lora of loras) {
            lastLora = workflow.addNode(new LoraLoader(-1, lastLora, lora.filename, lora.strength_model, lora.strength_clip))
        }

        // Prompt part
        const nodePositivePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, prompt))
        const nodeNegativePrompt = workflow.addNode(new CLIPTextEncode(-1, lastLora, negativePrompt))

        // Loaded image preprocess
        const nodeLoadImage = workflow.addNode(new LoadImage(-1, imageName))

        // Image upscale
        const nodeUpscaleImage = workflow.addNode(new UpscaleImage(-1, nodeLoadImage, width, height))

        // Build mask image for original image
        const nodeMaskImageOriginal = this.buildMasksImage(workflow, nodeUpscaleImage, extractedMasks, blurRadius, blurSigmaFactor)

        const nodeConvertImageToMask = workflow.addNode(new ImageToMask(-1, nodeMaskImageOriginal, ChannelType.RED))

        // Sample
        const nodeVAEEncode = workflow.addNode(new VAEEncodeForInpaint(-1, nodeUpscaleImage, nodeModel, nodeConvertImageToMask, maskGrowth))
        const nodeSampler = workflow.addNode(new KSampler(-1, lastLora, nodePositivePrompt, nodeNegativePrompt, nodeVAEEncode, steps, cfg, sampler, scheduler, seed, denoise))
        const nodeVAEDecode = workflow.addNode(new VAEDecode(-1, nodeSampler, nodeModel))

        workflow.addNode(new SaveImage(-1, nodeVAEDecode))
        workflow.addNode(new SaveImage(-1, nodeMaskImageOriginal))

        return workflow
    }

    static buildMasksImage(workflow, nodeUpscaleImage, extractedMasks, blurRadius = 10, blurSigmaFactor = 1) {
        // Downscale and masking
        const nodeDownscaleImage = workflow.addNode(new UpscaleImageBy(-1, nodeUpscaleImage, 0.5, UpscaleMethod.BICUBIC))

        let nodesCombineMasks = []
        let lastCombineMask
        for (const textToMask of extractedMasks) {
            const nodeMaskByText = workflow.addNode(new MaskByText(-1, nodeDownscaleImage, textToMask.positivePrompt, textToMask.negativePrompt, textToMask.precision, false))
            const nodeCombineMasks = workflow.addNode(new CombineMasks(-1, nodeMaskByText, null, CombineOperation.ADD, true, false))
            nodesCombineMasks.push(nodeCombineMasks)
            lastCombineMask = nodeCombineMasks
        }

        function combineMasks() {
            if (nodesCombineMasks.length === 1) return
            const nodeCombineMasks = nodesCombineMasks.shift()
            if (nodeCombineMasks) {
                lastCombineMask = workflow.addNode(new CombineMasks(-1, lastCombineMask, nodeCombineMasks, CombineOperation.ADD, true, false))
                combineMasks()
            }
        }

        if (nodesCombineMasks.length > 1) combineMasks()

        const nodeBlur = workflow.addNode(new BlurImage(-1, lastCombineMask, blurRadius, blurSigmaFactor))
        return workflow.addNode(new ChangeChannelCount(-1, nodeBlur, ChannelCountType.RGB))
    }
}
