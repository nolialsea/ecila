import {ChannelType} from "discord.js";

export default class DiscordTypingService {
    /**
     * Show the bot as typing in the channel of the message
     * @param {import('discord.js').TextMessage} userMessage
     * @param {import('discord.js').TextChannel} channel
     * @returns {Promise<void>}
     */
    static sendTyping(userMessage, channel = null) {
        const chan = userMessage?.channel || channel
        if (chan.type === ChannelType.GuildText) {
            chan.sendTyping?.().catch(console.error)
        }
    }
}