import config from "../classes/Config.js";
import DiscordImageService from "./DiscordImageService.js";
import FunctionCallingService from "./FunctionCallingService.js";
import AIService from "./AiService.js";
import DiscordTypingService from "./DiscordTypingService.js";
import axios from "axios";

let sharp
try {
    sharp = await import(`sharp`)
} catch {

}

/**
 * @returns {string} URL of the API with the last / trimmed off
 */
function getApiUrl() {
    return config.ECILA_API_URL?.endsWith('/') ? config.ECILA_API_URL.substring(0, config.ECILA_API_URL.length - 1) : config.ECILA_API_URL
}
export default class DiscordService {
    static botsGeneratingMessageInChannel = {}

    /**
     * @param {string} authorName
     * @returns {string}
     */
    static replaceAlias(authorName) {
        const aliases = config.aliases ?? {}
        for (const [username, alias] of Object.entries(aliases)) {
            if (username === authorName) return alias
        }
        return authorName
    }

    static async fetchMessages(channel, messageCount = 200) {
        let result = []
        let lastMessageID = null

        for (let i = 0; i < messageCount; i += 100) {
            const req = await channel.messages.fetch(lastMessageID ? {
                limit: 100, before: lastMessageID
            } : {limit: 100}).catch(console.error)
            if (req) result = result.concat([...req])
            if (result.length === 0) return []
            lastMessageID = result[result.length - 1][0]
        }

        // discord's stupidity workaround, does nothing except update cache
        if (messageCount > 200) {
            await channel.messages.fetch({limit: 100})
        }

        return result
    }

    /**
     * @param {import('discord.js').TextMessage} channel
     * @param {?import('discord.js').TextMessage} lastMessage
     * @param {?BotConfig} bot
     * @returns {Promise<{author: ?string, content: string}[]>}
     */
    static async getChannelMessages(channel, lastMessage = null, bot) {
        let resetMessageFound = false

        const pulledMessages = await this.fetchMessages(channel)

        if (lastMessage) {
            pulledMessages.splice(0, pulledMessages.findIndex(m => {
                return m[0] === lastMessage.id.toString()
            }))
        }

        const pulledMessagesTemp = pulledMessages
            .filter(m => !!m)
            .map(m => m[1])
            .filter(m => {
                if (m.cleanContent?.toLowerCase() === '!reset' || m.cleanContent?.toLowerCase() === `!reset ${bot.name.toLowerCase()}`) {
                    resetMessageFound = true
                }
                return !resetMessageFound;
            })
            .filter(m => !['!', '#', '\#', '/', '. '].some(c => m.cleanContent?.toLowerCase().startsWith(`${c}`)))

        const allMessages = []
        for (const m of pulledMessagesTemp) {
            const files = m.attachments
                ?.map(a => a)
                .filter(a => ['.txt', '.json', '.md', '.html', '.css', '.js', '.mjs', '.ts', '.py', '.ahk', '.srt']
                    .some(f => a.name.includes(f)))
                .map(a => a?.url)
                .reverse() || []

            for (const file of files) {
                const req = await axios.get(file)
                    .catch(console.error)
                if (req) {
                    const filename = file.substring(file.lastIndexOf('/') + 1)?.replace(/\?[^]*/ig, '')
                    const sentBy = m.member?.displayName || m.author?.username ? `\nSent by: ${DiscordService.replaceAlias(m.member?.displayName || m.author?.username)}` : ''
                    const isJSON = file.includes('.json')
                    const newMessage = {
                        author: {username: m.member?.displayName || m.author?.username},
                        cleanContent: `[Attached file: ${filename}${sentBy}\nContent:\n\`\`\`\n${isJSON ? JSON.stringify(await req.data, null, 2) : await req.data}\n\`\`\`\n]`
                    }
                    allMessages.push(newMessage)
                } else {
                    console.log("FILE COULDN'T BE LOADED SOMEHOW :PAIN:")
                }
            }

            allMessages.push(m)
        }

        function countImageTokens(width, height) {
            let h = Math.ceil(height / 512);
            let w = Math.ceil(width / 512);
            let n = w * h;
            return 86 + 170 * n;
        }

        async function getImageDimensions(input) {
            let buffer;

            // Check if the input is a URL or Base64
            if (input.startsWith('http') || input.startsWith('https')) {
                const response = await axios.get(input, {
                    responseType: 'arraybuffer',
                });
                buffer = Buffer.from(response.data, 'binary');
            } else {
                // Remove data URL scheme if it exists
                if (input.startsWith('data')) {
                    input = input.split(',')[1];
                }
                buffer = Buffer.from(input, 'base64');
            }


            if (!sharp) {
                return {width: 512, height: 512}
            } else {
                const meta = await sharp(buffer).metadata();

                return {
                    width: meta.width, height: meta.height,
                };
            }

        }

        let firstImage = true

        return Promise.all(allMessages
            .map(m => {
                const isMessageAuthorABot = !!m.author?.bot

                const isGPTVisionCompatible = true && bot.mainBackend === "ChatGPT" && (
                    !!bot?.textGenParameters?.OpenAI?.model?.startsWith('gpt-4-vision') || !!bot?.textGenParameters?.OpenAI?.model?.startsWith('gpt-4o')
                )
                const isClaudeVisionCompatible = false && bot.mainBackend === "Claude"
                    && !!bot?.textGenParameters?.Claude?.model?.startsWith('claude-3')

                const shouldUseImages = !isMessageAuthorABot
                    && (isGPTVisionCompatible || isClaudeVisionCompatible)

                const hasImages = (m.attachments
                    ?.map(a => a)
                    .filter(a => ['.png', '.jpg', '.jpeg', '.webp', '.gif'].some(f => a.name.includes(f)))
                    .map(a => a?.url.replace(/\?[^]*/ig, '')) || []).length > 0

                let useHighRes = false
                if (hasImages && firstImage) {
                    useHighRes = true
                    firstImage = false
                }

                return {
                    author: this.replaceAlias(m.member?.displayName || m.author?.username || 'Guest'),
                    content: m.cleanContent.replace(/<\|([^|]+)\|>/g, (match, p1) => {
                        return `<${p1.trim()}>`;
                    }),
                    imageUrls: shouldUseImages ? (m.attachments
                        ?.map(a => a)
                        .filter(a => ['.png', '.jpg', '.jpeg', '.webp', '.gif'].some(f => a.name.includes(f)))
                        .map(a => a?.url) || []) : [],
                    useHighRes: false,
                }
            })
            .filter(m => !!m.content || (!!m.imageUrls && m.imageUrls.length > 0))
            .reverse()
            .map(async m => {
                const isProxy = (bot.mainBackend === "ChatGPT" && bot?.textGenParameters?.OpenAI?.URL) || (bot.mainBackend === "Claude" && bot?.textGenParameters?.Claude?.URL)
                const hasImages = m.imageUrls && m.imageUrls.length > 0
                const isCorrectModel = (bot.mainBackend === "ChatGPT" && (bot?.textGenParameters?.OpenAI?.model?.startsWith('gpt-4-vision') || bot?.textGenParameters?.OpenAI?.model?.startsWith('gpt-4o')))
                    || (bot.mainBackend === "Claude" && bot?.textGenParameters?.Claude?.model?.startsWith('claude-3'))
                const forceBase64 = isCorrectModel && isProxy && hasImages
                if (forceBase64) {
                    for (const id in m.imageUrls) {
                        if (typeof id === "string") {
                            const req = await axios.get(m.imageUrls[id], {responseType: 'arraybuffer'})
                                .catch(console.error)
                            if (!req) {
                                m.imageUrls[id] = null
                            }
                            const contentType = req.headers.getContentType()
                            m.imageUrls[id] = "data:" + contentType + ";base64," + Buffer.from(req.data).toString('base64');
                        }
                    }
                    m.imageUrls = m.imageUrls.filter(b => !!b)
                }

                if ((bot?.textGenParameters?.OpenAI?.model?.startsWith('gpt-4-vision') || bot?.textGenParameters?.OpenAI?.model?.startsWith('gpt-4o')) && m.imageUrls && m.imageUrls.length > 0) {
                    m.imageUrls = m.imageUrls.map(iu=>{
                        return iu
                    })
                }
                return m
            }))
    }


    /**
     *
     * @param {Message} userMessage
     * @param {BotConfig} bot
     * @param {?string} channelTopic
     * @param {?Array<{author: ?string, content: string}>} messages_
     * @param {?object} functions
     * @param {?object} oldOAIParameters
     * @returns {Promise<void>}
     */
    static async generateAndSendMessage(userMessage, bot, channelTopic = null, messages_ = null, functions = null, oldOAIParameters = null, useSharedFolder = false) {
        if (this.botsGeneratingMessageInChannel[userMessage.channelId]?.[bot.name]) {
            // userMessage?.react?.('⌛').catch(err => null)
            // return console.log(`Bot ${bot?.name} tried to generate a message in channel ${userMessage?.channel?.name} but is already generating another one`)
        }
        if (!this.botsGeneratingMessageInChannel[userMessage.channelId]) this.botsGeneratingMessageInChannel[userMessage.channelId] = {}
        this.botsGeneratingMessageInChannel[userMessage.channelId][bot.name] = true

        const messages = messages_ ?? await DiscordService.getChannelMessages(userMessage.channel, null, bot)
        await DiscordTypingService.sendTyping(userMessage)

        /*############################################################################################################*/
        let functionCallOrMessage
        if (bot?.textGenParameters?.OpenAI?.functions && bot?.textGenParameters?.OpenAI?.functions.length > 0) {
            functionCallOrMessage = await FunctionCallingService.functionCall(userMessage, bot, channelTopic, messages, bot?.textGenParameters?.OpenAI?.functions, bot?.textGenParameters?.OpenAI?.function_call || "auto", functions)
        } else {
            functionCallOrMessage = await AIService.generateMessage(bot, messages, channelTopic, null, false, userMessage.channel, useSharedFolder)
        }

        if (oldOAIParameters) {
            // Put back old settings
            bot.textGenParameters.OpenAI = oldOAIParameters
        }
        /*############################################################################################################*/

        if (functionCallOrMessage) {
            if (typeof functionCallOrMessage === 'string') {
                await DiscordImageService.generateImagesAndSendMessage(userMessage, functionCallOrMessage, bot)
            }
        } else {
            if (bot.debug) {
                await DiscordService.sendCommandError(userMessage, `# Oh no, variable \`functionCallOrMessage\` was null! :headstone:`)
            } else {
                await DiscordService.sendCommandError(userMessage)
            }
        }
        delete this.botsGeneratingMessageInChannel[userMessage.channelId][bot.name]
    }


    static async sendCommandError(userMessage, errorMessage = null) {
        await userMessage?.react?.('🐞').catch(() => null)
        if (errorMessage) {
            await userMessage?.reply(errorMessage).catch(console.error)
        }
    }

    static async sendCommandConfirmation(userMessage) {
        await userMessage?.react('✅').catch(() => null)
    }
}
