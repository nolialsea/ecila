import {TextChannel} from "discord.js"
import fs from "fs";
import JsonService from "./JsonService.js";
import AIService from "./AiService.js";
import Utils from "../Utils.js";
import DiscordMessageService from "./DiscordMessageService.js";
import config from "../classes/Config.js";

export default class BotDataService {
    static data = {}
    static lastSaveTimestamp = 0
    static needsSave = false

    /**
     * Retrieves the data for the specified bot in the given channel.
     * @param {BotConfig} bot - The bot object.
     * @param {TextChannel} channel - The channel object.
     * @returns {Object | undefined} - The data object for the bot in the specified channel, or undefined if it doesn't exist.
     */
    static getData(bot, channel) {
        if (this.data[channel?.id]?.[bot?.name])
            return this.data[channel?.id]?.[bot?.name]
        else
            return bot?.data
    }

    /**
     * Sets the data for the specified bot in the given channel.
     * @param {BotConfig} bot - The bot object.
     * @param {TextChannel} channel - The channel object.
     * @param {Object} data - The new data for the bot in the specified channel.
     */
    static setData(bot, channel, data) {
        // Check if data is a valid object
        try {
            JSON.stringify(data)
        } catch {
            return
        }

        if (!bot?.name || !channel?.id) return
        if (!this.data[channel.id]) {
            this.data[channel.id] = {}
        }
        this.data[channel.id][bot.name] = JSON.parse(JSON.stringify(data))
        this.needsSave = true
    }

    static reset(bot, channel) {
        if (this.data[channel?.id]?.[bot?.name]) {
            delete this.data[channel.id][bot.name]
            BotDataService.needsSave = true
        }
    }

    static save() {
        if (!fs.existsSync('./save')) {
            fs.mkdirSync('./save')
        }
        fs.writeFileSync(`./save/botData.json`, JSON.stringify(BotDataService.data, null, 2))
        BotDataService.needsSave = false
        BotDataService.lastSaveTimestamp = Date.now()
    }

    static load() {
        const data = JsonService.loadJsonFile('./save/botData.json', true)
        if (data) {
            BotDataService.data = data
        }
    }

    static async updateRevamped(userMessage, content, bot, messages, nbMessageToLoad = null) {
        if (!bot.data && !BotDataService.getData(bot, userMessage.channel)) return

        const context = (
            bot.selfEditionInstructionFile
            ?? config.defaultSelfEditionGenerationInstructions
        )?.startsWith('[') ?
            (bot.selfEditionInstructionFile
                ?? config.defaultSelfEditionGenerationInstructions)
            : fs.readFileSync('./configs/presets/generators/selfEdition_contextBottom.txt').toString()

        const contextBottom = context.replace('${DATA}', Utils.jsonToText(BotDataService.getData(bot, userMessage.channel), true, false))

        const tempBot = JSON.parse(JSON.stringify(bot))
        delete tempBot.openAIContext.bottom

        tempBot.context = tempBot.openAIContext

        if (tempBot.selfUpdateParameters) {
            if (tempBot.selfUpdateParameters?.mainBackend?.toLowerCase() === "localgpt") {
                tempBot.textGenParameters.OpenAI = tempBot.selfUpdateParameters
                tempBot.mainBackend = 'LocalGPT'
            } else if (tempBot.selfUpdateParameters.model?.toLowerCase()?.includes('claude')) {
                tempBot.textGenParameters.Claude = tempBot.selfUpdateParameters
                tempBot.mainBackend = 'Claude'
            } else if (tempBot.selfUpdateParameters.model?.toLowerCase()?.includes('gpt')) {
                tempBot.textGenParameters.OpenAI = tempBot.selfUpdateParameters
                tempBot.mainBackend = 'ChatGPT'
            }
        }

        const numberOfMessagesToLoad = nbMessageToLoad === null ? 12 : nbMessageToLoad
        const tempMessages = messages.slice(-numberOfMessagesToLoad)
        tempMessages.push({author: "System", content: contextBottom})

        // Prevents unnecessary generation if no data to update
        if (!tempBot.data || Object.keys(tempBot.data).length < 2) {
            return tempBot.data
        }

        let result = await AIService.generateMessage(tempBot, tempMessages, null, userMessage.channel)

        result = result?.replace('CHARACTER:', '')?.replace(/```/ig, '')?.replace(/\*/ig, '').trim()

        console.log(`DATA UPDATE:\n${result?.split('\n').map(s => `  ${s}`)?.join('\n')}`)

        if (result) {
            const lines = result.split('\n')
            result = lines.join('\n')

            // parse result back into json data
            const resultData = Utils.textToJson(result)

            let newData
            let dataDiff = {}

            if (bot.enableDataExpansion) {
                newData = JSON.parse(JSON.stringify(resultData))
                const oldData = JSON.parse(JSON.stringify(BotDataService.getData(bot, userMessage.channel)))

                for (const name of Object.keys(resultData)) {
                    if (oldData[name].toLowerCase() !== resultData[name].toLowerCase()) {
                        dataDiff[name] = resultData[name]
                    }
                    if (!['note'].some(s => name.toLowerCase() === s)
                        && (bot.enableNameChange || name.toLowerCase() !== "name")) {
                        newData[name] = resultData[name]
                    }
                }

                for (const name of Object.keys(oldData)) {
                    if (oldData[name] && !resultData[name]) {
                        newData[name] = oldData[name]
                    }
                }
            } else {
                const oldData = JSON.parse(JSON.stringify(BotDataService.getData(bot, userMessage.channel)))

                // only update existing data
                for (const name of Object.keys(oldData)) {
                    if (resultData[name]) {
                        if (oldData[name]?.toLowerCase?.() !== resultData[name].toLowerCase()) {
                            dataDiff[name] = resultData[name]
                        }
                        if (bot.enableNameChange || name.toLowerCase() !== "name") {
                            oldData[name] = resultData[name]
                        }
                    }
                }

                for (const name of Object.keys(resultData)) {
                    if (resultData[name] && !oldData[name]) {
                        dataDiff[name] = resultData[name]
                    }
                }

                if (bot.debug && !bot.enableNameChange && dataDiff['Name'] && dataDiff['Name'] !== oldData['Name']) {
                    await DiscordMessageService.sendMessage(userMessage, `#Update was cancelled because the name changed:\n\`\`\`\n${Utils.jsonToText(dataDiff)}\n\`\`\`\n`, bot, false, true)
                } else {
                    newData = oldData
                }
            }

            console.log("NEW DATA: "+JSON.stringify(newData, null, 2))
            console.log("DATA DIFF: "+JSON.stringify(dataDiff, null, 2))

            // save result data into bot data (update)
            if (newData) {
                BotDataService.setData(bot, userMessage.channel, newData)
            }

            if (bot.debug && dataDiff && !Utils.isObjectEmpty(dataDiff)) {
                await DiscordMessageService.sendMessage(userMessage, `#[DEBUG] Updated data:\n\`\`\`\n${Utils.jsonToText(dataDiff)}\n\`\`\`\n`, bot, false, true, [], null)
            }

            return newData ? dataDiff : null
        } else {
            userMessage?.react('🐞').catch(console.error)
        }
    }
}

BotDataService.load()

// Auto save loop
setInterval(() => {
    if (BotDataService.needsSave && Date.now() - BotDataService.lastSaveTimestamp > 10 * 1000) {
        BotDataService.save()
    }
}, 1000)
