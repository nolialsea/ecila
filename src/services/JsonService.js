import axios from "axios";
import fs from "fs";


export default class JsonService {
    static async loadJsonUrl(url, silentError = false) {
        try {
            const res = await axios.get(url);
            const data = res.data;
            if (!data || typeof data !== 'object' || Array.isArray(data)) {
                throw new Error('Loaded data is not a valid JSON object.');
            }
            return data;
        } catch (err) {
            if (!silentError) {
                console.error(err);
            }
        }
    }

    static loadJsonFile(path, silentError = false) {
        try {
            const res = fs.readFileSync(path).toString('utf-8')
            return JSON.parse(res)
        } catch (err) {
            if (!silentError) {
                console.warn(err);
            }
        }
    }
}