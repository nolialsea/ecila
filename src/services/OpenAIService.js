import axios from "axios";
import tiktoken from "@dqbd/tiktoken";

/**
 * Constants used within the OpenAIService.
 */
const MODEL_DEFAULT = "gpt-3.5-turbo-0613";
const API_KEY_DEFAULT = "";
const MAX_TOKEN_LENGTH_DEFAULT = 3200; // 4096 (max) - 1024 (generation space)

/**
 * Replaces placeholders in a template string with corresponding values.
 *
 * @param {string} template - The template string containing placeholders in the format ${placeholderName}.
 * @param {Array<{name: string, value: string}>} placeholders - An array of placeholder objects with name and value.
 * @returns {string} - The template string with placeholders replaced by their corresponding values.
 */
function replacePlaceholders(template, placeholders) {
    if (!template || !placeholders) return template;

    return template.replace(/\${([\w\s]+)}/g, (match, p1) => {
        const placeholder = placeholders.find(p => p.name === p1.trim());
        return placeholder?.value ?? match;
    });
}

/**
 * Service class for interacting with OpenAI's API.
 */
export default class OpenAIService {
    /**
     * Prepares messages by applying context, placeholders, and formatting suitable for OpenAI API.
     *
     * @param {Object} bot - The bot configuration containing context and data.
     * @param {Array<Object>} messages - The array of message objects to process.
     * @param {Object} OAIParameters - Additional parameters for OpenAI API.
     * @returns {Array<Object>} - The processed and trimmed array of messages ready for API consumption.
     */
    static prepareMessages(bot, messages, OAIParameters = {}) {
        // Filter and map messages to OpenAI's expected format
        const chat = messages
            .filter(Boolean)
            .map(message => OpenAIService._formatMessage(bot, message))
            .filter(m => m && m.content);

        // Build context from bot configuration
        const {topContext, middleContext, bottomContext} = OpenAIService._buildFullContext(bot);

        // Determine maximum prompt length based on model or default
        const maxPromptLength = OpenAIService._determineMaxPromptLength(OAIParameters);

        // Trim messages to fit within the token limit
        const trimmedMessages = OpenAIService.trimMessagesByMaxTokenLength(
            chat,
            bottomContext,
            maxPromptLength,
            topContext,
            middleContext
        );

        // Replace placeholders in the trimmed messages
        const finalMessages = OpenAIService._replacePlaceholdersInMessages(bot, trimmedMessages);

        // console.log(JSON.stringify(finalMessages, null, 4))

        return finalMessages;
    }

    /**
     * Executes a chat generation request to OpenAI's API.
     *
     * @param {Array<Object>} messages - The array of message objects to send.
     * @param {string} [apiKey=API_KEY_DEFAULT] - The API key for authentication.
     * @param {Object} [OAIParameters={}] - Additional parameters for the API request.
     * @returns {Promise<string|Object>} - The generated response content or function call.
     * @throws Will throw an error if the API request fails.
     */
    static async runGenerationChatGPT(messages, apiKey = API_KEY_DEFAULT, OAIParameters = {}) {

        // Set default model if not provided
        const parameters = {model: MODEL_DEFAULT, ...OAIParameters, messages};

        if (parameters.model === "deepseek-reasoner") {
            parameters.messages.pop()
            //parameters.messages[parameters.messages.length-1].role = 'user'
        }

        /*if (parameters.model?.startsWith(`o3`)) {
            parameters.messages = parameters.messages.map(m => {
                if (m.role === 'system') {
                    m.role = 'developer'
                }
                if (m.name?.toLowerCase() === 'system') {
                    m.name = null
                }
                return {role: m.role, content: m.content, name: m.name}
            })
        }*/

        // Remove irrelevant parameters
        delete parameters.enabled;

        // Configure API endpoint
        let apiUrl = 'https://api.openai.com/v1/chat/completions';
        if (parameters.URL) {
            apiUrl = `${parameters.URL.replace(/\/$/, '')}`;
            delete parameters.URL;
        }

        // Override API key if provided in parameters
        if (parameters.key) {
            apiKey = parameters.key;
            delete parameters.key;
        }

        if (parameters.format) {
            delete parameters.format;
        }

        if (parameters.max_prompt_length) {
            delete parameters.max_prompt_length;
        }

        // Setup axios configuration
        const config = {
            method: 'post',
            url: apiUrl,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${apiKey}`
            },
            data: parameters,
            timeout: 120000 // 2 minutes timeout
        };

        try {
            const response = await axios(config);

            if (parameters.stream) {
                // Handle streaming responses if needed
                if (Array.isArray(response.data)) {
                    return response.data.map(chunk => chunk.choices?.[0]?.delta?.content).join('');
                }
                return '';
            }

            // Return the content or function call from the first choice
            const choice = response.data?.choices?.[0];
            return choice?.message?.content?.trim() || choice?.message?.function_call || '';
        } catch (error) {
            console.error('Error during OpenAI API request:', JSON.stringify(error));
            throw error;
        }
    }

    /**
     * Counts the number of tokens in the given messages.
     *
     * @param {Array<Object>} messages - The array of message objects to count tokens for.
     * @param {boolean} [includeAllMessages=false] - Whether to include all messages in the count.
     * @returns {number} - The total number of tokens.
     */
    static countTokens(messages, includeAllMessages = false) {
        if (!Array.isArray(messages)) {
            messages = [messages];
        }

        const tokenizer = tiktoken.encoding_for_model("gpt-3.5-turbo");
        let numTokens = 0;

        for (const msg of messages) {
            numTokens += 4; // Every message starts with 4 tokens

            for (const [key, value] of Object.entries(msg)) {
                if (key === 'content') {
                    if (Array.isArray(value)) {
                        const text = value.find(v => v.type === 'text')?.text || '';
                        const imageCount = value.filter(v => v.type === 'image_url').length;
                        numTokens += tokenizer.encode(text).length || 0;
                        numTokens += imageCount * 90; // Approximate tokens for images
                    } else if (typeof value === "string") {
                        numTokens += tokenizer.encode(value).length || 0;
                    }
                } else if (typeof value === "string") {
                    numTokens += tokenizer.encode(value).length || 0;
                    if (key === "name") {
                        numTokens -= 1; // Adjust for names
                    }
                }
            }
        }

        if (includeAllMessages) {
            numTokens += 2; // Every reply is primed with 2 tokens
        }

        tokenizer.free();
        return numTokens;
    }

    /**
     * Trims messages to ensure the total token count does not exceed the maximum allowed.
     *
     * @param {Array<Object>} messages - The array of message objects to trim.
     * @param {Array<Object>} bottomContext - Context messages to include at the bottom.
     * @param {number} maxTokenLength - The maximum allowed token length.
     * @param {Array<Object>} [topContext=[]] - Context messages to include at the top.
     * @param {Array<Object>} [middleContext=[]] - Context messages to include in the middle.
     * @returns {Array<Object>} - The trimmed array of messages.
     * @throws Will throw an error if the context alone exceeds the maximum token length.
     */
    static trimMessagesByMaxTokenLength(messages, bottomContext, maxTokenLength, topContext = [], middleContext = []) {
        const allContextMessages = [...topContext, ...middleContext, ...bottomContext];
        const totalTokens = OpenAIService.countTokens([...allContextMessages, ...messages], true);
        let trimmedMessages = [];
        let numTokens = OpenAIService.countTokens(allContextMessages);

        // If all messages fit within the limit, insert contexts accordingly
        if (totalTokens <= maxTokenLength) {
            if (middleContext.length > 0) {
                messages.splice(Math.floor(messages.length / 2), 0, ...middleContext);
            }
            if (topContext.length > 0) {
                messages.unshift(...topContext);
            }
            if (bottomContext.length > 0) {
                messages.push(...bottomContext);
            }
            return messages;
        }

        // Ensure contexts do not exceed the maximum token length
        if (numTokens > maxTokenLength) {
            console.error({bottomContext, middleContext, topContext});
            throw new Error('Context is too large to fit within the maximum token length.');
        }

        // Trim messages from the end until the token limit is respected
        for (const message of [...messages].reverse()) {
            const tokenLength = OpenAIService.countTokens([message]);
            if (numTokens + tokenLength > maxTokenLength) {
                break;
            }
            trimmedMessages.unshift(message);
            numTokens += tokenLength;
        }

        // Re-insert contexts
        if (middleContext.length > 0) {
            trimmedMessages.splice(Math.floor(trimmedMessages.length / 2), 0, ...middleContext);
        }
        if (topContext.length > 0) {
            trimmedMessages.unshift(...topContext);
        }
        if (bottomContext.length > 0) {
            trimmedMessages.push(...bottomContext);
        }

        return trimmedMessages;
    }

    /**
     * Formats a single message object according to OpenAI's API requirements.
     *
     * @private
     * @param {Object} bot - The bot configuration containing data and context.
     * @param {Object} message - The message object to format.
     * @returns {Object|null} - The formatted message or null if invalid.
     */
    static _formatMessage(bot, message) {
        if (!message || typeof message.content !== 'string') return null;

        const isSystem = (message.content.startsWith('[') && message.content.endsWith(']'));
        const contentAuthorMatch = message.content.match(/^\(as ([^)]*)\)/i);
        const contentAuthor = contentAuthorMatch?.[1]?.trim()?.replace(/<\|endoftext\|>/gi, '<end-of-text>') || null;
        const content = message.content.replace(/^\(as [^)]*\)/i, '').trim().replace(/<\|endoftext\|>/gi, '<end-of-text>');

        // Prepare placeholders from bot data
        const placeholders = bot.data
            ? Object.entries(bot.data).map(([name, value]) => ({name, value}))
            : [];

        // Determine if the model supports vision
        const isVisionModel = bot.OAIParameters?.model?.startsWith('gpt-4-vision') || bot.OAIParameters?.model?.startsWith('gpt-4o');
        const images = isVisionModel && message.imageUrls
            ? message.imageUrls.map(url => ({
                type: 'image_url',
                image_url: typeof url === "string" ? {url, detail: 'low'} : {url: url.url, detail: 'low'}
            }))
            : [];

        // Replace placeholders in content
        const finalContent = placeholders.length > 0 ? replacePlaceholders(content, placeholders) : content;

        // Determine the role of the message
        const role = isSystem
            ? 'system'
            : (message.author === bot.name || contentAuthor === bot.name)
                ? 'assistant'
                : 'user';

        // Construct the new message object
        const newMessage = {
            role,
            content: isVisionModel ? [{type: 'text', text: finalContent}, ...images] : finalContent
        };

        // Assign a sanitized name if applicable
        if (contentAuthor || !isSystem) {
            let name = contentAuthor || message.author || "Anonymous";
            name = name.replace(/ /g, '_').replace(/'/g, '-').replace(/[^a-zA-Z0-9_-]/g, '').substr(0, 36) || "Anonymous";
            newMessage.name = name;
        }

        // Remove name property if role is not user or assistant
        if (!['user', 'assistant'].includes(newMessage.role)) {
            delete newMessage.name;
        }

        return newMessage;
    }

    /**
     * Builds the complete context from the bot's configuration.
     *
     * @private
     * @param {Object} bot - The bot configuration containing context.
     * @returns {Object} - An object containing top, middle, and bottom context arrays.
     */
    static _buildFullContext(bot) {
        const buildContext = (context) => {
            if (typeof context === "string") {
                return [{
                    role: 'system',
                    content: context.replace(/<\|endoftext\|>/gi, '<end-of-text>')
                }];
            } else if (Array.isArray(context)) {
                return context
                    .filter(c => typeof c === "string")
                    .map(c => ({
                        role: 'system',
                        content: c.replace(/<\|endoftext\|>/gi, '<end-of-text>')
                    }));
            }
            return [];
        };

        return {
            topContext: buildContext(bot?.context?.top || bot?.context),
            middleContext: buildContext(bot?.context?.middle || []),
            bottomContext: buildContext(bot?.context?.bottom || [])
        };
    }

    /**
     * Determines the maximum prompt length based on the provided parameters and model.
     *
     * @private
     * @param {Object} OAIParameters - The parameters provided for the OpenAI API.
     * @returns {number} - The maximum prompt length in tokens.
     */
    static _determineMaxPromptLength(OAIParameters) {
        if (OAIParameters.max_prompt_length) {
            return OAIParameters.max_prompt_length;
        }

        const model = OAIParameters.model || MODEL_DEFAULT;
        if (model.startsWith('gpt-3.5-turbo-16k')) {
            return 14000;
        } else if (model.startsWith('gpt-4')) {
            return 5800;
        }

        return MAX_TOKEN_LENGTH_DEFAULT;
    }

    /**
     * Replaces placeholders within messages using the bot's data.
     *
     * @private
     * @param {Object} bot - The bot configuration containing data.
     * @param {Array<Object>} messages - The array of message objects to process.
     * @returns {Array<Object>} - The messages with placeholders replaced.
     */
    static _replacePlaceholdersInMessages(bot, messages) {
        if (!bot.data || typeof bot.data !== "object") return messages;

        const placeholders = Object.entries(bot.data).map(([name, value]) => ({name, value}));

        return messages.map(message => {
            let content = Array.isArray(message.content) ? message.content[0]?.text : message.content;

            content = content.replace(/\${([\w\s]+)}/g, (match, placeholder) => {
                const p = placeholders.find(p => p.name === placeholder.trim());
                return p?.value ?? match;
            });

            if (Array.isArray(message.content)) {
                if (message.content[0]?.text) {
                    message.content[0].text = content;
                }
            } else {
                message.content = content;
            }

            return message;
        });
    }
}
