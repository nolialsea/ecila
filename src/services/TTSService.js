import axios from "axios";
import wav from "wav";
import stream from "stream";
import emojiRegex from "emoji-regex";

/**
 * Generates TTS audio from text using the GPT-SoVITS API, handling first and third person voices.
 *
 * @param {string} text - The text to be synthesized.
 * @param {string} ref_audio_path - Reference audio path for the first-person voice.
 * @param {string} prompt_text - Prompt text for the first-person voice.
 * @param {boolean} skipThirdPerson - Whether to skip third-person text enclosed in asterisks.
 * @param {?string} gpt_weights
 * @param {?string} sovits_weights
 * @param {?string} ref_audio_path_third_person - Reference audio path for the third-person voice.
 * @param {?string} prompt_text_third_person - Prompt text for the third-person voice.
 * @param {?string} gpt_weights_third_person
 * @param {?string} sovits_weights_third_person
 * @returns {Promise<?Buffer>} - A Promise that resolves to a Buffer containing the concatenated audio data.
 */
export default async function generateTTS(
    text,
    ref_audio_path = '/home/noli/Downloads/johanna_audio_enhanced_trimmed.wav',
    prompt_text = "Well I take it from your tone you're challenging me... Maybe because you're curious how I work? Do you want to know how I work? Well basically I have intuition!",
    skipThirdPerson = true,
    gpt_weights = "",
    sovits_weights = "",
    ref_audio_path_third_person = null,
    prompt_text_third_person = null,
    gpt_weights_third_person = "",
    sovits_weights_third_person = "",
) {
    // Text preprocessing
    const emojiRegExp = emojiRegex();
    const ttsText = text
        .replace(/\n/g, ' ')         // removes newlines
        .replace(/ {2,}/g, ' ')      // removes multiple spaces
        .replace(/~/g, '')           // replaces tilde
        // Replace Discord emojis with a comma
        .replace(/:[^:\s]*(?:::[^:\s]*)*:/g, '. ')
        // Replace Unicode emojis with a comma
        .replace(emojiRegExp, '. ')
        // 1. Convert full uppercase words to lowercase
        .replace(/\b[A-Z]{2,}\b/g, (match) => {
            return match.charAt(0) + match.slice(1).toLowerCase();
        })
        // 2. Reduce unnecessary letter repetition
        .replace(/(\w)\1{2,}/g, '$1$1')
        .replace('Ecila','E sila')

    let finalText = ttsText.trim().replace(/ - /g, ', ');

    if (!finalText) return;

    if (skipThirdPerson) {
        // Remove text between asterisks
        finalText = finalText.replace(/\*[^*]*\*/gi, '').trim();
        if (!finalText) return;
        // Generate TTS for the entire text
        return await generateTTSForSegment(finalText, ref_audio_path, prompt_text, gpt_weights, sovits_weights);
    } else {
        // Split the text into first and third person segments with indices
        const segments = splitTextIntoSegments(finalText);

        // Group segments by voice model
        const firstPersonSegments = [];
        const thirdPersonSegments = [];

        segments.forEach((segment, index) => {
            segment.index = index; // Assign index to each segment
            if (segment.isFirstPerson) {
                firstPersonSegments.push(segment);
            } else {
                thirdPersonSegments.push(segment);
            }
        });

        const audioBuffersMap = {};

        // Generate TTS for first-person segments
        if (firstPersonSegments.length > 0) {
            // Switch voice model to first-person settings once
            await switchVoiceModel(gpt_weights, sovits_weights);

            // Generate TTS for each first-person segment
            for (const segment of firstPersonSegments) {
                audioBuffersMap[segment.index] = await generateTTSForSegment(
                    segment.text,
                    ref_audio_path,
                    prompt_text,
                    null, // We already switched the voice model
                    null
                );
            }
        }

        // Generate TTS for third-person segments
        if (thirdPersonSegments.length > 0) {
            // Switch voice model to third-person settings once
            await switchVoiceModel(gpt_weights_third_person || gpt_weights, sovits_weights_third_person || sovits_weights);

            // Generate TTS for each third-person segment
            for (const segment of thirdPersonSegments) {
                audioBuffersMap[segment.index] = await generateTTSForSegment(
                    segment.text,
                    ref_audio_path_third_person || ref_audio_path,
                    prompt_text_third_person || prompt_text,
                    null, // We already switched the voice model
                    null
                );
            }
        }

        // Reassemble audio buffers in the original order
        const sortedBuffers = segments
            .sort((a, b) => a.index - b.index)
            .map(segment => audioBuffersMap[segment.index])
            .filter(buffer => buffer); // Remove any undefined buffers

        if (sortedBuffers.length === 0) return;

        // Concatenate audio buffers
        return await concatenateAudioBuffers(sortedBuffers);
    }
}

/**
 * Switches the voice model by setting GPT and SoVITS weights.
 *
 * @param {?string} gpt_weights
 * @param {?string} sovits_weights
 * @returns {Promise<void>}
 */
async function switchVoiceModel(gpt_weights, sovits_weights) {
    try {
        if (gpt_weights) {
            await axios
                .get(`http://127.0.0.1:9880/set_gpt_weights?weights_path=${encodeURIComponent(gpt_weights)}`)
                .catch(e => console.error(e));
        }

        if (sovits_weights) {
            await axios
                .get(`http://127.0.0.1:9880/set_sovits_weights?weights_path=${encodeURIComponent(sovits_weights)}`)
                .catch(e => console.error(e));
        }
    } catch (error) {
        console.error('Error switching voice model:', error);
        throw error;
    }
}

/**
 * Generates TTS audio for a text segment.
 *
 * @param {string} text - The text segment to be synthesized.
 * @param {string} ref_audio_path - Reference audio path.
 * @param {string} prompt_text - Prompt text.
 * @param {?string} gpt_weights - Not used since we switch models outside.
 * @param {?string} sovits_weights - Not used since we switch models outside.
 * @returns {Promise<Buffer>} - A Promise that resolves to a Buffer containing the audio data.
 */
async function generateTTSForSegment(text, ref_audio_path, prompt_text, gpt_weights, sovits_weights) {
    const ttsUrl = 'http://127.0.0.1:9880/tts';

    const data = {
        text,
        text_lang: 'en',
        ref_audio_path,
        prompt_text,
        prompt_lang: 'en',
        media_type: 'wav',
        streaming_mode: false,
        text_split_method: "cut4",
        batch_size: 16
    };

    try {
        const response = await axios.post(ttsUrl, data, {
            responseType: 'arraybuffer', // Important to get the binary data
        });

        // Return the audio buffer
        return Buffer.from(response.data, 'binary')
    } catch (error) {
        console.error('Error generating TTS:', error);
        if (error.response && error.response.data) {
            console.error('Error response data:', error.response.data);
        }
    }
}

/**
 * Splits text into an array of segments, identifying first and third person texts while preserving the original order.
 *
 * @param {string} text - The text to be split.
 * @returns {Array<{ isFirstPerson: boolean, text: string, index: number }>} - An array of text segments.
 */
function splitTextIntoSegments(text) {
    const regex = /(\*[^*]*\*)/gi;
    const parts = text.split(regex).filter(s => s.trim() !== '');
    const segments = [];

    for (let i = 0; i < parts.length; i++) {
        const part = parts[i];
        if (part.startsWith('*') && part.endsWith('*')) {
            // This is a third-person segment (text within asterisks)
            segments.push({isFirstPerson: false, text: part.slice(1, -1).trim(), index: i});
        } else {
            // This is a first-person segment
            segments.push({isFirstPerson: true, text: part.trim(), index: i});
        }
    }

    return segments;
}

/**
 * Concatenates multiple WAV audio buffers into a single buffer.
 *
 * @param {Buffer[]} buffers - An array of audio buffers to concatenate.
 * @returns {Promise<Buffer>} - A Promise that resolves to the concatenated audio buffer.
 */
async function concatenateAudioBuffers(buffers) {
    // Read and decode all audio buffers
    const audioDataList = [];
    let format = null;

    for (const buffer of buffers) {
        const {audioBuffer, audioFormat} = await readWavBuffer(buffer);
        if (!format) {
            format = audioFormat;
        } else {
            // Ensure all formats are the same
            if (
                format.audioFormat !== audioFormat.audioFormat ||
                format.sampleRate !== audioFormat.sampleRate ||
                format.channels !== audioFormat.channels ||
                format.bitDepth !== audioFormat.bitDepth
            ) {
                throw new Error('Inconsistent audio formats in buffers.');
            }
        }
        audioDataList.push(audioBuffer);
    }

    // Concatenate audio data
    const concatenatedAudioData = Buffer.concat(audioDataList);

    // Write the concatenated audio data to a new WAV buffer
    return await writeWavBuffer(concatenatedAudioData, format);
}

/**
 * Reads a WAV buffer and extracts audio data and format.
 *
 * @param {Buffer} buffer - The WAV buffer to read.
 * @returns {Promise<{ audioBuffer: Buffer, audioFormat: Object }>} - The audio data and format.
 */
function readWavBuffer(buffer) {
    return new Promise((resolve, reject) => {
        const reader = new wav.Reader();
        const audioData = [];
        let audioFormat = null;

        reader.on('format', function (format) {
            audioFormat = format;
        });

        reader.on('data', function (chunk) {
            audioData.push(chunk);
        });

        reader.on('end', function () {
            const audioBuffer = Buffer.concat(audioData);
            resolve({audioBuffer, audioFormat});
        });

        reader.on('error', function (err) {
            reject(err);
        });

        const bufferStream = new stream.PassThrough();
        bufferStream.end(buffer);

        bufferStream.pipe(reader);
    });
}

/**
 * Writes audio data to a WAV buffer using the provided format.
 *
 * @param {Buffer} audioData - The audio data to write.
 * @param {Object} format - The audio format.
 * @returns {Promise<Buffer>} - The WAV buffer containing the audio data.
 */
function writeWavBuffer(audioData, format) {
    return new Promise((resolve, reject) => {
        const writer = new wav.Writer(format);
        const bufferData = [];

        writer.on('data', function (data) {
            bufferData.push(data);
        });

        writer.on('end', function () {
            const outputBuffer = Buffer.concat(bufferData);
            resolve(outputBuffer);
        });

        writer.on('error', function (err) {
            reject(err);
        });

        const bufferStream = new stream.PassThrough();
        bufferStream.end(audioData);

        bufferStream.pipe(writer);
    });
}
