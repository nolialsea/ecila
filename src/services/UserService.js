import config from "../classes/Config.js";

export default class UserService{
    /**
     * Checks weather this user has admin privileges
     * @param {Message<boolean>} userMessage
     * @returns {boolean}
     */
    static isAdmin(userMessage) {
        const memberRoles = userMessage.member?.roles?.valueOf()
        const roles = memberRoles
            ?.filter(r => r.name !== '@everyone')
            ?.filter(r => r.name !== 'everyone')
            ?.map(r => `${r.name}`)
        const isWhitelistedId = Object.keys(config.discordAdminIds).some(aid => userMessage.author.id.toString() === aid)
        const isWhitelistedRole = config.discordAdminRoleNames.some(rn => roles?.includes(rn))
        return isWhitelistedRole || isWhitelistedId
    }
}