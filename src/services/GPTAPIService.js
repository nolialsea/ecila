import axios from "axios";
import PromptBuilderService, {Format} from "./PromptBuilderService.js";
import TokenizerService from "./TokenizerService.js";

const RATE_LIMIT_PER_MINUTE = 9
const MIN_TIME_BETWEEN_REQUESTS = (60 / RATE_LIMIT_PER_MINUTE) * 1000
let lastInferenceTimestamp = Date.now()

async function throttle() {
    const delta = Date.now() - lastInferenceTimestamp

    if (delta < MIN_TIME_BETWEEN_REQUESTS) {
        const timeToWait = MIN_TIME_BETWEEN_REQUESTS - delta
        await new Promise(r => setTimeout(r, timeToWait))
    }

    lastInferenceTimestamp = Date.now()
}

async function tryUntil(callback, nbTries = 5) {
    let result = null

    for (let i = 0; i < nbTries; i++) {
        result = await callback()
        if (result && result?.trim?.()) {
            break
        }
    }

    return result
}

async function doRequest(parameters, params, apiKey, format) {
    return await tryUntil(async () => {
        let result
        try {
            if (params.stream) {
                // Using streaming response
                result = await GPTAPIService.handleStreamingResponse(parameters['URL'], params, apiKey);
            } else {
                // Standard response
                result = await axios.post(parameters['URL'], params, {
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization": `Bearer ${apiKey}`,
                        "Accept": "application/json"
                    },
                    timeout: 5 * 60 * 1000
                })
            }
        } catch (error) {
            console.error(error)
            throw error
        }

        let generated

        if (result?.data?.choices?.[0]?.text) {
            generated = result.data.choices[0].text.replace?.(format.stopToken, '').trim()
            if (params['stop']) {
                if (typeof params['stop'] !== "string") {
                    params['stop'].map(s => {
                        generated = generated.replace(s, '')
                    })
                }else{
                    generated = generated.replace(params['stop'], '')
                }
            }

            if (generated.endsWith('<')) {
                generated = generated.substring(0, generated.length - 1)
            }

            if (format.outputRegexReplace) {
                for (const reg of format.outputRegexReplace) {
                    generated = generated.replace(new RegExp(reg[0], 'g'), reg[1])
                }
            }
        }

        if (!generated || !generated.trim()){
            console.log(`Empty response detected.`)
        }else{
            console.log(`Generated: `+generated)
        }

        return generated
    })
}

export default class GPTAPIService {
    static async generate(bot, history, context, instructions, parameters = {}) {
        let apiKey

        const params = {
            ...parameters
        }

        if (params['enabled']) {
            delete params['enabled']
        }

        if (params['URL']) {
            delete params['URL']
        }

        if (params['mainBackend']) {
            delete params['mainBackend']
        }

        if (params?.['key']) {
            apiKey = params['key']
            delete params['key']
        }

        let maxPromptLength = 8192
        if (params?.['max_prompt_length']) {
            maxPromptLength = params['max_prompt_length']
            delete params['max_prompt_length']
        }

        let maxLength = 512
        if (params?.['max_length']) {
            maxLength = params['max_length']
        }
        if (params?.['max_tokens']) {
            maxLength = params['max_tokens']
        }

        const tokenizer = await TokenizerService.get('llama3')
        //const tokenizer = await TokenizerService.get('huggingface_tokenizer', 'meta-llama/Meta-Llama-3-70B-Instruct') // TODO: fetch model name from configs

        let format = new Format()
        if (params?.['format']) {
            format = params['format']
            delete params['format']
        }

        const placeholders = bot.data ? Object.entries(bot.data)?.map(([n, v]) => {
            return {name: n, value: v}
        }) : []

        function replacePlaceholders(template, placeholders) {
            return template?.replace(/\${([\w\s&-\/]+)}/g, (match) => {
                const inner = match.substring(2, match.length - 1)
                const p = placeholders?.find(p => p.name === inner)
                const value = p?.value;
                return value !== undefined ? value : match;
            });
        }

        let instructionsWithData = replacePlaceholders(instructions, placeholders)
        let contextWithData = replacePlaceholders(context, placeholders)

        params.prompt = await PromptBuilderService.buildPrompt(contextWithData, instructionsWithData, history, format, bot, tokenizer, maxPromptLength, maxLength)
        params.seed = Math.floor(Math.random() * 1000000)

        await throttle()

        try {
            return await doRequest(parameters, params, apiKey, format)
        } catch (error) {
            console.error(error)
            // throw new Error(JSON.stringify(error.response?.data || {error: "Unknown error, please contact EvilNoli to tell him you got that message and how!"}))
        }
    }

    static async handleStreamingResponse(URL, params, apiKey) {
        try {
            const stream = await axios({
                method: 'post',
                url: URL,
                data: params,
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization": `Bearer ${apiKey}`,
                    "Accept": "application/json"
                },
                responseType: 'stream',
                timeout: 5 * 60 * 1000
            });

            return new Promise((resolve, reject) => {
                let result = '';
                stream.data.on('data', chunk => {
                    result += chunk.toString();
                });

                stream.data.on('end', () => {
                    const finalResult = this.buildResult(result);
                    resolve({ data: { choices: [{ text: finalResult }] } });
                });

                stream.data.on('error', err => {
                    console.error(err);
                    // Decide whether to resolve with partial data or reject
                    // For now, let's reject the error
                    reject(err);
                });
            });
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    static buildResult(result){
        const fullResult = result.replace(/data: /gi, '')
        const resultArray = fullResult.split(/\n\s*\n/)

        let finalResult = ''

        for (const r of resultArray) {
            try {
                const item = JSON.parse(r.trim())
                const text = item.choices?.[0]?.text
                if (text) {
                    finalResult += text
                } else {
                }
            } catch {
            }
        }
        return finalResult
    }
}
