export default class TimeoutService {
    static smartAnswerTimeout = {}
    static answerTimeout = {}

    /**
     * @param {import('discord.js').TextMessage} userMessage
     * @returns {void}
     */
    static clearSmartAnswerTimeout(userMessage) {
        clearTimeout(TimeoutService.smartAnswerTimeout[userMessage?.channelId])
    }

    /**
     * @param {import('discord.js').TextMessage} userMessage
     * @param {import('discord.js').Client} client
     * @returns {void}
     */
    static clearAnswerTimeout(userMessage, client) {
        clearTimeout(TimeoutService.answerTimeout[userMessage?.channelId + client.application.id])
    }

    /**
     * @param {import('discord.js').TextMessage} userMessage
     * @param {Client} client
     * @returns {void}
     */
    static clearTimeout(userMessage, client) {
        this.clearSmartAnswerTimeout(userMessage)
        this.clearAnswerTimeout(userMessage, client)
    }
}