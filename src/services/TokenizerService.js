import llama3Tokenizer from 'llama3-tokenizer-js';
import {AutoTokenizer} from '@xenova/transformers';

/**
 * Service for managing and caching tokenizers.
 */
export default class TokenizerService {
    /**
     * Cache for storing loaded tokenizers.
     * @type {Object.<string, any>}
     */
    static cache = {};

    /**
     * Returns the tokenizer for a given Hugging Face model.
     *
     * @param {string} modelName - The name of the Hugging Face model.
     * @returns {Promise<any>} - The tokenizer instance.
     */
    static async getHuggingFaceTokenizer(modelName) {
        if (this.cache[modelName]) {
            return this.cache[modelName];
        }

        const tokenizer = await AutoTokenizer.from_pretrained(modelName);
        this.cache[modelName] = tokenizer;
        return tokenizer;
    }

    /**
     * Returns the tokenizer based on the specified name and optional model.
     *
     * @param {string} tokenizerType - The type of the tokenizer.
     * @param {?string} modelName - The optional name of the model for Hugging Face tokenizers.
     * @returns {Promise<?Object>} - The tokenizer instance.
     */
    static async get(tokenizerType, modelName = null) {
        const cacheKey = modelName ?? (tokenizerType + (modelName ? `-${modelName}` : ''));

        if (this.cache[cacheKey]) {
            return this.cache[cacheKey];
        }

        let tokenizer;
        if (tokenizerType.toLowerCase() === 'llama3') {
            tokenizer = llama3Tokenizer;
        } else if (tokenizerType.toLowerCase() === 'huggingface_tokenizer' && modelName) {
            tokenizer = await this.getHuggingFaceTokenizer(modelName);
        }

        this.cache[cacheKey] = tokenizer;
        return tokenizer;
    }

    /**
     * Utility function to get the token length of a string using a tokenizer.
     *
     * @param {string} text - The text to tokenize.
     * @param {Object} tokenizer - The tokenizer instance.
     * @returns {number} - The token length of the text.
     */
    static getTokenLength(text, tokenizer) {
        return tokenizer.encode(text).length;
    }
}
