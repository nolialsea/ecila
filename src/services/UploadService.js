import axios from "axios";

export default class UploadService {
    static async uploadImage(URL = "http://localhost:8188/", imageBuffer) {
        const body = new FormData()
        const file = new File([imageBuffer], `${Date.now()}.png`)
        body.append("image", file)
        const result = await axios.post(`${URL}upload/image`, body)
        return result?.data?.name
    }

    static async uploadVideo(URL = "http://localhost:8188/", videoBuffer, extension = "webm") {
        const body = new FormData();
        const file = new File([videoBuffer], `${Date.now()}.${extension}`);
        body.append("image", file);
        // If you have a dedicated `upload/video` endpoint:
        // const result = await axios.post(`${URL}upload/video`, body);
        // If not, fallback to `upload/image` as in your original code:
        const result = await axios.post(`${URL}upload/image`, body);
        return result?.data?.name;
    }
}
