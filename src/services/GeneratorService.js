import DiscordService from "./DiscordService.js";
import AIService from "./AiService.js";
import DiscordImageService from "./DiscordImageService.js";
import DiscordMessageService from "./DiscordMessageService.js";
import DiscordTypingService from "./DiscordTypingService.js";

export default class GeneratorService {
    static async generator(userMessage, message, bot, json, removeNewlines = false) {
        if (!json) {
            json = {context: {}, examples: []}
        }

        await DiscordService.sendCommandConfirmation(userMessage)
        await DiscordTypingService.sendTyping(userMessage)

        const content = (await AIService.generatorChatGPT(message, json, {}, bot)) || `ERROR`
        const text = (removeNewlines ? content?.replace?.(/\n/g, ' ') : content) || `ERROR`
        await DiscordMessageService.sendMessage(
            userMessage,
            text.includes('```') ? `# Result:\n${text}` : '# Result:\n```\n' + text + '\n```',
            bot,
            true,
            true,
            []
        )
    }

    static async generatorToImage(userMessage, message, bot, json, removeNewlines = false) {
        if (!json) {
            json = {context: {}, examples: []}
        }

        await DiscordService.sendCommandConfirmation(userMessage)
        await DiscordTypingService.sendTyping(userMessage)

        const content = (await AIService.generatorChatGPT(message, json, {}, bot)) || `ERROR`
        const text = (removeNewlines ? content?.replace?.(/\n/g, ' ') : content) || `ERROR`

        if (text && text !== 'ERROR') {
            const file = await DiscordImageService.getNAIImageAttachment(text, true)

            await DiscordMessageService.sendMessage(
                userMessage,
                text.includes('```') ? `# Result:\n${text}` : '# Result:\n```\n' + text + '\n```',
                bot,
                true,
                true,
                [file]
            )
        }

    }
}