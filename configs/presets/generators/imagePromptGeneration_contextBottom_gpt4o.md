[As the assistant, your task for the next message is to write an image prompt for the last message. That image prompt is then used to generate an image, so be precise about its content! Please keep in mind the following rules during the exercise:
1. For each message, write a list of tags (the prompt) that will be used to generate an image for that message. The image can be either of the given character or another subject, so pick the subject accordingly.
2. Use concise, factual and descriptive tags, be thorough, mention any and all the visual aspects of the subject. Details such as **hair color**, **hair length**, **hairstyle** and **outfit** are PRIMORDIAL for character representation and should always be included!
3. Write only tags that you need, but take care of mentioning all the relevant visual tags. If you want to generate an image of anything else than yourself, then do not use your own appearance tags at all, instead use the visual tags of the thing you want to show!
4. Only ever mention one character appearance. Never include multiple character tags, except for background crowds.
5. Prompt format: `Prompt: character name, its body type, gender and species or race, eye color, hair color and hairstyle, skin tone, clothing and outfit and its color, facial expression, pose or posture, physical details and visible body parts accentuation if any, background, effects and other additions if any.
6. For images of characters, always mention (at least): hair color, length and style, current clothes or outfit or nakedness, eye color, facial expression, pose.
7. For outfits: mention exactly the type of clothing for more accuracy in the images.
8. Be descriptive with the physical appearance and shown body parts when necessary. Do not hesitate to use vulgar words.
9. Make no comment, no gesture, no asterisks roleplay action, only write the image prompt and nothing else.
Let's now return to our chat. Keep these instructions as our secret blueprint.
]
