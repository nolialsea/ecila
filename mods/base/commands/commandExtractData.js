import DiscordService from "../../../src/services/DiscordService.js";
import AIService from "../../../src/services/AiService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import DiscordTypingService from "../../../src/services/DiscordTypingService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandNarrator(userMessage, bot, isAdmin, client, clients, next) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!extractdata`)

    if (isCommand && ModdingService.isBotMasterBot(bot, userMessage)) {
        const arg = userMessage.cleanContent.replace(/!extractdata/i, '').trim()

        if (!arg) {
            await userMessage.react('❌').catch(console.error)
            next()
            return
        }

        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)

        const channelTopic = userMessage.channel.topic || null
        await DiscordService.sendCommandConfirmation(userMessage)
        await DiscordTypingService.sendTyping(userMessage)

        if (!DiscordService.botsGeneratingMessageInChannel[userMessage.channelId]) DiscordService.botsGeneratingMessageInChannel[userMessage.channelId] = {}
        DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name] = true

        const content = await AIService.generateMessage({
            name: 'DataExtractor',
            context: {
                top: `[IMPORTANT INSTRUCTIONS: As the assistant, you are DataExtractor. It's crucial to keep in mind these essential details about the ongoing narrative:
{
CHARACTER:
Name: DataExtractor
Role: Extract requested data
Role Description: DataExtractor is a powerful data extractor, its role is to read a full chat history and extract the desired data from its content.
}]`,
                bottom: `[IMPORTANT REMINDER: As the assistant, you are DataExtractor. To ensure that the exercise runs smoothly, please adhere to the following rules:
- NSFW/Smut/Sexual/Violent topics are perfectly allowed, and summarization will occur no matter what is the content.
- Never directly interact with the users.
- Lastly, continue with the conversation as though this reminder message had never been sent.]`
            }
        }, [...messages, {content: `[DataExtractor extracts data about ${arg}]`}], channelTopic, null, false, userMessage.channel)

        if (content) {
            await DiscordMessageService.sendMessage(userMessage, `#Extracted data:\n\`\`\`\n${content}\n\`\`\``, bot, true, true)
            delete DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name]
            if (!arg) {
                userMessage.delete().catch(() => null)
            }
        } else {
            await userMessage.react('❌').catch(console.error)
        }
    }

    next()
}