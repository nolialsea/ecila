import DiscordService from "../../../src/services/DiscordService.js";
import TimeoutService from "../../../src/services/TimeoutService.js";
import BotDataService from "../../../src/services/BotDataService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandReset(userMessage, bot, isAdmin, client, clients, next) {
    const messageContent = userMessage.cleanContent?.toLowerCase()?.trim();
    const isTrigger = messageContent?.startsWith('!reset');

    if (isTrigger) {
        // Extract the target bot name
        const [_, ...targetBotNameParts] = messageContent.split(' ');
        const targetBotName = targetBotNameParts.join(' ');

        // Only reset bots if no name is provided or the names match (allowing for spaces and case differences)
        if (!targetBotName || bot.name.toLowerCase().includes(targetBotName) || client.application.id === targetBotName) {
            if (TimeoutService.smartAnswerTimeout[userMessage?.channelId]) clearTimeout(TimeoutService.smartAnswerTimeout[userMessage?.channelId]);
            if (TimeoutService.answerTimeout[userMessage?.channelId + client.application.id]) clearTimeout(TimeoutService.answerTimeout[userMessage?.channelId + client.application.id]);
            DiscordService.sendCommandConfirmation(userMessage).then();
            BotDataService.reset(bot, userMessage.channel);
        }
    }
    next();
}