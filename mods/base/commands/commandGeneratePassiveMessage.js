import DiscordService from "../../../src/services/DiscordService.js";
import TimeoutService from "../../../src/services/TimeoutService.js";
import SmartAnswerService from "../../../src/services/SmartAnswerService.js";
import {ChannelType} from "discord.js";
import config from "../../../src/classes/Config.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandGeneratePassiveMessage(userMessage, bot, isAdmin, client, clients, next) {
    const channelTopic = userMessage?.channel?.topic || null
    const channelName = userMessage?.channel?.name
    const isVisibleMessage = !(userMessage?.cleanContent?.startsWith?.('!')) && !(userMessage?.cleanContent?.startsWith?.('#')) && !(userMessage?.cleanContent?.startsWith?.('//')) && !(userMessage?.cleanContent?.startsWith?.('. ')) && userMessage?.cleanContent

    const isAllowedChannel = bot.monoBotChannels?.some(c => c === channelName)
        || userMessage.channel.type === ChannelType.DM && config.enableDirectMessages && config.discordAdminIds[userMessage.author.id.toString()]

    const isComingFromBotItself = client.application.id === userMessage.author.id
    if (!isVisibleMessage) return next()
    if ((config.bannedUsers && config.bannedUsers.includes(userMessage?.author?.id)) || (bot.bannedUsers && bot.bannedUsers.includes(userMessage?.author?.id))) {
        return next()
    }

    if (isAllowedChannel && isComingFromBotItself) {
        if (bot.monoBotSmartAnswers) {
            SmartAnswerService.sendSmartAnswer(userMessage, bot, channelTopic, true, clients).then()
        }
    } else if (isAllowedChannel) {
        if (TimeoutService.answerTimeout[userMessage?.channelId + client.application.id]) clearTimeout(TimeoutService.answerTimeout[userMessage?.channelId + client.application.id])
        const cooldown = bot.answerCooldown ?? config.answerCooldown ?? 15000
        TimeoutService.answerTimeout[userMessage?.channelId + client.application.id] = setTimeout(() => DiscordService.generateAndSendMessage(userMessage, bot, channelTopic, null, null, null, true), cooldown)
    }

    next()
}
