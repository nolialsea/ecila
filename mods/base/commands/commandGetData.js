import Utils from "../../../src/Utils.js";
import BotDataService from "../../../src/services/BotDataService.js";
import {AttachmentBuilder, ChannelType} from "discord.js";
import config from "../../../src/classes/Config.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandGetData(userMessage, bot, isAdmin, client, clients, next) {
    const channelName = userMessage?.channel?.name
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!getdata')
    const optionalBotName = userMessage?.cleanContent.match(/!getdata ?([a-zA-Z0-9_\-' ]*)?/i)?.[1]?.trim()
    const propertyName = userMessage.cleanContent.match(/!getdata ?([a-zA-Z0-9_\-' ]*)? ["“]([^"”]+)["”]/i)?.[2]?.trim()

    const isAllowedChannel = bot.monoBotChannels?.some(c => c === channelName)
        || userMessage.channel.type === ChannelType.DM && config.enableDirectMessages && config.discordAdminIds[userMessage.author.id.toString()]
        || optionalBotName === bot.name

    if (isTrigger && isAllowedChannel) {
        userMessage?.react('✅').catch(console.error)
        if (propertyName) {
            userMessage?.reply(`#Data:\n\`\`\`\n${BotDataService.getData(bot, userMessage.channel)[propertyName]}\n\`\`\`\n#To set your own data:\n\`\`\`\n!setData ${bot.name} "${propertyName}" ${BotDataService.getData(bot, userMessage.channel)[propertyName]}\n\`\`\`\n`).catch(() => {
                DiscordMessageService.sendMessage(userMessage, `#Data:\n\`\`\`\n!setData ${bot.name} "${propertyName}" ${BotDataService.getData(bot, userMessage.channel)[propertyName]}\n\`\`\`\n`, bot, true, true)
            })
        } else {
            const attachment = new AttachmentBuilder(Buffer.from(JSON.stringify(BotDataService.getData(bot, userMessage.channel), null, 4)), {name: `data_${optionalBotName}_${Date.now()}.json`})
            DiscordMessageService.sendMessage(userMessage, `#Data attached below. Load back with \`!setData ${bot.name}\` and attaching the file.`, bot, true, true, [attachment], null, null, true)
        }
    }

    next()
}
