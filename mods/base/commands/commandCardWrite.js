import DiscordService from "../../../src/services/DiscordService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import axios from "axios";
import {AttachmentBuilder} from "discord.js";
import Utils from "../../../src/Utils.js";
import CardService from "../../../src/services/CardService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function (userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent.toLowerCase().startsWith('!cardwrite')
    const isCorrectBot = ModdingService.isBotMasterBot(bot, userMessage)

    if (isTrigger && isCorrectBot) {
        const imageURL = userMessage.attachments.find(a => a.url.toLowerCase().includes('.png'))
        const textURL = userMessage.attachments.find(a => [".md", ".txt", ".json"].some(extension => a.url.toLowerCase().includes(extension)))

        if (!userMessage.attachments || !userMessage.attachments.size === 2 || !imageURL || !textURL) {
            await DiscordService.sendCommandError(userMessage, `#You need to provide a PNG image and a text file as attachment of this command!`)
            return next()
        }

        const imageResponse = await axios.get(imageURL.url, {responseType: 'arraybuffer'})
        const imageBuffer = Buffer.from(imageResponse.data, "utf-8")

        const textResponse = await axios.get(textURL.url)
        let card = await textResponse.data

        if (typeof card === "object") {
            card = JSON.stringify(card, null, 2)
        }

        await DiscordService.sendCommandConfirmation(userMessage)

        const cardBuffer = await Utils.tryCatch(() => {
            return CardService.createCard(imageBuffer, card);
        }, async (err) => {
            await DiscordService.sendCommandError(userMessage, `#${err.message}`)
        })

        if (!cardBuffer) {
            return next()
        }

        const newBotName = CardService.extractCharacterName(card)

        const attachment = new AttachmentBuilder(cardBuffer, {name: `${Date.now()}.png`})

        if (attachment) {
            await DiscordMessageService.sendMessage(userMessage, `#Here is ${newBotName}'s card!`, bot, true, true, [attachment], null, null)
                .catch(console.error)
        }
    }

    next()
}