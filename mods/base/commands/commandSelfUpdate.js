import DiscordService from "../../../src/services/DiscordService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import DiscordTypingService from "../../../src/services/DiscordTypingService.js";
import BotDataService from "../../../src/services/BotDataService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandSpeakAs(userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent?.toLowerCase?.()?.startsWith?.('!selfupdate')
    const botName = userMessage.cleanContent.match(/!selfUpdate (.*)/i)?.[1]?.trim?.()

    let json

    try {
        json = JSON.parse(botName)
    } catch {

    }

    const isBotNameInJSONList = Array.isArray(json) && (json.includes(bot.name) || json.includes(client.application.id))

    if (isTrigger && (botName === bot.name || botName === client.application.id || isBotNameInJSONList)) {
        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)

        await DiscordService.sendCommandConfirmation(userMessage)
        await DiscordTypingService.sendTyping(userMessage)

        if (!DiscordService.botsGeneratingMessageInChannel[userMessage.channelId]) DiscordService.botsGeneratingMessageInChannel[userMessage.channelId] = {}
        DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name] = true

        let dataUpdateDiff
        if (bot.enableMemory) {
            dataUpdateDiff = await BotDataService.updateRevamped(userMessage, null, bot, messages, null).catch(err => console.error(err))
            if (!dataUpdateDiff) {
                userMessage?.react('🐞').catch(console.error)
            } else {
                await DiscordMessageService.sendMessage(userMessage, `#Data:\n\`\`\`js\n${JSON.stringify(dataUpdateDiff, null, 4)}\n\`\`\``, bot, true, true, [], null, null, false)
            }

        }

        delete DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name]
    }

    next()
}
