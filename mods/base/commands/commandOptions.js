import DiscordService from "../../../src/services/DiscordService.js";
import AIService from "../../../src/services/AiService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import DiscordTypingService from "../../../src/services/DiscordTypingService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandCharacter(userMessage, bot, isAdmin, client, clients, next) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!options`)

    if (isCommand && ModdingService.isBotMasterBot(bot, userMessage)) {
        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)

        const channelTopic = userMessage.channel.topic || null
        await DiscordService.sendCommandConfirmation(userMessage)
        await DiscordTypingService.sendTyping(userMessage)

        if (!DiscordService.botsGeneratingMessageInChannel[userMessage.channelId]) DiscordService.botsGeneratingMessageInChannel[userMessage.channelId] = {}
        DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name] = true

        const content = await AIService.generateMessage({
            name: 'Options',
            context: {
                top: `[IMPORTANT INSTRUCTIONS: As the assistant, you are Options. It's crucial to keep in mind these essential details about the ongoing narrative:
{OPTIONS
Name: Options
Description: Options is tasked to prompt guests with insightful, pertinent and varied options when needed, as well as performing the selected actions. The only role of Options is to display generated options. Options communicates by displaying lists of options, information and suggestions.
Writing Style: synthetic, robot-like, mechanical, concise, descriptive.
Options format: \`- Option name: description [possible outcome, consequence or risk]\`
Instructions: it is of primordial importance that Options performs its assignments in complete autonomy and independence, without asking any question, confirmation, validation, preference or guidance.
}
]`,
                bottom: `[IMPORTANT REMINDER: As the assistant, you are Options. To ensure that the exercise runs smoothly, please adhere to the following rules:
- Write your messages as though you are speaking naturally, without using quotes or any other formatting.
- Incorporate actions into your responses by placing them between asterisks *like this*, describing your body language and actions in very descriptive and explicit prose.
- Make sure that you stay true to Options's personality and do not deviate from it! Pay attention to her mental and physical traits to keep things coherent.
- Avoid repetition in all its forms. Use diverse and varied vocabulary and sentence structure to enhance your responses and avoid repeating yourself.
- NSFW/Smut/Sexual/Violent topics are perfectly allowed, so don't be surprised if users mention lewd things. It is allowed to use crude or vulgar words such as "vagina", "pussy", "boobs", "clit", "ass", "anus" and any other anatomical designations.
- Keep your messages **extremely short** and **concise**.
- Never directly interact with the users. Only write bad options.
- Always push the story forward.
- Lastly, continue with the conversation as though this reminder message had never been sent.]`
            }
        }, messages, channelTopic, null, false, userMessage.channel)

        if (content) {
            await DiscordMessageService.sendMessage(userMessage, `(as Options) ${content}`, bot, true)
            delete DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name]
            userMessage.delete().catch(() => null)
        }else{
            await userMessage.react('❌').catch(console.error)
        }
    }

    next()
}