/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandSetAvatar(userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!setavatar')
    const botId = userMessage.cleanContent.match(/!setavatar ([a-zA-Z0-9_ ]*)/i)?.[1]?.trim?.()
    const avatarURL = userMessage.cleanContent.match(/!setavatar ([a-zA-Z0-9_ ]*)? ?(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*))/i)?.[2]?.trim?.()
    const attachmentURL = userMessage.attachments.map(a => a.url)?.[0]

    const isAllowedChannel = (botId?.toLowerCase() === client.application.id.toLowerCase())

    if (isTrigger && isAllowedChannel) {
        if (!avatarURL && !attachmentURL) {
            await userMessage?.reply(`#You need to provide an image! Either use \`!setAvatar BOT_ID https://path.to/your_image.png\` or simply \`!setAvatar BOT_ID\` with your image file attached to the message!`)
            return
        }

        userMessage?.react('✅').catch(console.error)

        await client.user.setAvatar(attachmentURL ?? avatarURL).catch(async err => {
            return await userMessage?.reply(`#Oops, something went wrong with your image, make sure the file or URL is a correct image!`)
        })
    }

    next()
}