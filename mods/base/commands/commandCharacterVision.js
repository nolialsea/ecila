import AIService from "../../../src/services/AiService.js";
import fs from "fs";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import DiscordService from "../../../src/services/DiscordService.js";
import DiscordImageService from "../../../src/services/DiscordImageService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import config from "../../../src/classes/Config.js";
import Utils from "../../../src/Utils.js";
import CardService from "../../../src/services/CardService.js";
import {AttachmentBuilder} from "discord.js";
import axios from "axios";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandCharacter(userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent.toLowerCase().startsWith('!visioncharacter')
    const isCorrectBot = ModdingService.isBotMasterBot(bot, userMessage)

    if (isTrigger && isCorrectBot) {
        if (!(userMessage.attachments && userMessage.attachments.size === 1 && userMessage.attachments.at(0).url)) {
            await DiscordService.sendCommandError(userMessage, `#You need to provide an image for this command.`)
            return next()
        }

        let message = userMessage.cleanContent.replace(/!visioncharacter/gi, '').trim()
        if (!message) {
            message = `Can you please generate a character based on that image?`
        }

        const contextTop = fs.readFileSync('./mods/base/resources/characterGenerator_contextTop.txt').toString()
        const contextBottom = fs.readFileSync('./mods/base/resources/characterGenerator_contextBottom.txt').toString()
        const tempBot = {
            ...bot,
            debug: false,
            name: "CharacterGenerator",
            openAIContext: {top: contextTop, bottom: contextBottom},
            context: {top: contextTop, bottom: contextBottom},
            enableMemory: false,
            data: {name: "CharacterGenerator"},
            disableImages: false,
            imagesNovelAI: {
                "enabled": true,
                "debug": true,
                "postPrompt": "medium shot, {{{realistic}}}, {{{photorealistic}}}, {{{very aesthetic}}}, {{{best quality}}}, {{{absurdres}}}",
                "negativePrompt": "",
                "portrait": true,
                "landscape": false,
                "imageCount": 1
            },
            TTS: {disabled: true}
        }

        if (config.characterGenerationImagesConfig) {
            Object.assign(tempBot, config.characterGenerationImagesConfig)
        }

        tempBot.textGenParameters.OpenAI = {
            "enabled": true,
            "model": "gpt-4o-2024-05-13",
            "temperature": 1.0,
            "presence_penalty": 0.0,
            "frequency_penalty": 0.0,
            "max_prompt_length": 6000,
            "max_tokens": 2048
        }

        await DiscordService.sendCommandConfirmation(userMessage)

        const examples = JSON.parse(fs.readFileSync(`./mods/base/resources/characterGenerator_examples.json`).toString())

        let imageUrls = userMessage.attachments.at(0).url.replace(/\?[^]*/ig, '')

        const req = await axios.get(imageUrls, {responseType: 'arraybuffer'})
            .catch(console.error)
        if (!req) {
            imageUrls = []
        }else{
            const contentType = req.headers.getContentType()
            imageUrls = ["data:" + contentType + ";base64," + Buffer.from(req.data).toString('base64')];
        }

        const messages = examples.concat([
            {
                author: userMessage.author.username ?? 'User',
                content: message,
                imageUrls: imageUrls
            }
        ])

        const result = await AIService.generateMessageChatGPT(tempBot, messages, null, null)

        if (result) {
            const finalResult = result
                .replace(/\*\*/gi, '')

            function extractContent(input) {
                const startMarker = "```markdown"
                const endMarker = "```"
                const startIndex = input.indexOf(startMarker) + startMarker.length;
                const endIndex = input.indexOf(endMarker, startIndex);
                return input.slice(startIndex, endIndex).trim();
            }

            const dataResult = extractContent(finalResult)

            const discordMessage = await DiscordMessageService.sendMessage(userMessage, `(as CharacterGenerator) ${finalResult}`, tempBot, true, true, [], null, null, true)

            messages.push({
                author: "CharacterGenerator",
                content: finalResult
            })
            let files = await DiscordImageService.generateImageAttachmentsRevamped(userMessage, finalResult, tempBot, false, false, messages, 2)
            files = files.filter(f => !!f)

            if (files?.length > 0 && dataResult) {
                for (const fileNumber in files){
                    files[fileNumber].attachment = await Utils.tryCatch(async () => {
                        return CardService.createCard(files[fileNumber].attachment, dataResult);
                    }, async (err) => {
                        await DiscordService.sendCommandError(userMessage, `#${err.message}`)
                    })

                    if (!files[fileNumber].attachment) {
                        return next()
                    }
                }
            } else {
                await DiscordService.sendCommandError(userMessage, `#Oh no, the card couldn't be generated!`)
                return next()
            }

            const newBotName = CardService.extractCharacterName(dataResult)

            if (newBotName) {
                files.push(new AttachmentBuilder(Buffer.from(dataResult), {name: `card_${newBotName}_${Date.now()}.md`}))
            }

            if (files && files.length > 0) {
                await discordMessage.edit({
                    content: discordMessage.content,
                    files: files
                }).catch(console.error)
            }
        } else {
            await DiscordService.sendCommandError(userMessage, "#Result couldn't be processed!")
        }
    }

    next()
}
