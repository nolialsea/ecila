import SmartAnswerService from "../../../src/services/SmartAnswerService.js";
import TimeoutService from "../../../src/services/TimeoutService.js";
import {ChannelType} from "discord.js";
import config from "../../../src/classes/Config.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function smartAnswer(userMessage, bot, isAdmin, client, clients, next) {
    const channelTopic = userMessage?.channel?.topic || null
    const channelName = userMessage?.channel?.name
    const isVisibleMessage = !(userMessage?.cleanContent?.startsWith?.('!')) && !(userMessage?.cleanContent?.startsWith?.('#')) && !(userMessage?.cleanContent?.startsWith?.('/#')) && !(userMessage?.cleanContent?.startsWith?.('\#')) && !(userMessage?.cleanContent?.startsWith?.('//')) && !(userMessage?.cleanContent?.startsWith?.('. ')) && userMessage?.cleanContent

    const isAllowedChannel = bot.multiBotChannels?.some(c => c === channelName)
        || userMessage.channel.type === ChannelType.DM && config.enableDirectMessages && config.discordAdminIds[userMessage.author.id.toString()]

    const isComingFromBotItself = client.application.id === userMessage.author.id

    if (!isVisibleMessage || !isAllowedChannel || isComingFromBotItself) return next()
    if ((config.bannedUsers && config.bannedUsers.includes(userMessage?.author?.id)) || (bot.bannedUsers && bot.bannedUsers.includes(userMessage?.author?.id))) {
        return next()
    }

    if (TimeoutService.smartAnswerTimeout[userMessage?.channelId]) clearTimeout(TimeoutService.smartAnswerTimeout[userMessage?.channelId])
    const cooldown = bot.smartAnswerCooldown ?? config.smartAnswerCooldown ?? 20000
    TimeoutService.smartAnswerTimeout[userMessage?.channelId] = setTimeout(() => SmartAnswerService.sendSmartAnswer(userMessage, bot, channelTopic, false, clients), cooldown)

    next()
}
