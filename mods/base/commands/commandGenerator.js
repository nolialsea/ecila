import GeneratorService from "../../../src/services/GeneratorService.js";
import DiscordService from "../../../src/services/DiscordService.js";
import JsonService from "../../../src/services/JsonService.js";
import ModdingService from "../../../src/services/ModdingService.js";


/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandGenerator(userMessage, bot, isAdmin, client, clients, next) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!gen`)
    if (isCommand && ModdingService.isBotMasterBot(bot, userMessage)) {
        const message = userMessage.cleanContent.replace(`!gen`, '').trim()

        if (isAdmin) {
            const fileUrls = userMessage.attachments.map(a => a.url)
            let json = await JsonService.loadJsonUrl(fileUrls?.[0])
            const result = await GeneratorService.generator(userMessage, message, bot, json)
            next()
            return result
        } else {
            await DiscordService.sendCommandError(userMessage)
        }
    }

    next()
}