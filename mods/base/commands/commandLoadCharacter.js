import DiscordService from "../../../src/services/DiscordService.js";
import axios from "axios";
import Utils from "../../../src/Utils.js"
import CardService from "../../../src/services/CardService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";


/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandCharacter(userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent.toLowerCase().startsWith('!load')
    const botId = userMessage.cleanContent.match(/!load ([a-zA-Z0-9_ ]*)/i)?.[1]?.trim?.()
    const cardTextURL = userMessage.cleanContent.match(/!load ([a-zA-Z0-9_ ]*) (https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*))/i)?.[2]?.trim?.()
    const cardAttachmentURL = userMessage.attachments.map(a => a.url)?.[0]
    const isCorrectBot = (botId?.toLowerCase() === client.application.id.toLowerCase())

    const isText = cardTextURL?.toLowerCase()?.includes('.txt') || cardTextURL?.toLowerCase()?.includes('.md')
        || cardAttachmentURL?.toLowerCase()?.includes('.txt') || cardAttachmentURL?.toLowerCase()?.includes('.md')

    const args = userMessage.cleanContent
        .replace(/^!load/i, '')
        .trim()
        .replace(/^[0-9]*/i, '')
        .trim()
        .replace(/^(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*))/i, '')
        .trim()

    if (isTrigger && isCorrectBot) {
        if (!isAdmin && !bot.editable){
            await DiscordService.sendCommandError(userMessage, `#Only admins can edit this bot!`)
            return next()
        }

        if (!cardTextURL && !cardAttachmentURL) {
            await userMessage?.reply(`#You need to provide an image card! Either use \`!load BOT_ID https://path.to/your_image.png\` or simply \`!load BOT_ID\` with your image card or text file attached to the message!`)
            return next()
        }

        let argsJSON = []
        if (args) {
            console.log('DATA LOADING...', args)
            try {
                argsJSON = JSON.parse(args)
                console.log(`DATA TO LOAD:`, argsJSON)
            } catch (err) {
                console.error(err)
            }
        }

        let card

        if (!isText) {
            const response = await axios.get(cardTextURL ?? cardAttachmentURL, {responseType: 'arraybuffer'})
            const buffer = Buffer.from(response.data, "utf-8")

            card = await Utils.tryCatch(async () => {
                return await CardService.extractCardContent(buffer)
            }, async (err) => {
                await DiscordService.sendCommandError(userMessage, `#${err.message}`)
            })
        } else {
            const response = await axios.get(cardTextURL ?? cardAttachmentURL)
            card = await response.data
            if (!card) {
                await DiscordService.sendCommandError(userMessage, "#Result couldn't be processed! Please verify your card!")
            }
        }

        if (!card) {
            return next()
        }

        const newBotName = CardService.extractCharacterName(card)

        if (!newBotName) {
            await DiscordService.sendCommandError(userMessage, `#No character name could be extracted from the card...`)
            return next()
        }

        // Auto detect dynamic properties
        argsJSON.push(...CardService.extractDynamicProperties(card))

        // Clean up dynamic properties symbol
        card = CardService.cleanupDynamicProperties(card)

        // Update the bot config (not saved)
        bot.name = newBotName
        if (bot.data) {
            bot.data.Name = newBotName

            if (argsJSON && Array.isArray(argsJSON) && argsJSON.length > 0) {
                bot.data = {Name: newBotName}

                for (const field of argsJSON) {
                    const line = card
                        .split('\n')
                        .find(s => s.startsWith(field))
                    if (!line) {
                        console.log(`Field "${field}" not found!`)
                    } else {
                        const value = line.replace(`${field}:`, '').trim()
                        card = card.replace(line, `${field}: \${${field}}`)
                        bot.data[field] = value
                    }
                }
                console.log(`LOADED DATA:`, bot.data)
            }
        }

        if (bot.openAIContext?.top) bot.openAIContext.top = `[IMPORTANT INSTRUCTIONS: As the assistant, you are ${newBotName}. It's crucial to keep in mind these essential details about the ongoing narrative:\n\n${card}\n]`

        // Update discord bot name
        if (client.user.username !== newBotName) {
            await client.user.setUsername(newBotName)
                .catch(err => {
                    console.log(err)
                    userMessage.channel.send(`#Couldn't change the bot name! This username is already taken by 9999 other discord users, or the name was changed more than twice in the last hour. Yes, discord is a shit platform.`)
                })
        }

        await DiscordService.sendCommandConfirmation(userMessage)

        if (card) {
            await DiscordMessageService.sendMessage(userMessage, `#Loaded ${newBotName}'s card! **Remember to use \`!reset\` if you've changed any dynamic fields!**`, bot, true)
        } else {
            await DiscordService.sendCommandError(userMessage, "#Result couldn't be processed!")
        }
    }

    next()
}
