import AIService from "../../../src/services/AiService.js";
import fs from "fs";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import DiscordService from "../../../src/services/DiscordService.js";
import DiscordImageService from "../../../src/services/DiscordImageService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import axios from "axios";
import {AttachmentBuilder} from "discord.js";
import Utils from "../../../src/Utils.js";
import CardService from "../../../src/services/CardService.js";
import config from "../../../src/classes/Config.js";

/**
 * Generates a character card for the given command message.
 *
 * @param {import('discord.js').Message} userMessage - The command message triggering the character generation.
 * @param {BotConfig} bot - The bot configuration object.
 * @param {boolean} isAdmin - A flag indicating whether the user is an admin.
 * @param {import('discord.js').Client} client - The Discord client instance.
 * @param {import('discord.js').Client[]} clients - An array of all Discord client instances.
 * @param {Function} next - A function to call to proceed to the next middleware.
 * @returns {Promise<void>}
 */
export default async function commandCharacter(userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent.toLowerCase().startsWith('!character')
    const isCorrectBot = ModdingService.isBotMasterBot(bot, userMessage)

    if (isTrigger && isCorrectBot) {
        if (userMessage.author.bot) {
            await DiscordMessageService.sendMessage(userMessage, `(as ${bot.name}) ${userMessage.cleanContent}`, bot, true)
                .catch(err => console.error(err))
        } else {
            await DiscordMessageService.sendMessage(userMessage, `(as ${DiscordService.replaceAlias(userMessage.member?.displayName || userMessage.author?.username)}) ${userMessage.cleanContent}`, bot, true)
                .catch(err => console.error(err))
        }

        let card
        if (userMessage.attachments && userMessage.attachments.size === 1 && userMessage.attachments.at(0).url) {
            const url = userMessage.attachments.at(0).url

            if (url.includes('.png')) {
                const response = await axios.get(url, {responseType: 'arraybuffer'})
                const buffer = Buffer.from(response.data, "utf-8")

                card = await Utils.tryCatch(async () => {
                    return await CardService.extractCardContent(buffer)
                }, async (err) => {
                    await DiscordService.sendCommandError(userMessage, `#${err.message}`)
                })

                if (!card || typeof card !== "string") {
                    return next()
                }
            } else if (['.txt', '.md', '.json'].some(f => url.includes(f))) {
                const response = await axios.get(url)
                if (!response) {
                    await DiscordService.sendCommandError(userMessage, `#Oh no, couldn't fetch the input file!`)
                    return next()
                }
                if (url.includes('.json')) {
                    card = JSON.stringify(response.data, null, 2)
                } else {
                    card = response.data
                }
            }
        }

        const message = userMessage.cleanContent.replace(/!character/gi, '').trim()
        const finalMessage = (message && card) ?
            message + `\n\`\`\`\n${card}\n\`\`\``
            : message ? message : `Hey hey CharacterGenerator! I downloaded a character card on a doubtful website... Could you please make a character with your format out of it? 😛 Here it is:\n\`\`\`\n${card}\n\`\`\``

        const contextTop = fs.readFileSync('./mods/base/resources/characterGenerator_contextTop.txt').toString()
        const contextBottom = fs.readFileSync('./mods/base/resources/characterGenerator_contextBottom.txt').toString()
        const tempBot = {
            ...bot,
            debug: false,
            name: "CharacterGenerator",
            openAIContext: {top: contextTop, bottom: contextBottom},
            context: {top: contextTop, bottom: contextBottom},
            enableMemory: false,
            data: {Name: "CharacterGenerator"},
            TTS: {disabled: true}
        }

        if (tempBot.characterGenerationTextConfig) {
            if (tempBot.characterGenerationTextConfig.model?.toLowerCase()?.includes('claude')) {
                tempBot.textGenParameters.Claude = tempBot.characterGenerationTextConfig
                tempBot.mainBackend = 'Claude'
            } else if (tempBot.characterGenerationTextConfig.model?.toLowerCase()?.includes('gpt')) {
                tempBot.textGenParameters.OpenAI = tempBot.characterGenerationTextConfig
                tempBot.mainBackend = 'ChatGPT'
            }
        }

        if (bot.characterGenerationImagesConfig || config.characterGenerationImagesConfig) {
            Object.assign(tempBot, bot.characterGenerationImagesConfig || config.characterGenerationImagesConfig)
        }

        if (bot.characterGenerationTextConfig) {
            tempBot.textGenParameters.OpenAI = bot.characterGenerationTextConfig
            tempBot.textGenParameters.Claude = bot.characterGenerationTextConfig
        }

        await DiscordService.sendCommandConfirmation(userMessage)

        const examples = JSON.parse(fs.readFileSync(`./mods/base/resources/characterGenerator_examples.json`).toString())

        const messages = examples.concat([
            {
                author: userMessage.author.username ?? 'User',
                content: finalMessage
            }
        ])

        const result = await AIService.generateMessage(tempBot, messages)

        if (result) {
            const finalResult = result
                .replace(/\*\*/gi, '')

            function extractContent(input) {
                const startMarker = "```markdown"
                const endMarker = "```"
                const indexOfStartIndex = input.indexOf(startMarker)
                if (indexOfStartIndex !== -1) {
                    const startIndex = input.indexOf(startMarker) + startMarker.length;
                    const endIndex = input.indexOf(endMarker, startIndex);
                    return input.slice(startIndex, endIndex).trim();
                } else {
                    const firstLine = input.split('\n')?.[0]
                    if (firstLine) {
                        if (firstLine.trim().endsWith(':')) {
                            return input.replace(firstLine, '').trim()
                        }
                        return input
                    }
                }
            }

            const dataResult = extractContent(finalResult)

            const discordMessage = await DiscordMessageService.sendMessage(userMessage, `(as CharacterGenerator) ${finalResult}`, tempBot, true, true, [], null, null, true)

            messages.push({
                author: "CharacterGenerator",
                content: finalResult
            })

            let files = await DiscordImageService.generateImageAttachmentsRevamped(userMessage, finalResult, tempBot, false, false, messages, 2)
            files = files.filter(f => !!f)

            if (files?.length > 0 && dataResult) {
                for (const fileNumber in files) {
                    files[fileNumber].attachment = await Utils.tryCatch(async () => {
                        return CardService.createCard(files[fileNumber].attachment, dataResult);
                    }, async (err) => {
                        await DiscordService.sendCommandError(userMessage, `#${err.message}`)
                    })

                    if (!files[fileNumber].attachment) {
                        return next()
                    }
                }
            } else {
                await DiscordService.sendCommandError(userMessage, `#Oh no, the card couldn't be generated!`)
            }

            const newBotName = CardService.extractCharacterName(dataResult)

            if (newBotName) {
                files.push(new AttachmentBuilder(Buffer.from(dataResult), {name: `card_${newBotName}_${Date.now()}.md`}))
            }

            if (files && files.length > 0) {
                await discordMessage.edit({
                    content: discordMessage.content,
                    files: files
                }).catch(console.error)
            }
        } else {
            await DiscordService.sendCommandError(userMessage, "#Result couldn't be processed!")
        }
    }

    next()
}
