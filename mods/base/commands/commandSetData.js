import BotDataService from "../../../src/services/BotDataService.js";
import {ChannelType} from "discord.js";
import config from "../../../src/classes/Config.js";
import DiscordService from "../../../src/services/DiscordService.js";
import axios from "axios";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandSetData(userMessage, bot, isAdmin, client, clients, next) {
    const channelName = userMessage?.channel?.name
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!setdata')
    const optionalBotName = userMessage.cleanContent.match(/!setdata ?([a-zA-Z0-9_' ]*)?/i)?.[1]?.trim?.()

    const isAllowedChannel = bot.monoBotChannels?.some(c => c === channelName)
        || userMessage.channel.type === ChannelType.DM && config.enableDirectMessages && config.discordAdminIds[userMessage.author.id.toString()]
        || (optionalBotName?.toLowerCase() === bot.name.toLowerCase())

    if (isTrigger && isAllowedChannel) {
        let propertyName = userMessage.cleanContent.match(/!setdata ?([a-zA-Z0-9_' ]*)? ["“]([^"”]+)["”]/i)
        propertyName = propertyName?.[2]?.trim()
        let propertyValue = userMessage.cleanContent.match(/!setdata ?([a-zA-Z0-9_' ]*)? ["“][^"”]+["”](.+)/i)
        propertyValue = propertyValue?.[2]?.trim()

        const textURL = userMessage.attachments.find(
            a => [".json"]
                .some(extension => a.url.toLowerCase().includes(extension))
        )

        if (!propertyName && !textURL) {
            await DiscordService.sendCommandError(userMessage, `#You need to provide either a property name and value (ex: \`!setData ${bot.name} "Property" value\`) or a .json file as attachment of this command!`)
            return next()
        }

        let newData
        if (textURL) {
            const textResponse = await axios.get(textURL.url)
                .catch(async (err) => await DiscordService.sendCommandError(userMessage, `#ERROR: ${err.message}`))
            newData = await textResponse.data
        }

        if (!newData && (!propertyName || !propertyValue)) {
            userMessage?.react('❌').catch(console.error)
            next()
            return
        }

        userMessage?.react('✅').catch(console.error)

        if (newData){
            const data = {...BotDataService.getData(bot, userMessage.channel), ...newData}
            BotDataService.setData(bot, userMessage.channel, data)
        }else {
            const data = BotDataService.getData(bot, userMessage.channel)
            data[propertyName] = propertyValue
            BotDataService.setData(bot, userMessage.channel, data)
        }
    }

    next()
}
