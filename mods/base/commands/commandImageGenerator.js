import GeneratorService from "../../../src/services/GeneratorService.js";
import DiscordService from "../../../src/services/DiscordService.js";
import JsonService from "../../../src/services/JsonService.js";
import ModdingService from "../../../src/services/ModdingService.js";


/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandImageGenerator(userMessage, bot, isAdmin, client, clients, next) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!imgen`)
    if (isCommand && ModdingService.isBotMasterBot(bot, userMessage)) {
        const message = userMessage.cleanContent.replace(`!imgen`, '').trim()

        if (isAdmin) {
            const fileUrls = userMessage.attachments.map(a => a.url)
            let json = await JsonService.loadJsonUrl(fileUrls?.[0])
            await GeneratorService.generatorToImage(userMessage, message, bot, json)

        } else {
            await DiscordService.sendCommandError(userMessage)
        }
    }

    next()
}