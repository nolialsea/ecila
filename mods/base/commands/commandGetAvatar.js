import {ChannelType} from "discord.js";
import config from "../../../src/classes/Config.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandGetAvatar(userMessage, bot, isAdmin, client, clients, next) {
    const channelName = userMessage?.channel?.name
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!getavatar')
    const optionalBotName = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!getavatar ' + bot.name.toLowerCase())

    const isAllowedChannel = bot.monoBotChannels?.some(c => c === channelName)
        || (userMessage.channel.type === ChannelType.DM && config.enableDirectMessages && config.discordAdminIds[userMessage.author.id.toString()])
        || optionalBotName

    if (isTrigger && isAllowedChannel) {
        userMessage?.react('✅').catch(console.error)
        userMessage?.reply(`#Current Avatar: ${client.user.avatarURL()}`).catch(() => {
            DiscordMessageService.sendMessage(userMessage, `#Current Avatar: ${client.user.avatarURL()}`, bot, false, true)
        })
    }

    next()
}