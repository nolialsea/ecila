import DiscordService from "../../../src/services/DiscordService.js";
import axios from "axios";
import CardService from "../../../src/services/CardService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import {AttachmentBuilder} from "discord.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function (userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent.toLowerCase().startsWith('!settings')
    const botId = userMessage.cleanContent.match(/!settings ([a-zA-Z0-9_ ]*)/i)?.[1]?.trim?.()
    const isCorrectBot = (botId?.toLowerCase() === client.application.id.toLowerCase())

    if (isTrigger && isCorrectBot) {

        if (!isAdmin && !bot.editable) {
            await DiscordService.sendCommandError(userMessage, `#Only admins can edit this bot!`)
            return next()
        }

        if (!userMessage.attachments || userMessage.attachments.size < 1) {
            await DiscordService.sendCommandError(userMessage, `#You need to provide a PNG image or a text file as attachment of this command!`)
            return next()
        }

        const imageURL = userMessage.attachments.find(
            a => a.url.toLowerCase().includes('.png')
        )
        const textURL = userMessage.attachments.find(
            a => [".md", ".txt", ".json"]
                .some(extension => a.url.toLowerCase().includes(extension))
        )

        if (!imageURL && !textURL) {
            await DiscordService.sendCommandError(userMessage, `#You need to provide a PNG image or a text file as attachment of this command! The text file can have either the ".txt", ".md" or ".json" extension.`)
            return next()
        }

        let settings
        if (imageURL) {
            const imageResponse = await axios.get(imageURL.url, {responseType: 'arraybuffer'})
                .catch(async (err) => await DiscordService.sendCommandError(userMessage, `#ERROR: ${err.message}`))
            const imageBuffer = Buffer.from(imageResponse.data, "utf-8")
            try {
                settings = await CardService.extractCardContent(imageBuffer)
            }catch(err){
                await DiscordService.sendCommandError(userMessage, `#You need to provide a JSON file as attachment of this command!`)
                return next()
            }
        } else if (textURL) {
            const textResponse = await axios.get(textURL.url)
                .catch(async (err) => await DiscordService.sendCommandError(userMessage, `#ERROR: ${err.message}`))
            settings = await textResponse.data
        }

        if (!settings) {
            return next()
        }

        if (typeof settings === "string") {
            try {
                settings = JSON.parse(settings)
            } catch (err) {
                await DiscordService.sendCommandError(userMessage, `#ERROR: The settings need to be in JSON format!`)
                return next()
            }
        }

        await DiscordService.sendCommandConfirmation(userMessage)

        Object.assign(bot, settings)

        await DiscordMessageService.sendMessage(
            userMessage,
            `#Settings have been loaded onto ${bot.name}! Loaded settings are attached below.`,
            bot,
            true,
            true,
            [new AttachmentBuilder(Buffer.from(JSON.stringify(settings, null, 2)), {name: `settings.json`})])
            .catch(console.error)
    }

    next()
}