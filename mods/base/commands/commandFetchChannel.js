import {ChannelType} from "discord.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function (userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!fetch')
    if (isTrigger) {
        userMessage?.react('✅').catch(console.error)
        await userMessage.channel.fetch(true).catch(console.error)
        await userMessage?.delete?.().catch(() => null)
    }

    next()
}