import {ChannelType} from "discord.js";
import config from "../../../src/classes/Config.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandPing(userMessage, bot, isAdmin, client, clients, next) {
    const channelName = userMessage?.channel?.name
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!ping')
    const isAllowedChannel = bot.monoBotChannels?.some(c => c === channelName) || bot.multiBotChannels?.some(c => c === channelName)
        || userMessage.channel.type === ChannelType.DM && config.enableDirectMessages && config.discordAdminIds[userMessage.author.id.toString()]

    if (isTrigger && isAllowedChannel) {
        userMessage?.react('✅').catch(console.error)
    }

    next()
}