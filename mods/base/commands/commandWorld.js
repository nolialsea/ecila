import AIService from "../../../src/services/AiService.js";
import fs from "fs";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import DiscordService from "../../../src/services/DiscordService.js";
import DiscordImageService from "../../../src/services/DiscordImageService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import axios from "axios";
import {AttachmentBuilder} from "discord.js";
import Utils from "../../../src/Utils.js";
import CardService from "../../../src/services/CardService.js";
import config from "../../../src/classes/Config.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function (userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent.toLowerCase().startsWith('!world')
    const isCorrectBot = ModdingService.isBotMasterBot(bot, userMessage)

    if (isTrigger && isCorrectBot) {
        if (userMessage.author.bot) {
            await DiscordMessageService.sendMessage(userMessage, `(as ${bot.name}) ${userMessage.cleanContent}`, bot, true)
                .catch(err => console.error(err))
        } else {
            await DiscordMessageService.sendMessage(userMessage, `(as ${DiscordService.replaceAlias(userMessage.member?.displayName || userMessage.author?.username)}) ${userMessage.cleanContent}`, bot, true)
                .catch(err => console.error(err))
        }

        let card
        if (userMessage.attachments && userMessage.attachments.size === 1 && userMessage.attachments.at(0).url) {
            const response = await axios.get(userMessage.attachments.at(0).url, {responseType: 'arraybuffer'})
            const buffer = Buffer.from(response.data, "utf-8")

            card = await Utils.tryCatch(async () => {
                return await CardService.extractCardContent(buffer)
            }, async (err) => {
                await DiscordService.sendCommandError(userMessage, `#${err.message}`)
            })

            if (!card || typeof card !== "string") {
                return next()
            }
        }

        const message = userMessage.cleanContent.replace(/!world/gi, '').trim()
        const finalMessage = (message && card) ?
            message + `\n\`\`\`\n${card}\n\`\`\``
            : message ? message : `Hey hey WorldGenerator! I downloaded a world card on a doubtful website... Could you please make a world with your format out of it? 😛 Here it is:\n\`\`\`\n${card}\n\`\`\``

        const contextTop = fs.readFileSync('./mods/base/resources/worldGenerator_contextTop.txt').toString()
        const contextBottom = fs.readFileSync('./mods/base/resources/worldGenerator_contextBottom.txt').toString()
        const tempBot = {
            ...bot,
            debug: false,
            name: "WorldGenerator",
            openAIContext: {top: contextTop, bottom: contextBottom},
            context: {top: contextTop, bottom: contextBottom},
            enableMemory: false,
            data: {name: "WorldGenerator"},
            TTS: {disabled: true}
        }

        if (tempBot.characterGenerationTextConfig) {
            if (tempBot.characterGenerationTextConfig.model?.toLowerCase()?.includes('claude')) {
                tempBot.textGenParameters.Claude = tempBot.characterGenerationTextConfig
                tempBot.mainBackend = 'Claude'
            } else if (tempBot.characterGenerationTextConfig.model?.toLowerCase()?.includes('gpt')) {
                tempBot.textGenParameters.OpenAI = tempBot.characterGenerationTextConfig
                tempBot.mainBackend = 'ChatGPT'
            }
        }

        await DiscordService.sendCommandConfirmation(userMessage)

        const examples = JSON.parse(fs.readFileSync(`./mods/base/resources/worldGenerator_examples.json`).toString())

        const messages = examples.concat([
            {
                author: userMessage.author.username ?? 'User',
                content: finalMessage
            }
        ])

        const result = await AIService.generateMessage(tempBot, messages, null, null)

        if (result) {
            const finalResult = result
                .replace(/\*\*/gi, '')

            function extractContent(input) {
                const startMarker = "```markdown"
                const endMarker = "```"
                const startIndex = input.indexOf(startMarker) + startMarker.length;
                const endIndex = input.indexOf(endMarker, startIndex);
                return input.slice(startIndex, endIndex).trim();
            }

            const dataResult = extractContent(finalResult)

            const discordMessage = await DiscordMessageService.sendMessage(userMessage, `(as WorldGenerator) ${finalResult}`, tempBot, true, true, [], null, null, true)

            messages.push({
                author: "WorldGenerator",
                content: finalResult
            })

            let files = await DiscordImageService.generateImageAttachmentsRevamped(userMessage, finalResult, tempBot, false, false, messages, 2)
            files = files.filter(f => !!f)

            if (files?.length > 0 && dataResult) {
                for (const fileNumber in files) {
                    files[fileNumber].attachment = await Utils.tryCatch(async () => {
                        return CardService.createCard(files[fileNumber].attachment, dataResult);
                    }, async (err) => {
                        await DiscordService.sendCommandError(userMessage, `#${err.message}`)
                    })

                    if (!files[fileNumber].attachment) {
                        return next()
                    }
                }
            } else {
                await DiscordService.sendCommandError(userMessage, `#Oh no, the card couldn't be generated!`)
            }

            files.push(new AttachmentBuilder(Buffer.from(dataResult), {name: `world_card_${Date.now()}.md`}))

            if (files && files.length > 0) {
                await discordMessage.edit({
                    content: discordMessage.content,
                    files: files
                }).catch(console.error)
            }
        } else {
            await DiscordService.sendCommandError(userMessage, "#Result couldn't be processed!")
        }
    }

    next()
}
