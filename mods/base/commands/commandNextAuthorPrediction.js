import DiscordService from "../../../src/services/DiscordService.js";
import AIService from "../../../src/services/AiService.js";
import {ChannelType} from "discord.js";
import config from "../../../src/classes/Config.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import DiscordTypingService from "../../../src/services/DiscordTypingService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandNextAuthorPrediction(userMessage, bot, isAdmin, client, clients, next) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!nextauthor`)
    if (isCommand) {
        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)
        const channelTopic = userMessage?.channel?.topic || null
        const channelName = userMessage?.channel?.name
        const isAllowedChannel = true || bot.multiBotChannels?.some(c => c === channelName)
            || userMessage.channel.type === ChannelType.DM && config.enableDirectMessages && config.discordAdminIds[userMessage.author.id.toString()]
        if (!isAllowedChannel || !isAdmin) return next()

        if (!bot.isMasterBot) return next()

        await DiscordTypingService.sendTyping(userMessage)
        await DiscordService.sendCommandConfirmation(userMessage)
        const predictedAuthors = await AIService.generateNextAuthorPrediction({
            name: '',
            context: ''
        }, messages, channelTopic)
        await DiscordMessageService.sendMessage(userMessage, `# Predicted next author (3 results): \n\`\`\`json\n${JSON.stringify(predictedAuthors, null, 2)}\n\`\`\``, bot, true, true)
    }

    next()
}