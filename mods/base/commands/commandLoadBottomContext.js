import DiscordService from "../../../src/services/DiscordService.js";
import axios from "axios";
import CardService from "../../../src/services/CardService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import {AttachmentBuilder} from "discord.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function (userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent.toLowerCase().startsWith('!instructions')
    const botId = userMessage.cleanContent.match(/!instructions ([a-zA-Z0-9_ ]*)/i)?.[1]?.trim?.()
    const isCorrectBot = (botId?.toLowerCase() === client.application.id.toLowerCase())

    if (isTrigger && isCorrectBot) {

        if (!isAdmin && !bot.editable) {
            await DiscordService.sendCommandError(userMessage, `#Only admins can edit this bot!`)
            return next()
        }

        if (!userMessage.attachments || userMessage.attachments.size < 1) {
            await DiscordService.sendCommandError(userMessage, `#You need to provide a PNG image or a text file as attachment of this command!`)
            return next()
        }

        const imageURL = userMessage.attachments.find(
            a => a.url.toLowerCase().includes('.png')
        )
        const textURL = userMessage.attachments.find(
            a => [".md", ".txt", ".json"]
                .some(extension => a.url.toLowerCase().includes(extension))
        )

        if (!imageURL && !textURL) {
            await DiscordService.sendCommandError(userMessage, `#You need to provide a PNG image or a text file as attachment of this command! The text file can have either the ".txt", ".md" or ".json" extension.`)
            return next()
        }

        let bottomContext
        if (imageURL) {
            const imageResponse = await axios.get(imageURL.url, {responseType: 'arraybuffer'})
                .catch(async (err) => await DiscordService.sendCommandError(userMessage, `#ERROR: ${err.message}`))
            const imageBuffer = Buffer.from(imageResponse.data, "utf-8")
            bottomContext = await CardService.extractCardContent(imageBuffer)
        } else if (textURL) {
            const textResponse = await axios.get(textURL.url)
                .catch(async (err) => await DiscordService.sendCommandError(userMessage, `#ERROR: ${err.message}`))
            bottomContext = await textResponse.data
        }

        if (!bottomContext) {
            return next()
        }

        try {
            bottomContext = JSON.parse(bottomContext)
        } catch (err) {
            // Silent fail
        }

        if (typeof bottomContext === "string" && !bottomContext.startsWith("[")) {
            await DiscordService.sendCommandError(userMessage, `#ERROR: The instructions must begin with an opening square bracket when they're in string form!`)
            return next()
        }

        await DiscordService.sendCommandConfirmation(userMessage)

        bot.openAIContext.bottom = bottomContext

        await DiscordMessageService.sendMessage(
            userMessage,
            `#Instructions have been loaded onto ${bot.name}! Loaded instructions (${Array.isArray(bottomContext) ? 'JSON' : 'text'} format) are attached below.`,
            bot,
            true,
            true,
            [new AttachmentBuilder(Buffer.from(Array.isArray(bottomContext) ? JSON.stringify(bottomContext, null, 2) : bottomContext), {name: `instructions.txt`})])
            .catch(console.error)
    }

    next()
}