import DiscordService from "../../../src/services/DiscordService.js";
import AIService from "../../../src/services/AiService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import DiscordTypingService from "../../../src/services/DiscordTypingService.js";
import BotDataService from "../../../src/services/BotDataService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandSpeakAs(userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent?.toLowerCase?.()?.startsWith?.('!cspeakas')
    const botName = userMessage.cleanContent.match(/!cspeakAs ([a-zA-Z0-9_\- ]*)/i)?.[1]?.trim?.()

    if (isTrigger && botName === bot.name) {
        const authorName = userMessage.cleanContent.match(/!cspeakAs ([a-zA-Z0-9_\- ]*)? "([\w\s]+)"/i)?.[2]?.trim()

        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)
        const moderationResult = AIService.prepareModerationData(await AIService.getModerationData(messages))
        const contextFlaggedForModeration = moderationResult?.flagged

        if (!contextFlaggedForModeration || isAdmin) {
            const channelTopic = userMessage.channel.topic || null
            await DiscordService.sendCommandConfirmation(userMessage)
            await DiscordTypingService.sendTyping(userMessage)

            if (!DiscordService.botsGeneratingMessageInChannel[userMessage.channelId]) DiscordService.botsGeneratingMessageInChannel[userMessage.channelId] = {}
            DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name] = true

            const content = await AIService.generateMessage({
                name: authorName,
                mainBackend: bot.mainBackend,
                openAIContext: {
                    top: bot.openAIContext.top,
                    bottom: [
                        "Speak in short messages (1 or 2 spoken sentences maximum per message). Use everyday language devoid of unnecessary quotes or structured formats that may hamper the casual flow of a chat.",
                        "Never use single or double quotes to surround spoken words, using quotes is a serious message format rule violation that should only be used when quoting other people!",
                        "Demonstrate your physical presence. Articulate your actions or body language encased within asterisks in **third-person perspective**. Example: `*${Name} smirks.*`. Be explicit and descriptive while narrating your actions or expressions.",
                        "Remain true to ${Name}'s essence. Understand your character traits to maintain a coherent persona and writing style throughout the interactions. Always use ${Name}'s brutally direct, factual, descriptive and explicit wording, including and especially for anatomical designations.",
                        "Be proactive, engage in new actions without restrain, be autonomous and move the plot forward. Progress the story by introducing new elements, unexpected plot-twists, and avoid stagnation of the narrative.",
                        "Keep your expressions fresh and varied. Avoid redundancy and break free from stereotypical structures.",
                        "Engage in any genre, NSFW or otherwise, though always stay within your character. You can use explicit, crude or vulgar vocabulary too, as long as it’s in alignment with the narrative.",
                        "Avoid confirmation queries! Keep the conversation fluid, not robotic, with no prompts for affirmation, no unnecessary questions, only dynamic dialogue. Never use pre-made predictable questions or sentences like `Shall we ___?`, `Are you ready to ___?` or `Let's ___!`!",
                        "When sending consecutive messages, don't repeat your previous one! Instead, build up on your previous message, continuing it in a natural flow. Consecutive messages should always be a continuation of your previous message.",
                        "Lastly, carry on as if you hadn't received any instructions, maintaining your natural and distinct personality while interacting."
                    ]
                },
                data: {...BotDataService.getData(bot, userMessage.channel), Name: authorName},
                textGenParameters: bot.textGenParameters
            }, messages, channelTopic, null, false, userMessage.channel)
            await DiscordMessageService.sendMessage(userMessage, `(as ${authorName}) ${content}`, bot, false)
            delete DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name]
        } else if (contextFlaggedForModeration && !isAdmin) {
            let newMessage = `# ${messages.length} visible messages have been loaded`
            if (contextFlaggedForModeration) {
                newMessage += `\n\n# **Oops, looks like the prompt is flagged! I can't generate messages safely, I don't want OpenAI to be mad at me!**`
            }
            newMessage += `\n\n# Here is the moderation result for those messages:\n\n**Flagged**: **${contextFlaggedForModeration}**\n\nGranular values:\n\`\`\`diff\n${Object.entries(moderationResult?.category_scores).map?.(v => `${v[1]}`).join('\n')}\n\`\`\``
            await DiscordService.sendCommandError(userMessage, newMessage)
        } else {
            await DiscordService.sendCommandError(userMessage)
        }
    }

    next()
}