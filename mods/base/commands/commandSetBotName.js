/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandSetBotName(userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!setname')
    const optionalBotId = userMessage.cleanContent.match(/!setname ([a-zA-Z0-9_\- ]*)/i)?.[1]?.trim?.()

    const isAllowedChannel = (optionalBotId?.toLowerCase() === client.application.id.toLowerCase())

    if (isTrigger && isAllowedChannel) {
        let newName = userMessage.cleanContent.match(/!setname ?([a-zA-Z0-9_\- ]*)? "([\w\s]+)"/i)
        newName = newName?.[2]?.trim()

        if (!newName) {
            userMessage?.react('❌').catch(console.error)
            next()
            return
        }

        userMessage?.react('✅').catch(console.error)

        if(client.user.username !== newName) {
            await client.user.setUsername(newName)
                .catch(err => {
                    console.log(err)
                    userMessage.channel.send(`#Couldn't change the bot name! This username is already taken by 9999 other discord users, or the name was changed more than twice in the last hour. Yes, discord is a shit platform.`)
                })
            client.application.name = newName
            return next()
        }

        await userMessage?.reply(`#Bot Discord name changed to ${newName}`)?.catch(err => null)
    }

    next()
}
