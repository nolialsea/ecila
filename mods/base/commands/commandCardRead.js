import DiscordService from "../../../src/services/DiscordService.js";
import extract from "png-chunks-extract";
import PNGtext from "png-chunk-text";
import axios from "axios";
import ModdingService from "../../../src/services/ModdingService.js";
import {AttachmentBuilder} from "discord.js";
import CardService from "../../../src/services/CardService.js";
import Utils from "../../../src/Utils.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function (userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage.cleanContent.toLowerCase().startsWith('!cardread')
    const cardTextURL = userMessage.cleanContent.match(/!cardRead (https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*))/i)?.[1]?.trim?.()
    const cardAttachmentURL = userMessage.attachments.map(a => a.url)?.[0]

    if (isTrigger && ModdingService.isBotMasterBot(bot, userMessage)) {

        if (!cardTextURL && !cardAttachmentURL) {
            await userMessage?.reply(`#You need to provide an image card! Either use \`!load BOT_ID https://path.to/your_image.png\` or simply \`!load BOT_ID\` with your image card file attached to the message!`)
            return next()
        }


        const response = await axios.get(cardTextURL ?? cardAttachmentURL, {responseType: 'arraybuffer'})
        const buffer = Buffer.from(response.data, "utf-8")

        const card = await Utils.tryCatch(async () => {
            return await CardService.extractCardContent(buffer)
        }, async (err) => {
            await DiscordService.sendCommandError(userMessage, `#${err.message}`)
        })

        if (!card || typeof card !== "string") {
            return next()
        }

        await DiscordService.sendCommandConfirmation(userMessage)

        if (card) {
            const attachment = new AttachmentBuilder(Buffer.from(card), {name: `card_${Date.now()}_${Date.now()}.md`})
            await DiscordMessageService.sendMessage(userMessage, `#Here is the card content:`, bot, true, true, [attachment])
        } else {
            await DiscordService.sendCommandError(userMessage, "#Result couldn't be processed!")
        }
    }

    next()
}
