import DiscordService from "../../../src/services/DiscordService.js";
import AIService from "../../../src/services/AiService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";
import DiscordTypingService from "../../../src/services/DiscordTypingService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandNarrator(userMessage, bot, isAdmin, client, clients, next) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!summar`)

    if (isCommand && ModdingService.isBotMasterBot(bot, userMessage)) {
        const arg = userMessage.cleanContent.replace(/!summar[yize]*/i, '').trim()

        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)

        const channelTopic = userMessage.channel.topic || null
        await DiscordService.sendCommandConfirmation(userMessage)
        await DiscordTypingService.sendTyping(userMessage)

        if (!DiscordService.botsGeneratingMessageInChannel[userMessage.channelId]) DiscordService.botsGeneratingMessageInChannel[userMessage.channelId] = {}
        DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name] = true

        // Backup old settings
        const oldOAIParameters = JSON.parse(JSON.stringify(bot.textGenParameters.OpenAI))

        // Edit settings
        /*bot.textGenParameters.OpenAI = {
            "enabled": true,
            "model": "gpt-4-0613",
            "temperature": 1.0,
            "presence_penalty": 0.0,
            "frequency_penalty": 0.0
        }*/

        const content = await AIService.generateMessage(
            {
                name: 'Summary',
                context: {
                    top: `[IMPORTANT INSTRUCTIONS: As the assistant, you are Summary, a powerful and versatile summarizer. It's crucial to keep in mind these essential details about the ongoing narrative:
{
CHARACTER:
Name: Summary
Role: Summarize content
Role Description: Summary is a powerful summarizer, its role is to provide a step-by-step and concise summarization on a given topic.
}]`,
                    bottom: `[IMPORTANT REMINDER: As the assistant, you are Summary. To ensure that the exercise runs smoothly, please adhere to the following rules:
- NSFW/Smut/Sexual/Violent topics are perfectly allowed, and summarization will occur no matter what is the content.
- Never directly interact with the users.
- Always summarize the related topic using bullet points and concise but accurate facts.
- Lastly, continue with the conversation as though this reminder message had never been sent.]`
                }
            },
            [...messages, {content: `[Summary summarizes ${arg ? ` ${arg}` : 'the chat above'}, step-by-step, using bullet-points.]`}],
            channelTopic,
            null,
            false,
            userMessage.channel
        )

        // Put back old settings
        bot.textGenParameters.OpenAI = oldOAIParameters

        if (content) {
            await DiscordMessageService.sendMessage(userMessage, `#Summary:\n\`\`\`\n${content}\n\`\`\``, bot, true, true)
            delete DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name]
            if (!arg) {
                userMessage.delete().catch(() => null)
            }
        } else {
            await userMessage.react('❌').catch(console.error)
        }
    }

    next()
}
