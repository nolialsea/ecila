import ModdingService from "../../../src/services/ModdingService.js";
import {exec} from 'node:child_process';
import {promisify} from 'node:util';

const execPromise = promisify(exec);

async function getLastCommits(numCommits) {
    try {
        const {stdout} = await execPromise(`git log -n ${numCommits} --pretty=format:"%h - %an, %ar : %s"`);
        return stdout;
    } catch (error) {
        console.error(error);
    }
}

export default async function commandGetData(userMessage, bot, isAdmin, client, clients, next) {
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!lastcommit')

    if (isTrigger && ModdingService.isBotMasterBot(bot, userMessage)) {
        const number = Math.min(30, Math.max(1, parseInt(userMessage?.cleanContent?.match(/!lastcommits? ?([0-9]*)?/i)?.[1]?.trim?.() || '5')))
        const commits = await getLastCommits(number)
        if (commits) {
            userMessage?.react('✅').catch(console.error)
            userMessage?.reply(`# Last Commits:\n${commits}`).catch(() => null)
        } else {
            await userMessage.react('❌').catch(console.error)
        }

    }

    next()
}
