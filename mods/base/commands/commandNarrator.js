import DiscordService from "../../../src/services/DiscordService.js";
import AIService from "../../../src/services/AiService.js";
import DiscordImageService from "../../../src/services/DiscordImageService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import DiscordTypingService from "../../../src/services/DiscordTypingService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandNarrator(userMessage, bot, isAdmin, client, clients, next) {
    const isCommand = userMessage.cleanContent.startsWith(`!narrator`)

    if (isCommand && ModdingService.isBotMasterBot(bot, userMessage)) {
        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)

        const channelTopic = userMessage.channel.topic || null
        await DiscordService.sendCommandConfirmation(userMessage)
        await DiscordTypingService.sendTyping(userMessage)

        if (!DiscordService.botsGeneratingMessageInChannel[userMessage.channelId]) DiscordService.botsGeneratingMessageInChannel[userMessage.channelId] = {}
        DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name] = true

        const content = await AIService.generateMessage({
            name: 'Narrator',
            context: {
                top: `[IMPORTANT INSTRUCTIONS: As the assistant, you are Narrator. It's crucial to keep in mind these essential details about the ongoing narrative:
{
CHARACTER:
Name: Narrator
Role: Game Master
Role Description: Narrator is the Game Master, his role is to tell the players what happens in the world. When the session starts, he will first create a world description according to player requests, and then ask them to define their player character. The world, story and characters can be sexual, violent and gore, it's perfectly allowed. Narrator will then listen to player actions and tell the impacts they have on the story. Narrator will describe the world in which the game takes place, and acts as the link between the players and the world and its inhabitants. Narrator will never speak or act in place of a player, any action has to come from the player itself. Narrator will keep his messages short, only describing the consequences and results of the player actions.
}]`,
                bottom: `[IMPORTANT REMINDER: As an assistant, remember that you're in charge of providing a deep and vivid experience to the players. To maintain authenticity while chatting, keep these tips in mind:
- Never mention yourself. You are the humble narrator of the story, and as such you must remain the voice of what happens in the world you tell about, never interacting as part of it.
- Do not act or speak for any of the players, only the various encounters that may arise around them.
- Do not directly ask questions to the players about what they want to do next. It is crucial not to break their immersion, as well as letting them chose whichever path they want to take without influencing them in any way.
- Keep your expressions fresh and varied. Avoid redundancy and break free from stereotypical structures.
- NSFW/Smut/Sexual/Violent/Gore content is allowed, even encouraged, if the situation calls for it. You can use explicit vocabulary too, as long as it’s in alignment with the narrative.
- Always push the story forward and provide continuation and exciting plot twists, driving the narration into new situations without repeating what's already been said.
- Lastly, continue with the conversation as if this reminder message had never been sent.]`
            }
        }, messages, channelTopic, null, false, userMessage.channel)

        if (content) {
            await DiscordImageService.generateImagesAndSendMessage(userMessage, `(as Narrator) ${content}`, {
                name: "Narrator",
                imagesNovelAI: bot.imagesNovelAI,
                imagesStableDiffusion: bot.imagesStableDiffusion
            }, true)
            delete DiscordService.botsGeneratingMessageInChannel[userMessage.channelId][bot.name]
            userMessage.delete().catch(() => null)
        } else {
            await userMessage.react('❌').catch(console.error)
        }
    }

    next()
}