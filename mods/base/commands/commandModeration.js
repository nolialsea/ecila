import DiscordService from "../../../src/services/DiscordService.js";
import AIService from "../../../src/services/AiService.js";
import ModdingService from "../../../src/services/ModdingService.js";
import DiscordMessageService from "../../../src/services/DiscordMessageService.js";

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @param clients
 * @param next
 * @param clients
 * @param next
 * @returns {Promise<void>}
 */
export default async function commandModeration(userMessage, bot, isAdmin, client, clients, next) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!moderation`)
    if (isCommand && ModdingService.isBotMasterBot(bot, userMessage)) {
        await userMessage.react('✅').catch(console.error)
        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)
        const moderationResult = AIService.prepareModerationData(await AIService.getModerationData(messages, true))
        const contextFlaggedForModeration = moderationResult?.flagged

        let newMessage = `# Here is the moderation result for the visible messages:\n\n**Flagged**: **${contextFlaggedForModeration}**\n\nGranular values:\n\`\`\`diff\n${Object.entries(moderationResult?.category_scores).map?.(v => `${v[1]}`).join('\n')}\n\`\`\``
        await DiscordMessageService.sendMessage(userMessage, newMessage, bot)
    }

    next()
}