import config from "../../../src/classes/Config.js";

export default async function (userReaction, user, bot, channelTopic, client, clients, next) {
    if (userReaction.emoji.name === "❌") {
        console.log("clicked on", "❌")
        if (!userReaction.message?.author.bot) return next()
        if (userReaction.message?.author.id !== client.application.id) return next()

        const guild = await userReaction.message?.guild?.fetch?.().catch(console.error)
        const infoChannel = await (await guild?.channels?.fetch?.())?.find(c => c?.name === config.deletedMessagesChannel)?.fetch?.().catch(console.error)
        if (infoChannel && userReaction.message?.guildId) {
            infoChannel.send(`${user} deleted a message in ${userReaction.message?.channel}`).catch(console.error)
            if (userReaction.message?.content) {
                infoChannel.send(userReaction.message?.content, {
                    embeds: userReaction.message?.embeds,
                    components: userReaction.message?.components,
                    files: userReaction.message?.attachments.map(a => a.url)
                }).catch(console.error)
            } else {
                infoChannel.send({
                    embeds: userReaction.message?.embeds,
                    components: userReaction.message?.components,
                    files: userReaction.message?.attachments.map(a => a.url)
                }).catch(console.error)
            }
        }

        await userReaction.message?.delete().catch(() => null)
    }

    next()
}