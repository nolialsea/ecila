import DiscordService from "../../../src/services/DiscordService.js";

export default async function (userReaction, user, bot, channelTopic, client, clients, next) {
    if (userReaction.emoji.name === "⏩" || userReaction.emoji.name === '🐞') {
        console.log("clicked on", "⏩")
        await DiscordService.generateAndSendMessage(userReaction.message, bot, channelTopic)
    }

    next()
}
