import DiscordAudioService from "../../../src/services/DiscordAudioService.js";

export default async function (userReaction, user, bot, channelTopic, client, clients, next) {
    if (bot.TTS?.disabled) return next()

    if (userReaction.emoji.name === "🗣️" || userReaction.emoji.name === "🗣") {
        console.log("clicked on", "🗣️")
        console.log('Generating TTS...')
        const files = [await DiscordAudioService.getTTSAudioDiscordAttachment(bot, userReaction.message.cleanContent)]
            .filter(v => !!v)

        if (files.length > 0) {
            console.log('Generated TTS!')
            userReaction.message.edit({files: files}).catch(() => null)
        } else {
            console.log("No TTS was generated.")
        }
    }

    next()
}
