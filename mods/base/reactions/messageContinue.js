import DiscordMessageService from "../../../src/services/DiscordMessageService.js";

export default async function (userReaction, user, bot, channelTopic, client, clients, next) {
    if (userReaction.emoji.name === "▶️") {
        console.log("clicked on", "▶️")
        await DiscordMessageService.continueMessage(userReaction.message, bot, channelTopic)
    }

    next()
}