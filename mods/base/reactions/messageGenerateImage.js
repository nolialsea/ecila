import DiscordImageService from "../../../src/services/DiscordImageService.js";
import DiscordService from "../../../src/services/DiscordService.js";

export default async function (userReaction, user, bot, channelTopic, client, clients, next) {
    if (userReaction.emoji.name === "🖼️" || userReaction.emoji.name === "🖼") {
        console.log("clicked on", "🖼️")
        console.log('Generating image(s)...')
        const messages = await DiscordService.getChannelMessages(userReaction.message.channel, null, bot)
        const files = await DiscordImageService.generateImageAttachmentsRevamped(userReaction.message, userReaction.message.cleanContent, bot, false, undefined, messages, 12)

        if (files && files.length > 0) {
            console.log('Generated image(s)!')
            userReaction.message.edit({files: files}).catch(() => null)
        } else {
            console.log("No image was generated.")
        }
    }

    next()
}
