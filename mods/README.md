# Introduction

Welcome to the ECILA Discord Chatbot Modding Documentation! This guide will provide you with an in-depth overview of the modding system, its structure, requirements and how to create your own mods to enhance the functionality of your ECILA bot. Whether you're looking to add a new command, a reaction, or an interaction event, ECILA's extensive modding system has got you covered. The modding system provides you the flexibility to add, modify or remove features according to your needs.

The ECILA bot itself is built as a base mod, which means it contains the basic functionalities and behaviours that come with the bot. Any other mod you create is loaded on top of the base mod and can extend or modify these behaviours.

**Prerequisites**

- Basic understanding of JavaScript and Node.js
- Knowledge of ES6 Modules as mods in ECILA are ES6 modules

# General Information

ECILA's modding system allows users to create separate modules for specific functionalities or behaviours. Each mod is treated as an ES6 module and is placed within the `ECILA/mods` directory. The order in which mods are loaded can be configured in the `ECILA/mods/modList.json` file located in the root of the `mods` folder.

A mod's metadata is stored as a JSON object within this file and contains two properties:

1. `name`: A string that identifies the mod.
2. `enabled`: A boolean value indicating whether the mod should be loaded when the bot starts.

For example:

```json
[
  {
    "name": "exampleMod",
    "enabled": true
  },
  {
    "name": "pollMod",
    "enabled": false
  }
]
```

# Project Structure

The project structure for ECILA and its mods is as follows:

```
ECILA/
├── mods/
│   ├── modList.json
│   ├── base/                # Base mod that contains the basic behaviours
│   │   ├── commands/        # Folder containing command files
│   │   ├── reactions/       # Folder containing reaction files
│   │   ├── interactions/    # Folder containing interaction files
│   │   └── scripts/         # Folder containing scripts
│   ├── exampleMod/          # An example mod
│   │   ├── package.json     # Lists mod's dependencies
│   │   ├── commands/
│   │   ├── reactions/
│   │   ├── interactions/
│   │   ├── scripts/
│   │   └── src/             # Folder for utility files, services, classes etc.
│   └── pollMod/
│       ├── package.json
│       ├── commands/
│       ├── reactions/
│       ├── interactions/
│       ├── scripts/
│       └── src/
├── src/
│   ├── classes/
│   │   ├── BotConfig.js     # Bot configuration class
│   │   └── Config.js        # Global configuration class
└── config.json                # Configuration file for ECILA
```

The base mod is always loaded first and its directory structure mirrors those of other mods. The base mod is located in the `ECILA/mods/base` folder and does not appear in the `modList.json` file. It provides the default behaviours and functionalities for the bot. The base mod, like all other mods, must have at least one of the following folders: `commands`, `reactions`, `interactions`, or `scripts`.

Files within these folders are automatically imported when the mod is loaded, and their default export functions are automatically called depending on the folder. Command functions are invoked when a command message is received, reaction functions when a reaction is added, interaction functions when an interaction occurs, and script functions when the application starts.

The specific function signatures for commands, reactions, and interactions will be covered in detail in the subsequent sections of this document.

To remove a base functionality, your mod should export a `remove` property that is an array of the names of the base mod functionalities to remove.

## Creating a New Mod

So, you're ready to create a new mod for ECILA, and you want to dive deep into the mod creation process? Well, let's get right into it! We will now walk through the steps to create your first mod and discuss how to add commands, reactions, and interaction events to ECILA.

Remember, to avoid getting lost, always refer to the mod structure overview we've previously discussed in the "Project Structure" section of the documentation.

### Setting up your Mod

To start creating a new mod:

1. Inside the `ECILA/mods` directory, create a new directory for your mod. Let's name our mod 'exampleMod' for this walkthrough.
2. In the `ECILA/mods/exampleMod` directory, create a `package.json` file if your mod has dependencies. ECILA will automatically parse this and add the dependencies when building the app.
3. Inside the same directory, you can create the following directories based on what you need for your mod: `commands`, `reactions`, `interactions`, and `scripts`.
4. Remember, your mod needs to have at least one of these directories.

Your directory should now look like this:

```
ECILA/
|-- mods/
|   |-- base/
|   |-- exampleMod/
|   |   |-- package.json (optional)
|   |   |-- commands/ (optional)
|   |   |-- reactions/ (optional)
|   |   |-- interactions/ (optional)
|   |   |-- scripts/ (optional)
|   |-- modList.json
```

Next, we'll move on to modifying the `modList.json` to include our new mod.

## Prerequisites

Before we start, ensure that your JavaScript files in the mod follow the **ES6 module** system. That means using `import` instead of `require`. The ECILA app is configured to work with ES6 modules and attempting to use `require` will result in errors.

## The Config and BotConfig Classes

The BotConfig class is available for each bot and contains every configuration of the bot itself, such as its name, enabled features, customization settings, etc. It's imported as `import { BotConfig } from 'ECILA/src/classes/BotConfig.js'`.

The global config object, which contains all the bots configs as part of it, is importable anywhere with `import config from 'ECILA/src/classes/Config.js`. No need to instantiate it or anything, the default export handles the job of loading the proper configuration file (`ECILA/config.json`).

In the next section, we will delve into a real example of mod creation, exploring all the aspects we've covered above. Stay tuned!

### Updating modList.json

The `modList.json` file found in the `ECILA/mods` directory defines the mods that will be loaded and their order. To include your new mod, add a new object in this JSON array with the following format:

```json
{
  "name": "exampleMod",
  "enabled": true
}
```

The "name" key is the name of your mod and it should be the same as the directory name. The "enabled" key indicates whether your mod should be loaded. If this key is set to `false`, ECILA will not load the mod.

### Creating Commands

Let's start with adding new commands. In the `commands` folder, create a new JavaScript file. The name of this file will be the name of the command. Make sure the file is an ES6 module and has a default export function.

The default export function will be called when the associated command is triggered. It should follow the below definition:

```js
/**
 * @param {Message} userMessage - The message that triggered the command
 * @param {BotConfig} bot - The configuration of the bot that received the command
 * @param {boolean} isAdmin - Whether the user who sent the command is an admin
 * @param {Client} client - The Discord.js client of the bot that received the command
 * @param {Client[]} clients - An array of all Discord.js clients running in the bot
 * @param {function} next - A function to call to continue to the next command mod
 */
export default async function (userMessage, bot, isAdmin, client, clients, next) {
  // your command logic here
}
```

### Creating Reactions

For reactions, in the `reactions` folder, create a new JavaScript file. Similar to commands, the file should be an ES6 module and have a default export function. The name of the file will be the name of the reaction.

The default export function will be called when the associated reaction is triggered. It should follow the below definition:

```js
/**
 * @param {MessageReaction} userReaction - The reaction that was added
 * @param {BotConfig} bot - The configuration of the bot that received the reaction
 * @param {string|null} channelTopic - The topic of the channel where the reaction was added
 * @param {function} next - A function to call to continue to the next reaction mod
 */
export default async function (userReaction, user, bot, channelTopic, client, clients, next) {
  // your reaction logic here
}
```

### Creating Interactions

For interactions, create a JavaScript file inside the `interactions` folder. As with commands and reactions, the name of the file will be the name of the interaction. The file should also be an ES6 module and have a default export function.

The default export function will be called when an associated interaction is triggered. It should follow the below definition:

```js
/**
 * @param {MessageComponentInteraction} interaction - The interaction
 * @param {BotConfig} bot - The configuration of the bot that received the command
 * @param {Client} client - The Discord.js client of the bot that received the command
 * @param {Client[]} clients - An array of all Discord.js clients running in the bot
 * @param {function} next - A function to call to continue to the next command mod
 */
export default async function (interaction, bot, client, clients, next) {
  // your interaction logic here
}
```

### Modifying Base Behavior

If you wish to modify the base behavior, create a file in the appropriate folder (`commands`, `reactions`, or `interactions`) with the same name as the base mod file you wish to modify. The new file will override the function depending on the mod loading order.

To remove one or more base behaviors, your mod should export a `remove` property that is an array of the names of the base mods to remove. For example, the following code removes the currently loaded behaviors for `baseCommand1` and `baseCommand2`:

```js
export const remove = ['baseCommand1', 'baseCommand2'];
```

This will remove the currently loaded behaviors for the files `ECILA/mods/{modName}/baseCommand1.js` and `ECILA/mods/{modName}/baseCommand2.js`.

### Creating Scripts

Scripts are special; they don't have a specific function definition. They are free form. Any JavaScript file placed in the `scripts` directory will be automatically imported and executed when the app starts.

### Auxiliary Files

For other files like services or classes, it is recommended to include them inside an `ECILA/mods/{modName}/src` folder at the root so to not confuse the scripts that will be auto imported and executed and the other utilities defined by the mod.

That's it! With these steps, you should be able to create a new mod for ECILA. Happy coding!
