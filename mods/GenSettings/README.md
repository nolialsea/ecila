# GenSettings Command Function
This function allows you to update the text generation parameters of the bot.
## Usage
Trigger the function by sending a message in a predefined channel or a direct message to the bot:
```
!gensettings "property_name" value
```
### Parameters
- `property_name`: The specific property you want to change, can be any of the following:
    - **Temperature**: `'temp'`
    - **Presence Penalty**: `'pres'`
    - **Frequency Penalty**: `'freq'`

- `value`: The new value you want to assign to the specified property.
## Examples
1. To set the temperature to 0.8, send:
```
!gensettings "temp" 0.8
```
2. To set the presence penalty to 0.2, send:
```
!gensettings "pres" 0.2
```
3. To set the frequency penalty to 0.2, send:
```
!gensettings "freq" 0.2
```

On a successful update, the bot will react with a 👍 (✅) emoji to confirm. If there's an error, the bot will react with the ❌ emoji.
Remember, only admins or users having access to monoBotChannels can change these settings.