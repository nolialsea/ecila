import { ChannelType } from "discord.js";
import config from "../../../src/classes/Config.js";

/**
 * @param {Message} userMessage - The user's message which triggered the command
 * @param {BotConfig} bot - The bot's configuration which received the command
 * @param {boolean} isAdmin - Whether the user who sent the command is an admin
 * @param {Client} client - The Discord.js client of the bot that received the command
 * @param {Client[]} clients - An array of all Discord.js clients running in the bot
 * @param {function} next - A function to call to proceed to the next middleware
 */
export default async function(userMessage, bot, isAdmin, client, clients, next) {
    const channelName = userMessage?.channel?.name;
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!gensettings');
    const isAllowedChannel = bot.monoBotChannels?.includes(channelName)
        || (userMessage.channel.type === ChannelType.DM && config.enableDirectMessages && isAdmin);

    if (!isTrigger || !isAllowedChannel || !isAdmin) {
        next();
        return;
    }

    const [, propertyName, propertyValue] = userMessage.cleanContent.match(/!gensettings "([^"]+)"(.+)?/) ?? [];

    if (!propertyName || !propertyValue) {
        userMessage?.react('❌').catch(console.error);
        next();
        return;
    }

    const propertyNameLower = propertyName.trim().toLowerCase();
    const setProperty = value => {
        const floatValue = parseFloat(value);
        if (propertyNameLower.startsWith("temp")) {
            bot.textGenParameters.OpenAI.temperature = floatValue;
        } else if (propertyNameLower.startsWith("pres")) {
            bot.textGenParameters.OpenAI.presence_penalty = floatValue;
        } else if (propertyNameLower.startsWith("freq")) {
            bot.textGenParameters.OpenAI.frequency_penalty = floatValue;
        }
    };

    setProperty(propertyValue.trim());
    userMessage?.react('✅').catch(console.error);
    next();
}
