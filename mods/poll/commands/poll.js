import {ActionRowBuilder, ButtonBuilder} from "discord.js";
import DiscordService from "../../../src/services/DiscordService.js";
import TimeoutService from "../../../src/services/TimeoutService.js";
import AIService from "../../../src/services/AiService.js";
import DiscordImageService from "../../../src/services/DiscordImageService.js";
import DiscordTypingService from "../../../src/services/DiscordTypingService.js";
import PollService from "../src/PollService.js";

const OPTIONS = "ABCDEFGHIJKLMOPQRSTUVWXYZ"

// Define the function to be called by the bot
// Function and properties names and descriptions are very important, this is what will build the "context" for that function
const FUNCTION_POLL = {
    "name": "make_poll",
    "description": "Create a poll message for users to vote on (4 options maximum, should be colorful, descriptive and fit to your personality!)",
    "parameters": {
        "type": "object",
        "properties": {
            "poll": {
                "type": "string",
                "description": "The prompt of the poll"
            },
            "options": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        },
        "required": [
            "poll",
            "options"
        ]
    }
}

/**
 * @param {Message} userMessage - The user's message which triggered the command
 * @param {BotConfig} bot - The bot's configuration which received the command
 * @param {boolean} isAdmin - Whether the user who sent the command is an admin
 * @param {Client} client - The Discord.js client of the bot that received the command
 * @param {Client[]} clients - An array of all Discord.js clients running in the app
 * @param {function} next - A function to call to proceed to the next middleware
 */
export default async function (userMessage, bot, isAdmin, client, clients, next) {
    const condition = userMessage.cleanContent.toLowerCase().startsWith("!poll" + bot.name.toLowerCase())
        || userMessage.cleanContent.toLowerCase().startsWith("!poll " + bot.name.toLowerCase())
    if (condition && isAdmin) {
        // This is to stop the timers for smart answer and auto answer stuff, so that other bots don't try to generate messages at the same time
        TimeoutService.clearTimeout(userMessage, client)

        // Pull channel messages, optional but if you want to edit them to add author's note or do some parsing, you can
        const messages = await DiscordService.getChannelMessages(userMessage.channel, null, bot)

        // Grab the arguments after the command, unused yet but could be used to inject an author's note
        const regex = new RegExp("!poll ?" + bot.name.toLowerCase(), "i")
        const arg = userMessage.cleanContent.replace(regex, '').trim()
        const minutes = parseInt(arg) || 1

        const channelTopic = userMessage.channel.topic || null

        // Backup old settings
        const oldOAIParameters = JSON.parse(JSON.stringify(bot.textGenParameters.OpenAI))

        bot.textGenParameters.OpenAI.URL = undefined
        bot.textGenParameters.OpenAI.key = undefined
        bot.textGenParameters.OpenAI.max_prompt_length = undefined

        // Edit settings
        bot.textGenParameters.OpenAI.functions = [FUNCTION_POLL]
        bot.textGenParameters.OpenAI.function_call = {"name": "make_poll"}  // forces the function

        // Notify that the command is processing
        await DiscordService.sendCommandConfirmation(userMessage)
        await DiscordTypingService.sendTyping(userMessage)

        // Generate the message
        const content = await AIService.generateMessage(bot, messages, channelTopic, null, false, userMessage.channel)

        // Put back old settings
        bot.textGenParameters.OpenAI = oldOAIParameters

        // If AI generation failed, we send an error and exit the function
        if (!content) {
            await DiscordService.sendCommandError(userMessage, `#Oops, the poll generation failed!`)
            // When you return inside a mod, ALWAYS use `next()` before or at the return!
            return next()
        }

        // Check if the content is an object!
        // If the content is a string, it means the AI generated a bot message and not a function call
        // In which case, we instead call the normal message generation tool
        // Same if the content doesn't have arguments as a string
        if (typeof content !== "object" || typeof content.arguments !== "string") {
            await DiscordImageService.generateImagesAndSendMessage(userMessage, content, bot)
            return next()
        }

        try {
            // Since arguments are always sent as a string, we need to parse them back into a JSON object
            content.arguments = JSON.parse(content.arguments)
        } catch {
            // If the JSON fails to parse, we send an error and return
            await DiscordService.sendCommandError(userMessage, `#Oops, the poll generation failed! (no json arguments)`)
            return next()
        }

        // If the poll or options are missing, we return an error too
        if (!content.arguments['poll'] || !content.arguments['options']) {
            const errorMessage = `#Result for function call (failed):\n\`\`\`\n${JSON.stringify(content, null, 2)}\n\`\`\``
            await DiscordService.sendCommandError(userMessage, errorMessage)
            return next()
        }

        // Random number to use as ID for the poll
        const randomNumber = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)

        // Parse the generated options
        const options = content.arguments['options']
            .map(o => o.replace(/option [0-9a-zA-Z]+:? ?/i, '').replace(/^[0-9a-zA-Z]\./i, '').trim())

        // Build the buttons
        const row = new ActionRowBuilder()
            .addComponents(...options
                .map((cao, i) =>
                    new ButtonBuilder()
                        .setCustomId(`option_${randomNumber}_${OPTIONS[i]}`)
                        .setLabel(OPTIONS[i].toString())
                        .setStyle(1)
                )
            )

        // Build the result message
        const message = `**Poll**: ${content.arguments['poll']} (${isNaN(minutes) ? 1 : minutes}min, 0 votes)\n${options.map((a, i) => `**${OPTIONS[i]}**. ${a}`).join('\n')}`

        // Send result message
        await DiscordImageService.generateImagesAndSendMessage(userMessage, message, bot, true, true, row)

        PollService.polls[randomNumber] = {
            title: content.arguments['poll'],
            options,
            userVotes: {}
        }

        setTimeout(async () => {
            const optionStrings = options.map((a, i) => {
                    const votes = Object.values(PollService.polls[randomNumber].userVotes).filter(v => v === i).length
                    return `**${OPTIONS[i]}**. ${a} (${votes} votes)`
                }
            )
            const message = `**Poll results!** ${content.arguments['poll']}\n${optionStrings.join('\n')}`
            await DiscordImageService.generateImagesAndSendMessage(userMessage, message, bot, true, true)
            delete PollService.polls[randomNumber]

            await DiscordService.generateAndSendMessage(userMessage, bot)
        }, 1000 * 60 * (isNaN(minutes) ? 1 : minutes))

        await userMessage?.delete().catch(() => null)
    }

    // Always call next() to allow the execution of other mods for the current message
    next();
}
