import PollService from "../src/PollService.js";

const OPTIONS = "ABCDEFGHIJKLMOPQRSTUVWXYZ"

/**
 * @param {import('discord.js').Interaction} interaction - The user's interaction
 * @param {BotConfig} bot - The bot configuration
 * @param {Client} client - The Discord.js client of the bot that received the command
 * @param {Client[]} clients - An array of all clients running in the app
 * @param next - A function to call to proceed to the next middleware
 */
export default async function (interaction, bot, client, clients, next) {
    // We are only waiting for a button click, so we return if interaction isn't one
    if (!interaction.isButton()) {
        // When you return inside a mod, ALWAYS use `next()` before or at the return!
        return next()
    }

    // Check if button is from a poll
    if (!interaction.customId.startsWith("option_")) {
        return next()
    }

    // Parse poll ID from customId
    // Sadly customId is the only way to pass information afaik
    // Custom ID has this format: option_<ID>_<OPTION LETTER>
    // Ex: option_125256124_B
    const pollId = interaction.customId
        // Remove the leading `option_`
        .replace('option_', '')
        // Remove the ending option letter
        .replace(/_[a-z]/i, '')

    // Same for option letter
    const pollOption = interaction.customId
        // Remove the leading `option_`
        .replace('option_', '')
        // Remove the poll id
        .replace(/[0-9]+_/i, '')

    if (PollService.polls[pollId]) {
        PollService.polls[pollId].userVotes[interaction.user.id] = OPTIONS.indexOf(pollOption)
    } else {
        return next()
    }

    await interaction.deferUpdate().catch(console.error)
    /*interaction.reply({
        content: `# You picked option ${pollOption} for the poll ${pollId}. Only you can see this message!`,
        // The ephemeral argument makes the reply only visible for the author of the interaction (button press)
        ephemeral: true
    })*/

    // Extract vote count from the message content
    let nbVotes = 0
    if (PollService.polls[pollId]?.userVotes) {
        nbVotes = Object.values(PollService.polls[pollId].userVotes)?.length || 0
    }
    // Copy content and increment the value of the vote counter
    const newContent = interaction.message?.cleanContent?.replace(/[0-9]+ votes\)/, `${nbVotes} votes)`)

    // Send message (don't forget `.catch(e => null)` or it will crash the app if something happens)
    await interaction.message?.edit(newContent).catch(() => null)
    // Alternatively, you can also use `.catch(console.error)` to redirect the errors to the console

    // Always call the next() function ONCE per mod, otherwise other mods can't execute (it's a safety feature)
    next()
}