# 🎩✨ Noli Frontend

This is the spiffy frontend for the Noli API, where magic meets minimalism! ✨

## 🛠️ Tools of the Trade

- ⚡ Vanilla JavaScript (because who needs frameworks, right?)
- 💅 Plain old HTML & CSS (keepin' it classic!)
- 📦 Webpack (for bundling all the goodness)

## 📁 Directory Structure

- `src/`: Where all the frontend sorcery happens
  - `index.html`: The enchanting entry point
  - `styles.css`: Where the styling magic is woven
  - `app.js`: The beating heart of the frontend
- `dist/`: The optimized output, ready to be shipped!

## 🚀 Setup & Build

1. Install dependencies: `npm install`
2. Spin up webpack: `npm start`
3. Open `dist/index.html` and let the magic unfold! ✨

Let's make frontend development fun again! 🎉